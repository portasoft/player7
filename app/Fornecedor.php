<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Fornecedor
 *
 * @package App
 * @property string $cnpj
 * @property string $razao
 * @property string $fantasia
 * @property string $contato
 * @property string $datacadastro
 * @property string $ie
 * @property string $im
 * @property string $segmento
 * @property string $telefone
 * @property string $celular
 * @property string $email
 * @property string $cep
 * @property string $endereco
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $uf
 * @property string $complemento
 * @property string $obs
 * @property string $foto
 * @property string $localizacao
*/
class Fornecedor extends Model
{
    use SoftDeletes;

    protected $fillable = ['cnpj', 'razao', 'fantasia', 'contato', 'datacadastro', 'ie', 'im', 'segmento', 'telefone', 'celular', 'email', 'cep', 'endereco', 'numero', 'bairro', 'cidade', 'uf', 'complemento', 'obs', 'foto', 'localizacao_address', 'localizacao_latitude', 'localizacao_longitude'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Fornecedor::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDatacadastroAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['datacadastro'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['datacadastro'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDatacadastroAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }
    
}
