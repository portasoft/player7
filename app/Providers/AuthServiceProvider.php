<?php

namespace App\Providers;

use App\Role;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $user = \Auth::user();

        
        // Auth gates for: User management
        Gate::define('user_management_access', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Roles
        Gate::define('role_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Users
        Gate::define('user_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: User actions
        Gate::define('user_action_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        // Auth gates for: Empresa
        Gate::define('empresa_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('empresa_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('empresa_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('empresa_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('empresa_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

          // Auth gates for: Clientes
          Gate::define('cliente_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Cliente
        Gate::define('cliente_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('cliente_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('cliente_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('cliente_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('cliente_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Agendahorario
        Gate::define('agendahorario_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('agendahorario_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('agendahorario_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('agendahorario_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('agendahorario_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
          // Auth gates for: Produtos
          Gate::define('produto_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Produto
        Gate::define('produto_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('produto_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('produto_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('produto_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('produto_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Deposito
        Gate::define('deposito_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('deposito_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('deposito_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('deposito_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('deposito_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Fornecedor
        Gate::define('fornecedor_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('fornecedor_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('fornecedor_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('fornecedor_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('fornecedor_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

             // Auth gates for: Referencia
             Gate::define('referencium_access', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('referencium_create', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('referencium_edit', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('referencium_view', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('referencium_delete', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
    
            // Auth gates for: Cor
            Gate::define('cor_access', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('cor_create', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('cor_edit', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('cor_view', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('cor_delete', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
    
            // Auth gates for: Tamanho
            Gate::define('tamanho_access', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('tamanho_create', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('tamanho_edit', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('tamanho_view', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
            Gate::define('tamanho_delete', function ($user) {
                return in_array($user->role_id, [1, 2]);
            });
        // Auth gates for: Produtos
        Gate::define('produto_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Entradanota
        Gate::define('entradanotum_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('entradanotum_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('entradanotum_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('entradanotum_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('entradanotum_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });


// Auth gates for: Devolucao
Gate::define('devolucao_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('devolucao_create', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('devolucao_edit', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('devolucao_view', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('devolucao_delete', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
 // Auth gates for: Administracao
 Gate::define('administracao_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});

// Auth gates for: Kititen
Gate::define('kititen_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('kititen_create', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('kititen_edit', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('kititen_view', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('kititen_delete', function ($user) {
    return in_array($user->role_id, [1, 2]);
});

 // Auth gates for: Contacontabil
 Gate::define('contacontabil_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contacontabil_create', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contacontabil_edit', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contacontabil_view', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contacontabil_delete', function ($user) {
    return in_array($user->role_id, [1, 2]);
});

// Auth gates for: Contacorrente
Gate::define('contacorrente_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contacorrente_create', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contacorrente_edit', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contacorrente_view', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contacorrente_delete', function ($user) {
    return in_array($user->role_id, [1, 2]);
});

// Auth gates for: Financeiro
Gate::define('financeiro_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});

// Auth gates for: Contaapagar
Gate::define('contaapagar_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contaapagar_create', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contaapagar_edit', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contaapagar_view', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contaapagar_delete', function ($user) {
    return in_array($user->role_id, [1, 2]);
});

// Auth gates for: Contareceber
Gate::define('contareceber_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contareceber_create', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contareceber_edit', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contareceber_view', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('contareceber_delete', function ($user) {
    return in_array($user->role_id, [1, 2]);
});

// Auth gates for: Despesa
Gate::define('despesa_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('despesa_create', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('despesa_edit', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('despesa_view', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('despesa_delete', function ($user) {
    return in_array($user->role_id, [1, 2]);
});

  // Auth gates for: Unidade
  Gate::define('unidade_access', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('unidade_create', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('unidade_edit', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('unidade_view', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
Gate::define('unidade_delete', function ($user) {
    return in_array($user->role_id, [1, 2]);
});
    // Auth gates for: Movimento
    Gate::define('movimento_access', function ($user) {
        return in_array($user->role_id, [1, 2]);
    });
    Gate::define('movimento_create', function ($user) {
        return in_array($user->role_id, [1, 2]);
    });
    Gate::define('movimento_edit', function ($user) {
        return in_array($user->role_id, [1, 2]);
    });
    Gate::define('movimento_view', function ($user) {
        return in_array($user->role_id, [1, 2]);
    });
    Gate::define('movimento_delete', function ($user) {
        return in_array($user->role_id, [1, 2]);
    });
        // Auth gates for: Agendatelefonica
        Gate::define('agendatelefonica_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('agendatelefonica_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('agendatelefonica_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('agendatelefonica_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('agendatelefonica_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        // Auth gates for: Producao
        Gate::define('producao_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('producao_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('producao_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('producao_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('producao_delete', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

    }
}
