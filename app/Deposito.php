<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Deposito
 *
 * @package App
 * @property string $descricao
 * @property string $empresa
*/
class Deposito extends Model
{
    use SoftDeletes;

    protected $fillable = ['descricao', 'empresa'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Deposito::observe(new \App\Observers\UserActionsObserver);
    }
    
}
