<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
/**
 * Class Produto
 *
 * @package App
 * @property string $codigobarra
 * @property string $descricao
 * @property string $grupo
 * @property string $marca
 * @property string $finalidade
 * @property string $referencia
 * @property string $cor
 * @property string $tamanho
 * @property string $regraimposto
 * @property string $tiporegra
 * @property integer $estoque
 * @property integer $estoquemin
 * @property decimal $custo
 * @property decimal $preco
 * @property decimal $margem
 * @property string $datacadastro
 * @property string $obs
 * @property string $foto
 * @property string $ncm
 * @property string $kit
*/
class Produto extends Model
{
    use SoftDeletes;

    protected $fillable = ['codigobarra', 'descricao', 'grupo', 'marca', 'finalidade', 'referencia', 'cor', 'tamanho', 'regraimposto',
     'tiporegra', 'estoque', 'estoquemin', 'custo', 'preco', 'margem', 'datacadastro', 'obs', 'foto', 'ncm', 'kit','status',
     'unidade','cfop','cst','stipi','stpis','stcofins','codigobarrasforne','descontomax','descontomin','fabricante','cest','empresa','deposito','codforne'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Produto::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setEstoqueAttribute($input)
    {
        $this->attributes['estoque'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setEstoqueminAttribute($input)
    {
        $this->attributes['estoquemin'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setCustoAttribute($input)
    {
        $this->attributes['custo'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setPrecoAttribute($input)
    {
        $this->attributes['preco'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setMargemAttribute($input)
    {
        $this->attributes['margem'] = $input ? $input : null;
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDatacadastroAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['datacadastro'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['datacadastro'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDatacadastroAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }
    public function retiraEstoque($qtde){
        $this->estoque -= $qtde;
        $this->update();
      }
    public function items(){
        return $this->hasMany('App\Vendaitem');
      }
      public function cor()
    {
        return $this->hasOne('App\Cor');
    }
    
}
