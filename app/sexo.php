<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sexo extends Model
{
  
    public $table = 'sexo';
    

    protected $dates = ['sexo'];


    public $fillable = [
        'sexo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'sexo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
