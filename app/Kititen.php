<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Kititen
 *
 * @package App
 * @property string $produto
 * @property integer $qtde
 * @property decimal $valor
 * @property decimal $base
 * @property string $kit
 * @property string $anexo
*/
class Kititen extends Model
{
    use SoftDeletes;

    protected $fillable = ['produto', 'qtde', 'valor', 'base', 'kit', 'anexo','idkit','tamanho','cor','OrdemCor'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Kititen::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setQtdeAttribute($input)
    {
        $this->attributes['qtde'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorAttribute($input)
    {
        $this->attributes['valor'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setBaseAttribute($input)
    {
        $this->attributes['base'] = $input ? $input : null;
    }
    
}
