<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estadocivil extends Model
{

    public $table = 'estadocivil';
    

    protected $dates = ['estadocivil'];


    public $fillable = [
        'estadocivil'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'estadocivil' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
