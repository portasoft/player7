<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Referencium
 *
 * @package App
 * @property string $descricao
*/
class Referencia extends Model
{
    use SoftDeletes;

    protected $fillable = ['descricao'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Referencia::observe(new \App\Observers\UserActionsObserver);
    }
    
}
