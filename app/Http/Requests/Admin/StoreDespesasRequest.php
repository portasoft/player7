<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreDespesasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fornecedor' => 'required',
            'data' => 'required|date_format:'.config('app.date_format'),
            'valor_conta' => 'required',
            'parcela' => 'max:2147483647|required|numeric',
        ];
    }
}
