<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreProdutosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'descricao' => 'required',
            // 'estoque' => 'max:2147483647|nullable|numeric',
            // 'estoquemin' => 'max:2147483647|nullable|numeric',
            // 'preco' => 'required',
            'datacadastro' => 'nullable|date_format:'.config('app.date_format'),
        'foto' => 'nullable|mimes:png,jpg,jpeg,gif',
        ];
    }
}
