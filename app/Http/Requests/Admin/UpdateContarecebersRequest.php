<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateContarecebersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'vencimento' => 'required|date_format:'.config('app.date_format'),
            'valorconta' => 'required',
            'parcela' => 'max:2147483647|required|numeric',
            'dataemissao' => 'nullable|date_format:'.config('app.date_format'),
        ];
    }
}
