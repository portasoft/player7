<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateContaapagarsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'fornecedor' => 'required',
            'vencimento' => 'required|date_format:'.config('app.date_format'),
            'valorconta' => 'required',
            'parcela' => 'max:2147483647|required|numeric',
            'databaixa' => 'nullable|date_format:'.config('app.date_format'),
            'dataemissao' => 'nullable|date_format:'.config('app.date_format'),
        ];
    }
}
