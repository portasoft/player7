<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrcamentosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cliente' => 'required',
            'produto' => 'required',
            'qtde' => 'max:2147483647|required|numeric',
            'validade' => 'nullable|date_format:'.config('app.date_format'),
            'data' => 'required|date_format:'.config('app.date_format'),
            'valortotal' => 'required',
            'valorunitario' => 'required',
        ];
    }
}
