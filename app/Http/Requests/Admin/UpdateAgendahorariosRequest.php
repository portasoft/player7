<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAgendahorariosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'data' => 'nullable|date_format:'.config('app.date_format'),
            'hora' => 'nullable|date_format:H:i:s',
            'retorno' => 'nullable|date_format:'.config('app.date_format'),
        ];
    }
}
