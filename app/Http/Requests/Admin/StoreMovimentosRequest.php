<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreMovimentosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data' => 'required|date_format:'.config('app.date_format'),
            'tipo' => 'required',
            'empresa' => 'required',
            'deposito' => 'max:2147483647|required|numeric',
            'usuario' => 'max:2147483647',
            'produto' => 'max:2147483647',
            'qtde' => 'max:2147483647|required|numeric',
        ];
    }
}
