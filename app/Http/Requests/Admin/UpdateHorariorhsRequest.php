<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateHorariorhsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'descricao' => 'required',
            'datainicio' => 'required|date_format:'.config('app.date_format'),
            'datafim' => 'required|date_format:'.config('app.date_format'),
            'horaentrada' => 'required|date_format:H:i:s',
            'horasaida' => 'required|date_format:H:i:s',
        ];
    }
}
