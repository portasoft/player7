<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreProducaosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'produtoid' => 'max:2147483647|required|numeric',
            // 'depositoorigem' => 'max:2147483647|nullable|numeric',
            // 'depositodestino' => 'max:2147483647|nullable|numeric',
        ];
    }
}
