<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEntradanotasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'dataemissao' => 'nullable|date_format:'.config('app.date_format'),
            'dataentrada' => 'nullable|date_format:'.config('app.date_format'),
            'datacadastro' => 'nullable|date_format:'.config('app.date_format'),
        ];
    }
}
