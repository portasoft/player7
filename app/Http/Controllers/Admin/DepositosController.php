<?php

namespace App\Http\Controllers\Admin;

use App\Deposito;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDepositosRequest;
use App\Http\Requests\Admin\UpdateDepositosRequest;
use Yajra\Datatables\Datatables;
use App\Empresa;
use Illuminate\Support\Facades\DB;
class DepositosController extends Controller
{
    /**
     * Display a listing of Deposito.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('deposito_access')) {
            return abort(401);
        }
        //
   
     
        if (request()->ajax()) {
            $query = DB::table('depositos')->where('deleted_at', '=', NULL)
            ->select('depositos.*', 
            (DB::raw("(SELECT razao FROM empresas u1 WHERE u1.id = depositos.empresa ) AS emp ")))
            ->get();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
            if (! Gate::allows('deposito_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $table = Datatables::of($query);
            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'deposito_';
                $routeKey = 'admin.depositos';
                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('descricao', function ($row) {
                return $row->descricao ? $row->descricao : '';
            });
            $table->editColumn('empresa', function ($row) {
                return $row->empresa ? $row->empresa : '';
            });
                return $table->make(true);
        }
        $empresa =Empresa::pluck('fantasia','id');
        return view('admin.depositos.index',compact('empresa'));
    }

    /**
     * Show the form for creating new Deposito.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('deposito_create')) {
            return abort(401);
        }
        $empresa =Empresa::pluck('fantasia','id');
        return view('admin.depositos.create',compact('empresa'));
    }

    /**
     * Store a newly created Deposito in storage.
     *
     * @param  \App\Http\Requests\StoreDepositosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepositosRequest $request)
    {
        if (! Gate::allows('deposito_create')) {
            return abort(401);
        }
        $deposito = Deposito::create($request->all());


        return redirect()->back()->with('success', true);
        // return redirect()->route('admin.depositos.index');
    }


    /**
     * Show the form for editing Deposito.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('deposito_edit')) {
            return abort(401);
        }
        if (request()->ajax()) {
            $query = DB::table('depositos')->where('deleted_at', '=', NULL)
            ->select('depositos.*', 
            (DB::raw("(SELECT razao FROM empresas u1 WHERE u1.id = depositos.empresa ) AS emp ")))
            ->get();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
            if (! Gate::allows('deposito_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $table = Datatables::of($query);
            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'deposito_';
                $routeKey = 'admin.depositos';
                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('descricao', function ($row) {
                return $row->descricao ? $row->descricao : '';
            });
            $table->editColumn('empresa', function ($row) {
                return $row->empresa ? $row->empresa : '';
            });
                return $table->make(true);
        }
    
        $deposito = Deposito::findOrFail($id);
        $empresa =Empresa::pluck('fantasia','id');
        return view('admin.depositos.edit', compact('deposito','empresa'));
    }

    /**
     * Update Deposito in storage.
     *
     * @param  \App\Http\Requests\UpdateDepositosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDepositosRequest $request, $id)
    {
        if (! Gate::allows('deposito_edit')) {
            return abort(401);
        }
        $deposito = Deposito::findOrFail($id);
        $deposito->update($request->all());


        return redirect()->back()->with('success', true);
        // return redirect()->route('admin.depositos.index');
    }


    /**
     * Display Deposito.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('deposito_view')) {
            return abort(401);
        }
        $deposito = Deposito::findOrFail($id);
   
        $vP = $deposito['empresa'];
      

        $Vempresas =  DB::table('empresas')->where('id', '=', $vP)->get();
     
        foreach ($Vempresas as $key => $value) {
            $VproP = $value->razao;
        }
     
    

        $deposito['empresa'] = $VproP;
     

        return view('admin.depositos.show', compact('deposito'));
    }


    /**
     * Remove Deposito from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('deposito_delete')) {
            return abort(401);
        }
        $deposito = Deposito::findOrFail($id);
        $deposito->delete();

        return redirect()->route('admin.depositos.index');
    }

    /**
     * Delete all selected Deposito at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('deposito_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Deposito::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Deposito from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('deposito_delete')) {
            return abort(401);
        }
        $deposito = Deposito::onlyTrashed()->findOrFail($id);
        $deposito->restore();

        return redirect()->route('admin.depositos.index');
    }

    /**
     * Permanently delete Deposito from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('deposito_delete')) {
            return abort(401);
        }
        $deposito = Deposito::onlyTrashed()->findOrFail($id);
        $deposito->forceDelete();

        return redirect()->route('admin.depositos.index');
    }
}
