<?php

namespace App\Http\Controllers\Admin;

use App\Despesa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDespesasRequest;
use App\Http\Requests\Admin\UpdateDespesasRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;
use DB;
use App\Contacontabil;
use App\User;
use App\Contacorrente;
class DespesasController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Despesa.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('despesa_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Despesa::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('despesa_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'despesas.id',
                'despesas.fornecedor',
                'despesas.data',
                'despesas.valor_conta',
                'despesas.valorpago',
                'despesas.parcela',
                'despesas.formapagamento',
                'despesas.contacontabil',
                'despesas.docmercantil',
                'despesas.contacorrente',
                'despesas.documento',
                'despesas.obs',
                'despesas.colaborador',
                'despesas.tipo',
                'despesas.juros',
                'despesas.desconto',
                'despesas.anexo',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'despesa_';
                $routeKey = 'admin.despesas';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('fornecedor', function ($row) {
                return $row->fornecedor ? $row->fornecedor : '';
            });
            $table->editColumn('data', function ($row) {
                return $row->data ? $row->data : '';
            });
            $table->editColumn('valor_conta', function ($row) {
                return $row->valor_conta ? $row->valor_conta : '';
            });
            $table->editColumn('valorpago', function ($row) {
                return $row->valorpago ? $row->valorpago : '';
            });
            $table->editColumn('parcela', function ($row) {
                return $row->parcela ? $row->parcela : '';
            });
            $table->editColumn('formapagamento', function ($row) {
                return $row->formapagamento ? $row->formapagamento : '';
            });
            $table->editColumn('contacontabil', function ($row) {
                return $row->contacontabil ? $row->contacontabil : '';
            });
            $table->editColumn('docmercantil', function ($row) {
                return $row->docmercantil ? $row->docmercantil : '';
            });
            $table->editColumn('contacorrente', function ($row) {
                return $row->contacorrente ? $row->contacorrente : '';
            });
            $table->editColumn('documento', function ($row) {
                return $row->documento ? $row->documento : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('colaborador', function ($row) {
                return $row->colaborador ? $row->colaborador : '';
            });
            $table->editColumn('tipo', function ($row) {
                return $row->tipo ? $row->tipo : '';
            });
            $table->editColumn('juros', function ($row) {
                return $row->juros ? $row->juros : '';
            });
            $table->editColumn('desconto', function ($row) {
                return $row->desconto ? $row->desconto : '';
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.despesas.index');
    }

    /**
     * Show the form for creating new Despesa.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('despesa_create')) {
            return abort(401);
        }
        $contacorrente = Contacorrente::pluck('banco','id');
        return view('admin.despesas.create',compact('contacorrente'));
    }
    function searchcontacontabil(Request $request){
        if($request->get('query'))
             {
                        $query = $request->get('query');
                      
                 $data = DB::table('contacontabils')
                   ->where('descricao','LIKE', $query.'%')
                     ->get();
                $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach($data as $row)
            {
                          $output .='<li class="device_result co" > <a href="#">'.$row->descricao.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    function searchusuario(Request $request){
        if($request->get('query'))
             {
                        $query = $request->get('query');
                      
                 $data = DB::table('users')
                   ->where('name','LIKE', $query.'%')
                     ->get();
                $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach($data as $row)
            {
                          $output .='<li class="device_result us" > <a href="#">'.$row->name.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    function searchfornecedor(Request $request){
        if($request->get('query'))
             {
                        $query = $request->get('query');
                      
                 $data = DB::table('fornecedors')
                   ->where('fantasia','LIKE', $query.'%')
                     ->get();
                $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach($data as $row)
            {
                          $output .='<li class="device_result n" > <a href="#">'.$row->fantasia.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    /**
     * Store a newly created Despesa in storage.
     *
     * @param  \App\Http\Requests\StoreDespesasRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDespesasRequest $request)
    {
        if (! Gate::allows('despesa_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $despesa = Despesa::create($request->all());



        return redirect()->route('admin.despesas.index');
    }


    /**
     * Show the form for editing Despesa.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('despesa_edit')) {
            return abort(401);
        }
        $despesa = Despesa::findOrFail($id);
        $contacorrente =Contacorrente::pluck('banco','id');
        return view('admin.despesas.edit', compact('despesa','contacorrente'));
    }

    /**
     * Update Despesa in storage.
     *
     * @param  \App\Http\Requests\UpdateDespesasRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDespesasRequest $request, $id)
    {
        if (! Gate::allows('despesa_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $despesa = Despesa::findOrFail($id);
        $despesa->update($request->all());



        return redirect()->route('admin.despesas.index');
    }


    /**
     * Display Despesa.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('despesa_view')) {
            return abort(401);
        }
        $despesa = Despesa::findOrFail($id);

        return view('admin.despesas.show', compact('despesa'));
    }


    /**
     * Remove Despesa from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('despesa_delete')) {
            return abort(401);
        }
        $despesa = Despesa::findOrFail($id);
        $despesa->delete();

        return redirect()->route('admin.despesas.index');
    }

    /**
     * Delete all selected Despesa at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('despesa_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Despesa::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Despesa from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('despesa_delete')) {
            return abort(401);
        }
        $despesa = Despesa::onlyTrashed()->findOrFail($id);
        $despesa->restore();

        return redirect()->route('admin.despesas.index');
    }

    /**
     * Permanently delete Despesa from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('despesa_delete')) {
            return abort(401);
        }
        $despesa = Despesa::onlyTrashed()->findOrFail($id);
        $despesa->forceDelete();

        return redirect()->route('admin.despesas.index');
    }
}
