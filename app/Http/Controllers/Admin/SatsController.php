<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Response;
use App\Produto;
use App\Cliente;
use App\Vendaespera;
use App\Vendatemp;
use App\Vendaesperaiten;
use App\User;
use App\Vendaitem;
use DB;
use App\Empresa;
use File;
class SatsController extends Controller
{
    // public function create(Request $request) {

    //     if ($request->session()->has('carrinho')) {
    //     $carrinho = $request->session()->get('carrinho');
    //     }else {
    //     $carrinho = [];
    //     $request->session()->put('carrinho', $carrinho);
    //     }

    //     return view('sats.index', ['carrinho'=>$carrinho]);
    //     }
    public function index(Request $request)
    {
        if (!Gate::allows('sat_access')) {
            return abort(401);
        }

        if ($request->session()->has('carrinho')) {
            $carrinho = $request->session()->get('carrinho');
        } else {
            $carrinho = [];
            $request->session()->put('carrinho', $carrinho);
        }
    // $user = auth()->user()->id;

        $venda = DB::table('vendatemps')->orderBy('id', 'DESC')->first();
        $vendaespera = DB::table('vendaesperas')->orderBy('id', 'DESC')->first();
        $vendasv = DB::table('vendaesperas')->get();
        return view('admin.sats.index', ['carrinho' => $carrinho, 'venda' => $venda, 'vendaespera' => $vendaespera])->with('vendasv', $vendasv);
    }
    public function fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');

            

            $produtos = DB::table('produtos')->where('descricao', 'LIKE', $query . '%')->orWhere('referencia', 'LIKE', $query . '%')->orWhere('codigobarra', 'LIKE', $query . '%')->get();

            

            $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach ($produtos as $row) {
                $output .= '<li class="device_result c"><a href="#">' . $row->descricao . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;

        }


    }
    // public function pesquisaCliente(Request $request){
    // $nome = $request->nome;

    // $clientes = DB::table('clientes')->where('nome', 'LIKE', $nome.'%')
    //  ->orWhere('id', 'LIKE', $nome.'%')->get();
    //  foreach ($clientes as $row) {


    //            return  $row->id;
    //     }

    // }

    function searchcliente(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('clientes')->where('nome', 'LIKE', $query . '%')->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach ($data as $row) {
                $output .= '<li class="device_result n" > <a href="#">' . $row->nome . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    function searchvendedor(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('users')->where('name', 'LIKE', $query . '%')->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach ($data as $row) {
                $output .= '<li class="device_result ve" > <a href="#">' . $row->name . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    function searchvendedorE(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('users')->where('name', 'LIKE', $query . '%')->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach ($data as $row) {
                $output .= '<li class="device_result vee" > <a href="#">' . $row->name . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    public function pagamento()
    {
        return view('admin.sats.pagamento');
    }

    public function verificaProdutoArray($request)
    {
        $id = $request->idProdutoAdiciona;

        $carrinho = $request->session()->get('carrinho');

        $produto = array_column($carrinho, 'produto');

        $key = array_search($id, array_column($produto, 'id'));

        return $key;

    }



    public function adicionarProduto(Request $request)
    {

        $key = $this->verificaProdutoArray($request);

        if ($key === false) {
            $vendaitem = new Vendaitem();

            $result = $vendaitem->adicionarProduto($request->idProdutoAdiciona, $request->qtde, $request->preco);
   
            
            if ($result) {
                $this->adicionarAoCarrinho($request, $vendaitem);

                return redirect()->route('Admin.sats.index');
            } else {

                return redirect()->route('Admin.sats.index')->with('msgError', 'Produto não adicionado,
        verifique o código,status ou quantidade disponivel/digitada do produto no estoque!');
            }
        }
        return redirect()->route('Admin.sats.index')->with('msgError', 'O Produto já estava na lista.
        Para alterar a quantidade do Produto, basta remove-lo e adicona-lo novamente com a quantidade correta');
    }


    public function adicionarAoCarrinho($request, $vendaitem)
    {
        $carrinho = $request->session()->get('carrinho');

        array_push($carrinho, $vendaitem);
        $request->session()->put('carrinho', $carrinho);
    }

    public function removeDoCarrinho(Request $request, $index)
    {
        $carrinho = $request->session()->get('carrinho');
        unset($carrinho[$index]);
        $request->session()->put('carrinho', $carrinho);
        return redirect()->route('admin.sats.index')->with('msg', 'Produto Removido');
    }

    public function listaProdutos()
    {
        $produtos = Produto::where('status', '=', 'ativado')->paginate(8);
        return view('sats.produtos', ['produtos' => $produtos]);
    }

    public function buscarProduto(Request $request)
    {
        $valorBusca = $request->campoBuscar;
        if ($request->campoEscolha == 'porCodigoProduto') {
            $produtos = Produto::where([
                ['id', '=', $valorBusca],
                ['status', '=', 'ativado']
            ])->paginate(8);
            return view('sats.produtos', ['produtos' => $produtos]);
        }

        if ($request->campoEscolha == "porTipoProduto") {
            $produtos = Produto::where([
                ['tipoProduto', 'like', '%' . $valorBusca . '%'],
                ['status', '=', 'ativado']
            ])->paginate(8);
            return view('sats.produtos', ['produtos' => $produtos]);
        }

    //Caso o valorBusca seja em branco
        return redirect("/sats/produtos")->with('msgError', 'Não esqueça de digitar um valor para busca!');
    }


    public function salvarVenda(Request $request)
    {
 
        $carrinho = $request->session()->get('carrinho');

        $finalizarim = $request->alvo;
   
        $queryNomefinaliza = $request->nome;
 
        if ($request->formaPagamento != 'nenhum' && (!empty($carrinho))) {
            $venda = new Vendatemp();
            $totalvenda = 0;
            foreach ($carrinho as $c) {
                $qtde = $c->qtde;
                $c->produto->retiraEstoque($qtde);
                $totalvenda += $c->valortotal;
            }
            if ($request->vendedor != '') {
                $name = $request->vendedor;
                $users = DB::table('users')->where('name', 'LIKE', $name . '%')
                    ->orWhere('id', 'LIKE', $name . '%')->get();
                    if ($users->count() > 0) {
                        foreach ($users as $row) {
                            $id_user = $row->id;
                        }
                        $venda->id_user = $id_user;
                    } else {
                        # code...
                    }
                    
              
            }
            if ($request->nome != '') {
                $nome = $request->nome;
                $clientes = DB::table('clientes')->where('nome', 'LIKE', $nome . '%')
                    ->orWhere('id', 'LIKE', $nome . '%')->get();
                    //VERIFICA SE A CONSULTA ESTA VAZIA
              if ($clientes->count() > 0) {
                      
                foreach ($clientes as $row) {
                    $id_cliente = $row->id;
                    $venda->id_cliente = $id_cliente;
                }
               
              }
             
          
             
            }
            $venda->totalvenda = $totalvenda;
            if ($request->cartao != '') {
                $venda->cartao = $request->cartao;
                $venda->tipocartao = $request->tipocartao;
            }
            if ($request->crediaio != '') {
                $venda->crediaio = $request->crediaio;
                $venda->dias = $request->dias;
            }
            $venda->dinheiro = $request->dinheiro;
            $venda->cheque = $request->cheque;
            $venda->dadocheque = $request->dadocheque;
            $venda->save();
            foreach ($carrinho as $c) {
                $vendaitem = new Vendaitem();
                $vendaitem->qtde = $c->qtde;

                $vendaitem->valortotalitens = $c->valortotal;
                $vendaitem->unitario = $c->produto->preco;
                $vendaitem->produto = $c->produto->id;
                $vendaitem->idvenda = $venda->id;
                $vendaitem->save();
            }
            $request->session()->forget('carrinho');
                // SAT
            $cn = 'C:\ACBrMonitorPLUS\Arqs\Venda\ENT.INI';
            $empresa = DB::table('empresas')->get();
            foreach ($empresa as $row) {
                $CNPJBD = $row->cnpj;
                $IEBD = $row->ie;
            }
      
         
            $CriarEnviarCfe = 'SAT.CriarEnviarCfe("[infCFe]';
            $versao = 'versao=0.07';
            $Identificacao = '[Identificacao]';
            $CNPJID = 'CNPJ=' . $CNPJBD;
            $signAC = 'signAC=12313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341234';
            $numeroCaixa = 'numeroCaixa=1';
            $Emitente = '[Emitente]';
            $CNPJEM = 'CNPJ=' . $CNPJBD;
            $IE = 'IE='.$IEBD;
            $cRegTribISSQN = 'cRegTribISSQN=1';
            $indRatISSQN = 'indRatISSQN=N';
            $Destinatario = '[Destinatario]';
            $cliente = DB::table('clientes')->where('nome', 'LIKE', $queryNomefinaliza . '%')->get();
            if ($clientes->count() > 0) {
                foreach ($cliente as $row) {
                    $NOMEBD = $row->nome;
                    $EnderecoBD = $row->endereco;
                    $numBD = $row->numero;
                    $bairroBD = $row->bairro;
                    $municipioBD = $row->cidade;
                   
                }
                $xNome = 'xNome='.$NOMEBD;
                } else {
                    $xNome = 'xNome=';
                }
   
         
            $Entrega = '[Entrega]';
            $xLgr = 'xLgr='.$EnderecoBD;
            $nro = 'nro='.$numBD;
            $nro2 = $numBD;
            $Bairro = 'xBairro='.$bairroBD;
            $xMun = 'xMun='.$municipioBD;
            $UF = 'UF=SP';

            $Produto001 = '[Produto001]';
         
            foreach ($carrinho as $c) {
            
                $produtos = DB::table('produtos')->where('id', 'LIKE', $c->produto->id. '%')->get();
                $xProd = 'xProd='.$c->produto->descricao;
                $infAdProd = 'infAdProd=';
           
                $cProd = 'cProd='.$c->produto->id;
 
                $qCom = 'qCom='.$c->qtde;
                $vUnCom = 'vUnCom='.$c->produto->preco;
            }
           
                if ($produtos->count() > 0) {
                    foreach ($produtos as $row) {
                        $cEAN = 'cEAN='.$row->codigobarra;
                        $NCM = 'NCM='.$row->ncm;
                        $CFOP = 'CFOP='.$row->cfop;
                        $unidades = DB::table('unidades')->where('id', 'LIKE', $row->unidade . '%')->get();
                        foreach ($unidades as $value) {
                            $value->descricao;
                      
                        }
            
                        $uCom = 'uCom='.$value->descricao;
                        // dd($uCom);
                    }
         
                    } else {
              
                    }
                            
            $Combustivel = 'Combustivel=0';
                   
            $indRegra = 'indRegra=A';
            $vDesc = 'vDesc=0';
            $vOutro = 'vOutro=0';
            $vItem12741 = 'vItem12741=0,01';
            $ObsFiscoDet001001 = '[ObsFiscoDet001001]';
            $xCampoDet = 'xCampoDet=C';
            $xTextoDet = 'xTextoDet=T';
            $ICMS001 = '[ICMS001]';
            $Orig = 'Orig=0';
            $CSOSN = 'CSOSN=102';
            $PIS001 = '[PIS001]';
            $CSTPIS = 'CST=49';
            $COFINS001 = '[COFINS001]';
            $CSTCOFINS = 'CST=49';
            $Total = '[Total]';
            $vCFeLei12741 = 'vCFeLei12741=0,01';
            $DescAcrEntr = '[DescAcrEntr]';
            $vAcresSubtot = 'vAcresSubtot=0';
            $vDescSubtot = 'vDescSubtot=0';
            $Pagto001 = '[Pagto001]';
            $cMPP1 = 'cMP=001';
            $vMPP1 = 'vMP=0,01';
            $Pagto002 = '[Pagto002]';
            $cMPP2 = 'cMP=002';
            $vMPP2 = 'vMP=0,02';
            $Pagto003 = '[Pagto003]';
            $cMPP3 = 'cMP=003';
            $vMPP3 = 'vMP=0,01';
            $DadosAdicionais = '[DadosAdicionais]';
            $infCpl = 'infCpl=----> VALOR TOTAL DO CUPOM FISCAL  R$ 0,04            $        $   Pgto.Din.: 0,01 / Pgto.Cheque: 0,02 / Pgto.Cred.: 0,01 / ';
            $ObsFisco001 = '[ObsFisco001]';
            $xCampo = 'xCampo=F';
            $xTexto = 'xTexto=F")';
            $quebra = chr(13) . chr(10);
        
  $cd = $CriarEnviarCfe . $quebra . $versao . $quebra . $Identificacao . $quebra . $CNPJID . $quebra . $signAC . $quebra . $numeroCaixa . $quebra . $Emitente . $quebra . $CNPJEM . $quebra
      . $IE . $quebra . $cRegTribISSQN . $quebra . $indRatISSQN . $quebra . $Destinatario . $quebra . $xNome . $quebra . $Entrega . $quebra . $xLgr.','. $nro2 . $quebra . $nro .$quebra . $Bairro . $quebra
      . $xMun . $quebra . $UF . $quebra . $Produto001 . $quebra . $cProd . $quebra . $infAdProd . $quebra . $cEAN . $quebra . $xProd . $quebra . $NCM . $quebra . $CFOP . $quebra . $uCom . $quebra
      . $Combustivel . $quebra . $qCom . $quebra . $vUnCom . $quebra . $indRegra . $quebra . $vDesc . $quebra . $vOutro . $quebra . $vItem12741 . $quebra . $ObsFiscoDet001001 . $quebra
      . $xCampoDet . $quebra . $xTextoDet . $quebra . $ICMS001 . $quebra . $Orig . $quebra . $CSOSN . $quebra . $PIS001 . $quebra . $CSTPIS . $quebra . $COFINS001 . $quebra . $CSTCOFINS . $quebra
      . $Total . $quebra . $vCFeLei12741 . $quebra . $DescAcrEntr . $quebra . $vAcresSubtot . $quebra . $vDescSubtot .  $quebra . $Pagto001 . $quebra . $cMPP1 . $quebra . $vMPP1 . $quebra . $Pagto002 . $quebra
      . $cMPP2 . $quebra . $vMPP2 . $quebra . $Pagto003 . $quebra . $cMPP3 . $quebra . $vMPP3 . $quebra . $DadosAdicionais . $quebra . $infCpl . $quebra . $ObsFisco001 . $quebra . $xCampo . $quebra
      . $xTexto;
      File::delete('C:\ACBrMonitorPLUS\Arqs\Saida\SAI.INI');
             file_put_contents($cn, $cd);
             $filename = 'C:\ACBrMonitorPLUS\Arqs\Saida\SAI.INI';

            //  if (!file_exists($filename)) {
            //    dd($filename, 'existe');
            //  } else {
            //     dd($filename, 'nao existe');
            //  }
            sleep(17);

           $arquivo  = file('C:\ACBrMonitorPLUS\Arqs\Saida\SAI.INI');
           $local = 'C:\ACBrMonitorPLUS\Arqs\Venda\ENT.INI';
           $y = 2;
           $x = $arquivo[$y];
     
           $valor = explode('Arquivo=',$x);
    
                    $txtinicio ='SAT.GerarPDFExtratoVenda("';
                    $txtfim = '","C:\EXTRATO\ESTRATO.PDF")';
                    $str = implode("", $valor);
          
                    $str2 = str_replace("\n", "", $str);
                     $impFinalPDF =  $txtinicio.$str2.$txtfim;
                     $impFinalPDF2 = str_replace("&nbsp;"," ",$impFinalPDF);
                    $impFinalPDF2 = str_replace("\n", "", $impFinalPDF);
                    $impFinalPDF2 = str_replace("\r", "", $impFinalPDF);
                    $impFinalPDF2 = preg_replace('/\s/',' ',$impFinalPDF);
                   
  
         file_put_contents($local, $impFinalPDF2);
      
                $finalizou = 1;
  
            return redirect()->route('admin.sats.index',compact('finalizou'))->with('msg', 'Venda Finalizada com sucesso!');

        } else {

            return redirect()->route('admin.sats.index')->with('msgError', 'Selecione uma forma de pagamento!');

        }
    }


    public function salvarEspera(Request $request)
    {

        $carrinho = $request->session()->get('carrinho');
        $finalizarim = $request->alvo;
        if (!empty($carrinho)) {
            $vendaEspera = new Vendaespera();
            $totalvenda = 0;
            foreach ($carrinho as $c) {
                $qtde = $c->qtde;
                $totalvenda += $c->valortotal;
            }
            if ($request->vendedorE != '') {
                $name = $request->vendedorE;
                $users = DB::table('users')->where('name', 'LIKE', $name . '%')
                    ->orWhere('id', 'LIKE', $name . '%')->get();
                foreach ($users as $row) {
                    $id_user = $row->id;
                }
                $vendaEspera->id_user = $id_user;

            } else {
                $user = auth()->user()->id;
                $vendaEspera->id_user = $user;
            }
            if ($request->nome != '') {
                $nome = $request->nome;
                $clientes = DB::table('clientes')->where('nome', 'LIKE', $nome . '%')
                    ->orWhere('id', 'LIKE', $nome . '%')->get();
                foreach ($clientes as $row) {
                    $id_cliente = $row->id;
                }
                $vendaEspera->id_cliente = $id_cliente;
            }
            $vendaEspera->total = $totalvenda;
            $vendaEspera->comanda = $request->comanda;

            $vendaEspera->save();
            foreach ($carrinho as $c) {
                $vendaitem = new Vendaesperaiten();
                $vendaitem->qtde = $c->qtde;

                $vendaitem->totalitens = $c->valortotal;
                $vendaitem->unitario = $c->produto->preco;
                $vendaitem->idproduto = $c->produto->id;
                $vendaitem->venda = $vendaEspera->id;
                $vendaitem->save();
            }
            $request->session()->forget('carrinho');

            return redirect()->route('admin.sats.index')->with('msg', 'Venda Finalizada com sucesso!');

        } else {

            return redirect()->route('admin.sats.index')->with('msgError', 'Selecione uma forma de pagamento!');

        }
    }

    // public function geraini(Request $request)
    // {
     
        
    // //   return redirect()->route('admin.sats.index');
    //     $cn = 'C:\ACBrMonitorPLUS\ENT.INI';
    //     $empresa = DB::table('empresas')->get();
    //     foreach ($empresa as $row) {
    //         $CNPJBD = $row->cnpj;
    //         $IEBD = $row->ie;
    //     }
    //     $data = DB::table('clientes')->where('nome', 'LIKE', $query . '%')->get();
      
    //     $CriarEnviarCfe = 'SAT.CriarEnviarCfe("[infCFe]';
    //     $versao = 'versao=0.07';
    //     $Identificacao = '[Identificacao]';
    //     $CNPJID = 'CNPJ=' . $CNPJBD;
    //     $signAC = '12313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341231321234123132123412313212341234';
    //     $numeroCaixa = '1';
    //     $Emitente = '[Emitente]';
    //     $CNPJEM = 'CNPJ=' . $CNPJBD;
    //     $IE = 'IE =' . $IEBD;
    //     $cRegTribISSQN = 'cRegTribISSQN=1';
    //     $indRatISSQN = 'indRatISSQN=N';
    //     $Destinatario = '[Destinatario]';
    //     $xNome = 'PAULO MELLO';
    //     $Entrega = '[Entrega]';
    //     $xLgr = 'TUGUCIGALPA, 149';
    //     $nro = '149';
    //     $Bairro = 'VILA GUAIRA';
    //     $xMun = 'PRESIDENTE PRUDENTE';
    //     $UF = 'SP';
    //     $Produto001 = '[Produto001]';
    //     $cProd = '1';
    //     $infAdProd = '';
    //     $cEAN = '';
    //     $xProd = 'MEIA DE MALHA INFANTIL edi';
    //     $NCM = '61159500';
    //     $CFOP = '5102';
    //     $uCom = 'PC';
    //     $Combustivel = '0';
    //     $qCom = '1,00';
    //     $vUnCom = '0,04';
    //     $indRegra = 'A';
    //     $vDesc = '0';
    //     $vOutro = '0';
    //     $vItem12741 = '0,01';
    //     $ObsFiscoDet001001 = '[ObsFiscoDet001001]';
    //     $xCampoDet = 'C';
    //     $xTextoDet = 'T';
    //     $ICMS001 = '[ICMS001]';
    //     $Orig = '0';
    //     $CSOSN = '102';
    //     $PIS001 = '[PIS001]';
    //     $CSTPIS = '49';
    //     $COFINS001 = '[COFINS001]';
    //     $CSTCOFINS = '49';
    //     $Total = '[Total]';
    //     $vCFeLei12741 = '0,01';
    //     $DescAcrEntr = '[DescAcrEntr]';
    //     $vAcresSubtot = '0';
    //     $vDescSubtot = '0';
    //     $Pagto001 = '[Pagto001]';
    //     $cMPP1 = '001';
    //     $vMPP1 = '0,01';
    //     $Pagto002 = '[Pagto002]';
    //     $cMPP2 = '002';
    //     $vMPP2 = '0,02';
    //     $Pagto003 = '[Pagto003]';
    //     $cMPP3 = '003';
    //     $vMPP3 = '0,01';
    //     $DadosAdicionais = '[DadosAdicionais]';
    //     $infCpl = '----> VALOR TOTAL DO CUPOM FISCAL  R$ 0,04            $        $   Pgto.Din.: 0,01 / Pgto.Cheque: 0,02 / Pgto.Cred.: 0,01 / ';
    //     $ObsFisco001 = '[ObsFisco001]';
    //     $xCampo = 'F';
    //     $xTexto = 'F")';
    //     $quebra = chr(13) . chr(10);
    //     $cd = $CriarEnviarCfe . $quebra . $versao . $quebra . $Identificacao . $quebra . $CNPJID . $quebra . $signAC . $quebra . $numeroCaixa . $quebra . $Emitente . $quebra . $CNPJEM . $quebra
    //         . $IE . $quebra . $cRegTribISSQN . $quebra . $indRatISSQN . $quebra . $Destinatario . $quebra . $xNome . $quebra . $Entrega . $quebra . $xLgr . $quebra . $nro . $quebra . $Bairro . $quebra
    //         . $xMun . $quebra . $UF . $quebra . $Produto001 . $quebra . $cProd . $quebra . $infAdProd . $quebra . $cEAN . $quebra . $xProd . $quebra . $NCM . $quebra . $CFOP . $quebra . $uCom . $quebra
    //         . $Combustivel . $quebra . $qCom . $quebra . $vUnCom . $quebra . $indRegra . $quebra . $vDesc . $quebra . $vOutro . $quebra . $vItem12741 . $quebra . $ObsFiscoDet001001 . $quebra
    //         . $xCampoDet . $quebra . $xTextoDet . $quebra . $ICMS001 . $quebra . $Orig . $quebra . $CSOSN . $quebra . $PIS001 . $quebra . $CSTPIS . $quebra . $COFINS001 . $quebra . $CSTCOFINS . $quebra
    //         . $Total . $quebra . $vCFeLei12741 . $quebra . $DescAcrEntr . $quebra . $vAcresSubtot . $quebra . $Pagto001 . $quebra . $cMPP1 . $quebra . $vMPP1 . $quebra . $Pagto002 . $quebra
    //         . $cMPP2 . $quebra . $vMPP2 . $quebra . $Pagto003 . $quebra . $cMPP3 . $quebra . $vMPP3 . $quebra . $DadosAdicionais . $quebra . $infCpl . $quebra . $ObsFisco001 . $quebra . $xCampo . $quebra
    //         . $xTexto;
    //     file_put_contents($cn, $cd);

    // }
        // public function buscaEspera(Request $request) {
        //    $vendasv = DB::table('vendaesperas')->get();
    
        //     return view('admin.sats.index')->with('vendasv',$vendasv);
    
          
        //     }

}

