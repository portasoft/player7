<?php

namespace App\Http\Controllers\Admin;

use App\Contaapagar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreContaapagarsRequest;
use App\Http\Requests\Admin\UpdateContaapagarsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;
use DB;
use App\Fornecedor;
use App\Contacorrente;
class ContaapagarsController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Contaapagar.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('contaapagar_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Contaapagar::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('contaapagar_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'contaapagars.id',
                'contaapagars.fornecedor',
                'contaapagars.vencimento',
                'contaapagars.valorconta',
                'contaapagars.valorapagar',
                'contaapagars.parcela',
                'contaapagars.formapagamento',
                'contaapagars.contacontabil',
                'contaapagars.docmercantil',
                'contaapagars.contacorrente',
                'contaapagars.documento',
                'contaapagars.colaborador',
                'contaapagars.obs',
                'contaapagars.tipo',
                'contaapagars.baixado',
                'contaapagars.databaixa',
                'contaapagars.juros',
                'contaapagars.desconto',
                'contaapagars.dataemissao',
                'contaapagars.anexo',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'contaapagar_';
                $routeKey = 'admin.contaapagars';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('fornecedor', function ($row) {
                return $row->fornecedor ? $row->fornecedor : '';
            });
            $table->editColumn('vencimento', function ($row) {
                return $row->vencimento ? $row->vencimento : '';
            });
            $table->editColumn('valorconta', function ($row) {
                return $row->valorconta ? $row->valorconta : '';
            });
            $table->editColumn('valorapagar', function ($row) {
                return $row->valorapagar ? $row->valorapagar : '';
            });
            $table->editColumn('parcela', function ($row) {
                return $row->parcela ? $row->parcela : '';
            });
            $table->editColumn('formapagamento', function ($row) {
                return $row->formapagamento ? $row->formapagamento : '';
            });
            $table->editColumn('contacontabil', function ($row) {
                return $row->contacontabil ? $row->contacontabil : '';
            });
            $table->editColumn('docmercantil', function ($row) {
                return $row->docmercantil ? $row->docmercantil : '';
            });
            $table->editColumn('contacorrente', function ($row) {
                return $row->contacorrente ? $row->contacorrente : '';
            });
            $table->editColumn('documento', function ($row) {
                return $row->documento ? $row->documento : '';
            });
            $table->editColumn('colaborador', function ($row) {
                return $row->colaborador ? $row->colaborador : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('tipo', function ($row) {
                return $row->tipo ? $row->tipo : '';
            });
            $table->editColumn('baixado', function ($row) {
                return $row->baixado ? $row->baixado : '';
            });
            $table->editColumn('databaixa', function ($row) {
                return $row->databaixa ? $row->databaixa : '';
            });
            $table->editColumn('juros', function ($row) {
                return $row->juros ? $row->juros : '';
            });
            $table->editColumn('desconto', function ($row) {
                return $row->desconto ? $row->desconto : '';
            });
            $table->editColumn('dataemissao', function ($row) {
                return $row->dataemissao ? $row->dataemissao : '';
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.contaapagars.index');
    }

    /**
     * Show the form for creating new Contaapagar.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('contaapagar_create')) {
            return abort(401);
        }
        $contacorrente = Contacorrente::pluck('banco','id');
        return view('admin.contaapagars.create',compact('contacorrente'));
    }
    function searchfornecedor(Request $request){
        if($request->get('query'))
             {
                        $query = $request->get('query');
                      
                 $data = DB::table('fornecedors')
                   ->where('fantasia','LIKE', $query.'%')
                     ->get();
                $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach($data as $row)
            {
                          $output .='<li class="device_result n" > <a href="#">'.$row->fantasia.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
  
      function searchcontacontabil(Request $request){
        if($request->get('query'))
             {
                        $query = $request->get('query');
                      
                 $data = DB::table('contacontabils')
                   ->where('descricao','LIKE', $query.'%')
                     ->get();
                $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach($data as $row)
            {
                          $output .='<li class="device_result co" > <a href="#">'.$row->descricao.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    function searchusuario(Request $request){
        if($request->get('query'))
             {
                        $query = $request->get('query');
                      
                 $data = DB::table('users')
                   ->where('name','LIKE', $query.'%')
                     ->get();
                $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach($data as $row)
            {
                          $output .='<li class="device_result us" > <a href="#">'.$row->name.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    /**
     * Store a newly created Contaapagar in storage.
     *
     * @param  \App\Http\Requests\StoreContaapagarsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContaapagarsRequest $request)
    {
        if (! Gate::allows('contaapagar_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $contaapagar = Contaapagar::create($request->all());



        return redirect()->route('admin.contaapagars.index');
    }


    /**
     * Show the form for editing Contaapagar.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('contaapagar_edit')) {
            return abort(401);
        }
        $contaapagar = Contaapagar::findOrFail($id);
        $contacorrente = Contacorrente::pluck('banco','id');
        return view('admin.contaapagars.edit', compact('contaapagar','contacorrente'));
    }

    /**
     * Update Contaapagar in storage.
     *
     * @param  \App\Http\Requests\UpdateContaapagarsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContaapagarsRequest $request, $id)
    {
        if (! Gate::allows('contaapagar_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $contaapagar = Contaapagar::findOrFail($id);
        $contaapagar->update($request->all());



        return redirect()->route('admin.contaapagars.index');
    }


    /**
     * Display Contaapagar.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('contaapagar_view')) {
            return abort(401);
        }
        $contaapagar = Contaapagar::findOrFail($id);

        return view('admin.contaapagars.show', compact('contaapagar'));
    }


    /**
     * Remove Contaapagar from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('contaapagar_delete')) {
            return abort(401);
        }
        $contaapagar = Contaapagar::findOrFail($id);
        $contaapagar->delete();

        return redirect()->route('admin.contaapagars.index');
    }

    /**
     * Delete all selected Contaapagar at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contaapagar_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contaapagar::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Contaapagar from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('contaapagar_delete')) {
            return abort(401);
        }
        $contaapagar = Contaapagar::onlyTrashed()->findOrFail($id);
        $contaapagar->restore();

        return redirect()->route('admin.contaapagars.index');
    }

    /**
     * Permanently delete Contaapagar from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('contaapagar_delete')) {
            return abort(401);
        }
        $contaapagar = Contaapagar::onlyTrashed()->findOrFail($id);
        $contaapagar->forceDelete();

        return redirect()->route('admin.contaapagars.index');
    }
}
