<?php

namespace App\Http\Controllers\Admin;

use App\Contacorrente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreContacorrentesRequest;
use App\Http\Requests\Admin\UpdateContacorrentesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;
use App\Empresa;
class ContacorrentesController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Contacorrente.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('contacorrente_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Contacorrente::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('contacorrente_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'contacorrentes.id',
                'contacorrentes.agencia',
                'contacorrentes.conta',
                'contacorrentes.banco',
                'contacorrentes.empresa',
                'contacorrentes.saldoinicial',
                'contacorrentes.saldo',
                'contacorrentes.anexo',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'contacorrente_';
                $routeKey = 'admin.contacorrentes';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('agencia', function ($row) {
                return $row->agencia ? $row->agencia : '';
            });
            $table->editColumn('conta', function ($row) {
                return $row->conta ? $row->conta : '';
            });
            $table->editColumn('banco', function ($row) {
                return $row->banco ? $row->banco : '';
            });
            $table->editColumn('empresa', function ($row) {
                return $row->empresa ? $row->empresa : '';
            });
            $table->editColumn('saldoinicial', function ($row) {
                return $row->saldoinicial ? $row->saldoinicial : '';
            });
            $table->editColumn('saldo', function ($row) {
                return $row->saldo ? $row->saldo : '';
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.contacorrentes.index');
    }

    /**
     * Show the form for creating new Contacorrente.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('contacorrente_create')) {
            return abort(401);
        }
        $empresa = Empresa::pluck('fantasia','id');
        return view('admin.contacorrentes.create',compact('empresa'));
    }

    /**
     * Store a newly created Contacorrente in storage.
     *
     * @param  \App\Http\Requests\StoreContacorrentesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContacorrentesRequest $request)
    {
        if (! Gate::allows('contacorrente_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $contacorrente = Contacorrente::create($request->all());



        return redirect()->route('admin.contacorrentes.index');
    }


    /**
     * Show the form for editing Contacorrente.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('contacorrente_edit')) {
            return abort(401);
        }
        $contacorrente = Contacorrente::findOrFail($id);
        $empresa = Empresa::pluck('fantasia','id');
        return view('admin.contacorrentes.edit', compact('contacorrente','empresa'));
    }

    /**
     * Update Contacorrente in storage.
     *
     * @param  \App\Http\Requests\UpdateContacorrentesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContacorrentesRequest $request, $id)
    {
        if (! Gate::allows('contacorrente_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $contacorrente = Contacorrente::findOrFail($id);
        $contacorrente->update($request->all());



        return redirect()->route('admin.contacorrentes.index');
    }


    /**
     * Display Contacorrente.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('contacorrente_view')) {
            return abort(401);
        }
        $contacorrente = Contacorrente::findOrFail($id);

        return view('admin.contacorrentes.show', compact('contacorrente'));
    }


    /**
     * Remove Contacorrente from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('contacorrente_delete')) {
            return abort(401);
        }
        $contacorrente = Contacorrente::findOrFail($id);
        $contacorrente->delete();

        return redirect()->route('admin.contacorrentes.index');
    }

    /**
     * Delete all selected Contacorrente at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contacorrente_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacorrente::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Contacorrente from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('contacorrente_delete')) {
            return abort(401);
        }
        $contacorrente = Contacorrente::onlyTrashed()->findOrFail($id);
        $contacorrente->restore();

        return redirect()->route('admin.contacorrentes.index');
    }

    /**
     * Permanently delete Contacorrente from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('contacorrente_delete')) {
            return abort(401);
        }
        $contacorrente = Contacorrente::onlyTrashed()->findOrFail($id);
        $contacorrente->forceDelete();

        return redirect()->route('admin.contacorrentes.index');
    }
}
