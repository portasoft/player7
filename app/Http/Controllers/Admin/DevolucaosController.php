<?php

namespace App\Http\Controllers\Admin;

use App\Devolucao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDevolucaosRequest;
use App\Http\Requests\Admin\UpdateDevolucaosRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

class DevolucaosController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Devolucao.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('devolucao_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Devolucao::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('devolucao_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'devolucaos.id',
                'devolucaos.data',
                'devolucaos.fornecedor',
                'devolucaos.motivo',
                'devolucaos.produto',
                'devolucaos.obs',
                'devolucaos.anexo',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'devolucao_';
                $routeKey = 'admin.devolucaos';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('data', function ($row) {
                return $row->data ? $row->data : '';
            });
            $table->editColumn('fornecedor', function ($row) {
                return $row->fornecedor ? $row->fornecedor : '';
            });
            $table->editColumn('motivo', function ($row) {
                return $row->motivo ? $row->motivo : '';
            });
            $table->editColumn('produto', function ($row) {
                return $row->produto ? $row->produto : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.devolucaos.index');
    }

    /**
     * Show the form for creating new Devolucao.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('devolucao_create')) {
            return abort(401);
        }
        return view('admin.devolucaos.create');
    }

    /**
     * Store a newly created Devolucao in storage.
     *
     * @param  \App\Http\Requests\StoreDevolucaosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDevolucaosRequest $request)
    {
        if (! Gate::allows('devolucao_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $devolucao = Devolucao::create($request->all());



        return redirect()->route('admin.devolucaos.index');
    }


    /**
     * Show the form for editing Devolucao.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('devolucao_edit')) {
            return abort(401);
        }
        $devolucao = Devolucao::findOrFail($id);

        return view('admin.devolucaos.edit', compact('devolucao'));
    }

    /**
     * Update Devolucao in storage.
     *
     * @param  \App\Http\Requests\UpdateDevolucaosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDevolucaosRequest $request, $id)
    {
        if (! Gate::allows('devolucao_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $devolucao = Devolucao::findOrFail($id);
        $devolucao->update($request->all());



        return redirect()->route('admin.devolucaos.index');
    }


    /**
     * Display Devolucao.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('devolucao_view')) {
            return abort(401);
        }
        $devolucao = Devolucao::findOrFail($id);

        return view('admin.devolucaos.show', compact('devolucao'));
    }


    /**
     * Remove Devolucao from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('devolucao_delete')) {
            return abort(401);
        }
        $devolucao = Devolucao::findOrFail($id);
        $devolucao->delete();

        return redirect()->route('admin.devolucaos.index');
    }

    /**
     * Delete all selected Devolucao at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('devolucao_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Devolucao::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Devolucao from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('devolucao_delete')) {
            return abort(401);
        }
        $devolucao = Devolucao::onlyTrashed()->findOrFail($id);
        $devolucao->restore();

        return redirect()->route('admin.devolucaos.index');
    }

    /**
     * Permanently delete Devolucao from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('devolucao_delete')) {
            return abort(401);
        }
        $devolucao = Devolucao::onlyTrashed()->findOrFail($id);
        $devolucao->forceDelete();

        return redirect()->route('admin.devolucaos.index');
    }
}
