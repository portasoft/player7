<?php

namespace App\Http\Controllers\Admin;

use App\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreClientesRequest;
use App\Http\Requests\Admin\UpdateClientesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;
use App\estadocivil;
class ClientesController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Cliente.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('cliente_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Cliente::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('cliente_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'clientes.id',
                'clientes.nome',
                'clientes.cpf',
                'clientes.rg',
                'clientes.celular',
                'clientes.telefone',
                'clientes.email',
                'clientes.cep',
                'clientes.endereco',
                'clientes.numero',
                'clientes.bairro',
                'clientes.cidade',
                'clientes.uf',
                'clientes.complemento',
                'clientes.estadocivil',
                'clientes.sexo',
                'clientes.datacadastro',
                'clientes.datanascimento',
                'clientes.renda',
                'clientes.limite',
                'clientes.cartao',
                'clientes.pontos',
                'clientes.debito',
                'clientes.obs',
                'clientes.tipopessoa',
                'clientes.naturalidade',
                'clientes.credito',
                'clientes.localizacao_address',
                'clientes.foto',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'cliente_';
                $routeKey = 'admin.clientes';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('nome', function ($row) {
                return $row->nome ? $row->nome : '';
            });
            $table->editColumn('cpf', function ($row) {
                return $row->cpf ? $row->cpf : '';
            });
            $table->editColumn('rg', function ($row) {
                return $row->rg ? $row->rg : '';
            });
            $table->editColumn('celular', function ($row) {
                return $row->celular ? $row->celular : '';
            });
            $table->editColumn('telefone', function ($row) {
                return $row->telefone ? $row->telefone : '';
            });
            $table->editColumn('email', function ($row) {
                return $row->email ? $row->email : '';
            });
            $table->editColumn('cep', function ($row) {
                return $row->cep ? $row->cep : '';
            });
            $table->editColumn('endereco', function ($row) {
                return $row->endereco ? $row->endereco : '';
            });
            $table->editColumn('numero', function ($row) {
                return $row->numero ? $row->numero : '';
            });
            $table->editColumn('bairro', function ($row) {
                return $row->bairro ? $row->bairro : '';
            });
            $table->editColumn('cidade', function ($row) {
                return $row->cidade ? $row->cidade : '';
            });
            $table->editColumn('uf', function ($row) {
                return $row->uf ? $row->uf : '';
            });
            $table->editColumn('complemento', function ($row) {
                return $row->complemento ? $row->complemento : '';
            });
            $table->editColumn('estadocivil', function ($row) {
                return $row->estadocivil ? $row->estadocivil : '';
            });
            $table->editColumn('sexo', function ($row) {
                return $row->sexo ? $row->sexo : '';
            });
            $table->editColumn('datacadastro', function ($row) {
                return $row->datacadastro ? $row->datacadastro : '';
            });
            $table->editColumn('datanascimento', function ($row) {
                return $row->datanascimento ? $row->datanascimento : '';
            });
            $table->editColumn('renda', function ($row) {
                return $row->renda ? $row->renda : '';
            });
            $table->editColumn('limite', function ($row) {
                return $row->limite ? $row->limite : '';
            });
            $table->editColumn('cartao', function ($row) {
                return $row->cartao ? $row->cartao : '';
            });
            $table->editColumn('pontos', function ($row) {
                return $row->pontos ? $row->pontos : '';
            });
            $table->editColumn('debito', function ($row) {
                return $row->debito ? $row->debito : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('tipopessoa', function ($row) {
                return $row->tipopessoa ? $row->tipopessoa : '';
            });
            $table->editColumn('naturalidade', function ($row) {
                return $row->naturalidade ? $row->naturalidade : '';
            });
            $table->editColumn('credito', function ($row) {
                return $row->credito ? $row->credito : '';
            });
            $table->editColumn('localizacao', function ($row) {
                return $row->localizacao ? $row->localizacao : '';
            });
            $table->editColumn('foto', function ($row) {
                if($row->foto) { return '<a href="'. asset(env('UPLOAD_PATH').'/' . $row->foto) .'" target="_blank"><img src="'. asset(env('UPLOAD_PATH').'/thumb/' . $row->foto) .'"/>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.clientes.index');
    }

    /**
     * Show the form for creating new Cliente.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('cliente_create')) {
            return abort(401);
        }
        // $estadocivil = estadocivil::pluck('estadocivil','id');
        return view('admin.clientes.create');
    }

    /**
     * Store a newly created Cliente in storage.
     *
     * @param  \App\Http\Requests\StoreClientesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClientesRequest $request)
    {
        if (! Gate::allows('cliente_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $cliente = Cliente::create($request->all());



        return redirect()->route('admin.clientes.index');
    }


    /**
     * Show the form for editing Cliente.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('cliente_edit')) {
            return abort(401);
        }
        $cliente = Cliente::findOrFail($id);
        // $estadocivil = estadocivil::pluck('estadocivil','id');
        return view('admin.clientes.edit', compact('cliente'));
    }

    /**
     * Update Cliente in storage.
     *
     * @param  \App\Http\Requests\UpdateClientesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClientesRequest $request, $id)
    {
        if (! Gate::allows('cliente_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $cliente = Cliente::findOrFail($id);
        $cliente->update($request->all());



        return redirect()->route('admin.clientes.index');
    }


    /**
     * Display Cliente.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('cliente_view')) {
            return abort(401);
        }
        $cliente = Cliente::findOrFail($id);

        return view('admin.clientes.show', compact('cliente'));
    }


    /**
     * Remove Cliente from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('cliente_delete')) {
            return abort(401);
        }
        $cliente = Cliente::findOrFail($id);
        $cliente->delete();

        return redirect()->route('admin.clientes.index');
    }

    /**
     * Delete all selected Cliente at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('cliente_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Cliente::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Cliente from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('cliente_delete')) {
            return abort(401);
        }
        $cliente = Cliente::onlyTrashed()->findOrFail($id);
        $cliente->restore();

        return redirect()->route('admin.clientes.index');
    }

    /**
     * Permanently delete Cliente from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('cliente_delete')) {
            return abort(401);
        }
        $cliente = Cliente::onlyTrashed()->findOrFail($id);
        $cliente->forceDelete();

        return redirect()->route('admin.clientes.index');
    }
}
