<?php

namespace App\Http\Controllers\Admin;

use App\Fotoexame;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreFotoexamesRequest;
use App\Http\Requests\Admin\UpdateFotoexamesRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

class FotoexamesController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Fotoexame.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('fotoexame_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Fotoexame::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('fotoexame_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'fotoexames.id',
                'fotoexames.exame',
                'fotoexames.foto',
                'fotoexames.anexo',
                'fotoexames.obs',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'fotoexame_';
                $routeKey = 'admin.fotoexames';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('exame', function ($row) {
                return $row->exame ? $row->exame : '';
            });
            $table->editColumn('foto', function ($row) {
                if($row->foto) { return '<a href="'. asset(env('UPLOAD_PATH').'/' . $row->foto) .'" target="_blank"><img src="'. asset(env('UPLOAD_PATH').'/thumb/' . $row->foto) .'"/>'; };
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });

            

            return $table->make(true);
        }

        return view('admin.fotoexames.index');
    }

    /**
     * Show the form for creating new Fotoexame.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('fotoexame_create')) {
            return abort(401);
        }
        return view('admin.fotoexames.create');
    }

    /**
     * Store a newly created Fotoexame in storage.
     *
     * @param  \App\Http\Requests\StoreFotoexamesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFotoexamesRequest $request)
    {
        if (! Gate::allows('fotoexame_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $fotoexame = Fotoexame::create($request->all());



        return redirect()->route('admin.fotoexames.index');
    }


    /**
     * Show the form for editing Fotoexame.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('fotoexame_edit')) {
            return abort(401);
        }
        $fotoexame = Fotoexame::findOrFail($id);

        return view('admin.fotoexames.edit', compact('fotoexame'));
    }

    /**
     * Update Fotoexame in storage.
     *
     * @param  \App\Http\Requests\UpdateFotoexamesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFotoexamesRequest $request, $id)
    {
        if (! Gate::allows('fotoexame_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $fotoexame = Fotoexame::findOrFail($id);
        $fotoexame->update($request->all());



        return redirect()->route('admin.fotoexames.index');
    }


    /**
     * Display Fotoexame.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('fotoexame_view')) {
            return abort(401);
        }
        $fotoexame = Fotoexame::findOrFail($id);

        return view('admin.fotoexames.show', compact('fotoexame'));
    }


    /**
     * Remove Fotoexame from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('fotoexame_delete')) {
            return abort(401);
        }
        $fotoexame = Fotoexame::findOrFail($id);
        $fotoexame->delete();

        return redirect()->route('admin.fotoexames.index');
    }

    /**
     * Delete all selected Fotoexame at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('fotoexame_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Fotoexame::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Fotoexame from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('fotoexame_delete')) {
            return abort(401);
        }
        $fotoexame = Fotoexame::onlyTrashed()->findOrFail($id);
        $fotoexame->restore();

        return redirect()->route('admin.fotoexames.index');
    }

    /**
     * Permanently delete Fotoexame from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('fotoexame_delete')) {
            return abort(401);
        }
        $fotoexame = Fotoexame::onlyTrashed()->findOrFail($id);
        $fotoexame->forceDelete();

        return redirect()->route('admin.fotoexames.index');
    }
}
