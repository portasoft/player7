<?php

namespace App\Http\Controllers\Admin;

use App\Tamanho;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreTamanhosRequest;
use App\Http\Requests\Admin\UpdateTamanhosRequest;
use Yajra\Datatables\Datatables;

class TamanhosController extends Controller
{
    /**
     * Display a listing of Tamanho.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('tamanho_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Tamanho::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('tamanho_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'tamanhos.id',
                'tamanhos.descricao',
                'tamanhos.grade',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'tamanho_';
                $routeKey = 'admin.tamanhos';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('descricao', function ($row) {
                return $row->descricao ? $row->descricao : '';
            });
            $table->editColumn('grade', function ($row) {
                return $row->grade ? $row->grade : '';
            });

            

            return $table->make(true);
        }

        return view('admin.tamanhos.index');
    }

    /**
     * Show the form for creating new Tamanho.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('tamanho_create')) {
            return abort(401);
        }
        return view('admin.tamanhos.create');
    }

    /**
     * Store a newly created Tamanho in storage.
     *
     * @param  \App\Http\Requests\StoreTamanhosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTamanhosRequest $request)
    {
        if (! Gate::allows('tamanho_create')) {
            return abort(401);
        }
        $tamanho = Tamanho::create($request->all());


        return redirect()->back()->with('success', true);
        // return redirect()->route('admin.tamanhos.index');
    }


    /**
     * Show the form for editing Tamanho.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('tamanho_edit')) {
            return abort(401);
        }
        $tamanho = Tamanho::findOrFail($id);

        return view('admin.tamanhos.edit', compact('tamanho'));
    }

    /**
     * Update Tamanho in storage.
     *
     * @param  \App\Http\Requests\UpdateTamanhosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTamanhosRequest $request, $id)
    {
        if (! Gate::allows('tamanho_edit')) {
            return abort(401);
        }
        $tamanho = Tamanho::findOrFail($id);
        $tamanho->update($request->all());


        return redirect()->back()->with('success', true);
        // return redirect()->route('admin.tamanhos.index');
    }


    /**
     * Display Tamanho.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('tamanho_view')) {
            return abort(401);
        }
        $tamanho = Tamanho::findOrFail($id);

        return view('admin.tamanhos.show', compact('tamanho'));
    }


    /**
     * Remove Tamanho from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('tamanho_delete')) {
            return abort(401);
        }
        $tamanho = Tamanho::findOrFail($id);
        $tamanho->delete();

        return redirect()->route('admin.tamanhos.index');
    }

    /**
     * Delete all selected Tamanho at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('tamanho_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Tamanho::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Tamanho from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('tamanho_delete')) {
            return abort(401);
        }
        $tamanho = Tamanho::onlyTrashed()->findOrFail($id);
        $tamanho->restore();

        return redirect()->route('admin.tamanhos.index');
    }

    /**
     * Permanently delete Tamanho from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('tamanho_delete')) {
            return abort(401);
        }
        $tamanho = Tamanho::onlyTrashed()->findOrFail($id);
        $tamanho->forceDelete();

        return redirect()->route('admin.tamanhos.index');
    }
}
