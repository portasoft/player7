<?php

namespace App\Http\Controllers\Admin;

use App\Fornecedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreFornecedorsRequest;
use App\Http\Requests\Admin\UpdateFornecedorsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

class FornecedorsController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Fornecedor.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('fornecedor_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Fornecedor::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('fornecedor_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'fornecedors.id',
                'fornecedors.cnpj',
                'fornecedors.razao',
                'fornecedors.fantasia',
                'fornecedors.contato',
                'fornecedors.datacadastro',
                'fornecedors.ie',
                'fornecedors.im',
                'fornecedors.segmento',
                'fornecedors.telefone',
                'fornecedors.celular',
                'fornecedors.email',
                'fornecedors.cep',
                'fornecedors.endereco',
                'fornecedors.numero',
                'fornecedors.bairro',
                'fornecedors.cidade',
                'fornecedors.uf',
                'fornecedors.complemento',
                'fornecedors.obs',
                'fornecedors.foto',
                'fornecedors.localizacao_address',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'fornecedor_';
                $routeKey = 'admin.fornecedors';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('cnpj', function ($row) {
                return $row->cnpj ? $row->cnpj : '';
            });
            $table->editColumn('razao', function ($row) {
                return $row->razao ? $row->razao : '';
            });
            $table->editColumn('fantasia', function ($row) {
                return $row->fantasia ? $row->fantasia : '';
            });
            $table->editColumn('contato', function ($row) {
                return $row->contato ? $row->contato : '';
            });
            $table->editColumn('datacadastro', function ($row) {
                return $row->datacadastro ? $row->datacadastro : '';
            });
            $table->editColumn('ie', function ($row) {
                return $row->ie ? $row->ie : '';
            });
            $table->editColumn('im', function ($row) {
                return $row->im ? $row->im : '';
            });
            $table->editColumn('segmento', function ($row) {
                return $row->segmento ? $row->segmento : '';
            });
            $table->editColumn('telefone', function ($row) {
                return $row->telefone ? $row->telefone : '';
            });
            $table->editColumn('celular', function ($row) {
                return $row->celular ? $row->celular : '';
            });
            $table->editColumn('email', function ($row) {
                return $row->email ? $row->email : '';
            });
            $table->editColumn('cep', function ($row) {
                return $row->cep ? $row->cep : '';
            });
            $table->editColumn('endereco', function ($row) {
                return $row->endereco ? $row->endereco : '';
            });
            $table->editColumn('numero', function ($row) {
                return $row->numero ? $row->numero : '';
            });
            $table->editColumn('bairro', function ($row) {
                return $row->bairro ? $row->bairro : '';
            });
            $table->editColumn('cidade', function ($row) {
                return $row->cidade ? $row->cidade : '';
            });
            $table->editColumn('uf', function ($row) {
                return $row->uf ? $row->uf : '';
            });
            $table->editColumn('complemento', function ($row) {
                return $row->complemento ? $row->complemento : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('foto', function ($row) {
                if($row->foto) { return '<a href="'. asset(env('UPLOAD_PATH').'/' . $row->foto) .'" target="_blank"><img src="'. asset(env('UPLOAD_PATH').'/thumb/' . $row->foto) .'"/>'; };
            });
            $table->editColumn('localizacao', function ($row) {
                return $row->localizacao ? $row->localizacao : '';
            });

            

            return $table->make(true);
        }

        return view('admin.fornecedors.index');
    }

    /**
     * Show the form for creating new Fornecedor.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('fornecedor_create')) {
            return abort(401);
        }
        return view('admin.fornecedors.create');
    }

    /**
     * Store a newly created Fornecedor in storage.
     *
     * @param  \App\Http\Requests\StoreFornecedorsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFornecedorsRequest $request)
    {
        if (! Gate::allows('fornecedor_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $fornecedor = Fornecedor::create($request->all());



        return redirect()->route('admin.fornecedors.index');
    }


    /**
     * Show the form for editing Fornecedor.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('fornecedor_edit')) {
            return abort(401);
        }
        $fornecedor = Fornecedor::findOrFail($id);

        return view('admin.fornecedors.edit', compact('fornecedor'));
    }

    /**
     * Update Fornecedor in storage.
     *
     * @param  \App\Http\Requests\UpdateFornecedorsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFornecedorsRequest $request, $id)
    {
        if (! Gate::allows('fornecedor_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $fornecedor = Fornecedor::findOrFail($id);
        $fornecedor->update($request->all());



        return redirect()->route('admin.fornecedors.index');
    }


    /**
     * Display Fornecedor.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('fornecedor_view')) {
            return abort(401);
        }
        $fornecedor = Fornecedor::findOrFail($id);

        return view('admin.fornecedors.show', compact('fornecedor'));
    }


    /**
     * Remove Fornecedor from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('fornecedor_delete')) {
            return abort(401);
        }
        $fornecedor = Fornecedor::findOrFail($id);
        $fornecedor->delete();

        return redirect()->route('admin.fornecedors.index');
    }

    /**
     * Delete all selected Fornecedor at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('fornecedor_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Fornecedor::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Fornecedor from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('fornecedor_delete')) {
            return abort(401);
        }
        $fornecedor = Fornecedor::onlyTrashed()->findOrFail($id);
        $fornecedor->restore();

        return redirect()->route('admin.fornecedors.index');
    }

    /**
     * Permanently delete Fornecedor from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('fornecedor_delete')) {
            return abort(401);
        }
        $fornecedor = Fornecedor::onlyTrashed()->findOrFail($id);
        $fornecedor->forceDelete();

        return redirect()->route('admin.fornecedors.index');
    }
}
