<?php

namespace App\Http\Controllers\Admin;
use Response;
use App\Movimento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreMovimentosRequest;
use App\Http\Requests\Admin\UpdateMovimentosRequest;
use App\Empresa;
use App\User;
use App\Fornecedor;
use App\Cor;
use App\Produto;
use App\Deposito;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;
class MovimentosController extends Controller
{
    /**
     * Display a listing of Movimento.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('movimento_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('movimento_delete')) {
                return abort(401);
            }
            $movimentos = Movimento::onlyTrashed()->get();
        } else {
            $movimentos = Movimento::all();
        }
        $cor = Cor::pluck('descricao','id');
        $fornecedor = Fornecedor::pluck('razao','id');
        $deposito = Deposito::pluck('descricao','id');
            $empresa = Empresa::pluck('fantasia','id');
            $cors = DB::table('cors')->get();
            $fornecedors = DB::table('fornecedors')->get();
            $users = DB::table('users')->get();
   
        return view('admin.movimentos.index', compact('cors','fornecedors','movimentos','empresa','deposito','cor','fornecedor','users'));
    }

    /**
     * Show the form for creating new Movimento.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('movimento_create')) {
            return abort(401);
        }
        return view('admin.movimentos.create');
    }

    /**
     * Store a newly created Movimento in storage.
     *
     * @param  \App\Http\Requests\StoreMovimentosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMovimentosRequest $request)
    {
         $produtoEscolha = Input::get('produto');
        $ProdID = DB::table('produtos')->where('descricao', 'LIKE', $produtoEscolha.'%')->where('cor','=',$request->cor)->where('codforne','=',$request->fornecedor)->first();

        $usuarioEscolha = Input::get('usuario');
        $UserID = DB::table('users')->where('name', 'LIKE', $usuarioEscolha.'%')->first();
        //  dd($UserID->id);
        $request->request->add(['produto' => $ProdID->id,'usuario'=> $UserID->id]);
     
        if (! Gate::allows('movimento_create')) {
            return abort(401);
        }
        $movimento = Movimento::create($request->all());
        // $QtdeAtual = DB::table('produtos')->where('id', 'LIKE', $ProdID->id.'%')->first();
             $id = $ProdID->id;
             $qtdenova = Input::get('qtde');
             $produto = Produto::find($id);    
             if (Input::get('tipo') ==  'Entrada') {
                $produto->estoque = $ProdID->estoque  + $qtdenova;
             } else {
                $produto->estoque = $ProdID->estoque  - $qtdenova;
             }
                
            $s = 1;
            
             $produto->save();
             return redirect()->back()->with('success', true);
            //  return redirect()->back('admin.movimentos.index')->with('message','Error');
    }


    /**
     * Show the form for editing Movimento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('movimento_edit')) {
            return abort(401);
        }
        if (request('show_deleted') == 1) {
            if (! Gate::allows('movimento_delete')) {
                return abort(401);
            }
            $movimentos = Movimento::onlyTrashed()->get();
        } else {
            $movimentos = Movimento::all();
        }
        $movimento = Movimento::findOrFail($id);
        $deposito = Deposito::pluck('descricao','id');
        $empresa = Empresa::pluck('fantasia','id');
        return view('admin.movimentos.edit', compact('movimento','deposito','empresa','movimentos'));
    }

    /**
     * Update Movimento in storage.
     *
     * @param  \App\Http\Requests\UpdateMovimentosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMovimentosRequest $request, $id)
    {
        if (! Gate::allows('movimento_edit')) {
            return abort(401);
        }
        $movimento = Movimento::findOrFail($id);
        $movimento->update($request->all());



        return redirect()->route('admin.movimentos.index');
    }


    /**
     * Display Movimento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('movimento_view')) {
            return abort(401);
        }
        $movimento = Movimento::findOrFail($id);

        return view('admin.movimentos.show', compact('movimento'));
    }


    /**
     * Remove Movimento from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('movimento_delete')) {
            return abort(401);
        }
        $movimento = Movimento::findOrFail($id);
        $movimento->delete();

        return redirect()->route('admin.movimentos.index');
    }

    /**
     * Delete all selected Movimento at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('movimento_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Movimento::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Movimento from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('movimento_delete')) {
            return abort(401);
        }
        $movimento = Movimento::onlyTrashed()->findOrFail($id);
        $movimento->restore();

        return redirect()->route('admin.movimentos.index');
    }

    /**
     * Permanently delete Movimento from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('movimento_delete')) {
            return abort(401);
        }
        $movimento = Movimento::onlyTrashed()->findOrFail($id);
        $movimento->forceDelete();

        return redirect()->route('admin.movimentos.index');
    }
    public function fetch(Request $request) {
        if ($request->get('query')) {
        $query = $request->get('query');
    
            
        $produtos = DB::table('produtos')->where('descricao', 'LIKE', $query.'%')->where('finalidade', '=', 'Kit')->get();
    
    
        $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
        foreach ($produtos as $row) {
        $output .='<li class="device_result c"><a href="#">'.$row->descricao.'</a></li>';
     
        }
        $output .= '</ul>';
        echo $output;
    
        }
    
    
        }
        public function Usuarioadiciona(Request $request) {
            if ($request->get('query')) {
            $query = $request->get('query');
            // dd($query);
        
            $user = DB::table('Users')->where('name', 'LIKE', $query.'%')->orWhere('id', 'LIKE', $query.'%')->get();
                  // dd($query);
        
            $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach ($user as $row) {
            $output .='<li class="device_result u"><a href="#">'.$row->name.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        
            }
        
        
            }
            public function altera(Request $request) {
                $produto =  new Produto();
            
                $produto->fornecedor = $emitente;
                $entrada->nf = $notaFiscal;
                $produto->update();
    $params = Request::all();
    $produto = Produto::find($id);
    $produto->update($params);

    return redirect()->action('ProdutoController@lista')->withInput(Request::only('nome'));


    }
}
