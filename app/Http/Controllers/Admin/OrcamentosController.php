<?php

namespace App\Http\Controllers\Admin;

use App\Orcamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreOrcamentosRequest;
use App\Http\Requests\Admin\UpdateOrcamentosRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

class OrcamentosController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Orcamento.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('orcamento_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Orcamento::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('orcamento_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'orcamentos.id',
                'orcamentos.cliente',
                'orcamentos.produto',
                'orcamentos.qtde',
                'orcamentos.validade',
                'orcamentos.data',
                'orcamentos.valortotal',
                'orcamentos.valorunitario',
                'orcamentos.obs',
                'orcamentos.finalizado',
                'orcamentos.anexo',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'orcamento_';
                $routeKey = 'admin.orcamentos';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('cliente', function ($row) {
                return $row->cliente ? $row->cliente : '';
            });
            $table->editColumn('produto', function ($row) {
                return $row->produto ? $row->produto : '';
            });
            $table->editColumn('qtde', function ($row) {
                return $row->qtde ? $row->qtde : '';
            });
            $table->editColumn('validade', function ($row) {
                return $row->validade ? $row->validade : '';
            });
            $table->editColumn('data', function ($row) {
                return $row->data ? $row->data : '';
            });
            $table->editColumn('valortotal', function ($row) {
                return $row->valortotal ? $row->valortotal : '';
            });
            $table->editColumn('valorunitario', function ($row) {
                return $row->valorunitario ? $row->valorunitario : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('finalizado', function ($row) {
                return $row->finalizado ? $row->finalizado : '';
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.orcamentos.index');
    }

    /**
     * Show the form for creating new Orcamento.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('orcamento_create')) {
            return abort(401);
        }
        return view('admin.orcamentos.create');
    }

    /**
     * Store a newly created Orcamento in storage.
     *
     * @param  \App\Http\Requests\StoreOrcamentosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrcamentosRequest $request)
    {
        if (! Gate::allows('orcamento_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $orcamento = Orcamento::create($request->all());



        return redirect()->route('admin.orcamentos.index');
    }


    /**
     * Show the form for editing Orcamento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('orcamento_edit')) {
            return abort(401);
        }
        $orcamento = Orcamento::findOrFail($id);

        return view('admin.orcamentos.edit', compact('orcamento'));
    }

    /**
     * Update Orcamento in storage.
     *
     * @param  \App\Http\Requests\UpdateOrcamentosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrcamentosRequest $request, $id)
    {
        if (! Gate::allows('orcamento_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $orcamento = Orcamento::findOrFail($id);
        $orcamento->update($request->all());



        return redirect()->route('admin.orcamentos.index');
    }


    /**
     * Display Orcamento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('orcamento_view')) {
            return abort(401);
        }
        $orcamento = Orcamento::findOrFail($id);

        return view('admin.orcamentos.show', compact('orcamento'));
    }


    /**
     * Remove Orcamento from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('orcamento_delete')) {
            return abort(401);
        }
        $orcamento = Orcamento::findOrFail($id);
        $orcamento->delete();

        return redirect()->route('admin.orcamentos.index');
    }

    /**
     * Delete all selected Orcamento at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('orcamento_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Orcamento::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Orcamento from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('orcamento_delete')) {
            return abort(401);
        }
        $orcamento = Orcamento::onlyTrashed()->findOrFail($id);
        $orcamento->restore();

        return redirect()->route('admin.orcamentos.index');
    }

    /**
     * Permanently delete Orcamento from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('orcamento_delete')) {
            return abort(401);
        }
        $orcamento = Orcamento::onlyTrashed()->findOrFail($id);
        $orcamento->forceDelete();

        return redirect()->route('admin.orcamentos.index');
    }
}
