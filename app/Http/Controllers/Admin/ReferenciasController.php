<?php

namespace App\Http\Controllers\Admin;

use App\Referencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreReferenciasRequest;
use App\Http\Requests\Admin\UpdateReferenciasRequest;
use Yajra\Datatables\Datatables;

class ReferenciasController extends Controller
{
    /**
     * Display a listing of Referencium.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('referencium_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Referencia::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('referencium_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'referencias.id',
                'referencias.descricao',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'referencium_';
                $routeKey = 'admin.referencias';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('descricao', function ($row) {
                return $row->descricao ? $row->descricao : '';
            });

            

            return $table->make(true);
        }

        return view('admin.referencias.index');
    }

    /**
     * Show the form for creating new Referencium.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('referencium_create')) {
            return abort(401);
        }
        return view('admin.referencias.create');
    }

    /**
     * Store a newly created Referencium in storage.
     *
     * @param  \App\Http\Requests\StoreReferenciasRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReferenciasRequest $request)
    {
        if (! Gate::allows('referencium_create')) {
            return abort(401);
        }
        $referencium = Referencia::create($request->all());



        return redirect()->route('admin.referencias.index');
    }


    /**
     * Show the form for editing Referencium.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('referencium_edit')) {
            return abort(401);
        }
        $referencia = Referencia::findOrFail($id);

        return view('admin.referencias.edit', compact('referencia'));
    }

    /**
     * Update Referencium in storage.
     *
     * @param  \App\Http\Requests\UpdateReferenciasRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReferenciasRequest $request, $id)
    {
        if (! Gate::allows('referencium_edit')) {
            return abort(401);
        }
        $referencium = Referencia::findOrFail($id);
        $referencium->update($request->all());



        return redirect()->route('admin.referencias.index');
    }


    /**
     * Display Referencium.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('referencium_view')) {
            return abort(401);
        }
        $referencia = Referencia::findOrFail($id);

        return view('admin.referencias.show', compact('referencia'));
    }


    /**
     * Remove Referencium from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('referencium_delete')) {
            return abort(401);
        }
        $referencia = Referencia::findOrFail($id);
        $referencia->delete();

        return redirect()->route('admin.referencias.index');
    }

    /**
     * Delete all selected Referencium at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('referencium_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Referencia::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Referencium from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('referencium_delete')) {
            return abort(401);
        }
        $referencium = Referencia::onlyTrashed()->findOrFail($id);
        $referencium->restore();

        return redirect()->route('admin.referencias.index');
    }

    /**
     * Permanently delete Referencium from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('referencium_delete')) {
            return abort(401);
        }
        $referencium = Referencia::onlyTrashed()->findOrFail($id);
        $referencium->forceDelete();

        return redirect()->route('admin.referencias.index');
    }
}
