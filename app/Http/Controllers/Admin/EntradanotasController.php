<?php

namespace App\Http\Controllers\Admin;

use App\Entradanota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreEntradanotasRequest;
use App\Http\Requests\Admin\UpdateEntradanotasRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;
use App\Fornecedor;
use App\Deposito;
use App\Formapagamento;
use DB;
use App\Produto;

class EntradanotasController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Entradanotum.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('entradanotum_access')) {
            return abort(401);
        }



        if (request()->ajax()) {

            $query = Entradanota::query();

            $template = 'actionsTemplate';
            if (request('show_deleted') == 1) {

                if (!Gate::allows('entradanotum_delete')) {
                    return abort(401);
                }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'entradanotas.id',
                'entradanotas.fornecedor',
                'entradanotas.dataemissao',
                'entradanotas.dataentrada',
                'entradanotas.valosprodutos',
                'entradanotas.baseicms',
                'entradanotas.valoricms',
                'entradanotas.valorsubst',
                'entradanotas.desconto',
                'entradanotas.despesas',
                'entradanotas.valornota',
                'entradanotas.deposito',
                'entradanotas.horaemissao',
                'entradanotas.valoripi',
                'entradanotas.valorfrete',
                'entradanotas.freteincluso',
                'entradanotas.tiponf',
                'entradanotas.cfop',
                'entradanotas.tipoentrada',
                'entradanotas.formapagamento',
                'entradanotas.valorservico',
                'entradanotas.serie',
                'entradanotas.centrocusto',
                'entradanotas.obs',
                'entradanotas.cnpjtransportadora',
                'entradanotas.chavenfe',
                'entradanotas.placa',
                'entradanotas.unidade',
                'entradanotas.datacadastro',
                'entradanotas.contacontabi',
                'entradanotas.xml',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey = 'entradanotum_';
                $routeKey = 'admin.entradanotas';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('fornecedor', function ($row) {
                return $row->fornecedor ? $row->fornecedor : '';
            });
            $table->editColumn('dataemissao', function ($row) {
                return $row->dataemissao ? $row->dataemissao : '';
            });
            $table->editColumn('dataentrada', function ($row) {
                return $row->dataentrada ? $row->dataentrada : '';
            });
            $table->editColumn('valosprodutos', function ($row) {
                return $row->valosprodutos ? $row->valosprodutos : '';
            });
            $table->editColumn('baseicms', function ($row) {
                return $row->baseicms ? $row->baseicms : '';
            });
            $table->editColumn('valoricms', function ($row) {
                return $row->valoricms ? $row->valoricms : '';
            });
            $table->editColumn('valorsubst', function ($row) {
                return $row->valorsubst ? $row->valorsubst : '';
            });
            $table->editColumn('desconto', function ($row) {
                return $row->desconto ? $row->desconto : '';
            });
            $table->editColumn('despesas', function ($row) {
                return $row->despesas ? $row->despesas : '';
            });
            $table->editColumn('valornota', function ($row) {
                return $row->valornota ? $row->valornota : '';
            });
            $table->editColumn('deposito', function ($row) {
                return $row->deposito ? $row->deposito : '';
            });
            $table->editColumn('horaemissao', function ($row) {
                return $row->horaemissao ? $row->horaemissao : '';
            });
            $table->editColumn('valoripi', function ($row) {
                return $row->valoripi ? $row->valoripi : '';
            });
            $table->editColumn('valorfrete', function ($row) {
                return $row->valorfrete ? $row->valorfrete : '';
            });
            $table->editColumn('freteincluso', function ($row) {
                return $row->freteincluso ? $row->freteincluso : '';
            });
            $table->editColumn('tiponf', function ($row) {
                return $row->tiponf ? $row->tiponf : '';
            });
            $table->editColumn('cfop', function ($row) {
                return $row->cfop ? $row->cfop : '';
            });
            $table->editColumn('tipoentrada', function ($row) {
                return $row->tipoentrada ? $row->tipoentrada : '';
            });
            $table->editColumn('formapagamento', function ($row) {
                return $row->formapagamento ? $row->formapagamento : '';
            });
            $table->editColumn('valorservico', function ($row) {
                return $row->valorservico ? $row->valorservico : '';
            });
            $table->editColumn('serie', function ($row) {
                return $row->serie ? $row->serie : '';
            });
            $table->editColumn('centrocusto', function ($row) {
                return $row->centrocusto ? $row->centrocusto : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('cnpjtransportadora', function ($row) {
                return $row->cnpjtransportadora ? $row->cnpjtransportadora : '';
            });
            $table->editColumn('chavenfe', function ($row) {
                return $row->chavenfe ? $row->chavenfe : '';
            });
            $table->editColumn('placa', function ($row) {
                return $row->placa ? $row->placa : '';
            });
            $table->editColumn('unidade', function ($row) {
                return $row->unidade ? $row->unidade : '';
            });
            $table->editColumn('datacadastro', function ($row) {
                return $row->datacadastro ? $row->datacadastro : '';
            });
            $table->editColumn('contacontabi', function ($row) {
                return $row->contacontabi ? $row->contacontabi : '';
            });
            $table->editColumn('xml', function ($row) {
                if ($row->xml) {
                    return '<a href="' . asset(env('UPLOAD_PATH') . '/' . $row->xml) . '" target="_blank">Download file</a>';
                };
            });



            return $table->make(true);
        }
        $deposito = Deposito::pluck('descricao', 'id');
        return view('admin.entradanotas.index', ['deposito' => $deposito]);
    }

    /**
     * Show the form for creating new Entradanotum.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('entradanotum_create')) {
            return abort(401);
        }
       
   
   
        // $formapagamento =Formapagamento::pluck('categoria','id');
        return view('admin.entradanotas.create');
    }

    /**
     * Store a newly created Entradanotum in storage.
     *
     * @param  \App\Http\Requests\StoreEntradanotasRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEntradanotasRequest $request)
    {

        if (!Gate::allows('entradanotum_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $entradanota = Entradanota::create($request->all());



        return redirect()->route('admin.entradanotas.index');
    }


    /**
     * Show the form for editing Entradanotum.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Gate::allows('entradanotum_edit')) {
            return abort(401);
        }
        $entradanota = Entradanota::findOrFail($id);

        return view('admin.entradanotas.edit', compact('Entradanota'));
    }

    /**
     * Update Entradanotum in storage.
     *
     * @param  \App\Http\Requests\UpdateEntradanotasRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEntradanotasRequest $request, $id)
    {
        if (!Gate::allows('entradanotum_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $entradanota = Entradanota::findOrFail($id);
        $entradanota->update($request->all());



        return redirect()->route('admin.entradanotas.index');
    }


    /**
     * Display Entradanotum.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        if (!Gate::allows('entradanotum_view')) {
            return abort(401);
        }
        $entradanota = Entradanota::findOrFail($id);

        return view('admin.entradanotas.show', compact('entradanota'));
    }


    /**
     * Remove Entradanotum from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Gate::allows('entradanotum_delete')) {
            return abort(401);
        }
        $entradanota = Entradanota::findOrFail($id);
        $entradanota->delete();

        return redirect()->route('admin.entradanotas.index');
    }

    /**
     * Delete all selected Entradanotum at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!Gate::allows('entradanotum_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Entradanota::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Entradanotum from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (!Gate::allows('entradanotum_delete')) {
            return abort(401);
        }
        $entradanota = Entradanota::onlyTrashed()->findOrFail($id);
        $entradanota->restore();

        return redirect()->route('admin.entradanotas.index');
    }

    /**
     * Permanently delete Entradanotum from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (!Gate::allows('entradanotum_delete')) {
            return abort(401);
        }
        $entradanota = Entradanota::onlyTrashed()->findOrFail($id);
        $entradanota->forceDelete();

        return redirect()->route('admin.entradanotas.index');
    }
    // public function lerxml(){
    //     //se o caminho esteja hospedado noutro servidor
    //    // $url = "https://www.........";

    //     //caso o caminho esteja hospedado no próprio servidor
    //     //coloque o ficheiro no caminho: 'public/assets/xml/file.xml'
    //     // $url = asset('/file.xml');
      
    //     // $data = file_get_contents($url);
      
    //     // $xml = simplexml_load_string($data);
    //     // dd($xml);
    //     $xml= simplexml_load_file('nota1.xml');
    //     // var_dump($xml);
    //   $f = $xml->infNFe->emit->CNPJ;
    //   $d= $xml->infNFe->emit->xNome;
    //      /* GRAVA O XML NO BANCO DE DADOS */
    //      $entrada = new Entradanota();
    //      $entrada->fornecedor =$f;
    //      $entrada->desconto =$d;
    //      $entrada->save();
    // }
    public function importartela(Request $request)
    {
             $ca = $request->importa;
             if ($ca != "") {
                $xml = simplexml_load_file('C:\Users\Edilson\Desktop\NOTE\\' . $ca);
                if (isset($xml)) {
                    foreach ($xml->NFe as $key => $item) {
                        if (isset($item->infNFe)) {	
                        //LENDO AS INFORMAÇÕES DE PRODUTOS NA NF-e (XML)
                            $semResultado = 0;
                            for ($i = 0; $i <= 1000; $i++) {
        
                                if (!empty($item->infNFe->det[$i]->prod->cProd)) {
                                    $semResultado = 0;
                                    $prod = $item->infNFe->det[$i]->prod->xProd;
                                    $estoq = $item->infNFe->det[$i]->prod->qCom;
                                    $un = $item->infNFe->det[$i]->prod->uCom;
                                    $unitario = $item->infNFe->det[$i]->prod->vUnCom;
                                    $ncm = $item->infNFe->det[$i]->prod->NCM;
                                    $cfop = $item->infNFe->det[$i]->prod->CFOP;
                                    $cProd = $item->infNFe->det[$i]->prod->cProd;
                                    // $user = User::where('email', $request->input('email'))->count();
                                    //  if($user > 0) { echo "There is data"; } else echo "No data"; 
                                    $notaFiscalExite = $xml->NFe->infNFe->ide->nNF;
                                    $entrada2 = Entradanota::firstOrNew(['nf' => $notaFiscalExite]);
                                            $produtoExiste = Produto::where('codforne', $cProd)->count();
                                        // $produtoExiste = DB::table('produtos')->where('codforne', 'LIKE', $cProd.'%')->get();
                                       if ($produtoExiste > 0 ) {
                                        $QtdeAtual = DB::table('produtos')->where('codforne', 'LIKE', $cProd.'%')->first();
                                        if ($entrada2->nf == $notaFiscalExite) {
                                               
                                        } else {
                                            $id = $QtdeAtual->id;
                                            $produto = Produto::find($id);
                                              
                                           $produto->descricao = $prod;
                                             $produto->estoque = $estoq  + $QtdeAtual->estoque;
                                             $produto->custo = $unitario;
                                             $produto->ncm = $ncm;
                                             $produto->cfop = $cfop;
                                             $produto->codforne = $cProd;
                                             $produto->preco = $unitario;
                                             $produto->save();
                                            }
                                           
                               }else{
                                    
                                            $produto = new Produto();
                                            $produto->descricao = $prod;
                                            $produto->estoque = $estoq;
                                            $produto->custo = $unitario;
                                            $produto->ncm = $ncm;
                                            $produto->cfop = $cfop;
                                            $produto->codforne = $cProd;
                                            $produto->preco = $unitario;
                                            $produto->save();
                                        }
                               }
                            } // FIM DO FOR
                        }
                    }
                }
                $notaFiscal = $xml->NFe->infNFe->ide->nNF;
                $serie = $xml->NFe->infNFe->ide->serie;
                $emissao = $xml->NFe->infNFe->ide->dhEmi;
                $emitente = $xml->NFe->infNFe->emit->xNome;
                $total = $xml->NFe->infNFe->pag->detPag->vPag;
                $chave = $xml->protNFe->infProt->chNFe;
                $cnpj = $xml->NFe->infNFe->emit->CNPJ;
                $fantasia = $xml->NFe->infNFe->emit->xFant;
                $endereco = $xml->NFe->infNFe->emit->enderEmit->xLgr;
                $nro = $xml->NFe->infNFe->emit->enderEmit->nro;
                $bairro = $xml->NFe->infNFe->emit->enderEmit->xBairro;
                $cidade = $xml->NFe->infNFe->emit->enderEmit->xMun;
                $cep = $xml->NFe->infNFe->emit->enderEmit->CEP;
                $fone = $xml->NFe->infNFe->emit->enderEmit->fone;
                $uf = $xml->NFe->infNFe->emit->enderEmit->UF;
                $ie = $xml->NFe->infNFe->emit->IE;
                $entrada = Entradanota::firstOrNew(['nf' => $notaFiscal]);
                $fornecedor = Fornecedor::firstOrNew(['cnpj' => $cnpj]);
                if ($entrada->nf == $notaFiscal) {
                    $s = 1;
                      //SALVA ENTRADA DE NOTA
                $entrada->fornecedor = $emitente;
                $entrada->nf = $notaFiscal;
                $entrada->save();
                //SALVA ENTRADA FORNECEDOR
                $fornecedor->cnpj = $cnpj;
                $fornecedor->razao = $emitente;
                $fornecedor->fantasia = $fantasia;
                $fornecedor->endereco = $endereco;
                $fornecedor->bairro = $bairro;
                $fornecedor->cidade = $cidade;
                $fornecedor->cep = $cep;
                $fornecedor->uf = $uf;
                $fornecedor->ie = $ie;
                $fornecedor->telefone = $fone;
                $fornecedor->numero = $nro;   
                $fornecedor->save();
                    return view('admin.entradanotas.index', ['s' => $s])->with('msgError', '');
                } else {
                    $v = $entrada->nf;
                    $s = 2;
                    //SALVA ENTRADA DE NOTA
                    $entrada->fornecedor = $emitente;
                    $entrada->nf = $notaFiscal;
                    $entrada->save();
                     //SALVA ENTRADA FORNECEDOR
                     $fornecedor->cnpj = $cnpj;
                     $fornecedor->razao = $emitente;
                     $fornecedor->fantasia = $fantasia;
                     $fornecedor->endereco = $endereco;
                     $fornecedor->endereco = $bairro;
                     $fornecedor->cidade = $cidade;
                     $fornecedor->cep = $cep;
                     $fornecedor->telefone = $fone;         
                     $fornecedor->numero = $nro;
                     $fornecedor->save();
                    $deposito = Deposito::pluck('descricao', 'id');
                    return view('admin.entradanotas.index', [
                        's' => $s,  'xml' => $xml, 'deposito' => $deposito, 'chave' => $chave, 'emitente' => $emitente, 'notaFiscal' => $notaFiscal,
                        'serie' => $serie, 'emissao' => $emissao, 'total' => $total
                    ])->with('msgError', '');
                }
             } else {
                 $s =3;
                return view('admin.entradanotas.index',compact('s'))->with('msgError','');
             }
             
      
    }
    // public function salvarVenda(Request $request) {
    //     if ($request->formaPagamento != 'nenhum' && ( ! empty($carrinho))) {
    //     $venda = new Vendatemp();
     
      
    //     $venda->dadocheque = $request->dadocheque;
    //     $venda->save();
    //     foreach ($carrinho as $c) {
    //     $vendaitem = new Vendaitem();
    //     $vendaitem->qtde = $c->qtde;
    //             }
    //             return redirect()->route('admin.sats.index')->with('msg', 'Venda Finalizada com sucesso!');
    
    //     }else {
    //             return redirect()->route('admin.sats.index')->with('msgError', 'Selecione uma forma de pagamento!');
    //         }
    //     }
    // public function lerxml(){
    //     ini_set('default_charset', 'UTF-8');
                    
    //     /* PEGA O ARQUIVO XML */
    //             if (isset($_POST['xml_nfe'])) {          
    //             if (is_uploaded_file($_FILES['upl']['tmp_name'])) {
    //               $xml = simplexml_load_file($_FILES['upl']['tmp_name']); /* Lê o arquivo XML e recebe um objeto com as informações */            
    //         }
    //     }
         
    //     /* GRAVA O XML NO BANCO DE DADOS */
    //     $x = 0;
    //     foreach ($xml as $contato){
    //         mysql_query("INSERT INTO Entradanota (fornecedor, desconto) 
    //                         VALUES 
    //                     ('$contato->fornecedor', '$contato->desconto')");
    //         if(mysql_affected_rows() != -1){
    //            $x++;
    //         }
    //     }
    //     echo "Foram importados $x registros!";
    // }
}
