<?php

namespace App\Http\Controllers\Admin;

use App\Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreServicosRequest;
use App\Http\Requests\Admin\UpdateServicosRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;
use App\Gruposervico;
class ServicosController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Servico.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('servico_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Servico::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('servico_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'servicos.id',
                'servicos.nome',
                'servicos.valor',
                'servicos.horas',
                'servicos.minutos',
                'servicos.comissao',
                'servicos.gruposervico',
                'servicos.doses',
                'servicos.periodo',
                'servicos.valobase',
                'servicos.datacarga',
                'servicos.inativo',
                'servicos.custo',
                'servicos.ncm',
                'servicos.participante',
                'servicos.servivoconsulta',
                'servicos.servivoretorno',
                'servicos.obs',
                'servicos.anexo',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'servico_';
                $routeKey = 'admin.servicos';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('nome', function ($row) {
                return $row->nome ? $row->nome : '';
            });
            $table->editColumn('valor', function ($row) {
                return $row->valor ? $row->valor : '';
            });
            $table->editColumn('horas', function ($row) {
                return $row->horas ? $row->horas : '';
            });
            $table->editColumn('minutos', function ($row) {
                return $row->minutos ? $row->minutos : '';
            });
            $table->editColumn('comissao', function ($row) {
                return $row->comissao ? $row->comissao : '';
            });
            $table->editColumn('gruposervico', function ($row) {
                return $row->gruposervico ? $row->gruposervico : '';
            });
            $table->editColumn('doses', function ($row) {
                return $row->doses ? $row->doses : '';
            });
            $table->editColumn('periodo', function ($row) {
                return $row->periodo ? $row->periodo : '';
            });
            $table->editColumn('valobase', function ($row) {
                return $row->valobase ? $row->valobase : '';
            });
            $table->editColumn('datacarga', function ($row) {
                return $row->datacarga ? $row->datacarga : '';
            });
            $table->editColumn('inativo', function ($row) {
                return $row->inativo ? $row->inativo : '';
            });
            $table->editColumn('custo', function ($row) {
                return $row->custo ? $row->custo : '';
            });
            $table->editColumn('ncm', function ($row) {
                return $row->ncm ? $row->ncm : '';
            });
            $table->editColumn('participante', function ($row) {
                return $row->participante ? $row->participante : '';
            });
            $table->editColumn('servivoconsulta', function ($row) {
                return $row->servivoconsulta ? $row->servivoconsulta : '';
            });
            $table->editColumn('servivoretorno', function ($row) {
                return $row->servivoretorno ? $row->servivoretorno : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.servicos.index');
    }

    /**
     * Show the form for creating new Servico.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('servico_create')) {
            return abort(401);
        }
        $gruposervico = Gruposervico::pluck('descricao','id');
        return view('admin.servicos.create',compact('gruposervico'));
    }

    /**
     * Store a newly created Servico in storage.
     *
     * @param  \App\Http\Requests\StoreServicosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServicosRequest $request)
    {
        if (! Gate::allows('servico_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $servico = Servico::create($request->all());



        return redirect()->route('admin.servicos.index');
    }


    /**
     * Show the form for editing Servico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('servico_edit')) {
            return abort(401);
        }
        $servico = Servico::findOrFail($id);
        $gruposervico = Gruposervico::pluck('descricao','id');
        return view('admin.servicos.edit', compact('servico','gruposervico'));
    }

    /**
     * Update Servico in storage.
     *
     * @param  \App\Http\Requests\UpdateServicosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServicosRequest $request, $id)
    {
        if (! Gate::allows('servico_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $servico = Servico::findOrFail($id);
        $servico->update($request->all());



        return redirect()->route('admin.servicos.index');
    }


    /**
     * Display Servico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('servico_view')) {
            return abort(401);
        }
        $servico = Servico::findOrFail($id);

        return view('admin.servicos.show', compact('servico'));
    }


    /**
     * Remove Servico from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('servico_delete')) {
            return abort(401);
        }
        $servico = Servico::findOrFail($id);
        $servico->delete();

        return redirect()->route('admin.servicos.index');
    }

    /**
     * Delete all selected Servico at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('servico_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Servico::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Servico from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('servico_delete')) {
            return abort(401);
        }
        $servico = Servico::onlyTrashed()->findOrFail($id);
        $servico->restore();

        return redirect()->route('admin.servicos.index');
    }

    /**
     * Permanently delete Servico from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('servico_delete')) {
            return abort(401);
        }
        $servico = Servico::onlyTrashed()->findOrFail($id);
        $servico->forceDelete();

        return redirect()->route('admin.servicos.index');
    }
}
