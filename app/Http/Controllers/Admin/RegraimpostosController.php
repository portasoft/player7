<?php

namespace App\Http\Controllers\Admin;

use App\Regraimposto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRegraimpostosRequest;
use App\Http\Requests\Admin\UpdateRegraimpostosRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;
use App\Tiporegra;
class RegraimpostosController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Regraimposto.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('regraimposto_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Regraimposto::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('regraimposto_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'regraimpostos.id',
                'regraimpostos.tiporegra',
                'regraimpostos.iva',
                'regraimpostos.uforigem',
                'regraimpostos.ufodestino',
                'regraimpostos.baseicms',
                'regraimpostos.aliquotaicms',
                'regraimpostos.baseicmsst',
                'regraimpostos.cst',
                'regraimpostos.csosn',
                'regraimpostos.taxatributaria',
                'regraimpostos.decreto',
                'regraimpostos.modalidade',
                'regraimpostos.cfop',
                'regraimpostos.classeipi',
                'regraimpostos.codigoenquadramento',
                'regraimpostos.tipocalculo',
                'regraimpostos.aliquotaipi',
                'regraimpostos.valorunidadeipi',
                'regraimpostos.stpis',
                'regraimpostos.tipocalculocofins',
                'regraimpostos.tributacaoissqn',
                'regraimpostos.ipizerado',
                'regraimpostos.stpisentrada',
                'regraimpostos.basepis',
                'regraimpostos.basecofins',
                'regraimpostos.basepisentrada',
                'regraimpostos.basecofinsentrada',
                'regraimpostos.aliquotapissentrada',
                'regraimpostos.aliquotacofinssentrada',
                'regraimpostos.stentrada',
                'regraimpostos.cfopentrada',
                'regraimpostos.csosnentrada',
                'regraimpostos.codsuframa',
                'regraimpostos.stipi',
                'regraimpostos.tipocalculoipisaida',
                'regraimpostos.aliquotaipisaida',
                'regraimpostos.valorunidadeipisaida',
                'regraimpostos.stipiisaida',
                'regraimpostos.aliquotaissqn',
                'regraimpostos.codigoservicoiss',
                'regraimpostos.cfopservico',
                'regraimpostos.aliquotainternaicms',
                'regraimpostos.coftributacaomunicipal',
                'regraimpostos.cnae',
                'regraimpostos.anexo',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'regraimposto_';
                $routeKey = 'admin.regraimpostos';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('tiporegra', function ($row) {
                return $row->tiporegra ? $row->tiporegra : '';
            });
            $table->editColumn('iva', function ($row) {
                return $row->iva ? $row->iva : '';
            });
            $table->editColumn('uforigem', function ($row) {
                return $row->uforigem ? $row->uforigem : '';
            });
            $table->editColumn('ufodestino', function ($row) {
                return $row->ufodestino ? $row->ufodestino : '';
            });
            $table->editColumn('baseicms', function ($row) {
                return $row->baseicms ? $row->baseicms : '';
            });
            $table->editColumn('aliquotaicms', function ($row) {
                return $row->aliquotaicms ? $row->aliquotaicms : '';
            });
            $table->editColumn('baseicmsst', function ($row) {
                return $row->baseicmsst ? $row->baseicmsst : '';
            });
            $table->editColumn('cst', function ($row) {
                return $row->cst ? $row->cst : '';
            });
            $table->editColumn('csosn', function ($row) {
                return $row->csosn ? $row->csosn : '';
            });
            $table->editColumn('taxatributaria', function ($row) {
                return $row->taxatributaria ? $row->taxatributaria : '';
            });
            $table->editColumn('decreto', function ($row) {
                return $row->decreto ? $row->decreto : '';
            });
            $table->editColumn('modalidade', function ($row) {
                return $row->modalidade ? $row->modalidade : '';
            });
            $table->editColumn('cfop', function ($row) {
                return $row->cfop ? $row->cfop : '';
            });
            $table->editColumn('classeipi', function ($row) {
                return $row->classeipi ? $row->classeipi : '';
            });
            $table->editColumn('codigoenquadramento', function ($row) {
                return $row->codigoenquadramento ? $row->codigoenquadramento : '';
            });
            $table->editColumn('tipocalculo', function ($row) {
                return $row->tipocalculo ? $row->tipocalculo : '';
            });
            $table->editColumn('aliquotaipi', function ($row) {
                return $row->aliquotaipi ? $row->aliquotaipi : '';
            });
            $table->editColumn('valorunidadeipi', function ($row) {
                return $row->valorunidadeipi ? $row->valorunidadeipi : '';
            });
            $table->editColumn('stpis', function ($row) {
                return $row->stpis ? $row->stpis : '';
            });
            $table->editColumn('tipocalculocofins', function ($row) {
                return $row->tipocalculocofins ? $row->tipocalculocofins : '';
            });
            $table->editColumn('tributacaoissqn', function ($row) {
                return $row->tributacaoissqn ? $row->tributacaoissqn : '';
            });
            $table->editColumn('ipizerado', function ($row) {
                return $row->ipizerado ? $row->ipizerado : '';
            });
            $table->editColumn('stpisentrada', function ($row) {
                return $row->stpisentrada ? $row->stpisentrada : '';
            });
            $table->editColumn('basepis', function ($row) {
                return $row->basepis ? $row->basepis : '';
            });
            $table->editColumn('basecofins', function ($row) {
                return $row->basecofins ? $row->basecofins : '';
            });
            $table->editColumn('basepisentrada', function ($row) {
                return $row->basepisentrada ? $row->basepisentrada : '';
            });
            $table->editColumn('basecofinsentrada', function ($row) {
                return $row->basecofinsentrada ? $row->basecofinsentrada : '';
            });
            $table->editColumn('aliquotapissentrada', function ($row) {
                return $row->aliquotapissentrada ? $row->aliquotapissentrada : '';
            });
            $table->editColumn('aliquotacofinssentrada', function ($row) {
                return $row->aliquotacofinssentrada ? $row->aliquotacofinssentrada : '';
            });
            $table->editColumn('stentrada', function ($row) {
                return $row->stentrada ? $row->stentrada : '';
            });
            $table->editColumn('cfopentrada', function ($row) {
                return $row->cfopentrada ? $row->cfopentrada : '';
            });
            $table->editColumn('csosnentrada', function ($row) {
                return $row->csosnentrada ? $row->csosnentrada : '';
            });
            $table->editColumn('codsuframa', function ($row) {
                return $row->codsuframa ? $row->codsuframa : '';
            });
            $table->editColumn('stipi', function ($row) {
                return $row->stipi ? $row->stipi : '';
            });
            $table->editColumn('tipocalculoipisaida', function ($row) {
                return $row->tipocalculoipisaida ? $row->tipocalculoipisaida : '';
            });
            $table->editColumn('aliquotaipisaida', function ($row) {
                return $row->aliquotaipisaida ? $row->aliquotaipisaida : '';
            });
            $table->editColumn('valorunidadeipisaida', function ($row) {
                return $row->valorunidadeipisaida ? $row->valorunidadeipisaida : '';
            });
            $table->editColumn('stipiisaida', function ($row) {
                return $row->stipiisaida ? $row->stipiisaida : '';
            });
            $table->editColumn('aliquotaissqn', function ($row) {
                return $row->aliquotaissqn ? $row->aliquotaissqn : '';
            });
            $table->editColumn('codigoservicoiss', function ($row) {
                return $row->codigoservicoiss ? $row->codigoservicoiss : '';
            });
            $table->editColumn('cfopservico', function ($row) {
                return $row->cfopservico ? $row->cfopservico : '';
            });
            $table->editColumn('aliquotainternaicms', function ($row) {
                return $row->aliquotainternaicms ? $row->aliquotainternaicms : '';
            });
            $table->editColumn('coftributacaomunicipal', function ($row) {
                return $row->coftributacaomunicipal ? $row->coftributacaomunicipal : '';
            });
            $table->editColumn('cnae', function ($row) {
                return $row->cnae ? $row->cnae : '';
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.regraimpostos.index');
    }

    /**
     * Show the form for creating new Regraimposto.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('regraimposto_create')) {
            return abort(401);
        }
        $tiporegra = Tiporegra::pluck('descricao','id');
        return view('admin.regraimpostos.create',compact('tiporegra'));
    }

    /**
     * Store a newly created Regraimposto in storage.
     *
     * @param  \App\Http\Requests\StoreRegraimpostosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRegraimpostosRequest $request)
    {
        if (! Gate::allows('regraimposto_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $regraimposto = Regraimposto::create($request->all());



        return redirect()->route('admin.regraimpostos.index');
    }


    /**
     * Show the form for editing Regraimposto.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('regraimposto_edit')) {
            return abort(401);
        }
        $regraimposto = Regraimposto::findOrFail($id);
        $tiporegra = Tiporegra::pluck('descricao','id');
        return view('admin.regraimpostos.edit', compact('regraimposto','tiporegra'));
    }

    /**
     * Update Regraimposto in storage.
     *
     * @param  \App\Http\Requests\UpdateRegraimpostosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRegraimpostosRequest $request, $id)
    {
        if (! Gate::allows('regraimposto_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $regraimposto = Regraimposto::findOrFail($id);
        $regraimposto->update($request->all());



        return redirect()->route('admin.regraimpostos.index');
    }


    /**
     * Display Regraimposto.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('regraimposto_view')) {
            return abort(401);
        }
        $regraimposto = Regraimposto::findOrFail($id);

        return view('admin.regraimpostos.show', compact('regraimposto'));
    }


    /**
     * Remove Regraimposto from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('regraimposto_delete')) {
            return abort(401);
        }
        $regraimposto = Regraimposto::findOrFail($id);
        $regraimposto->delete();

        return redirect()->route('admin.regraimpostos.index');
    }

    /**
     * Delete all selected Regraimposto at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('regraimposto_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Regraimposto::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Regraimposto from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('regraimposto_delete')) {
            return abort(401);
        }
        $regraimposto = Regraimposto::onlyTrashed()->findOrFail($id);
        $regraimposto->restore();

        return redirect()->route('admin.regraimpostos.index');
    }

    /**
     * Permanently delete Regraimposto from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('regraimposto_delete')) {
            return abort(401);
        }
        $regraimposto = Regraimposto::onlyTrashed()->findOrFail($id);
        $regraimposto->forceDelete();

        return redirect()->route('admin.regraimpostos.index');
    }
}
