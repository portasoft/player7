<?php

namespace App\Http\Controllers\Admin;

use App\Horariorh;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreHorariorhsRequest;
use App\Http\Requests\Admin\UpdateHorariorhsRequest;
use Yajra\Datatables\Datatables;

class HorariorhsController extends Controller
{
    /**
     * Display a listing of Horariorh.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('horariorh_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Horariorh::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('horariorh_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'horariorhs.id',
                'horariorhs.descricao',
                'horariorhs.datainicio',
                'horariorhs.datafim',
                'horariorhs.tipo',
                'horariorhs.horaentrada',
                'horariorhs.horasaida',
                'horariorhs.duracao',
                'horariorhs.tipointervalo',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'horariorh_';
                $routeKey = 'admin.horariorhs';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('descricao', function ($row) {
                return $row->descricao ? $row->descricao : '';
            });
            $table->editColumn('datainicio', function ($row) {
                return $row->datainicio ? $row->datainicio : '';
            });
            $table->editColumn('datafim', function ($row) {
                return $row->datafim ? $row->datafim : '';
            });
            $table->editColumn('tipo', function ($row) {
                return $row->tipo ? $row->tipo : '';
            });
            $table->editColumn('horaentrada', function ($row) {
                return $row->horaentrada ? $row->horaentrada : '';
            });
            $table->editColumn('horasaida', function ($row) {
                return $row->horasaida ? $row->horasaida : '';
            });
            $table->editColumn('duracao', function ($row) {
                return $row->duracao ? $row->duracao : '';
            });
            $table->editColumn('tipointervalo', function ($row) {
                return $row->tipointervalo ? $row->tipointervalo : '';
            });

            

            return $table->make(true);
        }

        return view('admin.horariorhs.index');
    }

    /**
     * Show the form for creating new Horariorh.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('horariorh_create')) {
            return abort(401);
        }
        return view('admin.horariorhs.create');
    }

    /**
     * Store a newly created Horariorh in storage.
     *
     * @param  \App\Http\Requests\StoreHorariorhsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHorariorhsRequest $request)
    {
        if (! Gate::allows('horariorh_create')) {
            return abort(401);
        }
        $horariorh = Horariorh::create($request->all());



        return redirect()->route('admin.horariorhs.index');
    }


    /**
     * Show the form for editing Horariorh.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('horariorh_edit')) {
            return abort(401);
        }
        $horariorh = Horariorh::findOrFail($id);

        return view('admin.horariorhs.edit', compact('horariorh'));
    }

    /**
     * Update Horariorh in storage.
     *
     * @param  \App\Http\Requests\UpdateHorariorhsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHorariorhsRequest $request, $id)
    {
        if (! Gate::allows('horariorh_edit')) {
            return abort(401);
        }
        $horariorh = Horariorh::findOrFail($id);
        $horariorh->update($request->all());



        return redirect()->route('admin.horariorhs.index');
    }


    /**
     * Display Horariorh.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('horariorh_view')) {
            return abort(401);
        }
        $horariorh = Horariorh::findOrFail($id);

        return view('admin.horariorhs.show', compact('horariorh'));
    }


    /**
     * Remove Horariorh from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('horariorh_delete')) {
            return abort(401);
        }
        $horariorh = Horariorh::findOrFail($id);
        $horariorh->delete();

        return redirect()->route('admin.horariorhs.index');
    }

    /**
     * Delete all selected Horariorh at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('horariorh_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Horariorh::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Horariorh from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('horariorh_delete')) {
            return abort(401);
        }
        $horariorh = Horariorh::onlyTrashed()->findOrFail($id);
        $horariorh->restore();

        return redirect()->route('admin.horariorhs.index');
    }

    /**
     * Permanently delete Horariorh from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('horariorh_delete')) {
            return abort(401);
        }
        $horariorh = Horariorh::onlyTrashed()->findOrFail($id);
        $horariorh->forceDelete();

        return redirect()->route('admin.horariorhs.index');
    }
}
