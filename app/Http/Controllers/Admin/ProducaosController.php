<?php

namespace App\Http\Controllers\Admin;

use App\Producao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProducaosRequest;
use App\Http\Requests\Admin\UpdateProducaosRequest;
use App\Deposito;
use App\Produto;
use App\Cor;
use App\Tamanho;
use App\Fornecedor;
use Illuminate\Support\Facades\DB;
class ProducaosController extends Controller
{
    /**
     * Display a listing of Producao.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Gate::allows('producao_access')) {
            return abort(401);
        }
        if (request('show_deleted') == 1) {
            if (! Gate::allows('producao_delete')) {
                return abort(401);
            }
            $producaos = Producao::onlyTrashed()->get();
        } else {
                 $producaos = DB::table('producaos')->where('deleted_at','=', NULL)->get();
         if ($producaos ->count() > 0)  {
        foreach ($producaos as $key => $value) {
                $idPro = $value->id;
        }
            $produtoids = DB::table('produtos')->where('id', '=', $idPro)->get();
            foreach ($produtoids as $key => $valueid) {
                $pid = $valueid->descricao;
            }
     }

        }
        // VAR

      
        $depositos = DB::table('depositos')->get();
        $cors = DB::table('cors')->get();
        $cors2 = DB::table('cors')->get();
        $tamanhos = DB::table('tamanhos')->get();
        $fornecedors = DB::table('fornecedors')->get();
        $produtos = DB::table('produtos')->get();

        $depositoorigem =Deposito::pluck('descricao','id');
        $depositodestino =Deposito::pluck('descricao','id');
        $fornecedor =Fornecedor::pluck('razao','id');
        $cor =Cor::pluck('descricao','id');
        $cor2 =Cor::pluck('descricao','id');
      
        $tamanho =Tamanho::pluck('descricao','id');
        $producao['data'] = date("d-m-Y");
        return view('admin.producaos.index', compact('cors','cors2','tamanhos','fornecedors','cor','cor2','tamanho','producaos','depositoorigem','depositodestino','depositos','produtos','fornecedor','producao'));
    }
    public function listproducao()
    {
     
        if (! Gate::allows('producao_access')) {
            return abort(401);
        }
        if (request('show_deleted') == 1) {
            if (! Gate::allows('producao_delete')) {
                return abort(401);
            }
            $producaos = Producao::onlyTrashed()->get();
        } else {
                 $producaos = DB::table('producaos')->where('deleted_at','=', NULL)->get();
         if ($producaos ->count() > 0)  {
        foreach ($producaos as $key => $value) {
                $idPro = $value->id;
        }
            $produtoids = DB::table('produtos')->where('id', '=', $idPro)->get();
            foreach ($produtoids as $key => $valueid) {
                $pid = $valueid->descricao;
            }
     }

        }
        $depositos = DB::table('depositos')->get();
        $produtos = DB::table('produtos')->get();

        $depositoorigem =Deposito::pluck('descricao','id');
        $depositodestino =Deposito::pluck('descricao','id');
        $fornecedor =Fornecedor::pluck('razao','id');
       
   
        
        // $custototal = DB::table('producaos')->where('lote', 'LIKE', $query . '%')->orWhere('referencia', 'LIKE', $query . '%')
        // ->orWhere('codigobarra', 'LIKE', $query . '%')->get();
        return view('admin.producaos.listproducao', compact('producaos','depositoorigem','depositodestino','depositos','produtos','fornecedor'));
    }
    /**
     * Show the form for creating new Producao.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('producao_create')) {
            return abort(401);
        }
        return view('admin.producaos.create');
    }

    /**
     * Store a newly created Producao in storage.
     *
     * @param  \App\Http\Requests\StoreProducaosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProducaosRequest $request)
    {
        if (! Gate::allows('producao_create')) {
            return abort(401);
        }
         $data = $request->all();
         $pro =  $request->produtoid;//OBTEM A DESCRICAO O PRODUTO INFORMADO NA PRODUCAO
         $produtos = DB::table('produtos')->where('descricao', '=',$pro)->first();//FAZ SELECT NOS PRODUTOS PARA OBTER OS VALORER
         
                $cust = 0;
                $cust2 = 0;
                $calculoTotal = 0;
                $calculoTotal2 = 0;
                $qtdeunitaria  = 0;
                $qtdeunitaria2  = 0;
                $custounitario = 0;
                $custounitario2 = 0;
                $qtdeinformada = 0;
                $resultMut = 0;
                $calculoCus2 =0;
                $calculoCus =0;
                $float =0;
                $esto=0;
                $custt=0;
             $maoobra =$produtos->custo; //CUSTO DE MAO DE OBRA DO PRODUTO
             $data['produtoid'] = $produtos->id;
             $idpp = $produtos->id;//ID DO PRODUTO
        $corInformada =  $request->cor;
        $corInformada2 =  $request->cor2;
        $forne  = $request->idfornecedor;
        $qtdeinformada = $request->qtde;//QTDE INFORMADO NA PRODUCAO   
  
             $estoqueCompos = DB::table('kititens')->where('produto','=',$idpp)->where('deleted_at', '=', NULL)
              ->select('kititens.*', 
          (DB::raw("(SELECT descricao FROM produtos u1 WHERE u1.id = kititens.kit ) AS kit2 ")))
          ->get();//ID DOS KITS
       
     
          foreach ($estoqueCompos as $key => $estkit) {
                   $estoquekit = $estkit->qtde;
                   $ordemKitCor = $estkit->OrdemCor;
                   $resultMut =  ( $estoquekit * $qtdeinformada);
                   if ($ordemKitCor == 1 ) {
                        $descricaoDaComposicao = DB::table('produtos')->where('finalidade','=','ITEM')
                        ->where('cor', '=', $corInformada)->where('deleted_at','=',NULL)
                        ->where('codforne','=',$forne)
                        ->where('descricao','=', $estkit->kit2)
                        ->first();//DESCRICAO DO KIT 
            
                        $float = (float)$descricaoDaComposicao->estoque;
                  
                        $custounitario  = $descricaoDaComposicao->custo;                
                        $calculoCus = $resultMut * $custounitario;
                  
                        $calculoTotal += $calculoCus ;
                    
                        if ($float < $resultMut ) {
                      
                    return redirect()->back()->with('alert', true);//ESTOQUE INSUFICIENTE
                        }
                   
                   }
                   if ($ordemKitCor == 2 ) {
                    $descricaoDaComposicao = DB::table('produtos')->where('finalidade','=','ITEM')
                    ->where('cor', '=', $corInformada2)->where('deleted_at','=',NULL)
                    ->where('codforne','=',$forne)
                    ->where('descricao','=', $estkit->kit2)
                    ->get();//DESCRICAO DO KIT 
                    foreach ($descricaoDaComposicao as $key => $valuerr) {
                       $esto = $valuerr->estoque;
                       $custt = $valuerr->custo;
                    }
                    $float = (float)$esto;
                    $custounitario2  = $custt;                
                    $calculoCus2 = $resultMut * $custounitario2;
              
                    $calculoTotal2 += $calculoCus2 ;
                 
                    if ($float < $resultMut ) {
                  
                return redirect()->back()->with('alert', true);//ESTOQUE INSUFICIENTE
                    }
               
               }
                
                }  
            
             
           
                    
       

          $custoFinale = ($calculoTotal + $calculoTotal2 ) ;
   
          $customaoobra = $maoobra * $qtdeinformada;
     
        $data['custo'] = $custoFinale +$customaoobra;
             $producao = Producao::create($data);
         
        return redirect()->route('admin.producaos.index');
       
    }


    /**
     * Show the form for editing Producao.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('producao_edit')) {
            return abort(401);
        }
                if (request('show_deleted') == 1) {
            if (! Gate::allows('producao_delete')) {
                return abort(401);
            }
            $producaos = Producao::onlyTrashed()->get();
        } else {
                 $producaos = DB::table('producaos')->where('deleted_at','=', NULL)->get();
         if ($producaos ->count() > 0)  {
        foreach ($producaos as $key => $value) {
            $idPro = $value->id;
        }
            $produtoids = DB::table('produtos')->where('id', '=', $idPro )->get();
            foreach ($produtoids as $key => $valueid) {
                $pid = $valueid->descricao;
            }
     }
        }
        $depositos = DB::table('depositos')->get();
        $cors = DB::table('cors')->get();
        $cors2 = DB::table('cors')->get();
        $tamanhos = DB::table('tamanhos')->get();
        $fornecedors = DB::table('fornecedors')->get();
        $produtos = DB::table('produtos')->get();
        $producaoP = DB::table('producaos')->where('id','=',$id)->get();
      foreach ($producaoP as $key => $value) {
         $prodEdit = $value->produtoid;
      }
       $produId = DB::table('produtos')->where('id','=',$prodEdit)->get();
       foreach ($produId as $key => $valueid) {
        $descedit = $valueid->descricao;
     }

        $producao = Producao::findOrFail($id);
      $producao['produtoid'] =$descedit;
      $producao['data'] = date("d-m-Y");
        // dd($producao);
        $depositoorigem =Deposito::pluck('descricao','id');
        $depositodestino =Deposito::pluck('descricao','id');
        $fornecedor =Fornecedor::pluck('razao','id');
        $cor =Cor::pluck('descricao','id');
        $cor2 =Cor::pluck('descricao','id');
        $tamanho =Tamanho::pluck('descricao','id');
       
        return view('admin.producaos.edit', compact('cors','cors2','tamanhos','cor2','fornecedors','cor','tamanho','producao','fornecedor','depositoorigem','depositodestino','producaos','produtos','depositos','produId'));
    }

    /**
     * Update Producao in storage.
     *
     * @param  \App\Http\Requests\UpdateProducaosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProducaosRequest $request, $id)
    {
        if (! Gate::allows('producao_edit')) {
            return abort(401);
        }
        $producao = Producao::findOrFail($id);
            $all = $request->all();
            $allP =$request->produtoid;
  
            $prodP = DB::table('produtos')->where('descricao', '=', $allP)->where('deleted_at','=', NULL)->get();
 
            foreach ($prodP as $key => $value) {
               $pP = $value->id;
            }
             $all['produtoid'] = $pP;
             $corr = $request->cor;  // COR INFORMADA
             $corr2 = $request->cor2;  // COR INFORMADA
        if ($request->situacao == 'COSTURA') { //BAIXAR SOMENTE ITENS DEPOSITO CORTE
         
         if ($producao->situacao == $request->situacao) {
             return redirect()->back()->with('alertCostura', true);//ESTOQUE INSUFICIENTE
            }else{
            
           $prod = $request->produtoid;
            $forne = $request->idfornecedor;
                $idProdBaixa = DB::table('produtos')->where('descricao', '=', $prod)->where('deleted_at','=', NULL) //PRODUTO FINAL 
            ->first();
           
                       $itenskit = DB::table('kititens')->where('produto', '=', $idProdBaixa->id)->where('deleted_at','=', NULL)->get();//PRODUTOS KIT INFORMADOS
          
                       foreach ($itenskit as $key => $value) {
                                $qtdeC = $request->qtde;
                                 $vvv= $value->qtde * $qtdeC;  
                                 $depBaixa = DB::table('depositos')->where('descricao','=','CORTE')->first();  
                                $ordemCores  = $value->OrdemCor;
                                $descricaoKit = DB::table('produtos')->where('id','=',$value->kit)->where('deleted_at', '=', NULL)->get();//DESCRICAO DO KIT 
                                 foreach ($descricaoKit as $key => $value) {
                                 $desc = $value->descricao;
                    }
                    $composicaoP2  = $request->composicao2;
                        $composicaoP  = $request->outros;
                        if ($ordemCores == 1 ) {
                            DB::table('produtos')->where('descricao','LIKE',$desc)->where('codforne','=',$forne)->where('deleted_at','=', NULL)
                            ->where('deposito',$depBaixa->id)->where('cor','=',$corr)->where('marca','=',$composicaoP)->decrement('estoque',$vvv);
                        }
                        if ($ordemCores == 2 ) {
                            DB::table('produtos')->where('descricao','LIKE',$desc)->where('codforne','=',$forne)->where('deleted_at','=', NULL)
                            ->where('deposito',$depBaixa->id)->where('cor','=',$corr2)->where('marca','=',$composicaoP2)->decrement('estoque',$vvv);
                        }
             
            
            
                }
           
             }
  
        }
        if ($request->situacao == 'CONCLUIDO') { //BAIXAR SOMENTE ITENS DEPOSITO CORTE
             if ($producao->situacao == $request->situacao) {
                 return redirect()->back()->with('alertCostura', true);//ESTOQUE INSUFICIENTE
                 }else{
                    $composicaoP2 = '';
               $prod = $request->produtoid;
                $idProdBaixa = DB::table('produtos')->where('descricao', '=', $prod)->where('deleted_at','=', NULL) //PRODUTO FINAL 
                ->first();
                              $itenskit = DB::table('kititens')->where('produto', '=', $idProdBaixa->id)->where('deleted_at','=', NULL)->get();//PRODUTOS KIT INFORMADOS
                          foreach ($itenskit as $key => $value) {
                                 $qtdeC = $request->qtde;
                                 $vvv= $value->qtde * $qtdeC;  
                                 $ordemCores2  = $value->OrdemCor;
                                 $depBaixa = DB::table('depositos')->where('descricao','=','COSTURA')->first();  
                                $corr = $request->cor;  // COR INFORMADA
                                $descricaoKit = DB::table('produtos')->where('id','=',$value->kit)->where('deleted_at', '=', NULL)->get();//DESCRICAO DO KIT 
                                
                                    foreach ($descricaoKit as $key => $value) {
                                        $desc = $value->descricao;
                                    }
                                    $forne = $request->idfornecedor;
                                    $composicaoP2  = $request->composicao2;
                                    $composicaoP  = $request->outros;
                                    if ($ordemCores2 == 1 ) {
                                        DB::table('produtos')->where('descricao','LIKE',$desc)->where('codforne','=',$forne)->where('deleted_at','=', NULL)
                                        ->where('deposito',$depBaixa->id)->where('cor','=',$corr)->where('marca','=',$composicaoP)->decrement('estoque',$vvv);
                                    }
                                    if ($ordemCores2 == 2 ) {
                                        DB::table('produtos')->where('descricao','LIKE',$desc)->where('codforne','=',$forne)->where('deleted_at','=', NULL)
                                        ->where('deposito',$depBaixa->id)->where('cor','=',$corr2)->where('marca','=',$composicaoP2)->decrement('estoque',$vvv);
                                    }
                   
                    // DB::table('produtos')->where('id',$value->kit)->where('deleted_at','=', NULL)->where('deposito',$depBaixa->id)->decrement('estoque',$vvv);
                // }
                    }
                    // dd($desc);
                 }
      
            }

            //ATUALIZAR ESTOQUE PARCIAL CORTE
            // $situac = $request->situacao;
            // if ($situac == 'CONCLUIDO') {
            //     $itensK = DB::table('kititens')
            // } else {
            //     # code...
            // }
                $producao->update($all);
        return redirect()->route('admin.producaos.index');
    }


    /**
     * Display Producao.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('producao_view')) {
            return abort(401);
        }
        $producao = Producao::findOrFail($id);

        $vDepositoO =  DB::table('depositos')->where('id', '=', $producao->depositoorigem)->get();

        foreach ($vDepositoO as $key => $valuedep) {
            $depori = $valuedep->descricao;
        }
        $vDepositoD =  DB::table('depositos')->where('id', '=', $producao->depositodestino)->get();

        foreach ($vDepositoD as $key => $valuedest) {
            $depdest = $valuedest->descricao;
        }
        //TAMANHO DESCRICAO
        $vTamanho =  DB::table('tamanhos')->where('id', '=', $producao->tamanho)->get();

        foreach ($vTamanho as $key => $valuetam) {
            $deptam = $valuetam->descricao;
        }
//FIM TAMANHO DESCRICAO
 //COR DESCRICAO
 $vCor =  DB::table('cors')->where('id', '=', $producao->cor)->get();

 foreach ($vCor as $key => $valuecor) {
     $depcor = $valuecor->descricao;
   
 }
 $vCor2 =  DB::table('cors')->where('id', '=', $producao->cor2)->get();
 $depcor2 = '';
 foreach ($vCor2 as $key => $valuecor2) {
   
     $depcor2 = $valuecor2->descricao;
 }
 
//FIM COR DESCRICAO
//COR DESCRICAO
$vForne =  DB::table('fornecedors')->where('id', '=', $producao->idfornecedor)->get();

foreach ($vForne as $key => $valuefor) {
    $depfor = $valuefor->razao;
//FIM COR DESCRICAO
           $vP = $producao['produtoid'];
           $Vprodutos =  DB::table('produtos')->where('id', '=', $vP)->get();

        foreach ($Vprodutos as $key => $value) {
            $VproP = $value->descricao;
        }

        $producao['data'] = date("d-m-Y");
        $producao['produtoid'] = $VproP;
        $producao['depositoorigem'] = $depori;
        $producao['depositodestino'] = $depdest;
        $producao['tamanho'] = $deptam;
        $producao['cor'] = $depcor;
        $producao['cor2'] = $depcor2;
        $producao['idfornecedor'] = $depfor;

   
        return view('admin.producaos.show', compact('producao'));
    }

    }
    /**
     * Remove Producao from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('producao_delete')) {
            return abort(401);
        }
        $producao = Producao::findOrFail($id);
        $producao->delete();

        return redirect()->route('admin.producaos.index');
    }

    /**
     * Delete all selected Producao at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('producao_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Producao::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Producao from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
       
        if (! Gate::allows('producao_delete')) {
            return abort(401);
        }
        $producao = Producao::onlyTrashed()->findOrFail($id);
        $producao->restore();

        return redirect()->route('admin.producaos.index');
    }

    /**
     * Permanently delete Producao from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
      
        if (! Gate::allows('producao_delete')) {
            return abort(401);
        }
        $producao = Producao::onlyTrashed()->findOrFail($id);
        $producao->forceDelete();

        return redirect()->route('admin.producaos.index');
    }
    public function fetchProducao(Request $request)
    {

        if ($request->get('query')) {
            $query = $request->get('query');

            $produtos = DB::table('produtos')->where('descricao', 'LIKE', $query . '%')->where('deleted_at','=', NULL)->Where('finalidade', '=','PRODUTO')->get();

            $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';

            foreach ($produtos as $row) {
                $output .= '<li class="device_result c"><a href="#">' . $row->descricao . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;

        }


    }
    //  public function importartelaP(Request $request)
    // {
    //     $DesProd = $request->idProdutoProducao;
    //     $produtos = DB::table('produtos')->where('descricao', 'LIKE', $DesProd.'%')->first();
    //                     $custo = $produtos->custo;
    //                     dd('kk');

    //                 return view('admin.producaos.index', ['s' => $s])->with('msgError', '');
    //             }




}
