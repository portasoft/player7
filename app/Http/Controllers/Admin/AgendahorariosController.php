<?php

namespace App\Http\Controllers\Admin;

use App\Agendahorario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAgendahorariosRequest;
use App\Http\Requests\Admin\UpdateAgendahorariosRequest;
use Yajra\Datatables\Datatables;

class AgendahorariosController extends Controller
{
    /**
     * Display a listing of Agendahorario.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('agendahorario_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Agendahorario::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('agendahorario_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'agendahorarios.id',
                'agendahorarios.data',
                'agendahorarios.hora',
                'agendahorarios.descricao',
                'agendahorarios.motivo',
                'agendahorarios.obs',
                'agendahorarios.retorno',
                'agendahorarios.situacao',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'agendahorario_';
                $routeKey = 'admin.agendahorarios';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('data', function ($row) {
                return $row->data ? $row->data : '';
            });
            $table->editColumn('hora', function ($row) {
                return $row->hora ? $row->hora : '';
            });
            $table->editColumn('descricao', function ($row) {
                return $row->descricao ? $row->descricao : '';
            });
            $table->editColumn('motivo', function ($row) {
                return $row->motivo ? $row->motivo : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('retorno', function ($row) {
                return $row->retorno ? $row->retorno : '';
            });
            $table->editColumn('situacao', function ($row) {
                return $row->situacao ? $row->situacao : '';
            });

            

            return $table->make(true);
        }

        return view('admin.agendahorarios.index');
    }

    /**
     * Show the form for creating new Agendahorario.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('agendahorario_create')) {
            return abort(401);
        }
        return view('admin.agendahorarios.create');
    }

    /**
     * Store a newly created Agendahorario in storage.
     *
     * @param  \App\Http\Requests\StoreAgendahorariosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgendahorariosRequest $request)
    {
        if (! Gate::allows('agendahorario_create')) {
            return abort(401);
        }
        $agendahorario = Agendahorario::create($request->all());


        return redirect()->back()->with('success', true);
        // return redirect()->route('admin.agendahorarios.index');
    }


    /**
     * Show the form for editing Agendahorario.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('agendahorario_edit')) {
            return abort(401);
        }
        $agendahorario = Agendahorario::findOrFail($id);

        return view('admin.agendahorarios.edit', compact('agendahorario'));
    }

    /**
     * Update Agendahorario in storage.
     *
     * @param  \App\Http\Requests\UpdateAgendahorariosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAgendahorariosRequest $request, $id)
    {
        if (! Gate::allows('agendahorario_edit')) {
            return abort(401);
        }
        $agendahorario = Agendahorario::findOrFail($id);
        $agendahorario->update($request->all());

  // return redirect()->back()->with('success', true);
  return redirect()->route('admin.agendahorarios.index')->with('success', true);
        // return redirect()->back()->with('success', true);
        // return redirect()->route('admin.agendahorarios.index');
    }


    /**
     * Display Agendahorario.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('agendahorario_view')) {
            return abort(401);
        }
        $agendahorario = Agendahorario::findOrFail($id);

        return view('admin.agendahorarios.show', compact('agendahorario'));
    }


    /**
     * Remove Agendahorario from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('agendahorario_delete')) {
            return abort(401);
        }
        $agendahorario = Agendahorario::findOrFail($id);
        $agendahorario->delete();

        return redirect()->route('admin.agendahorarios.index');
    }

    /**
     * Delete all selected Agendahorario at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('agendahorario_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Agendahorario::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Agendahorario from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('agendahorario_delete')) {
            return abort(401);
        }
        $agendahorario = Agendahorario::onlyTrashed()->findOrFail($id);
        $agendahorario->restore();

        return redirect()->route('admin.agendahorarios.index');
    }

    /**
     * Permanently delete Agendahorario from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('agendahorario_delete')) {
            return abort(401);
        }
        $agendahorario = Agendahorario::onlyTrashed()->findOrFail($id);
        $agendahorario->forceDelete();

        return redirect()->route('admin.agendahorarios.index');
    }
}
