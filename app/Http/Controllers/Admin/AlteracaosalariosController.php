<?php

namespace App\Http\Controllers\Admin;

use App\Alteracaosalario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAlteracaosalariosRequest;
use App\Http\Requests\Admin\UpdateAlteracaosalariosRequest;
use Yajra\Datatables\Datatables;
use App\User;
class AlteracaosalariosController extends Controller
{
    /**
     * Display a listing of Alteracaosalario.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('alteracaosalario_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Alteracaosalario::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('alteracaosalario_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'alteracaosalarios.id',
                'alteracaosalarios.colaborador',
                'alteracaosalarios.data',
                'alteracaosalarios.motivo',
                'alteracaosalarios.valor',
                'alteracaosalarios.obs',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'alteracaosalario_';
                $routeKey = 'admin.alteracaosalarios';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('colaborador', function ($row) {
                return $row->colaborador ? $row->colaborador : '';
            });
            $table->editColumn('data', function ($row) {
                return $row->data ? $row->data : '';
            });
            $table->editColumn('motivo', function ($row) {
                return $row->motivo ? $row->motivo : '';
            });
            $table->editColumn('valor', function ($row) {
                return $row->valor ? $row->valor : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });

            

            return $table->make(true);
        }

        return view('admin.alteracaosalarios.index');
    }

    /**
     * Show the form for creating new Alteracaosalario.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('alteracaosalario_create')) {
            return abort(401);
        }
        $usuario = User::pluck('nomecompleto','id');
        return view('admin.alteracaosalarios.create',compact('usuario'));
    }

    /**
     * Store a newly created Alteracaosalario in storage.
     *
     * @param  \App\Http\Requests\StoreAlteracaosalariosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAlteracaosalariosRequest $request)
    {
        if (! Gate::allows('alteracaosalario_create')) {
            return abort(401);
        }
        $alteracaosalario = Alteracaosalario::create($request->all());



        return redirect()->route('admin.alteracaosalarios.index');
    }


    /**
     * Show the form for editing Alteracaosalario.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('alteracaosalario_edit')) {
            return abort(401);
        }
        $alteracaosalario = Alteracaosalario::findOrFail($id);
        $usuario = User::pluck('nomecompleto','id');
        return view('admin.alteracaosalarios.edit', compact('alteracaosalario','usuario'));
    }

    /**
     * Update Alteracaosalario in storage.
     *
     * @param  \App\Http\Requests\UpdateAlteracaosalariosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAlteracaosalariosRequest $request, $id)
    {
        if (! Gate::allows('alteracaosalario_edit')) {
            return abort(401);
        }
        $alteracaosalario = Alteracaosalario::findOrFail($id);
        $alteracaosalario->update($request->all());



        return redirect()->route('admin.alteracaosalarios.index');
    }


    /**
     * Display Alteracaosalario.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('alteracaosalario_view')) {
            return abort(401);
        }
        $alteracaosalario = Alteracaosalario::findOrFail($id);

        return view('admin.alteracaosalarios.show', compact('alteracaosalario'));
    }


    /**
     * Remove Alteracaosalario from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('alteracaosalario_delete')) {
            return abort(401);
        }
        $alteracaosalario = Alteracaosalario::findOrFail($id);
        $alteracaosalario->delete();

        return redirect()->route('admin.alteracaosalarios.index');
    }

    /**
     * Delete all selected Alteracaosalario at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('alteracaosalario_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Alteracaosalario::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Alteracaosalario from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('alteracaosalario_delete')) {
            return abort(401);
        }
        $alteracaosalario = Alteracaosalario::onlyTrashed()->findOrFail($id);
        $alteracaosalario->restore();

        return redirect()->route('admin.alteracaosalarios.index');
    }

    /**
     * Permanently delete Alteracaosalario from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('alteracaosalario_delete')) {
            return abort(401);
        }
        $alteracaosalario = Alteracaosalario::onlyTrashed()->findOrFail($id);
        $alteracaosalario->forceDelete();

        return redirect()->route('admin.alteracaosalarios.index');
    }
}
