<?php

namespace App\Http\Controllers\Admin;

use App\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProdutosRequest;
use App\Http\Requests\Admin\UpdateProdutosRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

use App\Deposito;
use App\Cor;
use App\Tamanho;
use App\Fornecedor;
use App\Referencia;
use App\Unidade;
use Illuminate\Support\Facades\DB;
use JavaScript;
use DateTime;
class ProdutosController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Produto.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (! Gate::allows('produto_access')) {
            return abort(401);
        }

    //     $query = DB::table('produtos')->where('deleted_at', '=', NULL)
    //     ->select('produtos.*', 
    //     (DB::raw("(SELECT descricao FROM cors u1 WHERE u1.id = produtos.cor ) AS cor ")),
    //     (DB::raw("(SELECT descricao FROM tamanhos u1 WHERE u1.id = produtos.tamanho ) AS tamanho ")),
    //     (DB::raw("(SELECT razao FROM fornecedors u1 WHERE u1.id = produtos.codforne ) AS codforne ")),
    //     (DB::raw("(SELECT descricao FROM depositos u1 WHERE u1.id = produtos.deposito ) AS deposito")))

    //     ->get();
    // dd($query);
    $query = DB::table('produtos')->where('deleted_at', '=', NULL)
    ->select('produtos.*', 
    (DB::raw("(SELECT descricao FROM cors u1 WHERE u1.id = produtos.cor ) AS cor ")),
    (DB::raw("(SELECT descricao FROM tamanhos u1 WHERE u1.id = produtos.tamanho ) AS tamanho ")),
    (DB::raw("(SELECT razao FROM fornecedors u2 WHERE u2.id = produtos.codforne ) AS codforne ")),
    (DB::raw("(SELECT descricao FROM depositos u1 WHERE u1.id = produtos.deposito ) AS deposito")))

    ->get();
  
        if (request()->ajax()) {
     
            $query = DB::table('produtos')->where('deleted_at', '=', NULL)
            ->select('produtos.*', 
            (DB::raw("(SELECT descricao FROM cors u1 WHERE u1.id = produtos.cor ) AS cor ")),
            (DB::raw("(SELECT descricao FROM tamanhos u1 WHERE u1.id = produtos.tamanho ) AS tamanho ")),
            (DB::raw("(SELECT razao FROM fornecedors u2 WHERE u2.id = produtos.codforne ) AS codforne ")),
            (DB::raw("(SELECT descricao FROM depositos u1 WHERE u1.id = produtos.deposito ) AS deposito")))
    
            ->get();
          
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('produto_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            // dd($query);
          
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'produto_';
                $routeKey = 'admin.produtos';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('codigobarra', function ($row) {
                return $row->codigobarra ? $row->codigobarra : '';
            });
            $table->editColumn('descricao', function ($row) {
                return $row->descricao ? $row->descricao : '';
            });
            // $table->editColumn('grupo', function ($row) {
            //     return $row->grupo ? $row->grupo : '';
            // });
            $table->editColumn('marca', function ($row) {
                return $row->marca ? $row->marca : '';
            });
            $table->editColumn('finalidade', function ($row) {
                return $row->finalidade ? $row->finalidade : '';
            });
            $table->editColumn('referencia', function ($row) {
                return $row->referencia ? $row->referencia : '';
            });
            $table->editColumn('cor', function ($row) {
                return $row->cor ? $row->cor : '';
            });
            $table->editColumn('tamanho', function ($row) {
                return $row->tamanho ? $row->tamanho : '';
            });
            $table->editColumn('regraimposto', function ($row) {
                return $row->regraimposto ? $row->regraimposto : '';
            });
            $table->editColumn('tiporegra', function ($row) {
                return $row->tiporegra ? $row->tiporegra : '';
            });
            $table->editColumn('estoque', function ($row) {
                return $row->estoque ? $row->estoque : '';
            });
            $table->editColumn('estoquemin', function ($row) {
                return $row->estoquemin ? $row->estoquemin : '';
            });
            $table->editColumn('custo', function ($row) {
                return $row->custo ? $row->custo : '';
            });
            $table->editColumn('codforne', function ($row) {
                return $row->codforne ? $row->codforne : '';
            });
            $table->editColumn('deposito', function ($row) {
                return $row->deposito ? $row->deposito : '';
            });
            // $table->editColumn('preco', function ($row) {
            //     return $row->preco ? $row->preco : '';
            // });
            // $table->editColumn('margem', function ($row) {
            //     return $row->margem ? $row->margem : '';
            // });
            $table->editColumn('datacadastro', function ($row) {
                return $row->datacadastro ? $row->datacadastro : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('foto', function ($row) {
                if($row->foto) { return '<a href="'. asset(env('UPLOAD_PATH').'/' . $row->foto) .'" target="_blank"><img src="'. asset(env('UPLOAD_PATH').'/thumb/' . $row->foto) .'"/>'; };
           
            });
          
            $table->editColumn('ncm', function ($row) {
                return $row->ncm ? $row->ncm : '';
            });
            $table->editColumn('kit', function ($row) {
                return $row->kit ? $row->kit : '';
            });
            $table->editColumn('deposito', function ($row) {
                return $row->deposito ? $row->deposito : '';
            });


            

            return $table->make(true);
        }
   
     
     
        // JavaScript::put([
        //     'foo' => 'bar',
       
          
        // ]);
  
        return view('admin.produtos.index');
    }

    /**
     * Show the form for creating new Produto.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('produto_create')) {
            return abort(401);
        }
        $unidade =Unidade::pluck('descricao','descricao');
        $deposito =Deposito::pluck('descricao','descricao');
 
    
        $cor =cor::pluck('descricao','descricao');
        $forne =Fornecedor::pluck('razao','razao');
        $referencia =Referencia::pluck('descricao','descricao');
        $tamanho =tamanho::pluck('descricao','descricao');

  
        return view('admin.produtos.create',compact('cor','referencia','tamanho','deposito','unidade','forne'));
    }

    /**
     * Store a newly created Produto in storage.
     *
     * @param  \App\Http\Requests\StoreProdutosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProdutosRequest $request)
    {
        // $fcor = $request->cor;

        if (! Gate::allows('produto_create')) {
            return abort(401);
        }

        $fina = $request->finalidade;
        $produtoInf = $request->descricao;
            if ($fina == "ITEM") {
               $Criakit = DB::table('produtos')->where('finalidade','=','KIT')->where('descricao','=',$produtoInf)->get();
               if (!$Criakit->count() > 0){
                // dd('nao tem');
                $produto = new Produto();
                $produto->descricao =  $produtoInf;
                $produto->finalidade =  'KIT';
                $produto->save();
            }

            }
     
        if ($fina == "KIT")  {
            $produtosVer = DB::table('produtos')->where('descricao','=',$produtoInf)->where('finalidade','=','KIT')->get();
            if ($produtosVer->count() > 0){
                return redirect()->back()->with('success2', true);
            }
           
        }
    
      
            $request = $this->saveFiles($request);
            $data = $request->all();
            $vdata = $request->datacadastro;
            if ($vdata === null) {
                $data['datacadastro'] = date("Y-m-d");
            } 
            $corP = $request->cor;
            $tamanhoP = $request->tamanho;
            $depositoP = $request->deposito;
            $fornecedorP = $request->codforne;
            $corDesc = DB::table('cors')->where('descricao','=',$corP)->get();
            foreach ($corDesc as $key => $valuecor) {
                $data['cor'] = $valuecor->id;
            }
            $tamDesc = DB::table('tamanhos')->where('descricao','=',$tamanhoP)->get();
            foreach ($tamDesc as $key => $valuetam) {
                $data['tamanho'] = $valuetam->id;
            }
            $depDesc = DB::table('depositos')->where('descricao','=',$depositoP)->get();
    
            foreach ($depDesc as $key => $valuedep) {
                $data['deposito'] = $valuedep->id;
         
            }
            $forDesc = DB::table('fornecedors')->where('razao','=',$fornecedorP)->get();
            foreach ($forDesc as $key => $valuefor) {
                $data['codforne'] = $valuefor->id;
            }
            if ($request->custo == "" || $request->custo == "0" ) {
                $data['custo'] = "0.000";
            }
                // dd($data);
            $produto = Produto::create($data);
    
    
             return redirect()->back()->with('success', true);
            // return redirect()->route('admin.produtos.index')->with('success', true);
     

      
    }


    /**
     * Show the form for editing Produto.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
       
       
        if (! Gate::allows('produto_edit')) {
            return abort(401);
        }
        $fina = DB::table('produtos')->where('id','=',$id)->first();

        if ($fina->finalidade !== 'KIT') {
            $unidade =Unidade::pluck('descricao','id');
            $deposito =Deposito::pluck('descricao','id');
            $produto = Produto::findOrFail($id);
            $cor =cor::pluck('descricao','id');
            $referencia =Referencia::pluck('descricao','id');
            $tamanho =tamanho::pluck('descricao','id');
            $forne =Fornecedor::pluck('razao','id');
            return view('admin.produtos.edit', compact('deposito','forne','produto','cor','referencia','tamanho','unidade'));
        } else {
            return view('admin.produtos.index');
        }       
       
    }

    /**
     * Update Produto in storage.
     *
     * @param  \App\Http\Requests\UpdateProdutosRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProdutosRequest $request, $id)
    {
        if (! Gate::allows('produto_edit')) {
            return abort(401);
        }
        $fina = $request->finalidade;
        $produtoInf = $request->descricao;
  
     
        // if ($fina == "KIT")  {
        //     $produtosVer = DB::table('produtos')->where('descricao','=',$produtoInf)->where('finalidade','=','KIT')->get();
        //     if ($produtosVer->count() > 0){
        //         return redirect()->back()->with('success2', true);
        //     }
           
        // }
        $request = $this->saveFiles($request);
        $produto = Produto::findOrFail($id);
     

            // $all = $request->all();
            // $allCor =$request->cor;
            // $allTam =$request->tamanho;
            // $allDep =$request->deposito;
            // $allForne =$request->codforne;
      
           
            // $CorUp = DB::table('cors')->where('descricao', 'LIKE', $allCor . '%')->get();
            // foreach ($CorUp as $key => $value) {
            //    $pCorUpid = $value->id;
            // }
          
            // $pTamUp= DB::table('tamanhos')->where('descricao', 'LIKE', $allTam . '%')->get();
            // foreach ($pTamUp as $key => $value) {
            //    $ptamId = $value->id;
            // }
            // $pDepUp= DB::table('depositos')->where('descricao', 'LIKE', $allDep . '%')->get();
            // foreach ($pDepUp as $key => $value) {
            //    $pDepId= $value->id;
            // }
            // $pForUp= DB::table('fornecedors')->where('razao', 'LIKE', $allForne . '%')->get();
            // foreach ($pForUp as $key => $value) {
            //    $pForId= $value->id;
            // }

            // $all['cor'] = $pCorUpid;
            // $all['tamanho'] = $ptamId; 
            // $all['deposito'] = $pDepId; 
            // $all['codforne'] = $pForId; 
        $produto->update($request->all());
        return redirect()->route('admin.produtos.index')->with('success', true);
        // return redirect()->back()->with('success', true);

        // return redirect()->route('admin.produtos.index');
    }


    /**
     * Display Produto.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('produto_view')) {
            return abort(401);
        }
        $produto = Produto::findOrFail($id);
 
        $vCor = $produto['cor'];
        $vT = $produto['tamanho'];
        $vD = $produto['deposito'];
        $vForne = $produto['codforne'];
      
        $Vcores =  DB::table('cors')->where('id', '=', $vCor)->get();
        foreach ($Vcores as $key => $value) {
            $VCorDes = $value->descricao;
        }
        $Vtamanho =  DB::table('tamanhos')->where('id', '=', $vT)->get();
        foreach ($Vtamanho as $key => $valuek) {
            $VTamDes = $valuek->descricao;
        }
        $Vdeposito =  DB::table('depositos')->where('id', '=', $vD)->get();
        foreach ($Vdeposito as $key => $valuek) {
            $VTDepDesc = $valuek->descricao;
        }
        $VForne =  DB::table('fornecedors')->where('id', '=', $vForne)->get();
        foreach ($VForne as $key => $valuek) {
            $VforneDesc = $valuek->razao;
        }
    
        if ($Vcores ->count() > 0)  {
            $produto['cor'] = $VCorDes;
} 
if ($Vtamanho ->count() > 0)  {
    $produto['tamanho'] = $VTamDes;
} 

       
if ($VForne ->count() > 0)  {
    $produto['codforne'] = $VforneDesc;
} 
if ($Vdeposito ->count() > 0)  {
    $produto['deposito'] = $VTDepDesc;
} 
        
   
      

        return view('admin.produtos.show', compact('produto'));
    }


    /**
     * Remove Produto from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('produto_delete')) {
            return abort(401);
        }
        $produto = Produto::findOrFail($id);
        $produto->delete();

        return redirect()->route('admin.produtos.index');
    }

    /**
     * Delete all selected Produto at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('produto_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Produto::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Produto from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('produto_delete')) {
            return abort(401);
        }
        $produto = Produto::onlyTrashed()->findOrFail($id);
        $produto->restore();

        return redirect()->route('admin.produtos.index');
    }

    /**
     * Permanently delete Produto from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('produto_delete')) {
            return abort(401);
        }
        $produto = Produto::onlyTrashed()->findOrFail($id);
        $produto->forceDelete();

        return redirect()->route('admin.produtos.index');
    }
}
