<?php

namespace App\Http\Controllers\Admin;

use App\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreEmpresasRequest;
use App\Http\Requests\Admin\UpdateEmpresasRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;

class EmpresasController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Empresa.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('empresa_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Empresa::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('empresa_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'empresas.id',
                'empresas.cnpj',
                'empresas.razao',
                'empresas.datacadastro',
                'empresas.fantasia',
                'empresas.ie',
                'empresas.im',
                'empresas.contato',
                'empresas.regime',
                'empresas.fone',
                'empresas.celular',
                'empresas.email',
                'empresas.cep',
                'empresas.endereco',
                'empresas.numero',
                'empresas.bairro',
                'empresas.cidade',
                'empresas.uf',
                'empresas.complemento',
                'empresas.foto',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'empresa_';
                $routeKey = 'admin.empresas';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('cnpj', function ($row) {
                return $row->cnpj ? $row->cnpj : '';
            });
            $table->editColumn('razao', function ($row) {
                return $row->razao ? $row->razao : '';
            });
            $table->editColumn('datacadastro', function ($row) {
                return $row->datacadastro ? $row->datacadastro : '';
            });
            $table->editColumn('fantasia', function ($row) {
                return $row->fantasia ? $row->fantasia : '';
            });
            $table->editColumn('ie', function ($row) {
                return $row->ie ? $row->ie : '';
            });
            $table->editColumn('im', function ($row) {
                return $row->im ? $row->im : '';
            });
            $table->editColumn('contato', function ($row) {
                return $row->contato ? $row->contato : '';
            });
            $table->editColumn('regime', function ($row) {
                return $row->regime ? $row->regime : '';
            });
            $table->editColumn('fone', function ($row) {
                return $row->fone ? $row->fone : '';
            });
            $table->editColumn('celular', function ($row) {
                return $row->celular ? $row->celular : '';
            });
            $table->editColumn('email', function ($row) {
                return $row->email ? $row->email : '';
            });
            $table->editColumn('cep', function ($row) {
                return $row->cep ? $row->cep : '';
            });
            $table->editColumn('endereco', function ($row) {
                return $row->endereco ? $row->endereco : '';
            });
            $table->editColumn('numero', function ($row) {
                return $row->numero ? $row->numero : '';
            });
            $table->editColumn('bairro', function ($row) {
                return $row->bairro ? $row->bairro : '';
            });
            $table->editColumn('cidade', function ($row) {
                return $row->cidade ? $row->cidade : '';
            });
            $table->editColumn('uf', function ($row) {
                return $row->uf ? $row->uf : '';
            });
            $table->editColumn('complemento', function ($row) {
                return $row->complemento ? $row->complemento : '';
            });
            $table->editColumn('foto', function ($row) {
                if($row->foto) { return '<a href="'. asset(env('UPLOAD_PATH').'/' . $row->foto) .'" target="_blank"><img src="'. asset(env('UPLOAD_PATH').'/thumb/' . $row->foto) .'"/>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.empresas.index');
    }

    /**
     * Show the form for creating new Empresa.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('empresa_create')) {
            return abort(401);
        }
        return view('admin.empresas.create');
    }

    /**
     * Store a newly created Empresa in storage.
     *
     * @param  \App\Http\Requests\StoreEmpresasRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmpresasRequest $request)
    {
        if (! Gate::allows('empresa_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $empresa = Empresa::create($request->all());



        return redirect()->route('admin.empresas.index');
    }


    /**
     * Show the form for editing Empresa.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('empresa_edit')) {
            return abort(401);
        }
        $empresa = Empresa::findOrFail($id);

        return view('admin.empresas.edit', compact('empresa'));
    }

    /**
     * Update Empresa in storage.
     *
     * @param  \App\Http\Requests\UpdateEmpresasRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmpresasRequest $request, $id)
    {
        if (! Gate::allows('empresa_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $empresa = Empresa::findOrFail($id);
        $empresa->update($request->all());



        return redirect()->route('admin.empresas.index');
    }


    /**
     * Display Empresa.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('empresa_view')) {
            return abort(401);
        }
        $empresa = Empresa::findOrFail($id);

        return view('admin.empresas.show', compact('empresa'));
    }


    /**
     * Remove Empresa from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('empresa_delete')) {
            return abort(401);
        }
        $empresa = Empresa::findOrFail($id);
        $empresa->delete();

        return redirect()->route('admin.empresas.index');
    }

    /**
     * Delete all selected Empresa at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('empresa_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Empresa::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Empresa from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('empresa_delete')) {
            return abort(401);
        }
        $empresa = Empresa::onlyTrashed()->findOrFail($id);
        $empresa->restore();

        return redirect()->route('admin.empresas.index');
    }

    /**
     * Permanently delete Empresa from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('empresa_delete')) {
            return abort(401);
        }
        $empresa = Empresa::onlyTrashed()->findOrFail($id);
        $empresa->forceDelete();

        return redirect()->route('admin.empresas.index');
    }
}
