<?php

namespace App\Http\Controllers\Admin;

use App\Contareceber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreContarecebersRequest;
use App\Http\Requests\Admin\UpdateContarecebersRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;
use DB;
use App\Cliente;
use App\Contacontabil;
use App\User;
use App\Contacorrente;

class ContarecebersController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Contareceber.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('contareceber_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Contareceber::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('contareceber_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'contarecebers.id',
                'contarecebers.vencimento',
                'contarecebers.valorconta',
                'contarecebers.valorapagar',
                'contarecebers.valorpago',
                'contarecebers.parcela',
                'contarecebers.formapagamento',
                'contarecebers.docmercantil',
                'contarecebers.documento',
                'contarecebers.obs',
                'contarecebers.tipo',
                'contarecebers.baixado',
                'contarecebers.juros',
                'contarecebers.desconto',
                'contarecebers.dataemissao',
                'contarecebers.cliente',
                'contarecebers.formapagamentobaixa',
                'contarecebers.contacontabil',
                'contarecebers.colaborador',
                'contarecebers.contacorrente',
                'contarecebers.anexo',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'contareceber_';
                $routeKey = 'admin.contarecebers';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('vencimento', function ($row) {
                return $row->vencimento ? $row->vencimento : '';
            });
            $table->editColumn('valorconta', function ($row) {
                return $row->valorconta ? $row->valorconta : '';
            });
            $table->editColumn('valorapagar', function ($row) {
                return $row->valorapagar ? $row->valorapagar : '';
            });
            $table->editColumn('valorpago', function ($row) {
                return $row->valorpago ? $row->valorpago : '';
            });
            $table->editColumn('parcela', function ($row) {
                return $row->parcela ? $row->parcela : '';
            });
            $table->editColumn('formapagamento', function ($row) {
                return $row->formapagamento ? $row->formapagamento : '';
            });
            $table->editColumn('docmercantil', function ($row) {
                return $row->docmercantil ? $row->docmercantil : '';
            });
            $table->editColumn('documento', function ($row) {
                return $row->documento ? $row->documento : '';
            });
            $table->editColumn('obs', function ($row) {
                return $row->obs ? $row->obs : '';
            });
            $table->editColumn('tipo', function ($row) {
                return $row->tipo ? $row->tipo : '';
            });
            $table->editColumn('baixado', function ($row) {
                return $row->baixado ? $row->baixado : '';
            });
            $table->editColumn('juros', function ($row) {
                return $row->juros ? $row->juros : '';
            });
            $table->editColumn('desconto', function ($row) {
                return $row->desconto ? $row->desconto : '';
            });
            $table->editColumn('dataemissao', function ($row) {
                return $row->dataemissao ? $row->dataemissao : '';
            });
            $table->editColumn('cliente', function ($row) {
                return $row->cliente ? $row->cliente : '';
            });
            $table->editColumn('formapagamentobaixa', function ($row) {
                return $row->formapagamentobaixa ? $row->formapagamentobaixa : '';
            });
            $table->editColumn('contacontabil', function ($row) {
                return $row->contacontabil ? $row->contacontabil : '';
            });
            $table->editColumn('colaborador', function ($row) {
                return $row->colaborador ? $row->colaborador : '';
            });
            $table->editColumn('contacorrente', function ($row) {
                return $row->contacorrente ? $row->contacorrente : '';
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });

            

            return $table->make(true);
        }

        return view('admin.contarecebers.index');
    }

    /**
     * Show the form for creating new Contareceber.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('contareceber_create')) {
            return abort(401);
        }
        $contacorrente = Contacorrente::pluck('banco','id');
        return view('admin.contarecebers.create',compact('contacorrente'));
    }
    function searchcliente(Request $request){
        if($request->get('query'))
             {
                        $query = $request->get('query');
                      
                 $data = DB::table('clientes')
                   ->where('nome','LIKE', $query.'%')
                     ->get();
                $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach($data as $row)
            {
                          $output .='<li class="device_result cl" > <a href="#">'.$row->nome.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
  
      function searchcontacontabil(Request $request){
        if($request->get('query'))
             {
                        $query = $request->get('query');
                      
                 $data = DB::table('contacontabils')
                   ->where('descricao','LIKE', $query.'%')
                     ->get();
                $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach($data as $row)
            {
                          $output .='<li class="device_result co" > <a href="#">'.$row->descricao.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    
    function searchusuario(Request $request){
        if($request->get('query'))
             {
                        $query = $request->get('query');
                      
                 $data = DB::table('users')
                   ->where('name','LIKE', $query.'%')
                     ->get();
                $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach($data as $row)
            {
                          $output .='<li class="device_result us" > <a href="#">'.$row->name.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    /**
     * Store a newly created Contareceber in storage.
     *
     * @param  \App\Http\Requests\StoreContarecebersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContarecebersRequest $request)
    {
        if (! Gate::allows('contareceber_create')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $contareceber = Contareceber::create($request->all());



        return redirect()->route('admin.contarecebers.index');
    }


    /**
     * Show the form for editing Contareceber.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('contareceber_edit')) {
            return abort(401);
        }
        $contareceber = Contareceber::findOrFail($id);
        $contacorrente = Contacorrente::pluck('banco','id');
        return view('admin.contarecebers.edit', compact('contareceber','contacorrente'));
    }

    /**
     * Update Contareceber in storage.
     *
     * @param  \App\Http\Requests\UpdateContarecebersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContarecebersRequest $request, $id)
    {
        if (! Gate::allows('contareceber_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $contareceber = Contareceber::findOrFail($id);
        $contareceber->update($request->all());



        return redirect()->route('admin.contarecebers.index');
    }


    /**
     * Display Contareceber.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('contareceber_view')) {
            return abort(401);
        }
        $contareceber = Contareceber::findOrFail($id);

        return view('admin.contarecebers.show', compact('contareceber'));
    }


    /**
     * Remove Contareceber from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('contareceber_delete')) {
            return abort(401);
        }
        $contareceber = Contareceber::findOrFail($id);
        $contareceber->delete();

        return redirect()->route('admin.contarecebers.index');
    }

    /**
     * Delete all selected Contareceber at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contareceber_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contareceber::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Contareceber from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('contareceber_delete')) {
            return abort(401);
        }
        $contareceber = Contareceber::onlyTrashed()->findOrFail($id);
        $contareceber->restore();

        return redirect()->route('admin.contarecebers.index');
    }

    /**
     * Permanently delete Contareceber from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('contareceber_delete')) {
            return abort(401);
        }
        $contareceber = Contareceber::onlyTrashed()->findOrFail($id);
        $contareceber->forceDelete();

        return redirect()->route('admin.contarecebers.index');
    }
}
