<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class SystemCalendarController extends Controller
{
    public function index() 
    {
        $events = []; 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->servico; 
           $prefix         = 'Serviço:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->cliente; 
           $prefix         = 'Cliente:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->animal; 
           $prefix         = 'Animal:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->usuario; 
           $prefix         = 'Usuário:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->profissional; 
           $prefix         = 'Profissional:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->situacao; 
           $prefix         = 'Situação:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->hora; 
           $prefix         = 'Hora:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->valor; 
           $prefix         = 'Valor:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->telefone; 
           $prefix         = 'Fone:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->raca; 
           $prefix         = 'Raca:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->temperamento; 
           $prefix         = 'Temperamento:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->pacote; 
           $prefix         = 'Pacote:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->continuo; 
           $prefix         = 'Continuo:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 

        foreach (\App\Atendimento::all() as $atendimento) { 
           $crudFieldValue = $atendimento->getOriginal('data'); 

           if (! $crudFieldValue) {
               continue;
           }

           $eventLabel     = $atendimento->obs; 
           $prefix         = 'Obs:'; 
           $suffix         = ''; 
           $dataFieldValue = trim($prefix . " " . $eventLabel . " " . $suffix); 
           $events[]       = [ 
                'title' => $dataFieldValue, 
                'start' => $crudFieldValue, 
                'url'   => route('admin.atendimentos.edit', $atendimento->id)
           ]; 
        } 


       return view('admin.calendar' , compact('events')); 
    }

}
