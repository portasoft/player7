<?php

namespace App\Http\Controllers\Admin;

use App\Cor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCorsRequest;
use App\Http\Requests\Admin\UpdateCorsRequest;
use Yajra\Datatables\Datatables;

class CorsController extends Controller
{
    /**
     * Display a listing of Cor.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('cor_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Cor::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('cor_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'cors.id',
                'cors.descricao',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'cor_';
                $routeKey = 'admin.cors';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('descricao', function ($row) {
                return $row->descricao ? $row->descricao : '';
            });

            

            return $table->make(true);
        }

        return view('admin.cors.index');
    }

    /**
     * Show the form for creating new Cor.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('cor_create')) {
            return abort(401);
        }
        return view('admin.cors.create');
    }

    /**
     * Store a newly created Cor in storage.
     *
     * @param  \App\Http\Requests\StoreCorsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCorsRequest $request)
    {
        if (! Gate::allows('cor_create')) {
            return abort(401);
        }
        $cor = Cor::create($request->all());


        return redirect()->back()->with('success', true);
        // return redirect()->route('admin.cors.index');
    }


    /**
     * Show the form for editing Cor.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('cor_edit')) {
            return abort(401);
        }
        $cor = Cor::findOrFail($id);

        return view('admin.cors.edit', compact('cor'));
    }

    /**
     * Update Cor in storage.
     *
     * @param  \App\Http\Requests\UpdateCorsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCorsRequest $request, $id)
    {
        if (! Gate::allows('cor_edit')) {
            return abort(401);
        }
        $cor = Cor::findOrFail($id);
        $cor->update($request->all());


        return redirect()->back()->with('success', true);
        // return redirect()->route('admin.cors.index');
    }


    /**
     * Display Cor.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('cor_view')) {
            return abort(401);
        }
        $cor = Cor::findOrFail($id);

        return view('admin.cors.show', compact('cor'));
    }


    /**
     * Remove Cor from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('cor_delete')) {
            return abort(401);
        }
        $cor = Cor::findOrFail($id);
        $cor->delete();

        return redirect()->route('admin.cors.index');
    }

    /**
     * Delete all selected Cor at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('cor_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Cor::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Cor from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('cor_delete')) {
            return abort(401);
        }
        $cor = Cor::onlyTrashed()->findOrFail($id);
        $cor->restore();

        return redirect()->route('admin.cors.index');
    }

    /**
     * Permanently delete Cor from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('cor_delete')) {
            return abort(401);
        }
        $cor = Cor::onlyTrashed()->findOrFail($id);
        $cor->forceDelete();

        return redirect()->route('admin.cors.index');
    }
}
