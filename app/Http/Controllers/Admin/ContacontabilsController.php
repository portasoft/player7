<?php

namespace App\Http\Controllers\Admin;

use App\Contacontabil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreContacontabilsRequest;
use App\Http\Requests\Admin\UpdateContacontabilsRequest;
use Yajra\Datatables\Datatables;

class ContacontabilsController extends Controller
{
    /**
     * Display a listing of Contacontabil.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('contacontabil_access')) {
            return abort(401);
        }


        
        if (request()->ajax()) {
            $query = Contacontabil::query();
            $template = 'actionsTemplate';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('contacontabil_delete')) {
            return abort(401);
        }
                $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
            $query->select([
                'contacontabils.id',
                'contacontabils.origem',
                'contacontabils.descricao',
            ]);
            $table = Datatables::of($query);

            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'contacontabil_';
                $routeKey = 'admin.contacontabils';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('origem', function ($row) {
                return $row->origem ? $row->origem : '';
            });
            $table->editColumn('descricao', function ($row) {
                return $row->descricao ? $row->descricao : '';
            });

            

            return $table->make(true);
        }

        return view('admin.contacontabils.index');
    }

    /**
     * Show the form for creating new Contacontabil.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('contacontabil_create')) {
            return abort(401);
        }
        return view('admin.contacontabils.create');
    }

    /**
     * Store a newly created Contacontabil in storage.
     *
     * @param  \App\Http\Requests\StoreContacontabilsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContacontabilsRequest $request)
    {
        if (! Gate::allows('contacontabil_create')) {
            return abort(401);
        }
        $contacontabil = Contacontabil::create($request->all());



        return redirect()->route('admin.contacontabils.index');
    }


    /**
     * Show the form for editing Contacontabil.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('contacontabil_edit')) {
            return abort(401);
        }
        $contacontabil = Contacontabil::findOrFail($id);

        return view('admin.contacontabils.edit', compact('contacontabil'));
    }

    /**
     * Update Contacontabil in storage.
     *
     * @param  \App\Http\Requests\UpdateContacontabilsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContacontabilsRequest $request, $id)
    {
        if (! Gate::allows('contacontabil_edit')) {
            return abort(401);
        }
        $contacontabil = Contacontabil::findOrFail($id);
        $contacontabil->update($request->all());



        return redirect()->route('admin.contacontabils.index');
    }


    /**
     * Display Contacontabil.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('contacontabil_view')) {
            return abort(401);
        }
        $contacontabil = Contacontabil::findOrFail($id);

        return view('admin.contacontabils.show', compact('contacontabil'));
    }


    /**
     * Remove Contacontabil from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('contacontabil_delete')) {
            return abort(401);
        }
        $contacontabil = Contacontabil::findOrFail($id);
        $contacontabil->delete();

        return redirect()->route('admin.contacontabils.index');
    }

    /**
     * Delete all selected Contacontabil at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contacontabil_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Contacontabil::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Contacontabil from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('contacontabil_delete')) {
            return abort(401);
        }
        $contacontabil = Contacontabil::onlyTrashed()->findOrFail($id);
        $contacontabil->restore();

        return redirect()->route('admin.contacontabils.index');
    }

    /**
     * Permanently delete Contacontabil from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('contacontabil_delete')) {
            return abort(401);
        }
        $contacontabil = Contacontabil::onlyTrashed()->findOrFail($id);
        $contacontabil->forceDelete();

        return redirect()->route('admin.contacontabils.index');
    }
}
