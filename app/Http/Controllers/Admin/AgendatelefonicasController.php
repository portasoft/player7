<?php

namespace App\Http\Controllers\Admin;

use App\Agendatelefonica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAgendatelefonicasRequest;
use App\Http\Requests\Admin\UpdateAgendatelefonicasRequest;

class AgendatelefonicasController extends Controller
{
    /**
     * Display a listing of Agendatelefonica.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('agendatelefonica_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('agendatelefonica_delete')) {
                return abort(401);
            }
            $agendatelefonicas = Agendatelefonica::onlyTrashed()->get();
        } else {
            $agendatelefonicas = Agendatelefonica::all();
        }

        return view('admin.agendatelefonicas.index', compact('agendatelefonicas'));
    }

    /**
     * Show the form for creating new Agendatelefonica.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('agendatelefonica_create')) {
            return abort(401);
        }
        return view('admin.agendatelefonicas.create');
    }

    /**
     * Store a newly created Agendatelefonica in storage.
     *
     * @param  \App\Http\Requests\StoreAgendatelefonicasRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgendatelefonicasRequest $request)
    {
        if (! Gate::allows('agendatelefonica_create')) {
            return abort(401);
        }
        $agendatelefonica = Agendatelefonica::create($request->all());


        return redirect()->back()->with('success', true);
        // return redirect()->route('admin.agendatelefonicas.index');
    }


    /**
     * Show the form for editing Agendatelefonica.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('agendatelefonica_edit')) {
            return abort(401);
        }
        $agendatelefonica = Agendatelefonica::findOrFail($id);
        // dd($agendatelefonica);
        return view('admin.agendatelefonicas.edit', compact('agendatelefonica'));
    }

    /**
     * Update Agendatelefonica in storage.
     *
     * @param  \App\Http\Requests\UpdateAgendatelefonicasRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAgendatelefonicasRequest $request, $id)
    {
        if (! Gate::allows('agendatelefonica_edit')) {
            return abort(401);
        }
        $agendatelefonica = Agendatelefonica::findOrFail($id);
        $agendatelefonica->update($request->all());


        // return redirect()->back()->with('success', true);
         return redirect()->route('admin.agendatelefonicas.index')->with('success', true);
    }


    /**
     * Display Agendatelefonica.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('agendatelefonica_view')) {
            return abort(401);
        }
        $agendatelefonica = Agendatelefonica::findOrFail($id);

        return view('admin.agendatelefonicas.show', compact('agendatelefonica'));
    }


    /**
     * Remove Agendatelefonica from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('agendatelefonica_delete')) {
            return abort(401);
        }
        $agendatelefonica = Agendatelefonica::findOrFail($id);
        $agendatelefonica->delete();

        return redirect()->route('admin.agendatelefonicas.index');
    }

    /**
     * Delete all selected Agendatelefonica at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('agendatelefonica_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Agendatelefonica::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Agendatelefonica from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('agendatelefonica_delete')) {
            return abort(401);
        }
        $agendatelefonica = Agendatelefonica::onlyTrashed()->findOrFail($id);
        $agendatelefonica->restore();

        return redirect()->route('admin.agendatelefonicas.index');
    }

    /**
     * Permanently delete Agendatelefonica from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('agendatelefonica_delete')) {
            return abort(401);
        }
        $agendatelefonica = Agendatelefonica::onlyTrashed()->findOrFail($id);
        $agendatelefonica->forceDelete();

        return redirect()->route('admin.agendatelefonicas.index');
    }
}
