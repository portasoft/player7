<?php

namespace App\Http\Controllers\Admin;

use App\Kititen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreKititensRequest;
use App\Http\Requests\Admin\UpdateKititensRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use Yajra\Datatables\Datatables;
use App\Produto;
use App\Tamanho;
use Illuminate\Support\Facades\DB;
class KititensController extends Controller
{
    use FileUploadTrait;

    /**
     * Display a listing of Kititen.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('kititen_access')) {
            return abort(401);
        }
        // $query = DB::table('kititens')->where('deleted_at', '=', NULL)
        // ->select('kititens.*', 
        // (DB::raw("(SELECT descricao FROM produtos u1 WHERE u1.id = kititens.produto ) AS kit ")),
        
        // (DB::raw("(SELECT descricao FROM produtos u1 WHERE u1.id = kititens.kit ) AS item")))

        // ->get();
        // dd($query);
   
        if (request()->ajax()) {
           
            $query = DB::table('kititens')->where('deleted_at', '=', NULL)
            ->select('kititens.*', 
            (DB::raw("(SELECT descricao FROM produtos u1 WHERE u1.id = kititens.produto ) AS kit ")),
            (DB::raw("(SELECT descricao FROM tamanhos u1 WHERE u1.id = kititens.tamanho ) AS tamanho ")),
            (DB::raw("(SELECT descricao FROM produtos u1 WHERE u1.id = kititens.kit ) AS item")))
    
            ->get();
            
            // $query['base'] = '123.00';
            if(request('show_deleted') == 1) {
                
        if (! Gate::allows('kititen_delete')) {
            return abort(401);
        }
                 $query->onlyTrashed();
                $template = 'restoreTemplate';
            }
       
             $template = 'actionsTemplate';
        
         
            $table = Datatables::of($query);
         
            $table->setRowAttr([
                'data-entry-id' => '{{$id}}',
            ]);
            $table->addColumn('massDelete', '&nbsp;');
        
            $table->addColumn('actions', '&nbsp;');
            $table->editColumn('actions', function ($row) use ($template) {
                $gateKey  = 'kititen_';
                $routeKey = 'admin.kititens';

                return view($template, compact('row', 'gateKey', 'routeKey'));
            });
            $table->editColumn('produto', function ($row) {
                return $row->produto ? $row->produto : '';
            });
            $table->editColumn('qtde', function ($row) {
                return $row->qtde ? $row->qtde : '';
            });
            $table->editColumn('valor', function ($row) {
                return $row->valor ? $row->valor : '';
            });
            $table->editColumn('base', function ($row) {
                return $row->base ? $row->base : '';
            });
            $table->editColumn('kit', function ($row) {
                return $row->kit ? $row->kit : '';
            });
            $table->editColumn('tamanho', function ($row) {
                return $row->tamanho ? $row->tamanho : '';
            });
            $table->editColumn('anexo', function ($row) {
                if($row->anexo) { return '<a href="'.asset(env('UPLOAD_PATH').'/'.$row->anexo) .'" target="_blank">Download file</a>'; };
            });
          

        

            return $table->make(true);
        }
            // $produtop = DB::table('produtos')->get();
            // foreach ($produtop as $vp) {
            //     $ppp = $vp->descricao;
            // }
        //    dd($ppp);
        $tamanho = Tamanho::pluck('descricao','id');
       
        // dd($tamanho['']);
        return view('admin.kititens.index',compact('tamanho'));
    }

    /**
     * Show the form for creating new Kititen.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('kititen_create')) {
            return abort(401);
        }
        return view('admin.kititens.create');
    }

    /**
     * Store a newly created Kititen in storage.
     *
     * @param  \App\Http\Requests\StoreKititensRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreKititensRequest $request)
    {
        if (! Gate::allows('kititen_create')) {
            return abort(401);
        }

        $idkit = Kititen::find(DB::table('kititens')->max('id'));
        $request = $this->saveFiles($request);
        $data = $request->all();
        if($idkit ===null){
            $data['idkit'] = 1;
         }else{
            $data['idkit'] = $idkit->idkit + (1) ;
         }
         $pro =  $request->produto;
         $proitem =  $request->kit;
         $tam = $request->tamanho;
        //  dd($pro);
        //  //TAMANHOS
        //  $tamanhoss = DB::table('tamanhos')->where('descricao', 'LIKE', $tam . '%')->get();
        //  foreach ($tamanhoss as $t) {
        //     $data['tamanho'] = $t->id;
        //  } 
        //  //FIM TAMANHOS
         $produtos = DB::table('produtos')->where('descricao', '=', $pro)->get();
         foreach ($produtos as $p) {
            $data['produto'] = $p->id;
          
         } 
       
         $itens = DB::table('produtos')->where('descricao', '=', $proitem )
         ->where('finalidade','=','Kit')->get();
         foreach ($itens as $pi) {
            $data['kit'] = $pi->id;
            $custoC = $pi->custo;
            $est = $pi->estoque;
         }  
        //  dd($itens);
         $estInfor = $request->qtde;
    //    dd($estInfor,$est);
        // if ($est < $estInfor)  {
        //     return redirect()->back()->with('alert', true);//ESTOQUE INSUFICIENTE
        // } 
       
         $qtdeC = $request->qtde;
         $tot = $qtdeC * $custoC;
       $data['base'] = $tot;
       $data['OrdemCor'] = $request->cor;
       $kititen = Kititen::create($data);
     
       $ff  = '';
       $tamanho = Tamanho::pluck('descricao','id');
       
        return view('admin.kititens.index',compact('tamanho'),['ff'=>$ff]);
    }
    public function fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');

            

            $produtos = DB::table('produtos')->where('descricao', 'LIKE', $query . '%')->where('deleted_at',NULL )->where('finalidade','Produto' )->orWhere('referencia', 'LIKE', $query . '%')->orWhere('codigobarra', 'LIKE', $query . '%')->get();

            

            $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach ($produtos as $row) {
                $output .= '<li class="device_result c"><a href="#">' . $row->descricao . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;

        }
    

    }
    // public function importartelaKit(Request $request){
    //     $cus = $request->produto;
    //     dd('ddd');

    // }
    public function fetchkit(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');

            

            $produtos = DB::table('produtos')->where('descricao', 'LIKE', $query . '%')->where('deleted_at',NULL )->where('finalidade','Kit' )->get();

            

            $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach ($produtos as $row) {
                $output .= '<li class="device_result ckit"><a href="#">' . $row->descricao . '</a></li>';
            }
            $output .= '</ul>';
            echo $output;

        }


    }

    /**
     * Show the form for editing Kititen.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('kititen_edit')) {
            return abort(401);
        }
        // $query = DB::table('kititens')
        // ->select('kititens.*', 
        // (DB::raw("(SELECT descricao FROM produtos u1 WHERE u1.id = kititens.produto) AS kit")),
        // (DB::raw("(SELECT descricao FROM produtos u1 WHERE u1.id = kititens.kit) AS item")))


        // // $query = DB::table('kititens')->get()
        //  ->get();
         $itens = DB::table('kititens')->where('id', '=', $id)->get();
         foreach ($itens as $key => $value) {
            $kitC = $value->kit;
            $kitP = $value->produto;
         }
       
         $descC = DB::table('produtos')->where('id', '=', $kitC)->get();
         foreach ($descC as $key => $value) {
            $c = $value->descricao;
         }
         $descP = DB::table('produtos')->where('id', '=', $kitP)->get();
         foreach ($descP as $key => $value) {
            $p = $value->descricao;
         }
        
        //  dd($c,$p);
        $kititen = Kititen::findOrFail($id);
        $kititen['kit'] = $c;
        $kititen['produto'] = $p;
   
        $tamanho = Tamanho::pluck('descricao','id');
  
        return view('admin.kititens.edit', compact('kititen','tamanho'));
    }

    /**
     * Update Kititen in storage.
     *
     * @param  \App\Http\Requests\UpdateKititensRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateKititensRequest $request, $id)
    {
     
        if (! Gate::allows('kititen_edit')) {
            return abort(401);
        }
        $request = $this->saveFiles($request);
        $kititen = Kititen::findOrFail($id);

            $all = $request->all();
            $allP =$request->produto;
            $allC =$request->kit;
            $allCor =$request->cor;
      
            $prodP = DB::table('produtos')->where('descricao', '=', $allP)->get();
            foreach ($prodP as $key => $value) {
               $pP = $value->id;
            }
            $prodC = DB::table('produtos')->where('descricao', '=', $allC)->get();
            foreach ($prodC as $key => $value) {
               $pC = $value->id;
        
            $estoqueAtu = $value->estoque;
            }
            $estoqueInform = $request->qtde;
            // dd($estoqueInform,$estoqueAtu);
            if ($estoqueAtu < $estoqueInform)  {
                return redirect()->back()->with('alert', true);//ESTOQUE INSUFICIENTE
            } 

            $all['produto'] = $pP;
            $all['kit'] = $pC; 
            $all['OrdemCor'] = $allCor; 
        
        $kititen->update($all);



        return redirect()->route('admin.kititens.index');
    }


    /**
     * Display Kititen.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('kititen_view')) {
            return abort(401);
        }
        $kititen = Kititen::findOrFail($id);
        $vP = $kititen['produto'];
        $vC = $kititen['kit'];

        $Vprodutos =  DB::table('produtos')->where('id', '=', $vP )->get();
     
        foreach ($Vprodutos as $key => $value) {
            $VproP = $value->descricao;
        }
        $VprodutosK =  DB::table('produtos')->where('id', '=', $vC )->get();
     
        foreach ($VprodutosK as $key => $valuek) {
            $Vproc = $valuek->descricao;
        }
    

        $kititen['produto'] = $VproP;
        $kititen['kit'] = $Vproc;
        return view('admin.kititens.show', compact('kititen'));
    }


    /**
     * Remove Kititen from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('kititen_delete')) {
            return abort(401);
        }
        // dd($id);
        // dd($id);
        $kititen = Kititen::findOrFail($id);
      
        $kititen->delete();

        return redirect()->route('admin.kititens.index');
    }

    /**
     * Delete all selected Kititen at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
    
        if (! Gate::allows('kititen_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Kititen::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Kititen from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('kititen_delete')) {
            return abort(401);
        }
        $kititen = Kititen::onlyTrashed()->findOrFail($id);
        $kititen->restore();

        return redirect()->route('admin.kititens.index');
    }

    /**
     * Permanently delete Kititen from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('kititen_delete')) {
            return abort(401);
        }
        $kititen = Kititen::onlyTrashed()->findOrFail($id);
        $kititen->forceDelete();

        return redirect()->route('admin.kititens.index');
    }
}
