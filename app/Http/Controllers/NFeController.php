<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\NFeService;
class NFeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  201;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //    $nfe_service = new NFeService([
    //     "atualizacao" => "2018-09-10 13:38:21",
    //     "tpAmb" => 2,
    //     "razaosocial" => "EDILSON CHAGAS PORTA 31371084890",
    //     "siglaUF" => "SP",
    //     "cnpj" => "31254656000164",
    //     "schemes" => "PL_009_V4",
    //     "versao" => "4.00",
    //     // "tokenIBPT" => "AAAAAAA",
    //     // "CSC" => "GPB0JBWLUR6HWFTVEAS6RJ69GPCROFPBBB8G",
    //     // "CSCid" => "000002",
    //    ]);
    //    header('Content-type: text/xml; charset=UTF-8');
       //Gera o XML
    //    $xml= $nfe_service->gerarNFe();
       

    //    //Assinar
        // $signed_xml = $nfe_service->sign($xml);
        // return $signed_xml;
       //TRANSMITIR
    //    $resultado = $nfe_service->transmitir($signed_xml);
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
