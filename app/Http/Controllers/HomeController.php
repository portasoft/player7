<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Produto;
use App\Vendatemp;
use App\Vendaitem;
use App\Deposito;
use App\Movimento;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //QUANTIDADE DE USUARIOS
        $user = User::select(\DB::raw('count(*) as qtde, id'))
                    ->groupBy('id')
                    ->get();
         $total_counts = User::count();
        //QUANTIDADE DE PRODUTOS CADASTRADOS
         $produtos = Produto::select(\DB::raw('count(*) as qtde, id'))
         ->groupBy('id')
         ->get();
        $total_produtos = Produto::count();
       
               //QUANTIDADE DE DEPOSITOSS CADASTRADOS
         $total_deposito = Deposito::select(\DB::raw('count(*) as qtde, id'))
         ->groupBy('id')
         ->get();
        $total_deposito = Deposito::count();
           //QUANTIDADE DE MOVIMENTOS CADASTRADOS
           $total_movimento = Movimento::select(\DB::raw('count(*) as qtde, id'))
           ->groupBy('id')
           ->get();
          $total_movimento = Movimento::count();
 
        return view('home',compact('total_counts','total_produtos','total_vendas','total_item','total_deposito','total_movimento'));
    }
}
