<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Producao
 *
 * @package App
 * @property string $produtoid
 * @property decimal $estoqueatual
 * @property string $depositoorigem
 * @property string $depositodestino
 * @property decimal $custo
 * @property decimal $qtde
 * @property string $obs
 * 
*/
class Producao extends Model
{
    use SoftDeletes;

    protected $fillable = ['produtoid', 'estoqueatual', 'depositoorigem', 'depositodestino', 'custo', 'qtde', 'obs','situacao','tamanho',
    'referencia','lote','frente','costa','bolso','manga','capuz','punho','barra','etiqueta','outros',
    'orelha','chifre','data','idfornecedor','ordem','cos','cor','cor2','composicao2'];
    protected $hidden = [];
    
    

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setProdutoidAttribute($input)
    {
        $this->attributes['produtoid'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setEstoqueatualAttribute($input)
    {
        $this->attributes['estoqueatual'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setDepositoorigemAttribute($input)
    {
        $this->attributes['depositoorigem'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setDepositodestinoAttribute($input)
    {
        $this->attributes['depositodestino'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setCustoAttribute($input)
    {
        $this->attributes['custo'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setQtdeAttribute($input)
    {
        $this->attributes['qtde'] = $input ? $input : null;
    }
    
}
