<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Regraimposto
 *
 * @package App
 * @property string $tiporegra
 * @property string $iva
 * @property string $uforigem
 * @property string $ufodestino
 * @property string $baseicms
 * @property string $aliquotaicms
 * @property string $baseicmsst
 * @property string $cst
 * @property string $csosn
 * @property string $taxatributaria
 * @property string $decreto
 * @property string $modalidade
 * @property string $cfop
 * @property string $classeipi
 * @property string $codigoenquadramento
 * @property string $tipocalculo
 * @property string $aliquotaipi
 * @property decimal $valorunidadeipi
 * @property string $stpis
 * @property string $tipocalculocofins
 * @property string $tributacaoissqn
 * @property string $ipizerado
 * @property string $stpisentrada
 * @property string $basepis
 * @property string $basecofins
 * @property string $basepisentrada
 * @property string $basecofinsentrada
 * @property string $aliquotapissentrada
 * @property string $aliquotacofinssentrada
 * @property string $stentrada
 * @property string $cfopentrada
 * @property string $csosnentrada
 * @property string $codsuframa
 * @property string $stipi
 * @property string $tipocalculoipisaida
 * @property string $aliquotaipisaida
 * @property decimal $valorunidadeipisaida
 * @property string $stipiisaida
 * @property string $aliquotaissqn
 * @property string $codigoservicoiss
 * @property string $cfopservico
 * @property string $aliquotainternaicms
 * @property string $coftributacaomunicipal
 * @property string $cnae
 * @property string $anexo
*/
class Regraimposto extends Model
{
    use SoftDeletes;

    protected $fillable = ['tiporegra', 'iva', 'uforigem', 'ufodestino', 'baseicms', 'aliquotaicms', 'baseicmsst', 'cst', 'csosn', 'taxatributaria', 'decreto', 'modalidade', 'cfop', 'classeipi', 'codigoenquadramento', 'tipocalculo', 'aliquotaipi', 'valorunidadeipi', 'stpis', 'tipocalculocofins', 'tributacaoissqn', 'ipizerado', 'stpisentrada', 'basepis', 'basecofins', 'basepisentrada', 'basecofinsentrada', 'aliquotapissentrada', 'aliquotacofinssentrada', 'stentrada', 'cfopentrada', 'csosnentrada', 'codsuframa', 'stipi', 'tipocalculoipisaida', 'aliquotaipisaida', 'valorunidadeipisaida', 'stipiisaida', 'aliquotaissqn', 'codigoservicoiss', 'cfopservico', 'aliquotainternaicms', 'coftributacaomunicipal', 'cnae', 'anexo'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Regraimposto::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorunidadeipiAttribute($input)
    {
        $this->attributes['valorunidadeipi'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorunidadeipisaidaAttribute($input)
    {
        $this->attributes['valorunidadeipisaida'] = $input ? $input : null;
    }
    
}
