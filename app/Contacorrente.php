<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contacorrente
 *
 * @package App
 * @property string $agencia
 * @property string $conta
 * @property string $banco
 * @property string $empresa
 * @property decimal $saldoinicial
 * @property decimal $saldo
 * @property string $anexo
*/
class Contacorrente extends Model
{
    use SoftDeletes;

    protected $fillable = ['agencia', 'conta', 'banco', 'empresa', 'saldoinicial', 'saldo', 'anexo'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Contacorrente::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setSaldoinicialAttribute($input)
    {
        $this->attributes['saldoinicial'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setSaldoAttribute($input)
    {
        $this->attributes['saldo'] = $input ? $input : null;
    }
    
}
