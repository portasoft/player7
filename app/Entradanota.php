<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Entradanota
 *
 * @package App
 * @property string $fornecedor
 * @property string $dataemissao
 * @property string $dataentrada
 * @property decimal $valosprodutos
 * @property string $baseicms
 * @property decimal $valoricms
 * @property decimal $valorsubst
 * @property string $desconto
 * @property string $despesas
 * @property decimal $valornota
 * @property string $deposito
 * @property string $horaemissao
 * @property decimal $valoripi
 * @property decimal $valorfrete
 * @property string $freteincluso
 * @property string $tiponf
 * @property string $cfop
 * @property string $tipoentrada
 * @property string $formapagamento
 * @property decimal $valorservico
 * @property string $serie
 * @property string $centrocusto
 * @property string $obs
 * @property string $cnpjtransportadora
 * @property string $chavenfe
 * @property string $placa
 * @property string $unidade
 * @property string $datacadastro
 * @property string $contacontabi
 * @property string $xml
*/
class Entradanota extends Model
{
    use SoftDeletes;

    protected $fillable = ['fornecedor', 'dataemissao', 'dataentrada', 'valosprodutos', 'baseicms', 'valoricms', 'valorsubst', 'desconto', 'despesas', 'valornota', 'deposito', 'horaemissao', 'valoripi', 'valorfrete', 'freteincluso', 'tiponf', 'cfop', 'tipoentrada', 'formapagamento', 'valorservico', 'serie', 'centrocusto', 'obs', 'cnpjtransportadora', 'chavenfe', 'placa', 'unidade', 'datacadastro', 'contacontabi', 'xml'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Entradanota::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDataemissaoAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['dataemissao'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['dataemissao'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDataemissaoAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDataentradaAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['dataentrada'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['dataentrada'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDataentradaAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValosprodutosAttribute($input)
    {
        $this->attributes['valosprodutos'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValoricmsAttribute($input)
    {
        $this->attributes['valoricms'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorsubstAttribute($input)
    {
        $this->attributes['valorsubst'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValornotaAttribute($input)
    {
        $this->attributes['valornota'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValoripiAttribute($input)
    {
        $this->attributes['valoripi'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorfreteAttribute($input)
    {
        $this->attributes['valorfrete'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorservicoAttribute($input)
    {
        $this->attributes['valorservico'] = $input ? $input : null;
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDatacadastroAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['datacadastro'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['datacadastro'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDatacadastroAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }
    
}
