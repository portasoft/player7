<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Movimento
 *
 * @package App
 * @property string $data
 * @property string $tipo
 * @property string $empresa
 * @property integer $deposito
 * @property integer $usuario
 * @property integer $produto
 * @property integer $qtde
 * @property string $motivo
 * @property string $documento
 * @property string $obs
*/
class Movimento extends Model
{
    use SoftDeletes;

    protected $fillable = ['data', 'tipo', 'empresa', 'deposito', 'usuario', 'produto', 'qtde', 'motivo', 'documento', 'obs','cor','fornecedor'];
    protected $hidden = [];
    
    

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDataAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['data'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['data'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDataAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setDepositoAttribute($input)
    {
        $this->attributes['deposito'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setUsuarioAttribute($input)
    {
        $this->attributes['usuario'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setProdutoAttribute($input)
    {
        $this->attributes['produto'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setQtdeAttribute($input)
    {
        $this->attributes['qtde'] = $input ? $input : null;
    }
    
}
