<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contacontabil
 *
 * @package App
 * @property string $origem
 * @property string $descricao
*/
class Contacontabil extends Model
{
    use SoftDeletes;

    protected $fillable = ['origem', 'descricao'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Contacontabil::observe(new \App\Observers\UserActionsObserver);
    }
    
}
