<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Unidade
 *
 * @package App
 * @property string $descricao
*/
class Unidade extends Model
{
    use SoftDeletes;

    protected $fillable = ['descricao'];
    protected $hidden = [];
    
    
    
}
