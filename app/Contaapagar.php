<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contaapagar
 *
 * @package App
 * @property string $fornecedor
 * @property string $vencimento
 * @property decimal $valorconta
 * @property decimal $valorapagar
 * @property integer $parcela
 * @property string $formapagamento
 * @property string $contacontabil
 * @property string $docmercantil
 * @property string $contacorrente
 * @property string $documento
 * @property string $colaborador
 * @property string $obs
 * @property string $tipo
 * @property string $baixado
 * @property string $databaixa
 * @property decimal $juros
 * @property decimal $desconto
 * @property string $dataemissao
 * @property string $anexo
*/
class Contaapagar extends Model
{
    use SoftDeletes;

    protected $fillable = ['fornecedor', 'vencimento', 'valorconta', 'valorapagar', 'parcela', 'formapagamento', 'contacontabil', 'docmercantil', 'contacorrente', 'documento', 'colaborador', 'obs', 'tipo', 'baixado', 'databaixa', 'juros', 'desconto', 'dataemissao', 'anexo'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Contaapagar::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setVencimentoAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['vencimento'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['vencimento'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getVencimentoAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorcontaAttribute($input)
    {
        $this->attributes['valorconta'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorapagarAttribute($input)
    {
        $this->attributes['valorapagar'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setParcelaAttribute($input)
    {
        $this->attributes['parcela'] = $input ? $input : null;
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDatabaixaAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['databaixa'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['databaixa'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDatabaixaAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setJurosAttribute($input)
    {
        $this->attributes['juros'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setDescontoAttribute($input)
    {
        $this->attributes['desconto'] = $input ? $input : null;
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDataemissaoAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['dataemissao'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['dataemissao'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDataemissaoAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }
    
}
