<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cliente
 *
 * @package App
 * @property string $nome
 * @property string $cpf
 * @property string $rg
 * @property string $celular
 * @property string $telefone
 * @property string $email
 * @property string $cep
 * @property string $endereco
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $uf
 * @property string $complemento
 * @property string $estadocivil
 * @property string $sexo
 * @property string $datacadastro
 * @property string $datanascimento
 * @property decimal $renda
 * @property decimal $limite
 * @property string $cartao
 * @property string $pontos
 * @property decimal $debito
 * @property string $obs
 * @property string $tipopessoa
 * @property string $naturalidade
 * @property decimal $credito
 * @property string $localizacao
 * @property string $foto
*/
class Cliente extends Model
{
    use SoftDeletes;

    protected $fillable = ['nome', 'cpf', 'rg', 'celular', 'telefone', 'email', 'cep', 'endereco', 'numero', 'bairro', 'cidade', 'uf', 'complemento', 'estadocivil', 'sexo', 'datacadastro', 'datanascimento', 'renda', 'limite', 'cartao', 'pontos', 'debito', 'obs', 'tipopessoa', 'naturalidade', 'credito', 'foto', 'localizacao_address', 'localizacao_latitude', 'localizacao_longitude'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Cliente::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDatacadastroAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['datacadastro'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['datacadastro'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDatacadastroAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDatanascimentoAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['datanascimento'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['datanascimento'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDatanascimentoAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setRendaAttribute($input)
    {
        $this->attributes['renda'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setLimiteAttribute($input)
    {
        $this->attributes['limite'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setDebitoAttribute($input)
    {
        $this->attributes['debito'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setCreditoAttribute($input)
    {
        $this->attributes['credito'] = $input ? $input : null;
    }
    
}
