<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Agendatelefonica
 *
 * @package App
 * @property string $nome
 * @property string $operadora
 * @property string $celular1
 * @property string $celular2
 * @property string $telefone1
 * @property string $telefone2
 * @property string $emailpessoal
 * @property string $emailcomercial
 * @property string $dataaniversario
 * @property string $dataabertura
 * @property string $fundacao
 * @property string $endereco
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $uf
 * @property string $cep
 * @property string $obs
*/
class Agendatelefonica extends Model
{
    use SoftDeletes;

    protected $fillable = ['nome', 'operadora', 'celular1', 'celular2', 'telefone1', 'telefone2', 'emailpessoal', 'emailcomercial', 'dataaniversario', 'dataabertura', 'fundacao', 'endereco', 'numero', 'bairro', 'cidade', 'uf', 'cep', 'obs','tipo'];
    protected $hidden = [];
    
    

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDataaniversarioAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['dataaniversario'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['dataaniversario'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDataaniversarioAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDataaberturaAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['dataabertura'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['dataabertura'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDataaberturaAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }
    
}
