<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tamanho
 *
 * @package App
 * @property string $descricao
 * @property string $grade
*/
class Tamanho extends Model
{
    use SoftDeletes;

    protected $fillable = ['descricao', 'grade'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Tamanho::observe(new \App\Observers\UserActionsObserver);
    }
    
}
