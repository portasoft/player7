<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Despesa
 *
 * @package App
 * @property string $fornecedor
 * @property string $data
 * @property decimal $valor_conta
 * @property decimal $valorpago
 * @property integer $parcela
 * @property string $formapagamento
 * @property string $contacontabil
 * @property string $docmercantil
 * @property string $contacorrente
 * @property string $documento
 * @property string $obs
 * @property string $colaborador
 * @property string $tipo
 * @property decimal $juros
 * @property decimal $desconto
 * @property string $anexo
*/
class Despesa extends Model
{
    use SoftDeletes;

    protected $fillable = ['fornecedor', 'data', 'valor_conta', 'valorpago', 'parcela', 'formapagamento', 'contacontabil', 'docmercantil', 'contacorrente', 'documento', 'obs', 'colaborador', 'tipo', 'juros', 'desconto', 'anexo'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Despesa::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDataAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['data'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['data'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDataAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorContaAttribute($input)
    {
        $this->attributes['valor_conta'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setValorpagoAttribute($input)
    {
        $this->attributes['valorpago'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setParcelaAttribute($input)
    {
        $this->attributes['parcela'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setJurosAttribute($input)
    {
        $this->attributes['juros'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setDescontoAttribute($input)
    {
        $this->attributes['desconto'] = $input ? $input : null;
    }
    
}
