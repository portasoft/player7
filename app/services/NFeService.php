<?php
namespace App\Services;
use NFePHP\NFe\Make;

use NFePHP\Common\Certificate;
use stdClass;
use NFePHP\NFe\Tools;
use NFePHP\NFe\Factories\Protocol;
use NFePHP\NFe\Common\Standardize;

class NFeService{
    private $config;
    private $tools;
    public function __construct($config){
        $this->config = $config;
     
        $certificadoDigital = file_get_contents('C:\certificado\Certificado.pfx');
        $this->tools = new Tools(json_encode($config),Certificate::readPfx($certificadoDigital, '#Master2018'));
            // try {
            //     $xmlAssinado = $tools->signNFe($xml); // O conteúdo do XML assinado fica armazenado na variável $xmlAssinado
            // } catch (\Exception $e) {
            //     //aqui você trata possíveis exceptions da assinatura
            //     exit($e->getMessage());
            // }
           
    }
        public  function gerarNFe(){
            //Criar uma nota vazia
            $nfe = new Make();

            $stdInNFe = new stdClass();
            $stdInNFe->versao = '4.00'; //versão do layout
            $stdInNFe->Id = '';//se o Id de 44 digitos não for passado será gerado automaticamente
            $stdInNFe->pk_nItem = null; //deixe essa variavel sempre como NULL
                    
            $infNFe = $nfe->taginfNFe($stdInNFe);
            // IDE
            $stdIde = new stdClass();
            $stdIde->cUF = 35;
            $stdIde->cNF = rand(11111111,99999999);//
            $stdIde->natOp = 'REVENDA DE MERCADORIAS SIMPLES NACIONAL';
            //$std->indPag = "0"; //NÃO EXISTE MAIS NA VERSÃO 4.00 
            $stdIde->mod = 55;
            $stdIde->serie = 1;
            $stdIde->nNF = 2;
            $stdIde->dhEmi = date("Y-m-d\TH:i:sP");
            $stdIde->dhSaiEnt = date("Y-m-d\TH:i:sP");
            $stdIde->tpNF = 1;//Entrada ou saida
            $stdIde->idDest = 1; //Dentro ou fora do estado
            $stdIde->cMunFG = 3516200;
            $stdIde->tpImp = 1;//Tipo 1-impressao:retrato  - paisagem
            $stdIde->tpEmis = 1;//Conigencia ou normal etc...
            $stdIde->cDV = 2; //Digito verificador
            $stdIde->tpAmb = 2; //TIpop ambiente //2-Homolocacao
            $stdIde->finNFe = 1;//1-NFe normal / 2_NFe complementar/ 3-NFe de Ajuste
            $stdIde->indFinal = 1; 
            $stdIde->indPres = 1;
            $stdIde->procEmi = 0;
            $stdIde->verProc = '2.4.0'; //versao do sistema
            $stdIde->dhCont = null; //Data e hora da entrada em contigencia
            $stdIde->xJust = null; //Sefaz local nao responde

            $tagide = $nfe->tagide($stdIde);

            //EMITENTE
            $stdEmit = new stdClass();
            $stdEmit->xNome ="EDILSON CHAGAS PORTA 31371084890";
            $stdEmit->xFant ="PORTASOFT";
            $stdEmit->IE ="310711862114";
            // $std->IEST ="";
            // $std->IM ="";
            // $std->CNAE ="";
            $stdEmit->CRT = "3";
            $stdEmit->CNPJ ="31254656000164"; //indicar apenas um CNPJ ou CPF
            // $std->CPF ="";

            $emit = $nfe->tagemit($stdEmit);
            // ENDERECO DO EMITENTE
            $stdEnderEmit = new stdClass();
            $stdEnderEmit->xLgr="RUA PAULO CESAR MIRANDA";
            $stdEnderEmit->nro="3760";
            $stdEnderEmit->xCpl="";
            $stdEnderEmit->xBairro="CIC";
            $stdEnderEmit->cMun="3516200";
             $stdEnderEmit->xMun="FRANCA";
            $stdEnderEmit->UF="SP";
            $stdEnderEmit->CEP="14403237";
            $stdEnderEmit->cPais="1058";
            $stdEnderEmit->xPais="BRASIL";
            $stdEnderEmit->fone="4121098000";

            $enderEmit = $nfe->tagenderEmit($stdEnderEmit);

            //DESTINATARIO
            $stdDest = new stdClass();
            $stdDest->xNome = "EMPRESA TESTE";
            $stdDest->indIEDest = 9;
            // $stdDest->IE = "0963233556";
            // $stdDest->ISUF = ""; //MANAUS/AMAZONAS
            $stdDest->IM = "";
            $stdDest->email = "porta.T@hotmail.com";
       //     $stdDest->CNPJ = "40454144873"; //indicar apenas um CNPJ ou CPF ou idEstrangeiro
            $stdDest->CPF = "40454144873";
            // $stdDest->idEstrangeiro = "";

            $dest = $nfe->tagdest($stdDest);

            //ENDERECO DESTINATARIO
            $stdEnderDest = new stdClass();
            $stdEnderDest->xLgr = "RUA AVELINO ALGARTE BANHOS";
            $stdEnderDest->nro = "1310";
            $stdEnderDest->xCpl = "";
            $stdEnderDest->xBairro = "CIC";
            $stdEnderDest->cMun = "3516200";
            $stdEnderDest->xMun = "FRANCA";
            $stdEnderDest->UF = "SP";
            $stdEnderDest->CEP = "14402124";
            $stdEnderDest->cPais = "1058";
            $stdEnderDest->xPais = "BRASIL";
            $stdEnderDest->fone = "4121098000";

            $elem = $nfe->tagenderDest($stdEnderDest);

            //PRODUTOS
            //UTILIZAR FOREACH PARA ADICIONAR PRODUTO
            //EXEMPLO ABAIXO UTILIZANDO APENAS UM PRODUTO
            $stdProd = new stdClass();
            $stdProd->item = 1; //item da NFe
            $stdProd->cProd = "4450";
            $stdProd->cEAN = "7897534826649";//CODIGO DE BAIRRA DA CAIXA
            $stdProd->xProd = "BLUSA";
            $stdProd->NCM = "44170010";

            $stdProd->cBenef = ""; //incluido no layout 4.00

            // $stdProd->EXTIPI = "";
            $stdProd->CFOP = "5102";
            $stdProd->uCom = "UN";
            $stdProd->qCom = "1";
            $stdProd->vUnCom = "6.99";
            $stdProd->cEANTrib = "7897534826649";//CODIGO DE BARRA PARA UNIDADE
            $stdProd->uTrib = "UN";
            $stdProd->qTrib = "1";
            $stdProd->vUnTrib = "6.99";
            $stdProd->vProd =$this->format($stdProd->qTrib * $stdProd->vUnTrib);
            $stdProd->vFrete = "";
            $stdProd->vSeg = "";
            $stdProd->vDesc = null;
            $stdProd->vOutro = "";
            $stdProd->indTot = "1";
            // $stdProd->xPed = "";
            // $stdProd->nItemPed = "";
           // $stdProd->nFCI = ""; //IMPORTACAO

            $prod = $nfe->tagprod($stdProd);

            //INFORMACAO ADICIONAL DO PRODUTO
            $stdAdicional = new stdClass();
            $stdAdicional->item = 1; //item da NFe

            $stdAdicional->infAdProd = 'informacao adicional do item';

            $indAdProd = $nfe->taginfAdProd($stdAdicional);

            //IMPOSTO
            $stdImposto = new stdClass();
            $stdImposto->item = 1; //item da NFe
            $stdImposto->vTotTrib = 4.00;

            $imposto = $nfe->tagimposto($stdImposto);

            //DEVERA COLOCAR EM CADA LOOP EX:
            // $vProdTotal += $stdProd->vProd
            // ICMS
            $stdICMS = new stdClass();
            $stdICMS->item = 1; //item da NFe
            $stdICMS->orig = 0;
            $stdICMS->CST = "00";
            $stdICMS->modBC = "0";
            $stdICMS->vBC = $this->format($stdProd->vProd);
            $stdICMS->pICMS = 18.00;
            $stdICMS->vICMS =$this->format($stdICMS->vBC *($stdICMS->pICMS /100));
            // $stdICMS->pFCP = "";
            // $stdICMS->vFCP = "";
            // $stdICMS->vBCFCP = "";
            // $stdICMS->modBCST = "";
            // $stdICMS->pMVAST = "";
            // $stdICMS->pRedBCST = "";
            // $stdICMS->vBCST = "";
            // $stdICMS->pICMSST = "";
            // $stdICMS->vICMSST = "";
            // $stdICMS->vBCFCPST = "";
            // $stdICMS->pFCPST = "";
            // $stdICMS->vFCPST = "";
            // $stdICMS->vICMSDeson = "";
            // $stdICMS->motDesICMS = "";
            // $stdICMS->pRedBC = "";
            // $stdICMS->vICMSOp = "";
            // $stdICMS->pDif = "";
            // $stdICMS->vICMSDif = "";
            // $stdICMS->vBCSTRet = "";
            // $stdICMS->pST = "";
            // $stdICMS->vICMSSTRet = "";
            // $stdICMS->vBCFCPSTRet = "";
            // $stdICMS->pFCPSTRet = "";
            // $stdICMS->vFCPSTRet = "";
            // $stdICMS->pRedBCEfet = "";
            // $stdICMS->vBCEfet = "";
            // $stdICMS->pICMSEfet = "";
            // $stdICMS->vICMSEfet = "";

            $ICMS = $nfe->tagICMS($stdICMS);

            //PIS
            $stdPIS = new stdClass();
            $stdPIS->item = 1; //item da NFe
            $stdPIS->CST = '99';
            $stdPIS->vBC = $this->format($stdProd->vProd);
            $stdPIS->pPIS = 1.65;
            $stdPIS->vPIS =$this->format( $stdPIS->vBC *($stdPIS->pPIS /100));
            // $stdPIS->qBCProd = null;
            // $stdPIS->vAliqProd = null;

            $PIS = $nfe->tagPIS($stdPIS);

            //COFINS
            $stdCOFINS = new stdClass();
            $stdCOFINS->item = 1; //item da NFe
            $stdCOFINS->CST = '99';
            $stdCOFINS->vBC = $this->format($stdProd->vProd);
            $stdCOFINS->pCOFINS = 1.65;
            $stdCOFINS->vCOFINS = $this->format($stdCOFINS->vBC *($stdCOFINS->pCOFINS /100));
            // $stdCOFINS->qBCProd = null;
            // $stdCOFINS->vAliqProd = null;

            $COFINS = $nfe->tagCOFINS($stdCOFINS);

            //TOTAIS
            $stdICMSTot = new stdClass();
            $stdICMSTot->vBC  = "";
            $stdICMSTot->vICMS = "";
            $stdICMSTot->vICMSDeson = "";
            $stdICMSTot->vFCP = ""; //incluso no layout 4.00
            $stdICMSTot->vBCST = "";
            $stdICMSTot->vST = "";
            $stdICMSTot->vFCPST = ""; //incluso no layout 4.00
            $stdICMSTot->vFCPSTRet = ""; //incluso no layout 4.00
            $stdICMSTot->vProd = "";
            $stdICMSTot->vFrete = "";
            $stdICMSTot->vSeg = "";
            $stdICMSTot->vDesc = "";
            $stdICMSTot->vII = "";
            $stdICMSTot->vIPI = "";
            $stdICMSTot->vIPIDevol = ""; //incluso no layout 4.00
            $stdICMSTot->vPIS = "";
            $stdICMSTot->vCOFINS = "";
            $stdICMSTot->vOutro = "";
            $stdICMSTot->vNF = "";
            $stdICMSTot->vTotTrib = "";

            $ICMSTot = $nfe->tagICMSTot($stdICMSTot);

            //TRANSPORTADORA
            $stdTransp = new stdClass();
            $stdTransp->modFrete = 1;

            $transp = $nfe->tagtransp($stdTransp);

            //VOLUME
            $stdVol = new stdClass();
            $stdVol->item = 1; //indicativo do numero do volume
            $stdVol->qVol = 1;
            $stdVol->esp = 'CAIXA';
            // $stdVol->marca = 'OLX';
            // $stdVol->nVol = '11111';
            // $stdVol->pesoL = 10.50;
            // $stdVol->pesoB = 11.00;
            $vol = $nfe->tagvol($stdVol);

            //PAGAMENTO
            $stdPag = new stdClass();
            $stdPag->vTroco = 0.00; //incluso no layout 4.00, obrigatório informar para NFCe (65)
            
            $pag = $nfe->tagpag($stdPag);

            //DETALHE DO PAGAMENTO
            $stdDetPag = new stdClass();
            $stdDetPag->tPag = '14';
            $stdDetPag->vPag = $this->format($stdProd->vProd); //Obs: deve ser informado o valor pago pelo cliente
        //   dd($stdDetPag->vPag );
            // $stdDetPag->CNPJ = '12345678901234';
           // $stdDetPag->tBand = '01'; 
           // $stdDetPag->cAut = '3333333';
            // $stdDetPag->tpIntegra = 1; //incluso na NT 2015/002
            $stdDetPag->indPag = 0; //0= Pagamento à Vista 1= Pagamento à Prazo

            $detPag = $nfe->tagdetPag($stdDetPag);

            //INFORMACAO ADICIONAL DA NOTA
            $stdInfAdic = new stdClass();
            $stdInfAdic->infAdFisco = 'informacoes para o fisco';
            $stdInfAdic->infCpl = 'informacoes complementares';

            $infAdic = $nfe->taginfAdic($stdInfAdic);
            //MONTA A NOTA
      
                if ($nfe->montaNFe()) {
            return $nfe->getXML();
                }else{
                    throw new Exception("Erro ao gerar NF-e");
                }
        }

        //ASSINA NOTA FISCAL
public function sign($xml){
    return $this->tools->signNFe($xml);
}
//TRAsSITIR NOTA FISCAL PARA O FISCO
public function transmitir($signed_xml){
    $resp = $this->tools->sefazEnviaLote([$signed_xml],1,1);

    $st = new Standardize();
    $stdResposta = $st->toStd($resp);
    try {
        $protocol = new Protocol();
        $xmlProtocolado = $protocol->add($signed_xml,$resp);
        } catch (\Exception $e) {
        exit($e->getMessage());
    }
    file_put_contents('nota.xml',$xmlProtocolado);
    return $xmlProtocolado;
}

        public function format($number, $dec =2){
         
            return number_format ((float) $number ,$dec, ".","");
        }
}






