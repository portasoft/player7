<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cor
 *
 * @package App
 * @property string $descricao
*/
class Cor extends Model
{
    use SoftDeletes;

    protected $fillable = ['descricao'];
    protected $hidden = [];
    
    
    public static function boot()
    {
        parent::boot();

        Cor::observe(new \App\Observers\UserActionsObserver);
    }
  
    
}
