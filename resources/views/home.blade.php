@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div id="HNome" class="panel-heading center">
                    <h4 id="TNome">DASHBOARD</h4></div>

                <div class="panel-body">
                    {{-- @lang('quickadmin.qa_dashboard_text') --}}
                 
                            <!-- ./col -->
                            {{-- <div class="col-lg-3 col-xs-6">
                                <!-- small box -->
                                <div class="small-box bg-aqua">
                                  <div class="inner">
                                  <h3>{{$total_vendas}}</h3>
                      
                                    <p>VENDAS</p>
                                  </div>
                                  <div class="icon">
                                      
                                    <i class="fa fa-shopping-cart"></i>
                                  </div>
                                  <a href="{{ route('admin.produtos.index') }}" class="small-box-footer">
                                    VER <i class="fa fa-arrow-circle-right"></i>
                                  </a>I
                                </div>
                              </div> --}}
                              {{-- <div class="col-lg-3 col-xs-6">
                                  <!-- small box -->
                                  <div class="small-box bg-blue" style="background-color:skyblue">
                                    <div class="inner">
                                    <h3>{{$total_item}}</h3>
                        
                                      <p>ITENS VENDIDOS</p>
                                    </div>
                                    <div class="icon">
                                      <i class="fa fa-area-chart"></i>
                                    </div>
                                    <a href="{{ route('admin.produtos.index') }}" class="small-box-footer">
                                      VER <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                  </div>
                                </div> --}}
                              <div class="col-lg-3 col-xs-6">
                                  <!-- small box -->
                                  <div class="small-box bg-aqua">
                                    <div class="inner">
                                    <h3>{{$total_produtos}}</h3>
                        
                                      <p>PRODUTOS</p>
                                    </div>
                                    <div class="icon">
                                      <i class="fa fa-shopping-basket"></i>
                                    </div>
                                    <a href="{{ route('admin.produtos.index') }}" class="small-box-footer">
                                      VER <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                  </div>
                                </div>
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-green">
                                      <div class="inner">
                                      <h3>{{$total_deposito}}</h3>
                          
                                        <p>DEPÓSITOS</p>
                                      </div>
                                      <div class="icon">
                                          <i class="ion ion-stats-bars"></i>
                                      </div>
                                      <a href="{{ route('admin.produtos.index') }}" class="small-box-footer">
                                        VER <i class="fa fa-arrow-circle-right"></i>
                                      </a>
                                    </div>
                                  </div>
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                      <div class="inner">
                                      <h3>{{$total_counts}}</h3>
                          
                                        <p>USUÁRIOS</p>
                                      </div>
                                      <div class="icon">
                                        <i class="ion ion-person-add"></i>
                                      </div>
                                      <a href="{{ route('admin.users.index') }}" class="small-box-footer">
                                          VER <i class="fa fa-arrow-circle-right"></i>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="col-lg-3 col-xs-6">
                                      <!-- small box -->
                                      <div class="small-box bg-red">
                                        <div class="inner">
                                            <h3>{{$total_movimento}}</h3>
                            
                                          <p>MOVIMENTOS</p>
                                        </div>
                                        <div class="icon">
                                          <i class="ion ion-pie-graph"></i>
                                        </div>
                                        <a href="#" class="small-box-footer">
                                          VER <i class="fa fa-arrow-circle-right"></i>
                                        </a>
                                      </div>
                                    </div>
                                    
                </div>
        
                </div>
                <div class="panel panel-primary">
                    <div id="HNome" class="panel-heading center">
                      <h4 id="TNome">ACESSO RÁPIDO</h4></div>
    
                    <div class="panel-body">
                        <div class="box-body">
                           
                            <a class="btn btn-app" style="height:90px;width:100px" style="height:90px;width:100px"  href="{{ route('admin.agendatelefonicas.index') }}">
                              <i style="font-size:40px" class="fa fa-phone"></i> AGENDA
                            </a>
                            <a class="btn btn-app" style="height:90px;width:100px" href="{{ route('admin.empresas.index') }}">
                              <i style="font-size:40px" class="fa fa-building"></i> EMPRESA
                            </a>
                            <a class="btn btn-app" style="height:90px;width:100px" href="{{ route('admin.fornecedors.index') }}">
                              <i style="font-size:40px" class="fa fa-truck"></i> FORNECEDOR
                            </a>
                            <a class="btn btn-app" style="height:90px;width:100px" href="{{ route('admin.users.index') }}">
                              <i style="font-size:40px" class="fa fa-user"></i> USUÁRIO
                            </a>
                            <a class="btn btn-app" style="height:90px;width:100px" href="{{ route('admin.produtos.index') }}">
                              <i style="font-size:40px" class="fa fa-shopping-basket"></i> PRODUTO
                            </a>
                            <a class="btn btn-app" style="height:90px;width:100px" href="{{ route('admin.producaos.index') }}">
                              <i style="font-size:40px" class="fa fa-shopping-bag"></i> PRODUÇÃO
                            </a>
                                  <a class="btn btn-app" style="height:90px;width:100px" href="{{ route('admin.kititens.index') }}">
                              <span class="badge bg-yellow"></span>
                              <i style="font-size:40px" class="fa fa-flask"></i>COMPOSIÇÃO
                            </a>
                            {{-- <a class="btn btn-app" style="height:90px;width:100px" href="{{ route('admin.entradanotas.index') }}">
                              <span class="badge bg-yellow"></span>
                              <i style="font-size:40px" class="fa fa-file-text"></i> NOTA
                            </a> --}}
                          
                          </div>
                                                                      
                    </div>
                    </div>
            </div>
        </div>
    </div>
 
    
@endsection
