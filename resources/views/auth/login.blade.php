@extends('layouts.auth')

@section('content')
<!--===============================================================================================-->	

	{{-- <link rel="ico" type="image/png" href="images/icons/porta.png"/> --}}


<!--===============================================================================================-->
<link rel="stylesheet" href="{{ URL::asset('adminlte/bootstrap/css/bootstrap.min.css') }}">

<!--===============================================================================================-->
<link rel="stylesheet" href="{{ URL::asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">

<!--===============================================================================================-->
<link rel="stylesheet" href="{{ URL::asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">

<!--===============================================================================================-->
<link rel="stylesheet" href="{{ URL::asset('adminlte/animate/animate.css') }}">

<!--===============================================================================================-->	
<link rel="stylesheet" href="{{ URL::asset('adminlte/css-hamburgers/hamburgers.min.css') }}">

<!--===============================================================================================-->
<link rel="stylesheet" href="{{ URL::asset('adminlte/animsition/css/animsition.min.css') }}">

<!--===============================================================================================-->
<link rel="stylesheet" href="{{ URL::asset('adminlte/select2/select2.min.css') }}">

<!--===============================================================================================-->	
<link rel="stylesheet" href="{{ URL::asset('adminlte/daterangepicker/daterangepicker.css') }}">

<!--===============================================================================================-->
<link rel="stylesheet" href="{{ URL::asset('css/util.css') }}">

	<link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
	
<!--===============================================================================================-->

	
	<div class="limiter">
	
		<div class="container-login100">
			
			<div class="wrap-login100">
				<div class="login100-more" >
					
						<img  style="padding:10%;height:90%;"   src="{!! asset('images/logo.png') !!}">
				</div>
			
				
				<form class="login100-form validate-form" method="POST"
						  action="{{ url('login') }}">
						  
							@if (count($errors) > 0)
							
							<div class="alert alert-danger alert-dismissible" id="success-alert">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
									<h4><i class="icon fa fa-ban"></i>Acesso inválido!</h4>
								 </div> 
						
						@endif 
					<span class="login100-form-title p-b-43">
						
					<img  height="120px" src="{!! asset('images/lo.png') !!}">
				
					</span>
					
					
					<div class="wrap-input100 validate-input" data-validate = "Informe o e-mail">
                    <input  type="hidden" name="_token" value="{{ csrf_token() }}">

                    
                        <input  class="input100" type="email" class="form-control" name="email" value="{{ old('email') }}">
                     
						<span class="focus-input100"></span>
						<span class="label-input100">Email</span>
					</div>
					
					
					<div class="wrap-input100 validate-input" data-validate="Informe a senha">
                     
                        <input  class="input100" type="password" class="form-control" name="password">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>

					<div class="flex-sb-m w-full p-t-3 p-b-32">
						<!-- <div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div> -->

						<!-- <div>
							<a href="#" class="txt1">
								Forgot Password?
							</a>
						</div> -->
					</div>
			
{{-- 
					<div class="container-login100-form-btn"> --}}
		
						<button type="submit" class="btn btn-primary btn-block login100-form-btn"  style="margin-right: 15px;">
							Acessar </button>
					 {{-- @endif --}}
                 
						<!-- <button type="submit" class="login100-form-btn">
							Login
						</button> -->
					</div>
					
					<!-- <div class="text-center p-t-46 p-b-20">
						<span class="txt2">
							
						</span>
					</div> -->

					<!-- <div class="login100-form-social flex-c-m">
						<a href="#" class="login100-form-social-item flex-c-m bg1 m-r-5">
							<i class="fa fa-facebook-f" aria-hidden="true"></i>
						</a>

						<a href="#" class="login100-form-social-item flex-c-m bg2 m-r-5">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</div> -->
				</form>
				{{-- <div class="login100-more" style="background-image: url('images/bg-01.jpg');"> 
					<img  class="login100-more"   src="{!! asset('images/porta.png') !!}"> 
			 </div> --}}
				{{-- <img  class="login100-more"   src="{!! asset('images/porta.png') !!}"> --}}
				{{-- url({{ URL::asset('images/slides/2.jpg') }}) --}}
			{{-- <div class="login100-more" style="background-image:({{ URL::asset('images/porta.png') }})" ></div> --}}
				{{-- <div class="login100-more" style="background-image: url('images/bg-01.jpg');"> --}}
				{{-- <div class="login100-more" style="background-image: url('images/lo.png');"> --}}
				</div>
			</div>
		</div>
	</div>
	
	

	
	
<!--===============================================================================================-->
<script type="text/javascript" src="{{ URL::asset('js/jquery/jquery-3.2.1.min.js') }}"></script>

<!--===============================================================================================-->
<script type="text/javascript" src="{{ URL::asset('js/animsition/js/animsition.min.js') }}"></script>


<!--===============================================================================================-->
<script type="text/javascript" src="{{ URL::asset('js/bootstrap/js/popper.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap/js/bootstrap.min.js') }}"></script>

<!--===============================================================================================-->
<script type="text/javascript" src="{{ URL::asset('js/select2/select2.min.js') }}"></script>

<!--===============================================================================================-->
<script type="text/javascript" src="{{ URL::asset('js/daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/daterangepicker/daterangepicker.js') }}"></script>

<!--===============================================================================================-->
<script type="text/javascript" src="{{ URL::asset('js/countdowntime/countdowntime.js') }}"></script>

<!--===============================================================================================-->
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>


@endsection
@section('javascript') 
<script>
        $(document).ready(function () {
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
      
     });
 </script>
 @endsection