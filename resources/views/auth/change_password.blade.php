@extends('layouts.app')

@section('content')
	{{-- <h3 class="page-title">@lang('quickadmin.qa_change_password')</h3> --}}

	
		{!! Form::open(['method' => 'PATCH', 'route' => ['auth.change_password']]) !!}
		<!-- If no success message in flash session show change password form  -->
		<div class="panel panel-primary">
			<div id="HNome" class="panel-heading">
	<h4 id="TNome">ALTERAR SENHA</h4>
			</div>
			@if (session()->has('success'))
			<div class="alert alert-success alert-dismissible" id="success-alert">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
				<h4><i class="icon fa fa-check"></i>Alterado com sucesso!</h4>
			 </div> 
			@endif
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 form-group">
						{!! Form::label('current_password', trans('quickadmin.qa_current_password'), ['class' => 'control-label']) !!}
						{!! Form::password('current_password', ['class' => 'form-control', 'placeholder' => '']) !!}
						<p class="help-block"></p>
						@if($errors->has('current_password'))
							<p class="help-block">
								{{ $errors->first('current_password') }}
							</p>
						@endif
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 form-group">
						{!! Form::label('new_password', trans('quickadmin.qa_new_password'), ['class' => 'control-label']) !!}
						{!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => '']) !!}
						<p class="help-block"></p>
						@if($errors->has('new_password'))
							<p class="help-block">
								{{ $errors->first('new_password') }}
							</p>
						@endif
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 form-group">
						{!! Form::label('new_password_confirmation', trans('quickadmin.qa_password_confirm'), ['class' => 'control-label']) !!}
						{!! Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => '']) !!}
						<p class="help-block"></p>
						@if($errors->has('new_password_confirmation'))
							<p class="help-block">
								{{ $errors->first('new_password_confirmation') }}
							</p>
						@endif
					</div>
				</div>
			</div>
		</div>
		<button type="submit" class="btn btn-primary btn-lg">
			<i class="fa fa-refresh"> </i>
		ALTERAR</button>
		{{-- {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!} --}}
		{!! Form::close() !!}
	
	
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
@endsection

