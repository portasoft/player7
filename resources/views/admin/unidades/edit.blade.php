@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.unidade.title')</h3> --}}
    
    {!! Form::model($unidade, ['method' => 'PUT', 'route' => ['admin.unidades.update', $unidade->id]]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
         <h4 id="TNome">ALTERAR - UNIDADE</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>ALTERADO COM SUCESSO!</h4>
         </div> 
        @endif
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 form-group">
                    {!! Form::label('descricao', trans('quickadmin.unidade.fields.descricao').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('descricao', old('descricao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('descricao'))
                        <p class="help-block">
                            {{ $errors->first('descricao') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
            <i class="fa fa-refresh"> </i>
        ALTERAR</button>
    {{-- {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
@stop
@section('javascript') 
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
  
@endsection


