@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.unidade.title')</h3> --}}
    {{-- @can('unidade_create')
    <p>
        <a href="{{ route('admin.unidades.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    
    @endcan --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.unidades.store']]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
    <h4 id="TNome">CADASTRO - UNIDADE</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>CADASTRADO COM SUCESSO!</h4>
         </div> 
        @endif
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 form-group">
                    {!! Form::label('descricao', trans('quickadmin.unidade.fields.descricao').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('descricao', old('descricao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('descricao'))
                        <p class="help-block">
                            {{ $errors->first('descricao') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
            <i class="fa fa-save"> </i>
        SALVAR</button>
    {{-- {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
    @can('unidade_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.unidades.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.unidades.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
         <h4 id="TNome">RELATÓRIO - UNIDADES</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($unidades) > 0 ? 'datatable' : '' }} @can('unidade_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('unidade_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.unidade.fields.descricao')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($unidades) > 0)
                        @foreach ($unidades as $unidade)
                            <tr data-entry-id="{{ $unidade->id }}">
                                @can('unidade_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <td field-key='descricao'>{{ $unidade->descricao }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('unidade_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.unidades.restore', $unidade->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('unidade_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.unidades.perma_del', $unidade->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    {{-- @can('unidade_view')
                                    <a href="{{ route('admin.unidades.show',[$unidade->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan --}}
                                    @can('unidade_edit')
                                    <a href="{{ route('admin.unidades.edit',[$unidade->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('unidade_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.unidades.destroy', $unidade->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
    <script>
        @can('unidade_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.unidades.mass_destroy') }}'; @endif
        @endcan

    </script>
@endsection