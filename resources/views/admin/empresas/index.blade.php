@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.empresa.title')</h3> --}}
    @can('empresa_create')
    {{-- <p>
        <a href="{{ route('admin.empresas.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p> --}}
    <p>
        <a href="{{ route('admin.empresas.create') }}"  type="submit" class="btn btn-success btn-lg">
            <i class="fa fa-save"> </i>
        NOVO</a>
        
    </p>
    @endcan

    @can('empresa_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.empresas.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.empresas.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
                <h4 id="TNome">RELATÓRIO - EMPRESA</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('empresa_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('empresa_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.empresa.fields.cnpj')</th>
                        <th>@lang('quickadmin.empresa.fields.razao')</th>
                        <th>CADASTRO</th>
                        <th>@lang('quickadmin.empresa.fields.fantasia')</th>
                        <th>@lang('quickadmin.empresa.fields.ie')</th>
                        <th>@lang('quickadmin.empresa.fields.im')</th>
                        <th>@lang('quickadmin.empresa.fields.contato')</th>
                        <th>@lang('quickadmin.empresa.fields.regime')</th>
                        <th>@lang('quickadmin.empresa.fields.fone')</th>
                        <th>@lang('quickadmin.empresa.fields.celular')</th>
                        <th>@lang('quickadmin.empresa.fields.email')</th>
                        <th>@lang('quickadmin.empresa.fields.cep')</th>
                        <th>@lang('quickadmin.empresa.fields.endereco')</th>
                        <th>@lang('quickadmin.empresa.fields.numero')</th>
                        <th>@lang('quickadmin.empresa.fields.bairro')</th>
                        <th>@lang('quickadmin.empresa.fields.cidade')</th>
                        <th>@lang('quickadmin.empresa.fields.uf')</th>
                        <th>@lang('quickadmin.empresa.fields.complemento')</th>
                        <th>@lang('quickadmin.empresa.fields.foto')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('empresa_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.empresas.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.empresas.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('empresa_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'cnpj', name: 'cnpj'},
                {data: 'razao', name: 'razao'},
                {data: 'datacadastro', name: 'datacadastro'},
                {data: 'fantasia', name: 'fantasia'},
                {data: 'ie', name: 'ie'},
                {data: 'im', name: 'im'},
                {data: 'contato', name: 'contato'},
                {data: 'regime', name: 'regime'},
                {data: 'fone', name: 'fone'},
                {data: 'celular', name: 'celular'},
                {data: 'email', name: 'email'},
                {data: 'cep', name: 'cep'},
                {data: 'endereco', name: 'endereco'},
                {data: 'numero', name: 'numero'},
                {data: 'bairro', name: 'bairro'},
                {data: 'cidade', name: 'cidade'},
                {data: 'uf', name: 'uf'},
                {data: 'complemento', name: 'complemento'},
                {data: 'foto', name: 'foto'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection