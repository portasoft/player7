@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.empresa.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.empresas.store'], 'files' => true,]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">CADASTRO - EMPRESA</h4>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 form-group">
                        {!! Form::label('cnpj', trans('quickadmin.empresa.fields.cnpj').'', ['class' => 'control-label']) !!}
                        <input name="cnpj" type="text" id="cnpj" onBlur="ValidarCNPJ(formPJ.cnpj);" class="form-control"
                         onKeyPress="return txtBoxFormat(this.id, '99.999.999/9999-99',event);" maxlength="18" />
                    {{-- {!! Form::label('cnpj', trans('quickadmin.empresa.fields.cnpj').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('cnpj', old('cnpj'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('cnpj'))
                        <p class="help-block">
                            {{ $errors->first('cnpj') }}
                        </p>
                    @endif --}}
                </div>
            
                <div class="col-xs-3 form-group">
                    {!! Form::label('razao', trans('quickadmin.empresa.fields.razao').'', ['class' => 'control-label']) !!}
                    {!! Form::text('razao', old('razao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('razao'))
                        <p class="help-block">
                            {{ $errors->first('razao') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-3 form-group">
                        {!! Form::label('fantasia', trans('quickadmin.empresa.fields.fantasia').'', ['class' => 'control-label']) !!}
                        {!! Form::text('fantasia', old('fantasia'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('fantasia'))
                            <p class="help-block">
                                {{ $errors->first('fantasia') }}
                            </p>
                        @endif
                    </div>
         
            
             
            
                <div class="col-xs-2 form-group">
                    {!! Form::label('ie', trans('quickadmin.empresa.fields.ie').'', ['class' => 'control-label']) !!}
                    {!! Form::text('ie', old('ie'), ['class' => 'form-control', 'placeholder' => '','id'=>'ie']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('ie'))
                        <p class="help-block">
                            {{ $errors->first('ie') }}
                        </p>
                    @endif
                </div>
            
                <div class="col-xs-2 form-group">
                    {!! Form::label('im', trans('quickadmin.empresa.fields.im').'', ['class' => 'control-label']) !!}
                    {!! Form::text('im', old('im'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('im'))
                        <p class="help-block">
                            {{ $errors->first('im') }}
                        </p>
                    @endif
                </div>
            
                <div class="col-xs-2 form-group">
                    {!! Form::label('contato', trans('quickadmin.empresa.fields.contato').'', ['class' => 'control-label']) !!}
                    {!! Form::text('contato', old('contato'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('contato'))
                        <p class="help-block">
                            {{ $errors->first('contato') }}
                        </p>
                    @endif
                </div>
            
                <div class="col-xs-2 form-group">
                    {!! Form::label('regime', trans('quickadmin.empresa.fields.regime').'', ['class' => 'control-label']) !!}
                    {!! Form::text('regime', old('regime'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('regime'))
                        <p class="help-block">
                            {{ $errors->first('regime') }}
                        </p>
                    @endif
                </div>
            
                <div class="col-xs-2 form-group">
                        {!! Form::label('fone', trans('quickadmin.empresa.fields.fone').'', ['class' => 'control-label']) !!}
                        {!! Form::text('fone', old('fone'), ['class' => 'form-control', 'placeholder' => '','id'=>'fone','onkeypress'=>'validarTelefoneOk()',
                             'maxlength'=>'13']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('fone'))
                            <p class="help-block">
                                {{ $errors->first('fone') }}
                            </p>
                        @endif
                    </div>
            
         
                <div class="col-xs-2 form-group">
                        {!! Form::label('celular', trans('quickadmin.empresa.fields.celular').'', ['class' => 'control-label']) !!}
                        {!! Form::text('celular', old('celular'), ['class' => 'form-control', 'placeholder' => '','id'=>'celular','onkeypress'=>'validarCelularOk()',
                                 'maxlength'=>'14']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('celular'))
                            <p class="help-block">
                                {{ $errors->first('celular') }}
                            </p>
                        @endif
                    </div>
            
                <div class="col-xs-4 form-group">
                    {!! Form::label('email', trans('quickadmin.empresa.fields.email').'', ['class' => 'control-label']) !!}
                    {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('email'))
                        <p class="help-block">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                </div>
            
           
                <div class="col-xs-2 form-group">
                        {!! Form::label('cep', trans('quickadmin.empresa.fields.cep').'', ['class' => 'control-label']) !!}
                        {!! Form::text('cep', old('cep'), ['class' => 'form-control','id'=>'cep', 'placeholder' => '','onblur'=>'pesquisacep(this.value)','onkeypress'=>'mascaraCep()' ,'maxlength'=>'9']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('cep'))
                            <p class="help-block">
                                {{ $errors->first('cep') }}
                            </p>
                        @endif
                    </div>
                <div class="col-xs-4 form-group">
                    {!! Form::label('endereco', trans('quickadmin.empresa.fields.endereco').'', ['class' => 'control-label']) !!}
                    {!! Form::text('endereco', old('endereco'), ['class' => 'form-control', 'placeholder' => '','id'=>'endereco']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('endereco'))
                        <p class="help-block">
                            {{ $errors->first('endereco') }}
                        </p>
                    @endif
                </div>
            
          
                <div class="col-xs-1 form-group">
                        {!! Form::label('numero', trans('quickadmin.empresa.fields.numero').'', ['class' => 'control-label']) !!}
                        {!! Form::text('numero', old('numero'), ['class' => 'form-control', 'placeholder' => '', 'id'=>'numero','onkeypress'=>'numeros()','maxlength'=>'5']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('numero'))
                            <p class="help-block">
                                {{ $errors->first('numero') }}
                            </p>
                        @endif
                    </div>
                <div class="col-xs-3 form-group">
                    {!! Form::label('bairro', trans('quickadmin.empresa.fields.bairro').'', ['class' => 'control-label']) !!}
                    {!! Form::text('bairro', old('bairro'), ['class' => 'form-control', 'placeholder' => '','id'=>'bairro']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('bairro'))
                        <p class="help-block">
                            {{ $errors->first('bairro') }}
                        </p>
                    @endif
                </div>
            
                <div class="col-xs-2 form-group">
                    {!! Form::label('cidade', trans('quickadmin.empresa.fields.cidade').'', ['class' => 'control-label']) !!}
                    {!! Form::text('cidade', old('cidade'), ['class' => 'form-control', 'placeholder' => '','id'=>'cidade']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('cidade'))
                        <p class="help-block">
                            {{ $errors->first('cidade') }}
                        </p>
                    @endif
                </div>
            
                <div class="col-xs-1 form-group">
                    {!! Form::label('uf', trans('quickadmin.empresa.fields.uf').'', ['class' => 'control-label']) !!}
                    {!! Form::text('uf', old('uf'), ['class' => 'form-control', 'placeholder' => '','id'=>'uf']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('uf'))
                        <p class="help-block">
                            {{ $errors->first('uf') }}
                        </p>
                    @endif
                </div>
            
                <div class="col-xs-5 form-group">
                    {!! Form::label('complemento', trans('quickadmin.empresa.fields.complemento').'', ['class' => 'control-label']) !!}
                    {!! Form::text('complemento', old('complemento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('complemento'))
                        <p class="help-block">
                            {{ $errors->first('complemento') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-2 form-group">
                      <label for="">CADASTRO</label>
                        {!! Form::date('datacadastro', old('datacadastro'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('datacadastro'))
                            <p class="help-block">
                                {{ $errors->first('datacadastro') }}
                            </p>
                        @endif
                    </div>
             
                <div class="col-xs-4 form-group">
                    {!! Form::label('foto', trans('quickadmin.empresa.fields.foto').'', ['class' => 'control-label']) !!}
                    {!! Form::file('foto', ['class' => 'form-control']) !!}
                    {!! Form::hidden('foto_max_size', 2) !!}
                    {!! Form::hidden('foto_max_width', 4096) !!}
                    {!! Form::hidden('foto_max_height', 4096) !!}
                    <p class="help-block"></p>
                    @if($errors->has('foto'))
                        <p class="help-block">
                            {{ $errors->first('foto') }}
                        </p>
                    @endif
            
            </div>
            </div>
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
        <i class="fa fa-save"> </i>
    SALVAR</button>
    {{-- {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
  
   <script type="text/javascript">
            //valida o CNPJ digitado
            function ValidarCNPJ(ObjCnpj){
                    var cnpj = ObjCnpj.value;
                    var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
                    var dig1= new Number;
                    var dig2= new Number;
                    
                    exp = /\.|\-|\//g
                    cnpj = cnpj.toString().replace( exp, "" ); 
                    var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));
                            
                    for(i = 0; i<valida.length; i++){
                            dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);  
                            dig2 += cnpj.charAt(i)*valida[i];       
                    }
                    dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
                    dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));
                    
                    if(((dig1*10)+dig2) != digito)  
                            alert('CNPJ Invalido!');
                            
            }
            
            
            function txtBoxFormat(strField, sMask, evtKeyPress) {
                var i, nCount, sValue, fldLen, mskLen,bolMask, sCod, nTecla;
               
                if(document.all) { // Internet Explorer
                    nTecla = evtKeyPress.keyCode;
                }
                else if(document.layers) { // Nestcape
                    nTecla = evtKeyPress.which;
                }
                else if(document.getElementById) { // FireFox
                    nTecla = evtKeyPress.which;
                }
               
                if (nTecla != 8) {
               
                sValue = document.getElementById(strField).value;
               
                // Limpa todos os caracteres de formatação que
                // já estiverem no campo.
                sValue = sValue.toString().replace( "-", "" );
                sValue = sValue.toString().replace( "-", "" );
                sValue = sValue.toString().replace( ".", "" );
                sValue = sValue.toString().replace( ".", "" );
                sValue = sValue.toString().replace( "/", "" );
                sValue = sValue.toString().replace( "/", "" );
                sValue = sValue.toString().replace( "(", "" );
                sValue = sValue.toString().replace( "(", "" );
                sValue = sValue.toString().replace( ")", "" );
                sValue = sValue.toString().replace( ")", "" );
                sValue = sValue.toString().replace( " ", "" );
                sValue = sValue.toString().replace( " ", "" );
                sValue = sValue.toString().replace( ":", "" );
                fldLen = sValue.length;
                mskLen = sMask.length;
               
                i = 0;
                nCount = 0;
                sCod = "";
                mskLen = fldLen;
               
                while (i <= mskLen) {
                bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/"))
                bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))
                bolMask = bolMask || (sMask.charAt(i) == ":")
               
                if (bolMask) {
                sCod += sMask.charAt(i);
                mskLen++; }
                else {
                sCod += sValue.charAt(nCount);
                nCount++;
                }
               
                i++;
                }
               
                //objForm[strField].value = sCod;
                document.getElementById(strField).value = sCod;
               
                if (nTecla != 8) { // backspace
                    if (sMask.charAt(i-1) == "9") { // apenas números...
                        return ((nTecla > 47) && (nTecla < 58)); } // números de 0 a 9
                    else { // qualquer caracter...
                        return true;
                    }
                }
                else {
                    return true;
                }
                }
            }
            $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
        function validarTelefoneOk (){
                                $('#telefone1').keyup(function(){
                        var value = $(this).val();
                        var letras,
                            numeros,
                            letrasMaiusculas,
                            especial;
                                    var t = document.getElementById('telefone1').value;
                        if(/[A-Z,a-z]/gm.test(value)){
                        letras = "entrou"
                        }else{
                            letras = "";
                        }
                      if ( letras == "entrou") {
                        document.getElementById('telefone1').value = "";
                      }else{
                        var telefone= document.getElementById('telefone1').value;
                        if(telefone.length==1){
                         document.getElementById('telefone1').value ='(' + telefone;
                          }
                        else if (telefone.length==3){
                         document.getElementById('telefone1').value = telefone +')';
                        }
                        else if (telefone.length==8){
                         document.getElementById('telefone1').value = telefone +'-';
                        }
                      }                  
                        });
                        $('#fone').keyup(function(){
                        var value = $(this).val();
                        var letras,
                            numeros,
                            letrasMaiusculas,
                            especial;
                                    var t = document.getElementById('fone').value;
                        if(/[A-Z,a-z]/gm.test(value)){
                        letras = "entrou"
                        }else{
                            letras = "";
                        }
                      if ( letras == "entrou") {
                        document.getElementById('fone').value = "";
                      }else{
                        var telefone= document.getElementById('fone').value;
                        if(telefone.length==1){
                         document.getElementById('fone').value ='(' + telefone;
                          }
                        else if (telefone.length==3){
                         document.getElementById('fone').value = telefone +')';
                        }
                        else if (telefone.length==8){
                         document.getElementById('fone').value = telefone +'-';
                        }
                      }                  
                        });
                        
                            }
                            function validarCelularOk (){
                                $('#celular').keyup(function(){
                        var value = $(this).val();
                        var letras,
                            numeros,
                            letrasMaiusculas,
                            especial;
                                    var t = document.getElementById('celular').value;
                        if(/[A-Z,a-z]/gm.test(value)){
                        letras = "entrou"
                        }else{
                            letras = "";
                        }
                      if ( letras == "entrou") {
                        document.getElementById('celular').value = "";
                      }else{
                        var telefone= document.getElementById('celular').value;
                        if(telefone.length==1){
                         document.getElementById('celular').value ='(' + telefone;
                          }
                        else if (telefone.length==3){
                         document.getElementById('celular').value = telefone +')';
                        }
                        else if (telefone.length==9){
                         document.getElementById('celular').value = telefone +'-';
                        }
                      }                  
                        });
                        $('#celular2').keyup(function(){
                        var value = $(this).val();
                        var letras,
                            numeros,
                            letrasMaiusculas,
                            especial;
                                    var t = document.getElementById('celular2').value;
                        if(/[A-Z,a-z]/gm.test(value)){
                        letras = "entrou"
                        }else{
                            letras = "";
                        }
                      if ( letras == "entrou") {
                        document.getElementById('celular2').value = "";
                      }else{
                        var telefone= document.getElementById('celular2').value;
                        if(telefone.length==1){
                         document.getElementById('celular2').value ='(' + telefone;
                          }
                        else if (telefone.length==3){
                         document.getElementById('celular2').value = telefone +')';
                        }
                        else if (telefone.length==9){
                         document.getElementById('celular2').value = telefone +'-';
                        }
                      }                  
                        });
                        
                            }
                            function numeros(){
                          $('#numero').keyup(function(){
                          var value = $(this).val();
                          var letras;
                          if(/[A-Z,a-z]/gm.test(value)){
                          letras = "entrou"
                          }else{
                              letras = "";
                          }
                        if ( letras == "entrou") {
                          document.getElementById('numero').value = "";
                        }else{
                        
                         
                        }                  
                          });
                       }
                       function mascaraCep(){
                          
                          var cep = document.getElementById('cep').value;
                           if(cep.length==5){
                            document.getElementById('cep').value = cep +'-';
                       }
                       $('#cep').keyup(function(){
                          var value = $(this).val();
                          var letras;
                          if(/[A-Z,a-z]/gm.test(value)){
                          letras = "entrou"
                          }else{
                              letras = "";
                        
                          }
                          
                        if ( letras == "entrou") {
                          document.getElementById('cep').value = "";
                        }else{
                        
                         
                        }                  
                          });
                       }
                       function numeros(){
                            $('#txtNumero').keyup(function(){
                            var value = $(this).val();
                            var letras;
                            if(/[A-Z,a-z]/gm.test(value)){
                            letras = "entrou"
                            }else{
                                letras = "";
                            }
                          if ( letras == "entrou") {
                            document.getElementById('txtNumero').value = "";
                          }else{
                          
                           
                          }                  
                            });
                         }
                         function verificauf(){
                            $('#txtUf').keyup(function(){
                            var value = $(this).val();
                            var letras;
                            if(/[0-9]/gm.test(value)){
                            letras = "entrou"
                            }else{
                                letras = "";
                            }
                          if ( letras == "entrou") {
                            document.getElementById('txtUf').value = "";
                          }else{
                          
                           
                          }                  
                            });
                         }
                       function limpa_formulário_cep() {
              //Limpa valores do formulário de cep.
              document.getElementById('endereco').value=("");
              document.getElementById('bairro').value=("");
              document.getElementById('cidade').value=("");
              document.getElementById('uf').value=("");
              // document.getElementById('ibge').value=("");
      }
  
      function meu_callback(conteudo) {
          if (!("erro" in conteudo)) {
              //Atualiza os campos com os valores.
              document.getElementById('endereco').value=(conteudo.logradouro);
              document.getElementById('bairro').value=(conteudo.bairro);
              document.getElementById('cidade').value=(conteudo.localidade);
              document.getElementById('uf').value=(conteudo.uf);
              // document.getElementById('ibge').value=(conteudo.ibge);
          } //end if.
          else {
              //CEP não Encontrado.
              limpa_formulário_cep();
              alert("CEP não encontrado.");
          }
      }
                       function pesquisacep(valor) {
  
  //Nova variável "cep" somente com dígitos.
  var cep = valor.replace(/\D/g, '');
  
  //Verifica se campo cep possui valor informado.
  if (cep != "") {
  
      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;
  
      //Valida o formato do CEP.
      if(validacep.test(cep)) {
  
          //Preenche os campos com "..." enquanto consulta webservice.
          document.getElementById('endereco').value="...";
          document.getElementById('bairro').value="...";
          document.getElementById('cidade').value="...";
          document.getElementById('uf').value="...";
          // document.getElementById('ibge').value="...";
  
          //Cria um elemento javascript.
          var script = document.createElement('script');
  
          //Sincroniza com o callback.
          script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
  
          //Insere script no documento e carrega o conteúdo.
          document.body.appendChild(script);
  
      } //end if.
      else {
          //cep é inválido.
          limpa_formulário_cep();
          alert("Formato de CEP inválido.");
      }
  } //end if.
  else {
      //cep sem valor, limpa formulário.
      limpa_formulário_cep();
  }};
       
            </script>
            
  
            
@stop