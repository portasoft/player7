@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.empresa.title')</h3> --}}
    
    {!! Form::model($empresa, ['method' => 'PUT', 'route' => ['admin.empresas.update', $empresa->id], 'files' => true,]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
          <h4 id="TNome">ALTERAR - EMPRESA</h4>
        </div>

        <div class="panel-body">
                <div class="row">
                        <div class="col-xs-2 form-group">
                            {!! Form::label('cnpj', trans('quickadmin.empresa.fields.cnpj').'', ['class' => 'control-label']) !!}
                            {!! Form::text('cnpj', old('cnpj'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('cnpj'))
                                <p class="help-block">
                                    {{ $errors->first('cnpj') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-3 form-group">
                            {!! Form::label('razao', trans('quickadmin.empresa.fields.razao').'', ['class' => 'control-label']) !!}
                            {!! Form::text('razao', old('razao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('razao'))
                                <p class="help-block">
                                    {{ $errors->first('razao') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-3 form-group">
                                {!! Form::label('fantasia', trans('quickadmin.empresa.fields.fantasia').'', ['class' => 'control-label']) !!}
                                {!! Form::text('fantasia', old('fantasia'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('fantasia'))
                                    <p class="help-block">
                                        {{ $errors->first('fantasia') }}
                                    </p>
                                @endif
                            </div>
                 
                    
                     
                    
                        <div class="col-xs-2 form-group">
                            {!! Form::label('ie', trans('quickadmin.empresa.fields.ie').'', ['class' => 'control-label']) !!}
                            {!! Form::text('ie', old('ie'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('ie'))
                                <p class="help-block">
                                    {{ $errors->first('ie') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-2 form-group">
                            {!! Form::label('im', trans('quickadmin.empresa.fields.im').'', ['class' => 'control-label']) !!}
                            {!! Form::text('im', old('im'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('im'))
                                <p class="help-block">
                                    {{ $errors->first('im') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-2 form-group">
                            {!! Form::label('contato', trans('quickadmin.empresa.fields.contato').'', ['class' => 'control-label']) !!}
                            {!! Form::text('contato', old('contato'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('contato'))
                                <p class="help-block">
                                    {{ $errors->first('contato') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-2 form-group">
                            {!! Form::label('regime', trans('quickadmin.empresa.fields.regime').'', ['class' => 'control-label']) !!}
                            {!! Form::text('regime', old('regime'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('regime'))
                                <p class="help-block">
                                    {{ $errors->first('regime') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-2 form-group">
                            {!! Form::label('fone', trans('quickadmin.empresa.fields.fone').'', ['class' => 'control-label']) !!}
                            {!! Form::text('fone', old('fone'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('fone'))
                                <p class="help-block">
                                    {{ $errors->first('fone') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-2 form-group">
                            {!! Form::label('celular', trans('quickadmin.empresa.fields.celular').'', ['class' => 'control-label']) !!}
                            {!! Form::text('celular', old('celular'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('celular'))
                                <p class="help-block">
                                    {{ $errors->first('celular') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-4 form-group">
                            {!! Form::label('email', trans('quickadmin.empresa.fields.email').'', ['class' => 'control-label']) !!}
                            {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('email'))
                                <p class="help-block">
                                    {{ $errors->first('email') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-2 form-group">
                            {!! Form::label('cep', trans('quickadmin.empresa.fields.cep').'', ['class' => 'control-label']) !!}
                            {!! Form::text('cep', old('cep'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('cep'))
                                <p class="help-block">
                                    {{ $errors->first('cep') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-4 form-group">
                            {!! Form::label('endereco', trans('quickadmin.empresa.fields.endereco').'', ['class' => 'control-label']) !!}
                            {!! Form::text('endereco', old('endereco'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('endereco'))
                                <p class="help-block">
                                    {{ $errors->first('endereco') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-1 form-group">
                            {!! Form::label('numero', trans('quickadmin.empresa.fields.numero').'', ['class' => 'control-label']) !!}
                            {!! Form::text('numero', old('numero'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('numero'))
                                <p class="help-block">
                                    {{ $errors->first('numero') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-3 form-group">
                            {!! Form::label('bairro', trans('quickadmin.empresa.fields.bairro').'', ['class' => 'control-label']) !!}
                            {!! Form::text('bairro', old('bairro'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('bairro'))
                                <p class="help-block">
                                    {{ $errors->first('bairro') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-2 form-group">
                            {!! Form::label('cidade', trans('quickadmin.empresa.fields.cidade').'', ['class' => 'control-label']) !!}
                            {!! Form::text('cidade', old('cidade'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('cidade'))
                                <p class="help-block">
                                    {{ $errors->first('cidade') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-1 form-group">
                            {!! Form::label('uf', trans('quickadmin.empresa.fields.uf').'', ['class' => 'control-label']) !!}
                            {!! Form::text('uf', old('uf'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('uf'))
                                <p class="help-block">
                                    {{ $errors->first('uf') }}
                                </p>
                            @endif
                        </div>
                    
                        <div class="col-xs-5 form-group">
                            {!! Form::label('complemento', trans('quickadmin.empresa.fields.complemento').'', ['class' => 'control-label']) !!}
                            {!! Form::text('complemento', old('complemento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('complemento'))
                                <p class="help-block">
                                    {{ $errors->first('complemento') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-2 form-group">
                            <label for="">CADASTRO</label>
                                {!! Form::date('datacadastro', old('datacadastro'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('datacadastro'))
                                    <p class="help-block">
                                        {{ $errors->first('datacadastro') }}
                                    </p>
                                @endif
                            </div>
            <div class="row">
                <div class="col-xs-4 form-group">
                
                    {!! Form::label('foto', trans('quickadmin.empresa.fields.foto').'', ['class' => 'control-label']) !!}
                    {!! Form::file('foto', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                    {!! Form::hidden('foto_max_size', 2) !!}
                    {!! Form::hidden('foto_max_width', 4096) !!}
                    {!! Form::hidden('foto_max_height', 4096) !!}
                    @if ($empresa->foto)
                    <a href="{{ asset(env('UPLOAD_PATH').'/'.$empresa->foto) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/'.$empresa->foto) }}"></a>
                @endif
                    <p class="help-block"></p>
                    @if($errors->has('foto'))
                        <p class="help-block">
                            {{ $errors->first('foto') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
        <i class="fa fa-refresh"> </i>
    ALTERAR</button>
    {{-- {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop