@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.empresa.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
                <h4 id="TNome">VIZUALIZAR - EMPRESA</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.cnpj')</th>
                            <td field-key='cnpj'>{{ $empresa->cnpj }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.razao')</th>
                            <td field-key='razao'>{{ $empresa->razao }}</td>
                        </tr>
                        <tr>
                            <th>CADASTRO</th>
                            <td field-key='datacadastro'>{{ $empresa->datacadastro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.fantasia')</th>
                            <td field-key='fantasia'>{{ $empresa->fantasia }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.ie')</th>
                            <td field-key='ie'>{{ $empresa->ie }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.im')</th>
                            <td field-key='im'>{{ $empresa->im }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.contato')</th>
                            <td field-key='contato'>{{ $empresa->contato }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.regime')</th>
                            <td field-key='regime'>{{ $empresa->regime }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.fone')</th>
                            <td field-key='fone'>{{ $empresa->fone }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.celular')</th>
                            <td field-key='celular'>{{ $empresa->celular }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.email')</th>
                            <td field-key='email'>{{ $empresa->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.cep')</th>
                            <td field-key='cep'>{{ $empresa->cep }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.endereco')</th>
                            <td field-key='endereco'>{{ $empresa->endereco }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.numero')</th>
                            <td field-key='numero'>{{ $empresa->numero }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.bairro')</th>
                            <td field-key='bairro'>{{ $empresa->bairro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.cidade')</th>
                            <td field-key='cidade'>{{ $empresa->cidade }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.uf')</th>
                            <td field-key='uf'>{{ $empresa->uf }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.complemento')</th>
                            <td field-key='complemento'>{{ $empresa->complemento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.empresa.fields.foto')</th>
                            <td field-key='foto'>@if($empresa->foto)<a href="{{ asset(env('UPLOAD_PATH').'/' . $empresa->foto) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $empresa->foto) }}"/></a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.empresas.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
