@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.pelagem.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
          <h4>Vizualização de pelagem</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.pelagem.fields.descricao')</th>
                            <td field-key='descricao'>{{ $pelagem->descricao }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.pelagems.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
