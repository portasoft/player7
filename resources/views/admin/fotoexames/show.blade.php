@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.fotoexames.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
                <h4>Vizualização de foto de exames</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.fotoexames.fields.exame')</th>
                            <td field-key='exame'>{{ $fotoexame->exame }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fotoexames.fields.foto')</th>
                            <td field-key='foto'>@if($fotoexame->foto)<a href="{{ asset(env('UPLOAD_PATH').'/' . $fotoexame->foto) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $fotoexame->foto) }}"/></a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fotoexames.fields.anexo')</th>
                            <td field-key='anexo'>@if($fotoexame->anexo)<a href="{{ asset(env('UPLOAD_PATH').'/' . $fotoexame->anexo) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fotoexames.fields.obs')</th>
                            <td field-key='obs'>{{ $fotoexame->obs }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.fotoexames.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
