@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.fotoexames.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.fotoexames.store'], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
     <h4>Cadastro foto de exames</h4>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4 form-group">
                    {!! Form::label('exame', trans('quickadmin.fotoexames.fields.exame').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('exame', old('exame'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('exame'))
                        <p class="help-block">
                            {{ $errors->first('exame') }}
                        </p>
                    @endif
                </div>
          
              
          
                <div class="col-xs-4 form-group">
                    {!! Form::label('anexo', trans('quickadmin.fotoexames.fields.anexo').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('anexo', old('anexo')) !!}
                    {!! Form::file('anexo', ['class' => 'form-control']) !!}
                    {!! Form::hidden('anexo_max_size', 2) !!}
                    <p class="help-block"></p>
                    @if($errors->has('anexo'))
                        <p class="help-block">
                            {{ $errors->first('anexo') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-4 form-group">
                        {!! Form::label('foto', trans('quickadmin.fotoexames.fields.foto').'', ['class' => 'control-label']) !!}
                        {!! Form::file('foto', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                        {!! Form::hidden('foto_max_size', 2) !!}
                        {!! Form::hidden('foto_max_width', 4096) !!}
                        {!! Form::hidden('foto_max_height', 4096) !!}
                        <p class="help-block"></p>
                        @if($errors->has('foto'))
                            <p class="help-block">
                                {{ $errors->first('foto') }}
                            </p>
                        @endif
                    </div>
                <div class="col-xs-12 form-group">
                    {!! Form::label('obs', trans('quickadmin.fotoexames.fields.obs').'', ['class' => 'control-label']) !!}
                    {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('obs'))
                        <p class="help-block">
                            {{ $errors->first('obs') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

