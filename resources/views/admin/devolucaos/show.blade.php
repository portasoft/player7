@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.devolucao.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.devolucao.fields.data')</th>
                            <td field-key='data'>{{ $devolucao->data }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.devolucao.fields.fornecedor')</th>
                            <td field-key='fornecedor'>{{ $devolucao->fornecedor }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.devolucao.fields.motivo')</th>
                            <td field-key='motivo'>{{ $devolucao->motivo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.devolucao.fields.produto')</th>
                            <td field-key='produto'>{{ $devolucao->produto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.devolucao.fields.obs')</th>
                            <td field-key='obs'>{{ $devolucao->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.devolucao.fields.anexo')</th>
                            <td field-key='anexo'>@if($devolucao->anexo)<a href="{{ asset(env('UPLOAD_PATH').'/' . $devolucao->anexo) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.devolucaos.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
