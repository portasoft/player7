@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.deposito.title')</h3> --}}
    
    {!! Form::model($deposito, ['method' => 'PUT', 'route' => ['admin.depositos.update', $deposito->id]]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
        <h4 id="TNome">EDITAR - DEPÓSITO</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>ALTERADO COM SUCESSO!</h4>
         </div> 
        @endif
        <div class="panel-body">
                <div class="row">
                        <div class="col-xs-4 form-group">
                            {!! Form::label('descricao', trans('quickadmin.deposito.fields.descricao').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('descricao', old('descricao'), ['class' => 'form-control','autofocus', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('descricao'))
                                <p class="help-block">
                                    {{ $errors->first('descricao') }}
                                </p>
                            @endif
                        </div>
                          <div class="col-xs-4 form-group">
                            {!! Form::label('empresa', trans('quickadmin.deposito.fields.empresa').'*', ['class' => 'control-label']) !!}
                            {!! Form::select('empresa', $empresa,null, ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('empresa'))
                                <p class="help-block">
                                    {{ $errors->first('empresa') }}
                                </p>
                            @endif
                        </div>
                    </div>
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
            <i class="fa fa-refresh"> </i>
        ALTERAR</button>
    {{-- {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
    @can('deposito_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.depositos.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.depositos.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
        <h4 id="TNome">RELATÓRIO - DEPÓSITO</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('deposito_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('deposito_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.deposito.fields.descricao')</th>
                        <th>@lang('quickadmin.deposito.fields.empresa')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
    <script>
        @can('deposito_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.depositos.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.depositos.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('deposito_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'descricao', name: 'descricao'},
                {data: 'emp', name: 'emp'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>    
@stop

