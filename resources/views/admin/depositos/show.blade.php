@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.deposito.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
            <h4 id="TNome">VIZUALIZAR - DEPÓSITO</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.deposito.fields.descricao')</th>
                            <td field-key='descricao'>{{ $deposito->descricao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.deposito.fields.empresa')</th>
                            <td field-key='empresa'>{{ $deposito->empresa }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.depositos.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
