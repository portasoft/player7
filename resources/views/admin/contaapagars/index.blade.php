@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contaapagar.title')</h3> --}}
    @can('contaapagar_create')
    <p>
        <a href="{{ route('admin.contaapagars.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('contaapagar_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.contaapagars.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.contaapagars.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
           <h4>RELATÓRIO - CONTAS A PAGAR</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('contaapagar_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('contaapagar_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.contaapagar.fields.fornecedor')</th>
                        <th>@lang('quickadmin.contaapagar.fields.vencimento')</th>
                        <th>@lang('quickadmin.contaapagar.fields.valorconta')</th>
                        <th>@lang('quickadmin.contaapagar.fields.valorapagar')</th>
                        <th>@lang('quickadmin.contaapagar.fields.parcela')</th>
                        <th>@lang('quickadmin.contaapagar.fields.formapagamento')</th>
                        <th>@lang('quickadmin.contaapagar.fields.contacontabil')</th>
                        <th>@lang('quickadmin.contaapagar.fields.docmercantil')</th>
                        <th>@lang('quickadmin.contaapagar.fields.contacorrente')</th>
                        <th>@lang('quickadmin.contaapagar.fields.documento')</th>
                        <th>@lang('quickadmin.contaapagar.fields.colaborador')</th>
                        <th>@lang('quickadmin.contaapagar.fields.obs')</th>
                        <th>@lang('quickadmin.contaapagar.fields.tipo')</th>
                        <th>@lang('quickadmin.contaapagar.fields.baixado')</th>
                        <th>@lang('quickadmin.contaapagar.fields.databaixa')</th>
                        <th>@lang('quickadmin.contaapagar.fields.juros')</th>
                        <th>@lang('quickadmin.contaapagar.fields.desconto')</th>
                        <th>@lang('quickadmin.contaapagar.fields.dataemissao')</th>
                        <th>@lang('quickadmin.contaapagar.fields.anexo')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('contaapagar_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.contaapagars.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.contaapagars.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('contaapagar_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'fornecedor', name: 'fornecedor'},
                {data: 'vencimento', name: 'vencimento'},
                {data: 'valorconta', name: 'valorconta'},
                {data: 'valorapagar', name: 'valorapagar'},
                {data: 'parcela', name: 'parcela'},
                {data: 'formapagamento', name: 'formapagamento'},
                {data: 'contacontabil', name: 'contacontabil'},
                {data: 'docmercantil', name: 'docmercantil'},
                {data: 'contacorrente', name: 'contacorrente'},
                {data: 'documento', name: 'documento'},
                {data: 'colaborador', name: 'colaborador'},
                {data: 'obs', name: 'obs'},
                {data: 'tipo', name: 'tipo'},
                {data: 'baixado', name: 'baixado'},
                {data: 'databaixa', name: 'databaixa'},
                {data: 'juros', name: 'juros'},
                {data: 'desconto', name: 'desconto'},
                {data: 'dataemissao', name: 'dataemissao'},
                {data: 'anexo', name: 'anexo'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection