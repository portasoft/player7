@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contaapagar.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
         <h4>VIZUALIZAÇÃO - CONTAS A PAGAR</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.fornecedor')</th>
                            <td field-key='fornecedor'>{{ $contaapagar->fornecedor }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.vencimento')</th>
                            <td field-key='vencimento'>{{ $contaapagar->vencimento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.valorconta')</th>
                            <td field-key='valorconta'>{{ $contaapagar->valorconta }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.valorapagar')</th>
                            <td field-key='valorapagar'>{{ $contaapagar->valorapagar }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.parcela')</th>
                            <td field-key='parcela'>{{ $contaapagar->parcela }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.formapagamento')</th>
                            <td field-key='formapagamento'>{{ $contaapagar->formapagamento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.contacontabil')</th>
                            <td field-key='contacontabil'>{{ $contaapagar->contacontabil }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.docmercantil')</th>
                            <td field-key='docmercantil'>{{ $contaapagar->docmercantil }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.contacorrente')</th>
                            <td field-key='contacorrente'>{{ $contaapagar->contacorrente }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.documento')</th>
                            <td field-key='documento'>{{ $contaapagar->documento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.colaborador')</th>
                            <td field-key='colaborador'>{{ $contaapagar->colaborador }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.obs')</th>
                            <td field-key='obs'>{{ $contaapagar->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.tipo')</th>
                            <td field-key='tipo'>{{ $contaapagar->tipo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.baixado')</th>
                            <td field-key='baixado'>{{ $contaapagar->baixado }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.databaixa')</th>
                            <td field-key='databaixa'>{{ $contaapagar->databaixa }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.juros')</th>
                            <td field-key='juros'>{{ $contaapagar->juros }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.desconto')</th>
                            <td field-key='desconto'>{{ $contaapagar->desconto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.dataemissao')</th>
                            <td field-key='dataemissao'>{{ $contaapagar->dataemissao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contaapagar.fields.anexo')</th>
                            <td field-key='anexo'>@if($contaapagar->anexo)<a href="{{ asset(env('UPLOAD_PATH').'/' . $contaapagar->anexo) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.contaapagars.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
