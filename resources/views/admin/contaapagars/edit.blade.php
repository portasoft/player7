@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contaapagar.title')</h3> --}}
    
    {!! Form::model($contaapagar, ['method' => 'PUT', 'route' => ['admin.contaapagars.update', $contaapagar->id], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
           <h4>EDITAR - CONTAS A PAGAR</h4>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3 form-group">
                    {!! Form::label('fornecedor', trans('quickadmin.contaapagar.fields.fornecedor').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('fornecedor', old('fornecedor'), ['class' => 'form-control','id'=>'fornecedor','name'=>'fornecedor', 'placeholder' => '', 'required' => '']) !!}
                <div id="fornecedorlist"></div>
                    <p class="help-block"></p>
                    @if($errors->has('fornecedor'))
                        <p class="help-block">
                            {{ $errors->first('fornecedor') }}
                        </p>
                    @endif
                </div>
     
                <div class="col-xs-2 form-group">
                    {!! Form::label('vencimento', trans('quickadmin.contaapagar.fields.vencimento').'*', ['class' => 'control-label']) !!}
                    {!! Form::date('vencimento', old('vencimento'), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('vencimento'))
                        <p class="help-block">
                            {{ $errors->first('vencimento') }}
                        </p>
                    @endif
                </div>
       
                <div class="col-xs-2 form-group">
                    {!! Form::label('valorconta', trans('quickadmin.contaapagar.fields.valorconta').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('valorconta', old('valorconta'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorconta'))
                        <p class="help-block">
                            {{ $errors->first('valorconta') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-1 form-group">
                        {!! Form::label('parcela', trans('quickadmin.contaapagar.fields.parcela').'*', ['class' => 'control-label']) !!}
                        {!! Form::number('parcela', old('parcela'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('parcela'))
                            <p class="help-block">
                                {{ $errors->first('parcela') }}
                            </p>
                        @endif
                    </div>
            {{-- <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valorapagar', trans('quickadmin.contaapagar.fields.valorapagar').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valorapagar', old('valorapagar'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorapagar'))
                        <p class="help-block">
                            {{ $errors->first('valorapagar') }}
                        </p>
                    @endif
                </div>
            </div> --}}
            <div class="col-xs-2 form-group">
                    {!! Form::label('juros', trans('quickadmin.contaapagar.fields.juros').'', ['class' => 'control-label']) !!}
                    {!! Form::text('juros', old('juros'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('juros'))
                        <p class="help-block">
                            {{ $errors->first('juros') }}
                        </p>
                    @endif
                </div>
     
       
                <div class="col-xs-2 form-group">
                    {!! Form::label('desconto', trans('quickadmin.contaapagar.fields.desconto').'', ['class' => 'control-label']) !!}
                    {!! Form::text('desconto', old('desconto'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('desconto'))
                        <p class="help-block">
                            {{ $errors->first('desconto') }}
                        </p>
                    @endif
                </div>
            
         
         
                <div class="col-xs-2 form-group">
                    {!! Form::label('formapagamento', trans('quickadmin.contaapagar.fields.formapagamento').'', ['class' => 'control-label']) !!}
                    {!! Form::select('formapagamento',['Dinheiro','Cartão','Boleto','Cheque','Depósito'],null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('formapagamento'))
                        <p class="help-block">
                            {{ $errors->first('formapagamento') }}
                        </p>
                    @endif
                </div>
     
       
                <div class="col-xs-3 form-group">
                    {!! Form::label('contacontabil', trans('quickadmin.contaapagar.fields.contacontabil').'', ['class' => 'control-label']) !!}
                    {!! Form::text('contacontabil', old('contacontabil'), ['class' => 'form-control', 'id'=>'contacontabil','name'=>'contacontabil', 'placeholder' => '']) !!}
                    <div id="contacontabillist"></div>
                    <p class="help-block"></p>
                    @if($errors->has('contacontabil'))
                        <p class="help-block">
                            {{ $errors->first('contacontabil') }}
                        </p>
                    @endif
                </div>
        
            {{-- <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('docmercantil', trans('quickadmin.contaapagar.fields.docmercantil').'', ['class' => 'control-label']) !!}
                    {!! Form::text('docmercantil', old('docmercantil'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('docmercantil'))
                        <p class="help-block">
                            {{ $errors->first('docmercantil') }}
                        </p>
                    @endif
                </div>
            </div> --}}
          
                <div class="col-xs-3 form-group">
                    {!! Form::label('contacorrente', trans('quickadmin.contaapagar.fields.contacorrente').'', ['class' => 'control-label']) !!}
                    {!! Form::select('contacorrente', $contacorrente,null, ['class' => 'form-control', 'id'=>'contacorrente','name'=>'contacorrente','placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('contacorrente'))
                        <p class="help-block">
                            {{ $errors->first('contacorrente') }}
                        </p>
                    @endif
                </div>
        
         
                <div class="col-xs-2 form-group">
                    {!! Form::label('documento', trans('quickadmin.contaapagar.fields.documento').'', ['class' => 'control-label']) !!}
                    {!! Form::text('documento', old('documento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('documento'))
                        <p class="help-block">
                            {{ $errors->first('documento') }}
                        </p>
                    @endif
                </div>
      
                <div class="col-xs-2 form-group">
                        {!! Form::label('dataemissao', trans('quickadmin.contaapagar.fields.dataemissao').'', ['class' => 'control-label']) !!}
                        {!! Form::date('dataemissao', old('dataemissao'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('dataemissao'))
                            <p class="help-block">
                                {{ $errors->first('dataemissao') }}
                            </p>
                        @endif
                    </div>
        
                <div class="col-xs-3 form-group">
                    {!! Form::label('colaborador', trans('quickadmin.contaapagar.fields.colaborador').'', ['class' => 'control-label']) !!}
                    {!! Form::text('colaborador', old('colaborador'), ['class' => 'form-control','id'=>'usuario','name'=>'usuario', 'placeholder' => '']) !!}
                    <div id="usuariolist"></div>
                    <p class="help-block"></p>
                    @if($errors->has('colaborador'))
                        <p class="help-block">
                            {{ $errors->first('colaborador') }}
                        </p>
                    @endif
                </div>
           
            
            
            {{-- <div class="row">
                <div class="col-xs-2 form-group">
                    {!! Form::label('tipo', trans('quickadmin.contaapagar.fields.tipo').'', ['class' => 'control-label']) !!}
                    {!! Form::text('tipo', old('tipo'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tipo'))
                        <p class="help-block">
                            {{ $errors->first('tipo') }}
                        </p>
                    @endif
                </div>
            </div> --}}
       
                <div class="col-xs-2 form-group">
                    {!! Form::label('baixado', trans('quickadmin.contaapagar.fields.baixado').'', ['class' => 'control-label']) !!}
                    {!! Form::select('baixado',['Sim','Não'], null,['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('baixado'))
                        <p class="help-block">
                            {{ $errors->first('baixado') }}
                        </p>
                    @endif
                </div>
   
      
                <div class="col-xs-2 form-group">
                    {!! Form::label('databaixa', trans('quickadmin.contaapagar.fields.databaixa').'', ['class' => 'control-label']) !!}
                    {!! Form::date('databaixa', old('databaixa'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('databaixa'))
                        <p class="help-block">
                            {{ $errors->first('databaixa') }}
                        </p>
                    @endif
                </div>
       
         
            
 
        
     
                <div class="col-xs-5 form-group">
                    {!! Form::label('anexo', trans('quickadmin.contaapagar.fields.anexo').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('anexo', old('anexo')) !!}
                    {!! Form::file('anexo', ['class' => 'form-control']) !!}
                    {!! Form::hidden('anexo_max_size', 2) !!}
                    <p class="help-block"></p>
                    @if($errors->has('anexo'))
                        <p class="help-block">
                            {{ $errors->first('anexo') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-12 form-group">
                        {!! Form::label('obs', trans('quickadmin.contaapagar.fields.obs').'', ['class' => 'control-label']) !!}
                        {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('obs'))
                            <p class="help-block">
                                {{ $errors->first('obs') }}
                            </p>
                        @endif
                    </div>
            </div>
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
     <script type="text/javascript">
        $(document).ready(function(){
                 $('#fornecedor').keyup(function(){
                     var query = $(this).val();
                      if(query.text != '')
                     {
                       var _token = $('input[name="_token"]').val();
                            $.ajax({
                             url:"{{ route('contaapagars.searchfornecedor')}}",
                             method:"POST",
                             data:{query:query,_token:_token},
                             success:function(data)
                             {
                                 $('#fornecedorlist').fadeIn();
                                 $('#fornecedorlist').html(data);
                             }
                         })
                     }
                 });
                 $(document).on('click','li.n',function(){
                                     
                         $('#fornecedor').val($(this).text());
                         $('#fornecedorlist').fadeOut()
                 
                      })
             });
    </script>
     <script type="text/javascript">
        $(document).ready(function(){
                 $('#usuario').keyup(function(){
                     var query = $(this).val();
                      if(query.text != '')
                     {
                       var _token = $('input[name="_token"]').val();
                            $.ajax({
                             url:"{{ route('contaapagars.searchusuario')}}",
                             method:"POST",
                             data:{query:query,_token:_token},
                             success:function(data)
                             {
                                 $('#usuariolist').fadeIn();
                                 $('#usuariolist').html(data);
                             }
                         })
                     }
                 });
                 $(document).on('click','li.us',function(){
                                     
                         $('#usuario').val($(this).text());
                         $('#usuariolist').fadeOut()
                 
                      })
             });
    </script>
     <script type="text/javascript">
        $(document).ready(function(){
                 $('#contacontabil').keyup(function(){
                     var query = $(this).val();
                      if(query.text != '')
                     {
                       var _token = $('input[name="_token"]').val();
                            $.ajax({
                             url:"{{ route('contaapagars.searchcontacontabil')}}",
                             method:"POST",
                             data:{query:query,_token:_token},
                             success:function(data)
                             {
                                 $('#contacontabillist').fadeIn();
                                 $('#contacontabillist').html(data);
                             }
                         })
                     }
                 });
                 $(document).on('click','li.co',function(){
                                     
                         $('#contacontabil').val($(this).text());
                         $('#contacontabillist').fadeOut()
                 
                      })
             });
    </script>
            
@stop