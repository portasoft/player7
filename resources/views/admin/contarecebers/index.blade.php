@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contareceber.title')</h3> --}}
    @can('contareceber_create')
    <p>
        <a href="{{ route('admin.contarecebers.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('contareceber_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.contarecebers.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.contarecebers.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
      <h4>RELATÓRIO - CONTAS A RECEBER</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('contareceber_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('contareceber_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.contareceber.fields.vencimento')</th>
                        <th>@lang('quickadmin.contareceber.fields.valorconta')</th>
                        <th>@lang('quickadmin.contareceber.fields.valorapagar')</th>
                        <th>@lang('quickadmin.contareceber.fields.valorpago')</th>
                        <th>@lang('quickadmin.contareceber.fields.parcela')</th>
                        <th>@lang('quickadmin.contareceber.fields.formapagamento')</th>
                        <th>@lang('quickadmin.contareceber.fields.docmercantil')</th>
                        <th>@lang('quickadmin.contareceber.fields.documento')</th>
                        <th>@lang('quickadmin.contareceber.fields.obs')</th>
                        <th>@lang('quickadmin.contareceber.fields.tipo')</th>
                        <th>@lang('quickadmin.contareceber.fields.baixado')</th>
                        <th>@lang('quickadmin.contareceber.fields.juros')</th>
                        <th>@lang('quickadmin.contareceber.fields.desconto')</th>
                        <th>@lang('quickadmin.contareceber.fields.dataemissao')</th>
                        <th>@lang('quickadmin.contareceber.fields.cliente')</th>
                        <th>@lang('quickadmin.contareceber.fields.formapagamentobaixa')</th>
                        <th>@lang('quickadmin.contareceber.fields.contacontabil')</th>
                        <th>@lang('quickadmin.contareceber.fields.colaborador')</th>
                        <th>@lang('quickadmin.contareceber.fields.contacorrente')</th>
                        <th>@lang('quickadmin.contareceber.fields.anexo')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('contareceber_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.contarecebers.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.contarecebers.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('contareceber_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'vencimento', name: 'vencimento'},
                {data: 'valorconta', name: 'valorconta'},
                {data: 'valorapagar', name: 'valorapagar'},
                {data: 'valorpago', name: 'valorpago'},
                {data: 'parcela', name: 'parcela'},
                {data: 'formapagamento', name: 'formapagamento'},
                {data: 'docmercantil', name: 'docmercantil'},
                {data: 'documento', name: 'documento'},
                {data: 'obs', name: 'obs'},
                {data: 'tipo', name: 'tipo'},
                {data: 'baixado', name: 'baixado'},
                {data: 'juros', name: 'juros'},
                {data: 'desconto', name: 'desconto'},
                {data: 'dataemissao', name: 'dataemissao'},
                {data: 'cliente', name: 'cliente'},
                {data: 'formapagamentobaixa', name: 'formapagamentobaixa'},
                {data: 'contacontabil', name: 'contacontabil'},
                {data: 'colaborador', name: 'colaborador'},
                {data: 'contacorrente', name: 'contacorrente'},
                {data: 'anexo', name: 'anexo'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection