@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contareceber.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
           <h4>VIZUALIZAÇÃO - CONTAS A RECEBER</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.vencimento')</th>
                            <td field-key='vencimento'>{{ $contareceber->vencimento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.valorconta')</th>
                            <td field-key='valorconta'>{{ $contareceber->valorconta }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.valorapagar')</th>
                            <td field-key='valorapagar'>{{ $contareceber->valorapagar }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.valorpago')</th>
                            <td field-key='valorpago'>{{ $contareceber->valorpago }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.parcela')</th>
                            <td field-key='parcela'>{{ $contareceber->parcela }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.formapagamento')</th>
                            <td field-key='formapagamento'>{{ $contareceber->formapagamento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.docmercantil')</th>
                            <td field-key='docmercantil'>{{ $contareceber->docmercantil }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.documento')</th>
                            <td field-key='documento'>{{ $contareceber->documento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.obs')</th>
                            <td field-key='obs'>{{ $contareceber->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.tipo')</th>
                            <td field-key='tipo'>{{ $contareceber->tipo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.baixado')</th>
                            <td field-key='baixado'>{{ $contareceber->baixado }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.juros')</th>
                            <td field-key='juros'>{{ $contareceber->juros }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.desconto')</th>
                            <td field-key='desconto'>{{ $contareceber->desconto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.dataemissao')</th>
                            <td field-key='dataemissao'>{{ $contareceber->dataemissao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.cliente')</th>
                            <td field-key='cliente'>{{ $contareceber->cliente }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.formapagamentobaixa')</th>
                            <td field-key='formapagamentobaixa'>{{ $contareceber->formapagamentobaixa }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.contacontabil')</th>
                            <td field-key='contacontabil'>{{ $contareceber->contacontabil }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.colaborador')</th>
                            <td field-key='colaborador'>{{ $contareceber->colaborador }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.contacorrente')</th>
                            <td field-key='contacorrente'>{{ $contareceber->contacorrente }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contareceber.fields.anexo')</th>
                            <td field-key='anexo'>@if($contareceber->anexo)<a href="{{ asset(env('UPLOAD_PATH').'/' . $contareceber->anexo) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.contarecebers.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
