@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.agendatelefonica.title')</h3> --}}
    
    {!! Form::model($agendatelefonica, ['method' => 'PUT', 'route' => ['admin.agendatelefonicas.update', $agendatelefonica->id]]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
        <h4 id="TNome">ALTERAR - AGENDA TELEFÔNICA</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>ALTERADO COM SUCESSO!</h4>
         </div> 
        @endif
     
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3 form-group" >
                        <label for="">TIPO</label>
                         {!! Form::select('tipo', ['FISICA','JURIDICA'],'null', ['class' => 'form-control']) !!}
                         <p class="help-block"></p>
                         @if($errors->has('tipo'))
                             <p class="help-block">
                                 {{ $errors->first('tipo') }}
                             </p>
                         @endif
                     </div>
            <div class="col-xs-6 form-group">
                <label for="">NOME</label>
                {!! Form::text('nome', old('nome'), ['class' => 'form-control', 'required' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('nome'))
                    <p class="help-block">
                        {{ $errors->first('nome') }}
                    </p>
                @endif
            </div>
            
            <div class="col-xs-3 form-group">
              <label for="">NASCIMENTO</label>
                {!! Form::date('dataaniversario', old('dataaniversario'), ['class' => 'form-control date']) !!}
                <p class="help-block"></p>
                @if($errors->has('dataaniversario'))
                    <p class="help-block">
                        {{ $errors->first('dataaniversario') }}
                    </p>
                @endif
            </div>
            <div class="col-xs-3 form-group">

                {!! Form::label('operadora', trans('quickadmin.agendatelefonica.fields.operadora').'', ['class' => 'control-label']) !!}
                {!! Form::select('operadora',['CLARO'=> 'CLARO','TIM'=>'TIM','VIVO'=>'VIVO','CTBC'=>'CTBC','OI'=>'OI','EMBRATEL'=>'EMBRATEL'],'null', ['class' => 'form-control',  'required' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('operadora'))
                    <p class="help-block">
                        {{ $errors->first('operadora') }}
                    </p>
                @endif
            </div>
            <div class="col-xs-3 form-group">
                <label for="">TELEFONE</label>
                    {!! Form::text('telefone1', old('telefone1'), ['class' => 'form-control','id'=>'telefone1','onkeypress'=>'validarTelefoneOk()',
                         'maxlength'=>'13']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('telefone1'))
                        <p class="help-block">
                            {{ $errors->first('telefone1') }}
                        </p>
                    @endif
                </div>
                {{-- <div class="col-xs-2 form-group"style="width:165px;">
                        {!! Form::label('telefone2', trans('quickadmin.agendatelefonica.fields.telefone2').'', ['class' => 'control-label']) !!}
                        {!! Form::text('telefone2', old('telefone2'), ['class' => 'form-control', 'placeholder' => '','id'=>'telefone2','onkeypress'=>'validarTelefoneOk()',
                         'maxlength'=>'13']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('telefone2'))
                            <p class="help-block">
                                {{ $errors->first('telefone2') }}
                            </p>
                        @endif
                    </div> --}}
  
      
            <div class="col-xs-3 form-group">
                <label for="">CELULAR</label>
                {!! Form::text('celular1', old('celular1'), ['class' => 'form-control','id'=>'celular1','onkeypress'=>'validarCelularOk()',
                         'maxlength'=>'14']) !!}
                <p class="help-block"></p>
                @if($errors->has('celular1'))
                    <p class="help-block">
                        {{ $errors->first('celular1') }}
                    </p>
                @endif
            </div>
               
            <div class="col-xs-3 form-group">
                <label for="">CADASTRO</label>
                {!! Form::date('dataabertura', old('dataabertura'), ['class' => 'form-control date']) !!}
                <p class="help-block"></p>
                @if($errors->has('dataabertura'))
                    <p class="help-block">
                        {{ $errors->first('dataabertura') }}
                    </p>
                @endif
            </div>
            {{-- <div class="col-xs-2 form-group">
                {!! Form::label('celular2', trans('quickadmin.agendatelefonica.fields.celular2').'', ['class' => 'control-label']) !!}
                {!! Form::text('celular2', old('celular2'), ['class' => 'form-control', 'placeholder' => '','id'=>'celular2','onkeypress'=>'validarCelularOk()',
                         'maxlength'=>'14']) !!}
                <p class="help-block"></p>
                @if($errors->has('celular2'))
                    <p class="help-block">
                        {{ $errors->first('celular2') }}
                    </p>
                @endif
            </div> --}}
      
      
            <div class="col-xs-4 form-group">
           <label for="">E-MAIL</label>
                {!! Form::text('emailpessoal', old('emailpessoal'), ['class' => 'form-control']) !!}
                <p class="help-block"></p>
                @if($errors->has('emailpessoal'))
                    <p class="help-block">
                        {{ $errors->first('emailpessoal') }}
                    </p>
                @endif
            </div>
      
            {{-- <div class="col-xs-4 form-group">
                {!! Form::label('emailcomercial', trans('quickadmin.agendatelefonica.fields.emailcomercial').'', ['class' => 'control-label']) !!}
                {!! Form::text('emailcomercial', old('emailcomercial'), ['class' => 'form-control', 'placeholder' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('emailcomercial'))
                    <p class="help-block">
                        {{ $errors->first('emailcomercial') }}
                    </p>
                @endif
            </div>
       --}}
       
      
       
      
            {{-- <div class="col-xs-2 form-group">
                {!! Form::label('fundacao', trans('quickadmin.agendatelefonica.fields.fundacao').'', ['class' => 'control-label']) !!}
                {!! Form::text('fundacao', old('fundacao'), ['class' => 'form-control', 'placeholder' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('fundacao'))
                    <p class="help-block">
                        {{ $errors->first('fundacao') }}
                    </p>
                @endif
            </div> --}}
            <div class="col-xs-3 form-group">
                {!! Form::label('cep', trans('quickadmin.agendatelefonica.fields.cep').'', ['class' => 'control-label']) !!}
                {!! Form::text('cep', old('cep'), ['class' => 'form-control','id'=>'txtCep','onblur'=>'pesquisacep(this.value)','onkeypress'=>'mascaraCep()' ,'maxlength'=>'9']) !!}
                <p class="help-block"></p>
                @if($errors->has('cep'))
                    <p class="help-block">
                        {{ $errors->first('cep') }}
                    </p>
                @endif
            </div>
       
            <div class="col-xs-5 form-group">
                {!! Form::label('endereco', trans('quickadmin.agendatelefonica.fields.endereco').'', ['class' => 'control-label']) !!}
                {!! Form::text('endereco', old('endereco'), ['class' => 'form-control', 'id'=>'txtRua']) !!}
                <p class="help-block"></p>
                @if($errors->has('endereco'))
                    <p class="help-block">
                        {{ $errors->first('endereco') }}
                    </p>
                @endif
            </div>
      
      
            <div class="col-xs-2 form-group">
                {!! Form::label('numero', trans('quickadmin.agendatelefonica.fields.numero').'', ['class' => 'control-label']) !!}
                {!! Form::text('numero', old('numero'), ['class' => 'form-control', 'id'=>'txtNumero','onkeypress'=>'numeros()','maxlength'=>'5']) !!}
                <p class="help-block"></p>
                @if($errors->has('numero'))
                    <p class="help-block">
                        {{ $errors->first('numero') }}
                    </p>
                @endif
            </div>
      
            <div class="col-xs-4 form-group">
                {!! Form::label('bairro', trans('quickadmin.agendatelefonica.fields.bairro').'', ['class' => 'control-label']) !!}
                {!! Form::text('bairro', old('bairro'), ['class' => 'form-control', 'id'=>'txtBairro']) !!}
                <p class="help-block"></p>
                @if($errors->has('bairro'))
                    <p class="help-block">
                        {{ $errors->first('bairro') }}
                    </p>
                @endif
            </div>
     
            <div class="col-xs-4 form-group">
                {!! Form::label('cidade', trans('quickadmin.agendatelefonica.fields.cidade').'', ['class' => 'control-label']) !!}
                {!! Form::text('cidade', old('cidade'), ['class' => 'form-control',  'id'=>'txtCidade']) !!}
                <p class="help-block"></p>
                @if($errors->has('cidade'))
                    <p class="help-block">
                        {{ $errors->first('cidade') }}
                    </p>
                @endif
            </div>
      
            <div class="col-xs-2 form-group">
                {!! Form::label('uf', trans('quickadmin.agendatelefonica.fields.uf').'', ['class' => 'control-label']) !!}
                {!! Form::text('uf', old('uf'), ['class' => 'form-control', 'id'=>'txtUf','onkeypress'=>'verificauf()','maxlength'=>'2']) !!}
                <p class="help-block"></p>
                @if($errors->has('uf'))
                    <p class="help-block">
                        {{ $errors->first('uf') }}
                    </p>
                @endif
            </div>
      
       
      
            <div class="col-xs-12 form-group">
                {!! Form::label('obs', trans('quickadmin.agendatelefonica.fields.obs').'', ['class' => 'control-label']) !!}
                {!! Form::text('obs', old('obs'), ['class' => 'form-control']) !!}
                <p class="help-block"></p>
                @if($errors->has('obs'))
                    <p class="help-block">
                        {{ $errors->first('obs') }}
                    </p>
                @endif
            </div>
        </div>
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
        <i class="fa fa-refresh"> </i>
    ALTERAR</button>
    {{-- {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
    <script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop