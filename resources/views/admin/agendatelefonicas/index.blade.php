@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    {{-- <h3 class="page-title">@lang('quickadmin.agendatelefonica.title')</h3> --}}
    @can('agendatelefonica_create')
  
    <p>
        <a href="{{ route('admin.agendatelefonicas.create') }}"  type="submit" class="btn btn-success btn-lg">
            <i class="fa fa-plus-circle"> </i>
        NOVO</a>
        
    </p>
    @endcan

    @can('agendatelefonica_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.agendatelefonicas.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.agendatelefonicas.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
            {{-- @lang('quickadmin.qa_list') --}}
            <h4 id="TNome">RELATÓRIO - AGENDA TELEFONE</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>ALTERADO COM SUCESSO!</h4>
         </div> 
    @endif
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($agendatelefonicas) > 0 ? 'datatable' : '' }} @can('agendatelefonica_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('agendatelefonica_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                        <th>@lang('quickadmin.agendatelefonica.fields.nome')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.operadora')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.celular1')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.celular2')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.telefone1')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.telefone2')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.emailpessoal')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.emailcomercial')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.dataaniversario')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.dataabertura')</th>
                        {{-- <th>@lang('quickadmin.agendatelefonica.fields.fundacao')</th> --}}
                        <th>@lang('quickadmin.agendatelefonica.fields.endereco')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.numero')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.bairro')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.cidade')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.uf')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.cep')</th>
                        <th>@lang('quickadmin.agendatelefonica.fields.obs')</th>
                     
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($agendatelefonicas) > 0)
                        @foreach ($agendatelefonicas as $agendatelefonica)
                            <tr data-entry-id="{{ $agendatelefonica->id }}">
                                @can('agendatelefonica_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('agendatelefonica_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.agendatelefonicas.restore', $agendatelefonica->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('agendatelefonica_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.agendatelefonicas.perma_del', $agendatelefonica->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('agendatelefonica_view')
                                    <a href="{{ route('admin.agendatelefonicas.show',[$agendatelefonica->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('agendatelefonica_edit')
                                    <a href="{{ route('admin.agendatelefonicas.edit',[$agendatelefonica->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('agendatelefonica_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.agendatelefonicas.destroy', $agendatelefonica->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                                <td field-key='nome'>{{ $agendatelefonica->nome }}</td>
                                <td field-key='operadora'>{{ $agendatelefonica->operadora }}</td>
                                <td field-key='celular1'>{{ $agendatelefonica->celular1 }}</td>
                                <td field-key='celular2'>{{ $agendatelefonica->celular2 }}</td>
                                <td field-key='telefone1'>{{ $agendatelefonica->telefone1 }}</td>
                                <td field-key='telefone2'>{{ $agendatelefonica->telefone2 }}</td>
                                <td field-key='emailpessoal'>{{ $agendatelefonica->emailpessoal }}</td>
                                <td field-key='emailcomercial'>{{ $agendatelefonica->emailcomercial }}</td>
                                <td field-key='dataaniversario'>{{ $agendatelefonica->dataaniversario }}</td>
                                <td field-key='dataabertura'>{{ $agendatelefonica->dataabertura }}</td>
                                {{-- <td field-key='fundacao'>{{ $agendatelefonica->fundacao }}</td> --}}
                                <td field-key='endereco'>{{ $agendatelefonica->endereco }}</td>
                                <td field-key='numero'>{{ $agendatelefonica->numero }}</td>
                                <td field-key='bairro'>{{ $agendatelefonica->bairro }}</td>
                                <td field-key='cidade'>{{ $agendatelefonica->cidade }}</td>
                                <td field-key='uf'>{{ $agendatelefonica->uf }}</td>
                                <td field-key='cep'>{{ $agendatelefonica->cep }}</td>
                                <td field-key='obs'>{{ $agendatelefonica->obs }}</td>
                               
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="23">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('agendatelefonica_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.agendatelefonicas.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
$("#success-alert").slideUp(500);
});
  
 });
    </script>
@endsection