@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.agendatelefonica.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
   <h4 id="TNome">VIZUALIZAR - AGENDA TELEFÔNICA</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.nome')</th>
                            <td field-key='nome'>{{ $agendatelefonica->nome }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.operadora')</th>
                            <td field-key='operadora'>{{ $agendatelefonica->operadora }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.celular1')</th>
                            <td field-key='celular1'>{{ $agendatelefonica->celular1 }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.celular2')</th>
                            <td field-key='celular2'>{{ $agendatelefonica->celular2 }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.telefone1')</th>
                            <td field-key='telefone1'>{{ $agendatelefonica->telefone1 }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.telefone2')</th>
                            <td field-key='telefone2'>{{ $agendatelefonica->telefone2 }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.emailpessoal')</th>
                            <td field-key='emailpessoal'>{{ $agendatelefonica->emailpessoal }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.emailcomercial')</th>
                            <td field-key='emailcomercial'>{{ $agendatelefonica->emailcomercial }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.dataaniversario')</th>
                            <td field-key='dataaniversario'>{{ $agendatelefonica->dataaniversario }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.dataabertura')</th>
                            <td field-key='dataabertura'>{{ $agendatelefonica->dataabertura }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.fundacao')</th>
                            <td field-key='fundacao'>{{ $agendatelefonica->fundacao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.endereco')</th>
                            <td field-key='endereco'>{{ $agendatelefonica->endereco }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.numero')</th>
                            <td field-key='numero'>{{ $agendatelefonica->numero }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.bairro')</th>
                            <td field-key='bairro'>{{ $agendatelefonica->bairro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.cidade')</th>
                            <td field-key='cidade'>{{ $agendatelefonica->cidade }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.uf')</th>
                            <td field-key='uf'>{{ $agendatelefonica->uf }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.cep')</th>
                            <td field-key='cep'>{{ $agendatelefonica->cep }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendatelefonica.fields.obs')</th>
                            <td field-key='obs'>{{ $agendatelefonica->obs }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.agendatelefonicas.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop
