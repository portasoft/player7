@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.grade.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
          <h4>VIZUALIZAÇÃO - GRADE</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.grade.fields.descricao')</th>
                            <td field-key='descricao'>{{ $grade->descricao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.grade.fields.numeracao')</th>
                            <td field-key='numeracao'>{{ $grade->numeracao }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.grades.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
