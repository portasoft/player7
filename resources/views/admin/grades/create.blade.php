@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.grade.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.grades.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>CADASTRO - COR</h4>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4 form-group">
                    {!! Form::label('descricao', trans('quickadmin.grade.fields.descricao').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('descricao', old('descricao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('descricao'))
                        <p class="help-block">
                            {{ $errors->first('descricao') }}
                        </p>
                    @endif
                </div>
           
                <div class="col-xs-2 form-group">
                    {!! Form::label('numeracao', trans('quickadmin.grade.fields.numeracao').'', ['class' => 'control-label']) !!}
                    {!! Form::text('numeracao', old('numeracao'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('numeracao'))
                        <p class="help-block">
                            {{ $errors->first('numeracao') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

