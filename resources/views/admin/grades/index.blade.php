@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
{!! Form::open(['method' => 'POST', 'route' => ['admin.grades.store']]) !!}

<div class="panel panel-default">
    <div class="panel-heading">
        <h4>CADASTRO - COR</h4>
    </div>
    
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-4 form-group">
                {!! Form::label('descricao', trans('quickadmin.grade.fields.descricao').'*', ['class' => 'control-label']) !!}
                {!! Form::text('descricao', old('descricao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('descricao'))
                    <p class="help-block">
                        {{ $errors->first('descricao') }}
                    </p>
                @endif
            </div>
       
            <div class="col-xs-2 form-group">
                {!! Form::label('numeracao', trans('quickadmin.grade.fields.numeracao').'', ['class' => 'control-label']) !!}
                {!! Form::text('numeracao', old('numeracao'), ['class' => 'form-control', 'placeholder' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('numeracao'))
                    <p class="help-block">
                        {{ $errors->first('numeracao') }}
                    </p>
                @endif
            </div>
        </div>
        
    </div>
</div>

{!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
{!! Form::close() !!}

    {{-- <h3 class="page-title">@lang('quickadmin.grade.title')</h3> --}}
    {{-- @can('grade_create')
    <p>
        <a href="{{ route('admin.grades.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan --}}

    @can('grade_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.grades.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.grades.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>RELATÓRIO - GRADE</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('grade_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('grade_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.grade.fields.descricao')</th>
                        <th>@lang('quickadmin.grade.fields.numeracao')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('grade_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.grades.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.grades.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('grade_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'descricao', name: 'descricao'},
                {data: 'numeracao', name: 'numeracao'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection