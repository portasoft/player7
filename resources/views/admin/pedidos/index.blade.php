@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.pedido.title')</h3> --}}
    @can('pedido_create')
    <p>
        <a href="{{ route('admin.pedidos.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('pedido_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.pedidos.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.pedidos.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>RELATÓRIO - PEDIDO</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('pedido_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('pedido_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.pedido.fields.fornecedor')</th>
                        <th>@lang('quickadmin.pedido.fields.datapedido')</th>
                        <th>@lang('quickadmin.pedido.fields.contato')</th>
                        <th>@lang('quickadmin.pedido.fields.prazo')</th>
                        <th>@lang('quickadmin.pedido.fields.transportadora')</th>
                        <th>@lang('quickadmin.pedido.fields.dataaprovacao')</th>
                        <th>@lang('quickadmin.pedido.fields.dataentrega')</th>
                        <th>@lang('quickadmin.pedido.fields.empresa')</th>
                        <th>@lang('quickadmin.pedido.fields.obs')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('pedido_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.pedidos.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.pedidos.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('pedido_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'fornecedor', name: 'fornecedor'},
                {data: 'datapedido', name: 'datapedido'},
                {data: 'contato', name: 'contato'},
                {data: 'prazo', name: 'prazo'},
                {data: 'transportadora', name: 'transportadora'},
                {data: 'dataaprovacao', name: 'dataaprovacao'},
                {data: 'dataentrega', name: 'dataentrega'},
                {data: 'empresa', name: 'empresa'},
                {data: 'obs', name: 'obs'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection