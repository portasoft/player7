@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.pedido.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
          <h4>VIZUALIZAÇÃO - PEDIDO</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.pedido.fields.fornecedor')</th>
                            <td field-key='fornecedor'>{{ $pedido->fornecedor }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pedido.fields.datapedido')</th>
                            <td field-key='datapedido'>{{ $pedido->datapedido }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pedido.fields.contato')</th>
                            <td field-key='contato'>{{ $pedido->contato }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pedido.fields.prazo')</th>
                            <td field-key='prazo'>{{ $pedido->prazo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pedido.fields.transportadora')</th>
                            <td field-key='transportadora'>{{ $pedido->transportadora }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pedido.fields.dataaprovacao')</th>
                            <td field-key='dataaprovacao'>{{ $pedido->dataaprovacao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pedido.fields.dataentrega')</th>
                            <td field-key='dataentrega'>{{ $pedido->dataentrega }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pedido.fields.empresa')</th>
                            <td field-key='empresa'>{{ $pedido->empresa }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.pedido.fields.obs')</th>
                            <td field-key='obs'>{{ $pedido->obs }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.pedidos.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
