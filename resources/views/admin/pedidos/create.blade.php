@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.pedido.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.pedidos.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
         <h4>CADASTRO - PEDIDO</h4>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4 form-group">
                    {!! Form::label('fornecedor', trans('quickadmin.pedido.fields.fornecedor').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('fornecedor', $fornecedor,null, ['class' => 'form-control', 'placeholder' => '','required'=>'']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('fornecedor'))
                        <p class="help-block">
                            {{ $errors->first('fornecedor') }}
                        </p>
                    @endif
                </div>
        
                <div class="col-xs-2 form-group">
                    {!! Form::label('datapedido', trans('quickadmin.pedido.fields.datapedido').'', ['class' => 'control-label']) !!}
                    {!! Form::date('datapedido', old('datapedido'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('datapedido'))
                        <p class="help-block">
                            {{ $errors->first('datapedido') }}
                        </p>
                    @endif
                </div>
        
                <div class="col-xs-2 form-group">
                    {!! Form::label('contato', trans('quickadmin.pedido.fields.contato').'', ['class' => 'control-label']) !!}
                    {!! Form::text('contato', old('contato'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('contato'))
                        <p class="help-block">
                            {{ $errors->first('contato') }}
                        </p>
                    @endif
                </div>
        
                <div class="col-xs-1 form-group">
                    {!! Form::label('prazo', trans('quickadmin.pedido.fields.prazo').'', ['class' => 'control-label']) !!}
                    {!! Form::text('prazo', old('prazo'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('prazo'))
                        <p class="help-block">
                            {{ $errors->first('prazo') }}
                        </p>
                    @endif
                </div>
        
                <div class="col-xs-3 form-group">
                    {!! Form::label('transportadora', trans('quickadmin.pedido.fields.transportadora').'', ['class' => 'control-label']) !!}
                    {!! Form::text('transportadora', old('transportadora'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('transportadora'))
                        <p class="help-block">
                            {{ $errors->first('transportadora') }}
                        </p>
                    @endif
                </div>
        
                <div class="col-xs-2 form-group">
                    {!! Form::label('dataaprovacao', trans('quickadmin.pedido.fields.dataaprovacao').'', ['class' => 'control-label']) !!}
                    {!! Form::date('dataaprovacao', old('dataaprovacao'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('dataaprovacao'))
                        <p class="help-block">
                            {{ $errors->first('dataaprovacao') }}
                        </p>
                    @endif
                </div>
        
                <div class="col-xs-2 form-group">
                    {!! Form::label('dataentrega', trans('quickadmin.pedido.fields.dataentrega').'', ['class' => 'control-label']) !!}
                    {!! Form::date('dataentrega', old('dataentrega'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('dataentrega'))
                        <p class="help-block">
                            {{ $errors->first('dataentrega') }}
                        </p>
                    @endif
                </div>
        
                <div class="col-xs-3 form-group">
                    {!! Form::label('empresa', trans('quickadmin.pedido.fields.empresa').'', ['class' => 'control-label']) !!}
                    {!! Form::text('empresa', old('empresa'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('empresa'))
                        <p class="help-block">
                            {{ $errors->first('empresa') }}
                        </p>
                    @endif
                </div>
        
                <div class="col-xs-5 form-group">
                    {!! Form::label('obs', trans('quickadmin.pedido.fields.obs').'', ['class' => 'control-label']) !!}
                    {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('obs'))
                        <p class="help-block">
                            {{ $errors->first('obs') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop