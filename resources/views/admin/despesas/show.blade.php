@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.despesa.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>VIZUALIZAÇÃO - DESPESA</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.fornecedor')</th>
                            <td field-key='fornecedor'>{{ $despesa->fornecedor }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.data')</th>
                            <td field-key='data'>{{ $despesa->data }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.valor-conta')</th>
                            <td field-key='valor_conta'>{{ $despesa->valor_conta }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.valorpago')</th>
                            <td field-key='valorpago'>{{ $despesa->valorpago }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.parcela')</th>
                            <td field-key='parcela'>{{ $despesa->parcela }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.formapagamento')</th>
                            <td field-key='formapagamento'>{{ $despesa->formapagamento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.contacontabil')</th>
                            <td field-key='contacontabil'>{{ $despesa->contacontabil }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.docmercantil')</th>
                            <td field-key='docmercantil'>{{ $despesa->docmercantil }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.contacorrente')</th>
                            <td field-key='contacorrente'>{{ $despesa->contacorrente }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.documento')</th>
                            <td field-key='documento'>{{ $despesa->documento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.obs')</th>
                            <td field-key='obs'>{{ $despesa->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.colaborador')</th>
                            <td field-key='colaborador'>{{ $despesa->colaborador }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.tipo')</th>
                            <td field-key='tipo'>{{ $despesa->tipo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.juros')</th>
                            <td field-key='juros'>{{ $despesa->juros }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.desconto')</th>
                            <td field-key='desconto'>{{ $despesa->desconto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.despesa.fields.anexo')</th>
                            <td field-key='anexo'>@if($despesa->anexo)<a href="{{ asset(env('UPLOAD_PATH').'/' . $despesa->anexo) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.despesas.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
