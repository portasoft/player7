@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.despesa.title')</h3> --}}
    @can('despesa_create')
    <p>
        <a href="{{ route('admin.despesas.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('despesa_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.despesas.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.despesas.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
          <h4>RELATÓRIO - DESPESA</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('despesa_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('despesa_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.despesa.fields.fornecedor')</th>
                        <th>@lang('quickadmin.despesa.fields.data')</th>
                        <th>@lang('quickadmin.despesa.fields.valor-conta')</th>
                        <th>@lang('quickadmin.despesa.fields.valorpago')</th>
                        <th>@lang('quickadmin.despesa.fields.parcela')</th>
                        <th>@lang('quickadmin.despesa.fields.formapagamento')</th>
                        <th>@lang('quickadmin.despesa.fields.contacontabil')</th>
                        <th>@lang('quickadmin.despesa.fields.docmercantil')</th>
                        <th>@lang('quickadmin.despesa.fields.contacorrente')</th>
                        <th>@lang('quickadmin.despesa.fields.documento')</th>
                        <th>@lang('quickadmin.despesa.fields.obs')</th>
                        <th>@lang('quickadmin.despesa.fields.colaborador')</th>
                        <th>@lang('quickadmin.despesa.fields.tipo')</th>
                        <th>@lang('quickadmin.despesa.fields.juros')</th>
                        <th>@lang('quickadmin.despesa.fields.desconto')</th>
                        <th>@lang('quickadmin.despesa.fields.anexo')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('despesa_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.despesas.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.despesas.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('despesa_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'fornecedor', name: 'fornecedor'},
                {data: 'data', name: 'data'},
                {data: 'valor_conta', name: 'valor_conta'},
                {data: 'valorpago', name: 'valorpago'},
                {data: 'parcela', name: 'parcela'},
                {data: 'formapagamento', name: 'formapagamento'},
                {data: 'contacontabil', name: 'contacontabil'},
                {data: 'docmercantil', name: 'docmercantil'},
                {data: 'contacorrente', name: 'contacorrente'},
                {data: 'documento', name: 'documento'},
                {data: 'obs', name: 'obs'},
                {data: 'colaborador', name: 'colaborador'},
                {data: 'tipo', name: 'tipo'},
                {data: 'juros', name: 'juros'},
                {data: 'desconto', name: 'desconto'},
                {data: 'anexo', name: 'anexo'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection