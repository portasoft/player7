@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.despesa.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.despesas.store'], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
          <h4>CADASTRO - DESPESA</h4>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3 form-group">
                    {!! Form::label('fornecedor', trans('quickadmin.despesa.fields.fornecedor').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('fornecedor', old('fornecedor'), ['class' => 'form-control','id'=>'fornecedor','name'=>'fornecedor', 'placeholder' => '', 'required' => '']) !!}
                <div id="fornecedorlist"></div>
                    <p class="help-block"></p>
                    @if($errors->has('fornecedor'))
                        <p class="help-block">
                            {{ $errors->first('fornecedor') }}
                        </p>
                    @endif
                </div>
          
              
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('valor_conta', trans('quickadmin.despesa.fields.valor-conta').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('valor_conta', old('valor_conta'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valor_conta'))
                        <p class="help-block">
                            {{ $errors->first('valor_conta') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-2 form-group">
                    {!! Form::label('data', trans('quickadmin.despesa.fields.data').'*', ['class' => 'control-label']) !!}
                    {!! Form::date('data', old('data'), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('data'))
                        <p class="help-block">
                            {{ $errors->first('data') }}
                        </p>
                    @endif
                </div>
            {{-- <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valorpago', trans('quickadmin.despesa.fields.valorpago').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valorpago', old('valorpago'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorpago'))
                        <p class="help-block">
                            {{ $errors->first('valorpago') }}
                        </p>
                    @endif
                </div>
            </div> --}}
    
                <div class="col-xs-1 form-group">
                    {!! Form::label('parcela', trans('quickadmin.despesa.fields.parcela').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('parcela', old('parcela'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('parcela'))
                        <p class="help-block">
                            {{ $errors->first('parcela') }}
                        </p>
                    @endif
                </div>
          
            
                <div class="col-xs-2 form-group">
                    {!! Form::label('juros', trans('quickadmin.despesa.fields.juros').'', ['class' => 'control-label']) !!}
                    {!! Form::text('juros', old('juros'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('juros'))
                        <p class="help-block">
                            {{ $errors->first('juros') }}
                        </p>
                    @endif
                </div>
        
                <div class="col-xs-2 form-group">
                    {!! Form::label('desconto', trans('quickadmin.despesa.fields.desconto').'', ['class' => 'control-label']) !!}
                    {!! Form::text('desconto', old('desconto'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('desconto'))
                        <p class="help-block">
                            {{ $errors->first('desconto') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-2 form-group">
                    {!! Form::label('formapagamento', trans('quickadmin.despesa.fields.formapagamento').'', ['class' => 'control-label']) !!}
                    {!! Form::select('formapagamento',['Dinheiro','Cartão','Cheque','Depósito'],null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('formapagamento'))
                        <p class="help-block">
                            {{ $errors->first('formapagamento') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-3 form-group">
                    {!! Form::label('contacontabil', trans('quickadmin.despesa.fields.contacontabil').'', ['class' => 'control-label']) !!}
                    {!! Form::text('contacontabil', old('contacontabil'), ['class' => 'form-control','id'=>'contacontabil','name'=>'contacontabil', 'placeholder' => '']) !!}
                    <div id="contacontabillist"></div>
                    <p class="help-block"></p>
                    @if($errors->has('contacontabil'))
                        <p class="help-block">
                            {{ $errors->first('contacontabil') }}
                        </p>
                    @endif
                </div>
           
            {{-- <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('docmercantil', trans('quickadmin.despesa.fields.docmercantil').'', ['class' => 'control-label']) !!}
                    {!! Form::text('docmercantil', old('docmercantil'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('docmercantil'))
                        <p class="help-block">
                            {{ $errors->first('docmercantil') }}
                        </p>
                    @endif
                </div>
            </div> --}}
      
                <div class="col-xs-3 form-group">
                    {!! Form::label('contacorrente', trans('quickadmin.despesa.fields.contacorrente').'', ['class' => 'control-label']) !!}
                    {!! Form::select('contacorrente',$contacorrente,null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('contacorrente'))
                        <p class="help-block">
                            {{ $errors->first('contacorrente') }}
                        </p>
                    @endif
                </div>
           
                <div class="col-xs-2 form-group">
                    {!! Form::label('documento', trans('quickadmin.despesa.fields.documento').'', ['class' => 'control-label']) !!}
                    {!! Form::text('documento', old('documento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('documento'))
                        <p class="help-block">
                            {{ $errors->first('documento') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-2 form-group">
                    {!! Form::label('colaborador', trans('quickadmin.despesa.fields.colaborador').'', ['class' => 'control-label']) !!}
                    {!! Form::text('colaborador', old('colaborador'), ['class' => 'form-control','id'=>'usuario','name'=>'usuario', 'placeholder' => '']) !!}
                 <div id="usuariolist"></div>
                    <p class="help-block"></p>
                    @if($errors->has('colaborador'))
                        <p class="help-block">
                            {{ $errors->first('colaborador') }}
                        </p>
                    @endif
                </div>
             
        
              
         
            {{-- <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('tipo', trans('quickadmin.despesa.fields.tipo').'', ['class' => 'control-label']) !!}
                    {!! Form::text('tipo', old('tipo'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tipo'))
                        <p class="help-block">
                            {{ $errors->first('tipo') }}
                        </p>
                    @endif
                </div>
            </div> --}}
       
             
         
                <div class="col-xs-4 form-group">
                    {!! Form::label('anexo', trans('quickadmin.despesa.fields.anexo').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('anexo', old('anexo')) !!}
                    {!! Form::file('anexo', ['class' => 'form-control']) !!}
                    {!! Form::hidden('anexo_max_size', 2) !!}
                    <p class="help-block"></p>
                    @if($errors->has('anexo'))
                        <p class="help-block">
                            {{ $errors->first('anexo') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-8 form-group">
                    {!! Form::label('obs', trans('quickadmin.despesa.fields.obs').'', ['class' => 'control-label']) !!}
                    {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('obs'))
                        <p class="help-block">
                            {{ $errors->first('obs') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
     <script type="text/javascript">
        $(document).ready(function(){
                 $('#usuario').keyup(function(){
                     var query = $(this).val();
                      if(query.text != '')
                     {
                       var _token = $('input[name="_token"]').val();
                            $.ajax({
                             url:"{{ route('despesas.searchusuario')}}",
                             method:"POST",
                             data:{query:query,_token:_token},
                             success:function(data)
                             {
                                 $('#usuariolist').fadeIn();
                                 $('#usuariolist').html(data);
                             }
                         })
                     }
                 });
                 $(document).on('click','li.us',function(){
                                     
                         $('#usuario').val($(this).text());
                         $('#usuariolist').fadeOut()
                 
                      })
             });
    </script>
     <script type="text/javascript">
        $(document).ready(function(){
                 $('#contacontabil').keyup(function(){
                     var query = $(this).val();
                      if(query.text != '')
                     {
                       var _token = $('input[name="_token"]').val();
                            $.ajax({
                             url:"{{ route('despesas.searchcontacontabil')}}",
                             method:"POST",
                             data:{query:query,_token:_token},
                             success:function(data)
                             {
                                 $('#contacontabillist').fadeIn();
                                 $('#contacontabillist').html(data);
                             }
                         })
                     }
                 });
                 $(document).on('click','li.co',function(){
                                     
                         $('#contacontabil').val($(this).text());
                         $('#contacontabillist').fadeOut()
                 
                      })
             });
    </script>
     <script type="text/javascript">
        $(document).ready(function(){
                 $('#fornecedor').keyup(function(){
                     var query = $(this).val();
                      if(query.text != '')
                     {
                       var _token = $('input[name="_token"]').val();
                            $.ajax({
                             url:"{{ route('despesas.searchfornecedor')}}",
                             method:"POST",
                             data:{query:query,_token:_token},
                             success:function(data)
                             {
                                 $('#fornecedorlist').fadeIn();
                                 $('#fornecedorlist').html(data);
                             }
                         })
                     }
                 });
                 $(document).on('click','li.n',function(){
                                     
                         $('#fornecedor').val($(this).text());
                         $('#fornecedorlist').fadeOut()
                 
                      })
             });
    </script>
            
@stop