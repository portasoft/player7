@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.user-actions.title')</h3> --}}
    @can('user_action_create')
    <p>
        
        
    </p>
    @endcan

    

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
         <h4 id="TNome">RELATÓRIO - AÇÕES DE USUÁRIOS</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($user_actions) > 0 ? 'datatable' : '' }} ">
                <thead>
                    <tr>
                        
                        <th>Data</th>
                        <th>@lang('quickadmin.user-actions.fields.user')</th>
                        <th>@lang('quickadmin.user-actions.fields.action')</th>
                        <th>Formulário</th>
                        <!-- <th>@lang('quickadmin.user-actions.fields.action-id')</th> -->
                        
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($user_actions) > 0)
                        @foreach ($user_actions as $user_action)
                            <tr data-entry-id="{{ $user_action->id }}">
                                
                                <td>{{ $user_action->created_at or '' }}</td>
                                <td field-key='user'>{{ $user_action->user->name or '' }}</td>
                                @if ($user_action->action == 'created' )
                                <td field-key='action'>Criou</td>
                                   @endif
                                   @if ($user_action->action == 'updated' )
                                <td field-key='action'>Atualizou</td>
                                   @endif
                                   @if ($user_action->action == 'deleted' )
                                <td field-key='action'>Deletou</td>
                                   @endif
                                <td field-key='action_model'>{{ $user_action->action_model }}</td>
                                <!-- <td field-key='action_id'>{{ $user_action->action_id }}</td> -->
                                
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        
    </script>
@endsection