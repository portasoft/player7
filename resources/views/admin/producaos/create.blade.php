@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.producao.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.producaos.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
    
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3 form-group">
                    {!! Form::label('produtoid', trans('quickadmin.producao.fields.produtoid').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('produtoid', old('produtoid'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('produtoid'))
                        <p class="help-block">
                            {{ $errors->first('produtoid') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-1 form-group">
                    {!! Form::label('estoqueatual', trans('quickadmin.producao.fields.estoqueatual').'', ['class' => 'control-label']) !!}
                    {!! Form::text('estoqueatual', old('estoqueatual'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('estoqueatual'))
                        <p class="help-block">
                            {{ $errors->first('estoqueatual') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('depositoorigem', trans('quickadmin.producao.fields.depositoorigem').'', ['class' => 'control-label']) !!}
                    {!! Form::number('depositoorigem', old('depositoorigem'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('depositoorigem'))
                        <p class="help-block">
                            {{ $errors->first('depositoorigem') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('depositodestino', trans('quickadmin.producao.fields.depositodestino').'', ['class' => 'control-label']) !!}
                    {!! Form::number('depositodestino', old('depositodestino'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('depositodestino'))
                        <p class="help-block">
                            {{ $errors->first('depositodestino') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('custo', trans('quickadmin.producao.fields.custo').'', ['class' => 'control-label']) !!}
                    {!! Form::text('custo', old('custo'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('custo'))
                        <p class="help-block">
                            {{ $errors->first('custo') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-1 form-group">
                    {!! Form::label('qtde', trans('quickadmin.producao.fields.qtde').'', ['class' => 'control-label']) !!}
                    {!! Form::text('qtde', old('qtde'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('qtde'))
                        <p class="help-block">
                            {{ $errors->first('qtde') }}
                        </p>
                    @endif
                </div>
          
                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('obs', trans('quickadmin.producao.fields.obs').'', ['class' => 'control-label']) !!}
                    {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('obs'))
                        <p class="help-block">
                            {{ $errors->first('obs') }}
                        </p>
                    @endif
                </div> --}}
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

