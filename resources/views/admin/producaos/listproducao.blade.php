@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    {{-- <h3 class="page-title">@lang('quickadmin.producao.title')</h3> --}}
    {{-- @can('producao_create')
    <p>
        <a href="{{ route('admin.producaos.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan --}}

    @can('producao_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.producaos.listproducao') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.producaos.listproducao') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">RELATÓRIO - PRODUÇÃO</h4>
        </div>

        <div class="panel-body table-responsive">
            <table id="tb"   class="table table-bordered table-striped {{ count($producaos) > 0 ? 'datatable' : '' }} @can('producao_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
             
                <thead>
                    <tr>
                        @can('producao_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                        
                        <th>@lang('quickadmin.producao.fields.produtoid')</th>
                        {{-- <th>@lang('quickadmin.producao.fields.estoqueatual')</th> --}}
                        <th>@lang('quickadmin.producao.fields.depositoorigem')</th>
                        <th>@lang('quickadmin.producao.fields.depositodestino')</th>
                        <th>@lang('quickadmin.producao.fields.custo')</th>
                        <th>@lang('quickadmin.producao.fields.qtde')</th>
                        <th>@lang('quickadmin.producao.fields.situacao')</th>
                        <th>@lang('quickadmin.producao.fields.tamanho')</th>
                        <th>@lang('quickadmin.producao.fields.referencia')</th>
                        <th>@lang('quickadmin.producao.fields.lote')</th>
                        <th>@lang('quickadmin.producao.fields.barra')</th>
                        <th>@lang('quickadmin.producao.fields.bolso')</th>
                        <th>@lang('quickadmin.producao.fields.capuz')</th>
                        <th>@lang('quickadmin.producao.fields.costa')</th>
                        <th>@lang('quickadmin.producao.fields.frente')</th>
                        <th>@lang('quickadmin.producao.fields.etiqueta')</th>
                        <th>@lang('quickadmin.producao.fields.manga')</th>
                        <th>@lang('quickadmin.producao.fields.punho')</th>
                        <th>@lang('quickadmin.producao.fields.obs')</th>
                        
                     
                    </tr>
                </thead>
                
                <tbody>
                  
                    @if (count($producaos) > 0)
              
                        @foreach ($producaos as $producao)
                  
               
              
                            <tr data-entry-id="{{ $producao->id }}">
                                @can('producao_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan
                                @if( request('show_deleted') == 1 )
                              
                                <td>
                                    @can('producao_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.producaos.restore', $producao->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('producao_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.producaos.perma_del', $producao->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('producao_view')
                                    <a href="{{ route('admin.producaos.show',[$producao->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                 
                                    {{-- @can('producao_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.producaos.destroy', $producao->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan --}}
                                </td>
                                @endif
                             
                                @foreach ($produtos as $pro)
                                @if ($producao->produtoid == $pro->id)
                                <td field-key='produtoid'>{{ $pro->descricao }}</td>
                                @endif
                                @endforeach
                        
                                {{-- <td field-key='estoqueatual'>{{ $producao->estoqueatual }}</td> --}}
                                @foreach ($depositos as $dep)
                                @if ($producao->depositoorigem == $dep->id)
                                <td field-key='depositoorigem'>{{ $dep->descricao}}</td>
                                @endif
                                @endforeach
                             
                                @foreach ($depositos as $depd)
                                @if ($producao->depositodestino == $depd->id)
                                <td field-key='depositodestino'>{{ $depd->descricao}}</td>
                                @endif
                                @endforeach
                        
                                <td class="qtd" field-key='custo'>{{ $producao->custo }}</td>
                           
                
                                <td field-key='qtde'>{{ $producao->qtde }}</td>

                                @if (($producao->situacao == 'COSTURA'))
                                <td class="label label-danger " field-key='situacao'>{{ $producao->situacao }}</td>
                                @endif
                                @if (($producao->situacao == 'CONCLUIDO'))
                                <td class="label label-success " field-key='situacao'>{{ $producao->situacao }}</td>
                                @endif
                                @if (($producao->situacao == 'CORTE'))
                                <td class="label label-primary" field-key='situacao'>{{ $producao->situacao }}</td>
                                @endif
                          
                          
                                <td field-key='tamanho'>{{ $producao->tamanho }}</td>
                                <td field-key='referencia'>{{ $producao->referencia }}</td>
                                <td field-key='lote'>{{ $producao->lote }}</td>
                                @if (isset($producao->barra))
                                <td field-key='barra'>{{ $producao->barra }}</td>
                                @else
                                <td field-key='barra'></td>
                                @endif
                             @if (isset($producao->bolso ))
                             <td field-key='bolso'>{{ $producao->bolso }}</td>
                             @else
                             <td field-key='bolso'></td>
                             @endif
                             @if (isset($producao->capuz ))
                             <td field-key='capuz'>{{ $producao->capuz }}</td>
                             @else
                             <td field-key='capuz'></td>
                             @endif
                             @if (isset($producao->costa ))
                             <td field-key='costa'>{{ $producao->costa }}</td>
                             @else
                             <td field-key='costa'></td>
                             @endif
                             @if (isset($producao->frente ))
                             <td field-key='frente'>{{ $producao->frente }}</td> 
                             @else
                             <td field-key='frente'></td>
                             @endif
                             @if (isset($producao->etiqueta ))
                             <td field-key='etiqueta'>{{ $producao->etiqueta }}</td>
                             @else
                             <td field-key='etiqueta'></td> 
                             @endif
                             @if (isset($producao->manga ))
                             <td field-key='manga'>{{ $producao->manga }}</td>
                             @else
                             <td field-key='manga'></td> 
                             @endif
                             @if (isset($producao->punho ))
                              <td field-key='punho'>{{ $producao->punho }}</td>
                             @else
                              <td field-key='punho'></td>
                             @endif
                          
                 
                                <td field-key='obs'>{{ $producao->obs }}</td>
                                {{-- <td field-key='obs'>{{ $producao->obs }}</td> --}}
                               
                            </tr>
                          
                       
                     
                        @endforeach
                    @else
                        <tr>
                            <td colspan="12">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
             
            
             
           
                {{-- console.log("Soma da primeira coluna: " + getTotal(4)); --}}
               {{-- <td field-key='custo'>{{ $valortotal }}</td> --}}
            </table>
          
            {{-- <div class="total"></div> 
          --}}
        
                    
                  </div>
                
        </div>
        
    </div>
@stop

@section('javascript') 
    <script>
        @can('producao_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.producaos.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function(){
             $('#idProdutoProducao').keyup(function(){
                 var query = $(this).val();
                 if(query.text != '' )
                 {
                   var _token = $('input[name="_token"]').val();
                      $.ajax({
                         url:"{{ route('producaos.fetchProducao')}}",
                   
                         method:"POST",
                         data:{query:query,_token:_token},
                         success:function(data)
                         {
                             $('#idProdutoProducaolist').fadeIn();
                             $('#idProdutoProducaolist').html(data);
                         }
                     })
                 }
               
             });
    //          $(function(){
    //   var total = 0;
    //   $('.qtd').each(function(){
    //     total += parseInt(jQuery(this).text());
    //   });
      
    //   $('.total').html(total);

    // });

             $(document).on('click','li.c',function(){
                                 
                                 $('#idProdutoProducao').val($(this).text());
                                 $('#idProdutoProducaolist').fadeOut()
                                 var _token = $('input[name="_token"]').val();
                      $.ajax({
                         url:"{{ route('admin.producaos.importartelaP')}}",
                   
                         method:"POST",
                         data:{query:query,_token:_token},
                         success:function(data)
                         {
                             $('#idProdutoProducaolist').fadeIn();
                             $('#idProdutoProducaolist').html(data);
                         }
                     })
                         
                              })
                     })
          
               
    </script>



@endsection