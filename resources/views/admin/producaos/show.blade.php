    @extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.producao.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
   <h4 id="TNome">PRODUÇÃO - PLAYER 7</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.producao.fields.produtoid')</th>
                            <td field-key='produtoid'>{{ $producao->produtoid }}</td>
                        </tr>
                      
                        <tr>
                            <th>COMPOSICAO1</th>
                            <td field-key='kit'>{{ $producao->outros }}</td>
                        </tr>
                        <tr>
                            <th>COR1</th>
                            <td field-key='cor'>{{ $producao->cor }}</td>
                        </tr>
                        <tr>
                            <th>COMPOSICAO2</th>
                            <td field-key='kit'>{{ $producao->composicao2 }}</td>
                        </tr>
                        <tr>
                                <th>COR2</th>
                                <td field-key='cor2'>{{ $producao->cor2 }}</td>
                            </tr>
                        <tr>
                            <th>REFERENCIA</th>
                            <td field-key='referencia'>{{ $producao->referencia }}</td>
                        </tr>
                        <tr>
                            <th>QTDE</th>
                            <td field-key='qtde'>{{ $producao->qtde }}</td>
                        </tr>
                    
                        <tr>
                            <th>TAMANHO</th>
                            <td field-key='tamanho'>{{ $producao->tamanho }}</td>
                        </tr>
                    
                     
                        <tr>
                            <th>FORNECEDOR</th>
                            <td field-key='idfornecedor'>{{ $producao->idfornecedor }}</td>
                        </tr>
                        <tr>
                            <th>ORIGEM</th>
                            <td field-key='depositoorigem'>{{ $producao->depositoorigem }}</td>
                        </tr>
                        <tr>
                            <th>DESTINO</th>
                            <td field-key='depositodestino'>{{ $producao->depositodestino }}</td>
                        </tr>
                        <tr>
                            <th>COSTA</th>
                            <td field-key='depositodestino'>{{ $producao->costa }}</td>
                        </tr>
                     
                        <tr>
                            <th>FRENTE</th>
                            <td field-key='depositodestino'>{{ $producao->frente }}</td>
                        </tr>
                     
                        <tr>
                            <th>MANGA</th>
                            <td field-key='depositodestino'>{{ $producao->manga }}</td>
                        </tr>
                     
                        <tr>
                            <th>PUNHO</th>
                            <td field-key='depositodestino'>{{ $producao->punho }}</td>
                        </tr>
                     
                        <tr>
                            <th>ETIQUETA</th>
                            <td field-key='depositodestino'>{{ $producao->etiqueta }}</td>
                        </tr>
                     
                        <tr>
                            <th>CAPUZ</th>
                            <td field-key='depositodestino'>{{ $producao->capuz }}</td>
                        </tr>
                     
                        <tr>
                            <th>BARRA</th>
                            <td field-key='depositodestino'>{{ $producao->barra }}</td>
                        </tr>
                     
                        <tr>
                            <th>BOLSO</th>
                            <td field-key='depositodestino'>{{ $producao->bolso }}</td>
                        </tr>
                     
                        <tr>
                            <th>ORELHA</th>
                            <td field-key='depositodestino'>{{ $producao->orelha }}</td>
                        </tr>
                     
                        <tr>
                            <th>CHIFRE</th>
                            <td field-key='depositodestino'>{{ $producao->chifre }}</td>
                        </tr>
                     
                        <tr>
                            <th>COS</th>
                            <td field-key='depositodestino'>{{ $producao->cos }}</td>
                        </tr>
                        <tr>
                            <th>LOTE</th>
                            <td field-key='qtde'>{{ $producao->lote }}</td>
                        </tr>
                   
                     
                  
                        <tr>
                            <th>@lang('quickadmin.producao.fields.situacao')</th>
                            <td field-key='qtde'>{{ $producao->situacao }}</td>
                        </tr>
                        <tr>
                            <th>DATA</th>
                            <td field-key='qtde'>{{ $producao->data }}</td>
                        </tr>
                      
<!--                      
                        <tr>
                            <th>@lang('quickadmin.producao.fields.obs')</th>
                            <td field-key='obs'>{{ $producao->obs }}</td>
                        </tr> -->
                    </table>
                </div>
            </div>

            <!-- <p>&nbsp;</p> -->

            <!-- <a href="{{ route('admin.producaos.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a> -->
        </div>
    </div>
@stop


