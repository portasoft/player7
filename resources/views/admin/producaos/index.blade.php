@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
{{-- <h3 class="page-title">@lang('quickadmin.producao.title')</h3> --}}
{!! Form::open(['method' => 'POST', 'route' => ['admin.producaos.store']]) !!}

<div class="panel panel-primary">
    <div id="HNome" class="panel-heading">
      <h4 id="TNome">CADASTRO - PRODUÇÃO</h4>
    </div>
    @if (session()->has('alert'))
    <div class="alert alert-warning alert-dismissible" id="success-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
        <h4><i class="icon fa fa-check"></i>OPERAÇÃO NÃO REALIZADA,VERIFIQUE O ESTOQUE!</h4>
     </div> 
    @endif
  
    <div class="panel-body">
        <div class="row">
                {{-- <form action="importartelaP" method="POST"> --}}
            <div class="col-xs-4 form-group">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                {!! Form::label('produtoid', trans('quickadmin.producao.fields.produtoid').'*', ['class' => 'control-label']) !!}
                {!! Form::text('produtoid', old('produtoid'), ['class' => 'form-control', 'autofocus','id'=>'idProdutoProducao','autocomplete'=>'off', 'placeholder' => '', 'required' => '']) !!}
                <div id="idProdutoProducaolist"></div>
             
            </div>
            <div class="col-xs-2 form-group">
           <label for="">COMPOSIÇÃO 1</label>
           {!! Form::select('outros',['100% ALGODAO FIO 20'=>'100% ALGODAO FIO 20','100%'=>'100%','100% ALGODAO FIO 24'=>'100% ALGODAO FIO 24','75/25'=>'75/25','50/50'=>'50/50','NDF'=>'NDF'],NULL, ['class' => 'form-control', 'placeholder' => '','id'=>'outros']) !!}
                <p class="help-block"></p>
            </div>
            <div class="col-xs-2 form-group">
                <label for="">COR 1</label>
                {!! Form::select('cor', $cor,null,['class' => 'form-control ', 'placeholder' => '','required','id'=>'cor']) !!}
           <br>
            </div>
            <div class="col-xs-2 form-group">
                    <label for="">COMPOSIÇÃO 2</label>
                    {!! Form::select('composicao2',['100% ALGODAO FIO 20'=>'100% ALGODAO FIO 20','100%'=>'100%','100% ALGODAO FIO 24'=>'100% ALGODAO FIO 24','75/25'=>'75/25','50/50'=>'50/50'],NULL, ['class' => 'form-control', 'placeholder' => '','id'=>'composicao2']) !!}
                         <p class="help-block"></p>
                     </div>
            <div class="col-xs-2 form-group">
                <label for="">COR 2</label>
                {!! Form::select('cor2', $cor2,null,['class' => 'form-control', 'placeholder' => '','id'=>'cor2']) !!}
           <br>
            </div>
            <div class="col-xs-2 form-group">
                <label for="">TAMANHO</label>
                {!! Form::select('tamanho',$tamanho,NULL, ['class' => 'form-control', 'placeholder' => '','required']) !!}
             
            </div>
            <div class="col-xs-2 form-group">
                {!! Form::label('qtde', trans('quickadmin.producao.fields.qtde').'', ['class' => 'control-label']) !!}
                {!! Form::number('qtde', old('qtde'), ['class' => 'form-control','required']) !!}
             
            </div>
      
            <div class="col-xs-2 form-group">
                <label for="">REF</label>
                {!! Form::text('referencia', old('referencia'), ['class' => 'form-control', 'placeholder' => '']) !!}
               
            </div>
           
       
            <div class="col-xs-4 form-group">
                <label for="">FORNECEDOR</label>
                {!! Form::select('idfornecedor',$fornecedor,null, ['class' => 'form-control','id'=>'idfornecedor','placeholder' => '']) !!}
             </div>
             <div class="col-xs-2 form-group">
                <label for="">SITUAÇÃO</label>
                     {!! Form::select('situacao',['CORTE'=>'CORTE'],NULL, ['class' => 'form-control', 'placeholder' => '','required','id'=>'situacao']) !!}
                 
                    
                 </div>
        
            <div class="col-xs-2 form-group">
                {!! Form::label('depositoorigem', trans('quickadmin.producao.fields.depositoorigem').'', ['class' => 'control-label']) !!}
                {!! Form::select('depositoorigem', $depositoorigem,null,['class' => 'form-control', 'placeholder' => '','required']) !!}
               
            </div>
      
            <div class="col-xs-2 form-group">
                {!! Form::label('depositodestino', trans('quickadmin.producao.fields.depositodestino').'', ['class' => 'control-label']) !!}
                {!! Form::select('depositodestino',$depositodestino,null,['class' => 'form-control', 'placeholder' => '','required']) !!}       
           
            </div>
            <div class="col-xs-2 form-group">
                <label for="">LOTE</label>
                     {!! Form::text('lote',old('lote'), ['class' => 'form-control', 'placeholder' => '','id'=>'lote']) !!}
                 </div>
                 <div  class="col-xs-2">
                    <label for="">DATA</label>
                         {!! Form::date('data',old('data'), ['class' => 'form-control date', 'placeholder' => '','id'=>'data','required']) !!}
                 </div>    
                                    
                 <div class="col-xs-4 form-group">
                        <label for="">OBS</label>
                        {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                   
                    </div>                                
                                 <div class="col-xs-1 form-group">
                                    <label for="">COSTA</label>
                                         {!! Form::select('costa',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'costa']) !!}
                                 </div>
                                 <div class="col-xs-1 form-group">
                                    <label for="">FRENTE</label>
                                         {!! Form::select('frente',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'frente']) !!}
                                 </div>
                            
                                 <div class="col-xs-1 form-group">
                                    <label for="">MANGA</label>
                                         {!! Form::select('manga',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'manga']) !!}
                                
                                        </div>
                                 <div class="col-xs-1 form-group">
                                    <label for="">PUNHO</label>
                                         {!! Form::select('punho',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'punho']) !!}
                          
                                        </div>
                                        <div class="col-xs-1 form-group">
                                            <label for="">ETIQUETA</label>
                                                 {!! Form::select('etiqueta',['1'=>'1','2'=>'2','3'=>'3','4'=>'4'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'etiqueta']) !!}
                                         </div>
                                         <div class="col-xs-1 form-group">
                                            <label for="">CAPUZ</label>
                                                 {!! Form::select('capuz',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'capuz']) !!}
                                         </div>
                                   
                                               
                                                    
                                                         <div class="col-xs-1 form-group">
                                                            <label for="">BARRA</label>
                                                                 {!! Form::select('barra',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'barra']) !!}
                                                         </div>
                                                         <div class="col-xs-1 form-group">
                                                            <label for="">BOLSO</label>
                                                                 {!! Form::select('bolso',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'bolso']) !!}
                                                         </div>
                                                      
                                   
                                 <div class="col-xs-1 form-group">
                                    <label for="">ORELHA</label>
                                         {!! Form::select('orelha',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'orelha']) !!}
                                 </div>
                                 <div class="col-xs-1 form-group">
                                    <label for="">CHIFRE</label>
                                         {!! Form::select('chifre',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'chifre']) !!}
                                 </div>
                                 <div class="col-xs-1 form-group">
                                    <label for="">COS</label>
                                         {!! Form::select('cos',['1'=>'1','2'=>'2'],null, ['class' => 'form-control', 'placeholder' => '','id'=>'chifre']) !!}
                                 </div>
                             
                           
                             
              
      
         
                
        </div>
  
    </div>

</div>

<button type="submit" class="btn btn-primary btn-lg">
    <i class="fa fa-save"> </i>
SALVAR</button>
    
{{-- {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!} --}}
{!! Form::close() !!}
    {{-- <h3 class="page-title">@lang('quickadmin.producao.title')</h3> --}}
    {{-- @can('producao_create')
    <p>
        <a href="{{ route('admin.producaos.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan --}}

    @can('producao_delete')
    <p>
        {{-- <ul class="list-inline">
            <li><a href="{{ route('admin.producaos.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.producaos.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul> --}}
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">RELATÓRIO - PRODUÇÃO</h4>
        </div>

        <div class="panel-body table-responsive">
            <table   class="table table-bordered table-striped {{ count($producaos) > 0 ? 'datatable' : '' }} @can('producao_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
             
                <thead>
                    <tr>
                        @can('producao_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                        <th>@lang('quickadmin.producao.fields.produtoid')</th>
                        <th>COMP1</th>
                        <th>COR1</th>
                        <th>COMP2</th>
                        <th>COR2</th>
                        <th>@lang('quickadmin.producao.fields.qtde')</th>
                        <th>@lang('quickadmin.producao.fields.custo')</th>
                        <th>REF</th>
                   
                        <th>TAM</th>
                        <th>@lang('quickadmin.producao.fields.lote')</th>
                        <th>@lang('quickadmin.producao.fields.depositoorigem')</th>
                        <th>@lang('quickadmin.producao.fields.depositodestino')</th>
                        <th>@lang('quickadmin.producao.fields.situacao')</th>
                        <th>FORNE</th>
                        <th>DATA</th>
                        <th>@lang('quickadmin.producao.fields.barra')</th>
                        <th>@lang('quickadmin.producao.fields.bolso')</th>
                        <th>@lang('quickadmin.producao.fields.capuz')</th>
                        <th>@lang('quickadmin.producao.fields.costa')</th>
                        <th>@lang('quickadmin.producao.fields.frente')</th>
                        <th>ETIQ</th>
                        <th>@lang('quickadmin.producao.fields.manga')</th>
                        <th>@lang('quickadmin.producao.fields.punho')</th>
                        <th>ORELHA</th>
                        <th>CHIFRE</th>
                        <th>COS</th>
                        <th>@lang('quickadmin.producao.fields.obs')</th>
                     
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($producaos) > 0)
                        @foreach ($producaos as $producao)
                          
                          
                            <tr data-entry-id="{{ $producao->id }}">
                                @can('producao_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('producao_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.producaos.restore', $producao->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('producao_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.producaos.perma_del', $producao->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('producao_view')
                                    <a href="{{ route('admin.producaos.show',[$producao->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('producao_edit')
                                    <a href="{{ route('admin.producaos.edit',[$producao->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('producao_delete')
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.producaos.destroy', $producao->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>
                                @endif
                                @foreach ($produtos as $pro)
                                @if ($producao->produtoid == $pro->id)
                                <td field-key='produtoid'>{{ $pro->descricao }}</td>
                                @endif
                                @endforeach
                                <td field-key='data'>{{ $producao->outros }}</td>
                                @foreach ($cors as $cord)
                                @if ($producao->cor == $cord->id)
                                <td field-key='cor'>{{ $cord->descricao}}</td>
                                @endif
                                @endforeach
                                <td field-key='data'>{{ $producao->composicao2 }}</td>
                                @if ($producao->cor2 == "")
                                <td field-key='cor2'></td>
                                @endif
                                @foreach ($cors2 as $cord2)
                                @if ($producao->cor2 == $cord2->id)
                                <td field-key='cor2'>{{ $cord2->descricao}}</td>
                                @endif
                                                            
                                @endforeach
                                <td field-key='qtde'>{{ $producao->qtde }}</td>
                                <td field-key='custo'>{{ $producao->custo }}</td>
                                <td field-key='referencia'>{{ $producao->referencia }}</td>
                              
                                @foreach ($tamanhos as $tamd)
                                @if ($producao->tamanho == $tamd->id)
                                <td field-key='tamanho'>{{ $tamd->descricao}}</td>
                                @endif
                                @endforeach
                                <td field-key='lote'>{{ $producao->lote }}</td>
                                @foreach ($depositos as $dep)
                                @if ($producao->depositoorigem == $dep->id)
                                <td field-key='depositoorigem'>{{ $dep->descricao}}</td>
                                @endif
                                @endforeach
                             
                                @foreach ($depositos as $depd)
                                @if ($producao->depositodestino == $depd->id)
                                <td field-key='depositodestino'>{{ $depd->descricao}}</td>
                                @endif
                                @endforeach
                                @if (($producao->situacao == 'COSTURA'))
                                <td class="label label-danger " field-key='situacao'>{{ $producao->situacao }}</td>
                                @endif
                                @if (($producao->situacao == 'CONCLUIDO'))
                                <td class="label label-success " field-key='situacao'>{{ $producao->situacao }}</td>
                                @endif
                                @if (($producao->situacao == 'CORTE'))
                                <td class="label label-primary" field-key='situacao'>{{ $producao->situacao }}</td>
                                @endif
                                @foreach ($fornecedors as $ford)
                                @if ($producao->idfornecedor == $ford->id)
                                <td field-key='idfornecedor'>{{ $ford->razao}}</td>
                                @endif
                                @endforeach
                                <td field-key='data'>{{ $producao->data }}</td>
                        
                                 @if (isset($producao->barra))
                                <td field-key='barra'>{{ $producao->barra }}</td>
                                @else
                                <td field-key='barra'></td>
                                @endif
                             @if (isset($producao->bolso ))
                             <td field-key='bolso'>{{ $producao->bolso }}</td>
                             @else
                             <td field-key='bolso'></td>
                             @endif
                             @if (isset($producao->capuz ))
                             <td field-key='capuz'>{{ $producao->capuz }}</td>
                             @else
                             <td field-key='capuz'></td>
                             @endif
                             @if (isset($producao->costa ))
                             <td field-key='costa'>{{ $producao->costa }}</td>
                             @else
                             <td field-key='costa'></td>
                             @endif
                             @if (isset($producao->frente ))
                             <td field-key='frente'>{{ $producao->frente }}</td> 
                             @else
                             <td field-key='frente'></td>
                             @endif
                             @if (isset($producao->etiqueta ))
                             <td field-key='etiqueta'>{{ $producao->etiqueta }}</td>
                             @else
                             <td field-key='etiqueta'></td> 
                             @endif
                             @if (isset($producao->manga ))
                             <td field-key='manga'>{{ $producao->manga }}</td>
                             @else
                             <td field-key='manga'></td> 
                             @endif
                             @if (isset($producao->punho ))
                              <td field-key='punho'>{{ $producao->punho }}</td>
                             @else
                              <td field-key='punho'></td>
                             @endif
                             @if (isset($producao->orelha ))
                              <td field-key='orelha'>{{ $producao->orelha }}</td>
                             @else
                              <td field-key='orelha'></td>
                             @endif
                             @if (isset($producao->chifre ))
                             <td field-key='chifre'>{{ $producao->chifre }}</td>
                            @else
                             <td field-key='chifre'></td>
                            @endif
                            @if (isset($producao->cos ))
                            <td field-key='cos'>{{ $producao->cos }}</td>
                           @else
                            <td field-key='cos'></td>
                           @endif
                          
                 
                                <td field-key='obs'>{{ $producao->obs }}</td>
                                {{-- <td field-key='obs'>{{ $producao->obs }}</td> --}}
                               
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="12">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
    <script>
        @can('producao_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.producaos.mass_destroy') }}'; @endif
        @endcan
        function moeda(z){

v = z.value;

v=v.replace(/\D/g,"") //permite digitar apenas números

v=v.replace(/[0-9]{12}/,"inválido") //limita pra máximo 999.999.999,99

v=v.replace(/(\d{1})(\d{8})$/,"$1.$2") //coloca ponto antes dos últimos 8 digitos

v=v.replace(/(\d{1})(\d{1,2})$/,"$1.$2") //coloca ponto antes dos últimos 5 digitos

//v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2") //coloca virgula antes dos últimos 2 digitos

z.value = v;

}
        $(document).ready(function(){
             $('#idProdutoProducao').keyup(function(){
                 var query = $(this).val();
                 if(query.text != '' )
                 {
                   var _token = $('input[name="_token"]').val();
                      $.ajax({
                         url:"{{ route('producaos.fetchProducao')}}",
                   
                         method:"POST",
                         data:{query:query,_token:_token},
                         success:function(data)
                         {
                             $('#idProdutoProducaolist').fadeIn();
                             $('#idProdutoProducaolist').html(data);
                         }
                     })
                 }
               
             });
      
             $(document).on('click','li.c',function(){
                                 
                                 $('#idProdutoProducao').val($(this).text());
                                 $('#idProdutoProducaolist').fadeOut()
                                 var _token = $('input[name="_token"]').val();
                      $.ajax({
                         url:"{{ route('admin.producaos.importartelaP')}}",
                   
                         method:"POST",
                         data:{query:query,_token:_token},
                         success:function(data)
                         {
                             $('#idProdutoProducaolist').fadeIn();
                             $('#idProdutoProducaolist').html(data);
                         }
                     })
                         
                              })
                     })
          
               
    </script>



@endsection