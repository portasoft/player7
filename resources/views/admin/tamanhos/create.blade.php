@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.tamanho.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.tamanhos.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>CADASTRO - TAMANHO</h4>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4 form-group">
                    {!! Form::label('descricao', trans('quickadmin.tamanho.fields.descricao').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('descricao', old('descricao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('descricao'))
                        <p class="help-block">
                            {{ $errors->first('descricao') }}
                        </p>
                    @endif
                </div>
         
                <div class="col-xs-2 form-group">
                    {!! Form::label('grade', trans('quickadmin.tamanho.fields.grade').'', ['class' => 'control-label']) !!}
                    {!! Form::text('grade', old('grade'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('grade'))
                        <p class="help-block">
                            {{ $errors->first('grade') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

