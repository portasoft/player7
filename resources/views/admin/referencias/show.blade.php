@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.referencia.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
           <h4>VIZUALIZAÇÃO - REFERÊNCIA</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.referencia.fields.descricao')</th>
                            <td field-key='descricao'>{{ $referencia->descricao }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.referencias.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
