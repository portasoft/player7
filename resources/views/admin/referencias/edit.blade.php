@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.referencia.title')</h3> --}}
    
    {!! Form::model($referencia, ['method' => 'PUT', 'route' => ['admin.referencias.update', $referencia->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
         <h4>ALTERAR - REFERÊNCIA</h4>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4 form-group">
                    {!! Form::label('descricao', trans('quickadmin.referencia.fields.descricao').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('descricao', old('descricao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('descricao'))
                        <p class="help-block">
                            {{ $errors->first('descricao') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
            <i class="fa fa-refresh"> </i>
        ALTERAR</button>
    {{-- {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
    @can('referencium_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.referencias.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.referencias.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
           <h4>RELATÓRIO - REFERÊNCIA</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('referencium_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('referencium_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.referencia.fields.descricao')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('referencium_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.referencias.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.referencias.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('referencium_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'descricao', name: 'descricao'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@stop

