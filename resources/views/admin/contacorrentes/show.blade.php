@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contacorrente.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
         <h4>VIZUALIZAÇÃO - CONTA CORRENTE</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.contacorrente.fields.agencia')</th>
                            <td field-key='agencia'>{{ $contacorrente->agencia }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contacorrente.fields.conta')</th>
                            <td field-key='conta'>{{ $contacorrente->conta }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contacorrente.fields.banco')</th>
                            <td field-key='banco'>{{ $contacorrente->banco }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contacorrente.fields.empresa')</th>
                            <td field-key='empresa'>{{ $contacorrente->empresa }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contacorrente.fields.saldoinicial')</th>
                            <td field-key='saldoinicial'>{{ $contacorrente->saldoinicial }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contacorrente.fields.saldo')</th>
                            <td field-key='saldo'>{{ $contacorrente->saldo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contacorrente.fields.anexo')</th>
                            <td field-key='anexo'>@if($contacorrente->anexo)<a href="{{ asset(env('UPLOAD_PATH').'/' . $contacorrente->anexo) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.contacorrentes.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
