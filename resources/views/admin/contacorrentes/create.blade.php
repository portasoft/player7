@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contacorrente.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.contacorrentes.store'], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>CADASTRO - CONTA CORRENTE</h4>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 form-group">
                    {!! Form::label('agencia', trans('quickadmin.contacorrente.fields.agencia').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('agencia', old('agencia'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('agencia'))
                        <p class="help-block">
                            {{ $errors->first('agencia') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('conta', trans('quickadmin.contacorrente.fields.conta').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('conta', old('conta'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('conta'))
                        <p class="help-block">
                            {{ $errors->first('conta') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('banco', trans('quickadmin.contacorrente.fields.banco').'', ['class' => 'control-label']) !!}
                    {!! Form::text('banco', old('banco'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('banco'))
                        <p class="help-block">
                            {{ $errors->first('banco') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-3 form-group">
                    {!! Form::label('empresa', trans('quickadmin.contacorrente.fields.empresa').'', ['class' => 'control-label']) !!}
                    {!! Form::select('empresa',$empresa,null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('empresa'))
                        <p class="help-block">
                            {{ $errors->first('empresa') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('saldoinicial', trans('quickadmin.contacorrente.fields.saldoinicial').'', ['class' => 'control-label']) !!}
                    {!! Form::text('saldoinicial', old('saldoinicial'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('saldoinicial'))
                        <p class="help-block">
                            {{ $errors->first('saldoinicial') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('saldo', trans('quickadmin.contacorrente.fields.saldo').'', ['class' => 'control-label']) !!}
                    {!! Form::text('saldo', old('saldo'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('saldo'))
                        <p class="help-block">
                            {{ $errors->first('saldo') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-4 form-group">
                    {!! Form::label('anexo', trans('quickadmin.contacorrente.fields.anexo').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('anexo', old('anexo')) !!}
                    {!! Form::file('anexo', ['class' => 'form-control']) !!}
                    {!! Form::hidden('anexo_max_size', 2) !!}
                    <p class="help-block"></p>
                    @if($errors->has('anexo'))
                        <p class="help-block">
                            {{ $errors->first('anexo') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

