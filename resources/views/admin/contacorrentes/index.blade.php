@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contacorrente.title')</h3> --}}
    @can('contacorrente_create')
    <p>
        <a href="{{ route('admin.contacorrentes.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('contacorrente_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.contacorrentes.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.contacorrentes.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
          <h4>RELATÓRIO - CONTA CORRENTE</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('contacorrente_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('contacorrente_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.contacorrente.fields.agencia')</th>
                        <th>@lang('quickadmin.contacorrente.fields.conta')</th>
                        <th>@lang('quickadmin.contacorrente.fields.banco')</th>
                        <th>@lang('quickadmin.contacorrente.fields.empresa')</th>
                        <th>@lang('quickadmin.contacorrente.fields.saldoinicial')</th>
                        <th>@lang('quickadmin.contacorrente.fields.saldo')</th>
                        <th>@lang('quickadmin.contacorrente.fields.anexo')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('contacorrente_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.contacorrentes.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.contacorrentes.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('contacorrente_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'agencia', name: 'agencia'},
                {data: 'conta', name: 'conta'},
                {data: 'banco', name: 'banco'},
                {data: 'empresa', name: 'empresa'},
                {data: 'saldoinicial', name: 'saldoinicial'},
                {data: 'saldo', name: 'saldo'},
                {data: 'anexo', name: 'anexo'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection