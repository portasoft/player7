@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.fornecedor.title')</h3> --}}
    
    {!! Form::model($fornecedor, ['method' => 'PUT', 'route' => ['admin.fornecedors.update', $fornecedor->id], 'files' => true,]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">EDITAR - FORNECEDOR</h4>
        </div>

        <div class="panel-body">
                <div class="row">
                        <div class="col-xs-2 form-group">
                            {!! Form::label('cnpj', trans('quickadmin.fornecedor.fields.cnpj').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('cnpj', old('cnpj'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('cnpj'))
                                <p class="help-block">
                                    {{ $errors->first('cnpj') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-4 form-group">
                            {!! Form::label('razao', trans('quickadmin.fornecedor.fields.razao').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('razao', old('razao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('razao'))
                                <p class="help-block">
                                    {{ $errors->first('razao') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-4 form-group">
                            {!! Form::label('fantasia', trans('quickadmin.fornecedor.fields.fantasia').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('fantasia', old('fantasia'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('fantasia'))
                                <p class="help-block">
                                    {{ $errors->first('fantasia') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-2 form-group">
                            {!! Form::label('contato', trans('quickadmin.fornecedor.fields.contato').'', ['class' => 'control-label']) !!}
                            {!! Form::text('contato', old('contato'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('contato'))
                                <p class="help-block">
                                    {{ $errors->first('contato') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-2 form-group">
                            {!! Form::label('datacadastro', trans('quickadmin.fornecedor.fields.datacadastro').'', ['class' => 'control-label']) !!}
                            {!! Form::date('datacadastro', old('datacadastro'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('datacadastro'))
                                <p class="help-block">
                                    {{ $errors->first('datacadastro') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-2 form-group">
                            {!! Form::label('ie', trans('quickadmin.fornecedor.fields.ie').'', ['class' => 'control-label']) !!}
                            {!! Form::text('ie', old('ie'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('ie'))
                                <p class="help-block">
                                    {{ $errors->first('ie') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-2 form-group">
                            {!! Form::label('im', trans('quickadmin.fornecedor.fields.im').'', ['class' => 'control-label']) !!}
                            {!! Form::text('im', old('im'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('im'))
                                <p class="help-block">
                                    {{ $errors->first('im') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-2 form-group">
                            {!! Form::label('segmento', trans('quickadmin.fornecedor.fields.segmento').'', ['class' => 'control-label']) !!}
                            {!! Form::text('segmento', old('segmento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('segmento'))
                                <p class="help-block">
                                    {{ $errors->first('segmento') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-2 form-group">
                            {!! Form::label('telefone', trans('quickadmin.fornecedor.fields.telefone').'', ['class' => 'control-label']) !!}
                            {!! Form::text('telefone', old('telefone'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('telefone'))
                                <p class="help-block">
                                    {{ $errors->first('telefone') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-2 form-group">
                            {!! Form::label('celular', trans('quickadmin.fornecedor.fields.celular').'', ['class' => 'control-label']) !!}
                            {!! Form::text('celular', old('celular'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('celular'))
                                <p class="help-block">
                                    {{ $errors->first('celular') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-4 form-group">
                            {!! Form::label('email', trans('quickadmin.fornecedor.fields.email').'', ['class' => 'control-label']) !!}
                            {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('email'))
                                <p class="help-block">
                                    {{ $errors->first('email') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-2 form-group">
                            {!! Form::label('cep', trans('quickadmin.fornecedor.fields.cep').'', ['class' => 'control-label']) !!}
                            {!! Form::text('cep', old('cep'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('cep'))
                                <p class="help-block">
                                    {{ $errors->first('cep') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-4 form-group">
                            {!! Form::label('endereco', trans('quickadmin.fornecedor.fields.endereco').'', ['class' => 'control-label']) !!}
                            {!! Form::text('endereco', old('endereco'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('endereco'))
                                <p class="help-block">
                                    {{ $errors->first('endereco') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-2 form-group">
                            {!! Form::label('numero', trans('quickadmin.fornecedor.fields.numero').'', ['class' => 'control-label']) !!}
                            {!! Form::text('numero', old('numero'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('numero'))
                                <p class="help-block">
                                    {{ $errors->first('numero') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-4 form-group">
                            {!! Form::label('bairro', trans('quickadmin.fornecedor.fields.bairro').'', ['class' => 'control-label']) !!}
                            {!! Form::text('bairro', old('bairro'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('bairro'))
                                <p class="help-block">
                                    {{ $errors->first('bairro') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-3 form-group">
                            {!! Form::label('cidade', trans('quickadmin.fornecedor.fields.cidade').'', ['class' => 'control-label']) !!}
                            {!! Form::text('cidade', old('cidade'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('cidade'))
                                <p class="help-block">
                                    {{ $errors->first('cidade') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-1 form-group">
                            {!! Form::label('uf', trans('quickadmin.fornecedor.fields.uf').'', ['class' => 'control-label']) !!}
                            {!! Form::text('uf', old('uf'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('uf'))
                                <p class="help-block">
                                    {{ $errors->first('uf') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-4 form-group">
                            {!! Form::label('complemento', trans('quickadmin.fornecedor.fields.complemento').'', ['class' => 'control-label']) !!}
                            {!! Form::text('complemento', old('complemento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('complemento'))
                                <p class="help-block">
                                    {{ $errors->first('complemento') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-8 form-group">
                            {!! Form::label('obs', trans('quickadmin.fornecedor.fields.obs').'', ['class' => 'control-label']) !!}
                            {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('obs'))
                                <p class="help-block">
                                    {{ $errors->first('obs') }}
                                </p>
                            @endif
                        </div>
                  
                        <div class="col-xs-4 form-group">
                            {!! Form::label('foto', trans('quickadmin.fornecedor.fields.foto').'', ['class' => 'control-label']) !!}
                            {!! Form::file('foto', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                            {!! Form::hidden('foto_max_size', 2) !!}
                            {!! Form::hidden('foto_max_width', 4096) !!}
                            {!! Form::hidden('foto_max_height', 4096) !!}
                            <p class="help-block"></p>
                            @if($errors->has('foto'))
                                <p class="help-block">
                                    {{ $errors->first('foto') }}
                                </p>
                            @endif
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('localizacao_address', trans('quickadmin.fornecedor.fields.localizacao').'', ['class' => 'control-label']) !!}
                            {!! Form::text('localizacao_address', old('localizacao_address'), ['class' => 'form-control map-input', 'id' => 'localizacao-input']) !!}
                            {!! Form::hidden('localizacao_latitude', 0 , ['id' => 'localizacao-latitude']) !!}
                            {!! Form::hidden('localizacao_longitude', 0 , ['id' => 'localizacao-longitude']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('localizacao'))
                                <p class="help-block">
                                    {{ $errors->first('localizacao') }}
                                </p>
                            @endif
                        </div>
                    </div> --}}
        {{--             
                    <div id="localizacao-map-container" style="width:100%;height:200px; ">
                        <div style="width: 100%; height: 100%" id="localizacao-map"></div>
                    </div>
                    @if(!env('GOOGLE_MAPS_API_KEY'))
                        <b>'GOOGLE_MAPS_API_KEY' is not set in the .env</b>
                    @endif --}}
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
        <i class="fa fa-refresh"> </i>
    ALTERAR</button>
        
    {{-- {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
   <script src="/adminlte/js/mapInput.js"></script>
   <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop