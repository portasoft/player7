@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.fornecedor.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
         <h4 id="TNome">VIZUALIZAR - FORNECEDOR</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.cnpj')</th>
                            <td field-key='cnpj'>{{ $fornecedor->cnpj }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.razao')</th>
                            <td field-key='razao'>{{ $fornecedor->razao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.fantasia')</th>
                            <td field-key='fantasia'>{{ $fornecedor->fantasia }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.contato')</th>
                            <td field-key='contato'>{{ $fornecedor->contato }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.datacadastro')</th>
                            <td field-key='datacadastro'>{{ $fornecedor->datacadastro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.ie')</th>
                            <td field-key='ie'>{{ $fornecedor->ie }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.im')</th>
                            <td field-key='im'>{{ $fornecedor->im }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.segmento')</th>
                            <td field-key='segmento'>{{ $fornecedor->segmento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.telefone')</th>
                            <td field-key='telefone'>{{ $fornecedor->telefone }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.celular')</th>
                            <td field-key='celular'>{{ $fornecedor->celular }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.email')</th>
                            <td field-key='email'>{{ $fornecedor->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.cep')</th>
                            <td field-key='cep'>{{ $fornecedor->cep }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.endereco')</th>
                            <td field-key='endereco'>{{ $fornecedor->endereco }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.numero')</th>
                            <td field-key='numero'>{{ $fornecedor->numero }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.bairro')</th>
                            <td field-key='bairro'>{{ $fornecedor->bairro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.cidade')</th>
                            <td field-key='cidade'>{{ $fornecedor->cidade }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.uf')</th>
                            <td field-key='uf'>{{ $fornecedor->uf }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.complemento')</th>
                            <td field-key='complemento'>{{ $fornecedor->complemento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.obs')</th>
                            <td field-key='obs'>{{ $fornecedor->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.foto')</th>
                            <td field-key='foto'>@if($fornecedor->foto)<a href="{{ asset(env('UPLOAD_PATH').'/' . $fornecedor->foto) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $fornecedor->foto) }}"/></a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.fornecedor.fields.localizacao')</th>
                            <td field-key='localizacao'>{{ $fornecedor->localizacao_address }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.fornecedors.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
