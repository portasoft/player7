@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.fornecedor.title')</h3> --}}
    @can('fornecedor_create')
    {{-- <p>
        <a href="{{ route('admin.fornecedors.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p> --}}
    <p>
        <a href="{{ route('admin.fornecedors.create') }}"  type="submit" class="btn btn-success btn-lg">
            <i class="fa fa-save"> </i>
        NOVO</a>
        
    </p>
    @endcan

    @can('fornecedor_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.fornecedors.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.fornecedors.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
       <h4 id="TNome">RELATÓRIO - FORNECEDOR</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('fornecedor_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('fornecedor_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.fornecedor.fields.cnpj')</th>
                        <th>@lang('quickadmin.fornecedor.fields.razao')</th>
                        <th>@lang('quickadmin.fornecedor.fields.fantasia')</th>
                        <th>@lang('quickadmin.fornecedor.fields.contato')</th>
                        <th>@lang('quickadmin.fornecedor.fields.datacadastro')</th>
                        <th>@lang('quickadmin.fornecedor.fields.ie')</th>
                        <th>@lang('quickadmin.fornecedor.fields.im')</th>
                        <th>@lang('quickadmin.fornecedor.fields.segmento')</th>
                        <th>@lang('quickadmin.fornecedor.fields.telefone')</th>
                        <th>@lang('quickadmin.fornecedor.fields.celular')</th>
                        <th>@lang('quickadmin.fornecedor.fields.email')</th>
                        <th>@lang('quickadmin.fornecedor.fields.cep')</th>
                        <th>@lang('quickadmin.fornecedor.fields.endereco')</th>
                        <th>@lang('quickadmin.fornecedor.fields.numero')</th>
                        <th>@lang('quickadmin.fornecedor.fields.bairro')</th>
                        <th>@lang('quickadmin.fornecedor.fields.cidade')</th>
                        <th>@lang('quickadmin.fornecedor.fields.uf')</th>
                        <th>@lang('quickadmin.fornecedor.fields.complemento')</th>
                        <th>@lang('quickadmin.fornecedor.fields.obs')</th>
                        <th>@lang('quickadmin.fornecedor.fields.foto')</th>
                        <th>@lang('quickadmin.fornecedor.fields.localizacao')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('fornecedor_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.fornecedors.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.fornecedors.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('fornecedor_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'cnpj', name: 'cnpj'},
                {data: 'razao', name: 'razao'},
                {data: 'fantasia', name: 'fantasia'},
                {data: 'contato', name: 'contato'},
                {data: 'datacadastro', name: 'datacadastro'},
                {data: 'ie', name: 'ie'},
                {data: 'im', name: 'im'},
                {data: 'segmento', name: 'segmento'},
                {data: 'telefone', name: 'telefone'},
                {data: 'celular', name: 'celular'},
                {data: 'email', name: 'email'},
                {data: 'cep', name: 'cep'},
                {data: 'endereco', name: 'endereco'},
                {data: 'numero', name: 'numero'},
                {data: 'bairro', name: 'bairro'},
                {data: 'cidade', name: 'cidade'},
                {data: 'uf', name: 'uf'},
                {data: 'complemento', name: 'complemento'},
                {data: 'obs', name: 'obs'},
                {data: 'foto', name: 'foto'},
                {data: 'localizacao_address', name: 'localizacao_address'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection