@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.users.title')</h3> --}}
    @can('user_create')
    
    <p>
        <a href="{{ route('admin.users.create') }}"  type="submit" class="btn btn-success btn-lg">
            <i class="fa fa-plus-circle"> </i>
        NOVO</a>
        
    </p>
    @endcan

    

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">RELATÓRIO - USUÁRIOS</h4> 
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>ALTERADO COM SUCESSO!</h4>
         </div> 
        @endif
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($users) > 0 ? 'datatable' : '' }} @can('user_delete') dt-select @endcan">
                <thead>
                    <tr>
                        @can('user_delete')
                            <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        @endcan

                        <th>@lang('quickadmin.users.fields.name')</th>
                        <th>@lang('quickadmin.users.fields.email')</th>
                        <th>@lang('quickadmin.users.fields.role')</th>
                        <th>NOME</th>
                        {{-- <th>@lang('quickadmin.users.fields.datanascimento')</th> --}}
                        {{-- <th>@lang('quickadmin.users.fields.cep')</th>
                        <th>@lang('quickadmin.users.fields.endereco')</th>
                        <th>@lang('quickadmin.users.fields.numero')</th>
                        <th>@lang('quickadmin.users.fields.bairro')</th>
                        <th>@lang('quickadmin.users.fields.cidade')</th>
                        <th>@lang('quickadmin.users.fields.uf')</th>
                        <th>@lang('quickadmin.users.fields.complemento')</th> --}}
                        <th>@lang('quickadmin.users.fields.fone')</th>
                        <th>@lang('quickadmin.users.fields.celular')</th>
                        {{-- <th>@lang('quickadmin.users.fields.cpf')</th> --}}
                        {{-- <th>@lang('quickadmin.users.fields.rg')</th>
                        <th>@lang('quickadmin.users.fields.datacadastro')</th>
                        <th>@lang('quickadmin.users.fields.comissao')</th>
                        <th>@lang('quickadmin.users.fields.salario')</th>
                        <th>@lang('quickadmin.users.fields.empresa')</th> --}}
                        <th>@lang('quickadmin.users.fields.situacao')</th>
                        {{-- <th>@lang('quickadmin.users.fields.obs')</th> --}}
                        <th>@lang('quickadmin.users.fields.vendedor')</th>
                        <th>@lang('quickadmin.users.fields.foto')</th>
                        {{-- <th>@lang('quickadmin.users.fields.localizacao')</th> --}}
                                                <th>&nbsp;</th>

                    </tr>
                </thead>
                
                <tbody>
                    @if (count($users) > 0)
                        @foreach ($users as $user)
                            <tr data-entry-id="{{ $user->id }}">
                                @can('user_delete')
                                    <td></td>
                                @endcan

                                <td field-key='name'>{{ $user->name }}</td>
                                <td field-key='email'>{{ $user->email }}</td>
                                <td field-key='role'>{{ $user->role->title or '' }}</td>
                                <td field-key='nomecompleto'>{{ $user->nomecompleto }}</td>
                                {{-- <td field-key='datanascimento'>{{ $user->datanascimento }}</td> --}}
                                {{-- <td field-key='cep'>{{ $user->cep }}</td>
                                <td field-key='endereco'>{{ $user->endereco }}</td>
                                <td field-key='numero'>{{ $user->numero }}</td>
                                <td field-key='bairro'>{{ $user->bairro }}</td>
                                <td field-key='cidade'>{{ $user->cidade }}</td>
                                <td field-key='uf'>{{ $user->uf }}</td>
                                <td field-key='complemento'>{{ $user->complemento }}</td> --}}
                                <td field-key='fone'>{{ $user->fone }}</td>
                                <td field-key='celular'>{{ $user->celular }}</td>
                                {{-- <td field-key='cpf'>{{ $user->cpf }}</td> --}}
                                {{-- <td field-key='rg'>{{ $user->rg }}</td>
                                <td field-key='datacadastro'>{{ $user->datacadastro }}</td>
                                <td field-key='comissao'>{{ $user->comissao }}</td>
                                <td field-key='salario'>{{ $user->salario }}</td>
                                <td field-key='empresa'>{{ $user->empresa }}</td> --}}
                                <td field-key='situacao'>{{ $user->situacao }}</td>
                                {{-- <td field-key='obs'>{{ $user->obs }}</td> --}}
                                <td field-key='vendedor'>{{ $user->vendedor }}</td>
                                <td field-key='foto'>@if($user->foto)<a href="{{ asset(env('UPLOAD_PATH').'/' . $user->foto) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $user->foto) }}"/></a>@endif</td>
                                {{-- <td field-key='localizacao'>{{ $user->localizacao_address }}</td> --}}
                                                                <td>
                                    @can('user_view')
                                    <a href="{{ route('admin.users.show',[$user->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    @can('user_edit')
                                    <a href="{{ route('admin.users.edit',[$user->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('user_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.users.destroy', $user->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan
                                </td>

                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="32">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
    <script>
        @can('user_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.users.mass_destroy') }}';
        @endcan

    </script>
@endsection