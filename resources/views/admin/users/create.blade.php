@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.users.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.users.store'], 'files' => true,]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
          <h4 id="TNome">CADASTRO - USUÁRIO</h4> 
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>CADASTRADO COM SUCESSO!</h4>
         </div> 
        @endif
        <div class="panel-body">
                <div class="row">
                        <div class="col-xs-6 form-group">
                            {!! Form::label('nomecompleto', trans('quickadmin.users.fields.nomecompleto').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('nomecompleto', old('nomecompleto'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('nomecompleto'))
                                <p class="help-block">
                                    {{ $errors->first('nomecompleto') }}
                                </p>
                            @endif
                        </div>
                     
                            <div class="col-xs-3 form-group">
                                    {!! Form::label('situacao', trans('quickadmin.users.fields.situacao').'', ['class' => 'control-label']) !!}
                                    {!! Form::select('situacao',['Ativo','Inátivo'],null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('situacao'))
                                        <p class="help-block">
                                            {{ $errors->first('situacao') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="col-xs-3 form-group">
                                        {!! Form::label('vendedor', trans('quickadmin.users.fields.vendedor').'', ['class' => 'control-label']) !!}
                                        {!! Form::select('vendedor',['Sim','Não'],null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('vendedor'))
                                            <p class="help-block">
                                                {{ $errors->first('vendedor') }}
                                            </p>
                                        @endif
                                    </div>
                                <div class="col-xs-3 form-group">
                                         {!! Form::label('role_id', trans('quickadmin.users.fields.role').'*', ['class' => 'control-label']) !!}
                                         {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                                         <p class="help-block"></p>
                                         @if($errors->has('role_id'))
                                         <p class="help-block">
                                         {{ $errors->first('role_id') }}
                                         </p>
                                        @endif
                                </div>
                                <div class="col-xs-3 form-group">
                                        <label for="">CADASTRO</label>
                                        {!! Form::date('datacadastro', old('datacadastro'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('datacadastro'))
                                            <p class="help-block">
                                                {{ $errors->first('datacadastro') }}
                                            </p>
                                        @endif
                                </div>
                                <div class="col-xs-3 form-group">
                                         {!! Form::label('cpf', trans('quickadmin.users.fields.cpf').'', ['class' => 'control-label']) !!}
                                         {!! Form::text('cpf', old('cpf'), ['class' => 'form-control','id'=>'cpf', 'placeholder' => '','onKeyUp'=>'mascara_cpf()',
                                          'maxlength'=>'14']) !!}
                                         <p class="help-block"></p>
                                         @if($errors->has('cpf'))
                                         <p class="help-block">
                                         {{ $errors->first('cpf') }}
                                         </p>
                                        @endif
                                </div>
                                <div class="col-xs-3 form-group">
                                         {!! Form::label('rg', trans('quickadmin.users.fields.rg').'', ['class' => 'control-label']) !!}
                                         {!! Form::text('rg', old('rg'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                         <p class="help-block"></p>
                                          @if($errors->has('rg'))
                                         <p class="help-block">
                                          {{ $errors->first('rg') }}
                                         </p>
                                         @endif
                                </div>
                                <div class="col-xs-3 form-group">
                                        <label for="">NASCIMENTO</label>
                                         {!! Form::date('datanascimento', old('datanascimento'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                                         <p class="help-block"></p>
                                         @if($errors->has('datanascimento'))
                                         <p class="help-block">
                                         {{ $errors->first('datanascimento') }}
                                        </p>
                                         @endif
                                 </div>
                                                  
                                <div class="col-xs-3 form-group">
                                    <label for="">TELEFONE</label>
                                         {!! Form::text('fone', old('fone'), ['class' => 'form-control', 'placeholder' => '','onkeypress'=>'validarTelefoneOk()',
                                         'maxlength'=>'13']) !!}
                                         <p class="help-block"></p>
                                          @if($errors->has('fone'))
                                         <p class="help-block">
                                         {{ $errors->first('fone') }}
                                         </p>
                                         @endif
                                 </div>
                                <div class="col-xs-3 form-group">
                                    <label for="">CELULAR</label>
                                         {!! Form::text('celular', old('celular'), ['class' => 'form-control', 'placeholder' => '','onkeypress'=>'validarCelularOk()',
                                         'maxlength'=>'14']) !!}
                                         <p class="help-block"></p>
                                         @if($errors->has('celular'))
                                        <p class="help-block">
                                         {{ $errors->first('celular') }}
                                        </p>
                                        @endif
                                </div>
                                <div class="col-xs-3 form-group">
                                    <label for="">CEP</label>
                                        {!! Form::text('cep', old('cep'), ['class' => 'form-control', 'placeholder' => '','onblur'=>'pesquisacep(this.value)','onkeypress'=>'mascaraCep()' ,'maxlength'=>'9']) !!}
                                         <p class="help-block"></p>
                                        @if($errors->has('cep'))
                                        <p class="help-block">
                                        {{ $errors->first('cep') }}
                                         </p>
                                        @endif
                                 </div>
                                 <div class="col-xs-5 form-group">
                                    <label for="">RUA</label>
                                        {!! Form::text('endereco', old('endereco'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                         <p class="help-block"></p>
                                        @if($errors->has('endereco'))
                                        <p class="help-block">
                                        {{ $errors->first('endereco') }}
                                         </p>
                                        @endif
                                 </div>
                                 <div class="col-xs-2 form-group">
                                        {!! Form::label('numero', trans('quickadmin.users.fields.numero').'', ['class' => 'control-label']) !!}
                                        {!! Form::text('numero', old('numero'), ['class' => 'form-control','id'=>'numero','placeholder' => '','onkeypress'=>'numeros()','maxlength'=>'5']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('numero'))
                                        <p class="help-block">
                                        {{ $errors->first('numero') }}
                                        </p>
                                         @endif
                                 </div>
                                 <div class="col-xs-5 form-group">
                                         {!! Form::label('bairro', trans('quickadmin.users.fields.bairro').'', ['class' => 'control-label']) !!}
                                         {!! Form::text('bairro', old('bairro'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                        <p class="help-block"></p>
                                         @if($errors->has('bairro'))
                                        <p class="help-block">
                                        {{ $errors->first('bairro') }}
                                        </p>
                                        @endif
                                 </div>
                                 <div class="col-xs-4 form-group">
                                         {!! Form::label('cidade', trans('quickadmin.users.fields.cidade').'', ['class' => 'control-label']) !!}
                                         {!! Form::text('cidade', old('cidade'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                         <p class="help-block"></p>
                                         @if($errors->has('cidade'))
                                         <p class="help-block">
                                        {{ $errors->first('cidade') }}
                                        </p>
                                        @endif
                                 </div>
                                  <div class="col-xs-2 form-group">
                                         {!! Form::label('uf', trans('quickadmin.users.fields.uf').'', ['class' => 'control-label']) !!}
                                         {!! Form::text('uf', old('uf'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                         <p class="help-block"></p>
                                          @if($errors->has('uf'))
                                         <p class="help-block">
                                          {{ $errors->first('uf') }}
                                          </p>
                                         @endif
                                  </div>
                                   <div class="col-xs-6 form-group">
                                         {!! Form::label('complemento', trans('quickadmin.users.fields.complemento').'', ['class' => 'control-label']) !!}
                                         {!! Form::text('complemento', old('complemento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                         <p class="help-block"></p>
                                        @if($errors->has('complemento'))
                                        <p class="help-block">
                                         {{ $errors->first('complemento') }}
                                          </p>
                                         @endif
                                     </div>
                                  
                                  
                                     <div class="col-xs-2 form-group">
                                                    {!! Form::label('comissao', trans('quickadmin.users.fields.comissao').'', ['class' => 'control-label']) !!}
                                                    {!! Form::text('comissao', old('comissao'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                                    <p class="help-block"></p>
                                                    @if($errors->has('comissao'))
                                                        <p class="help-block">
                                                            {{ $errors->first('comissao') }}
                                                        </p>
                                                    @endif
                                    </div>
                                           
                                    <div class="col-xs-3 form-group">
                                                    {!! Form::label('salario', trans('quickadmin.users.fields.salario').'', ['class' => 'control-label']) !!}
                                                    {!! Form::text('salario', old('salario'), ['class' => 'form-control', 'placeholder' => '','onKeyUp'=>'moeda(this)']) !!}
                                                    <p class="help-block"></p>
                                                    @if($errors->has('salario'))
                                                        <p class="help-block">
                                                            {{ $errors->first('salario') }}
                                                        </p>
                                                    @endif
                                    </div>
                                    
                                    <div class="col-xs-5 form-group">
                                            {!! Form::label('empresa', trans('quickadmin.users.fields.empresa').'', ['class' => 'control-label']) !!}
                                            {!! Form::select('empresa',$empresa,null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                            <p class="help-block"></p>
                                            @if($errors->has('empresa'))
                                                <p class="help-block">
                                                    {{ $errors->first('empresa') }}
                                                </p>
                                            @endif
                                    </div>
                                    <div class="col-xs-2 form-group">
                                        {!! Form::label('foto', trans('quickadmin.users.fields.foto').'', ['class' => 'control-label']) !!}
                                        {!! Form::file('foto', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                                        {!! Form::hidden('foto_max_size', 2) !!}
                                        {!! Form::hidden('foto_max_width', 4096) !!}
                                        {!! Form::hidden('foto_max_height', 4096) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('foto'))
                                            <p class="help-block">
                                                {{ $errors->first('foto') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="col-xs-5 form-group">
                                            {!! Form::label('email', trans('quickadmin.users.fields.email').'*', ['class' => 'control-label']) !!}
                                            {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                            <p class="help-block"></p>
                                            @if($errors->has('email'))
                                                <p class="help-block">
                                                    {{ $errors->first('email') }}
                                                </p>
                                            @endif
                                    </div>
                                  
                                        {{-- <div class="col-xs-5 form-group">
                                                {!! Form::label('obs', trans('quickadmin.users.fields.obs').'', ['class' => 'control-label']) !!}
                                                {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                                <p class="help-block"></p>
                                                @if($errors->has('obs'))
                                                    <p class="help-block">
                                                        {{ $errors->first('obs') }}
                                                    </p>
                                                @endif
                                            </div> --}}
                                            <div class="col-xs-3 form-group">
                                                    {!! Form::label('name', trans('quickadmin.users.fields.name').'*', ['class' => 'control-label']) !!}
                                                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                                    <p class="help-block"></p>
                                                    @if($errors->has('name'))
                                                        <p class="help-block">
                                                            {{ $errors->first('name') }}
                                                        </p>
                                                    @endif
                                                </div>
                            <div class="col-xs-3 form-group">
                                {!! Form::label('password', trans('quickadmin.users.fields.password').'*', ['class' => 'control-label']) !!}
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '','required']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('password'))
                                    <p class="help-block">
                                        {{ $errors->first('password') }}
                                    </p>
                                @endif
                            </div>
                        
                </div>
            
             
            {{-- <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('localizacao_address', trans('quickadmin.users.fields.localizacao').'', ['class' => 'control-label']) !!}
                    {!! Form::text('localizacao_address', old('localizacao_address'), ['class' => 'form-control map-input', 'id' => 'localizacao-input']) !!}
                    {!! Form::hidden('localizacao_latitude', 0 , ['id' => 'localizacao-latitude']) !!}
                    {!! Form::hidden('localizacao_longitude', 0 , ['id' => 'localizacao-longitude']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('localizacao'))
                        <p class="help-block">
                            {{ $errors->first('localizacao') }}
                        </p>
                    @endif
                </div>
            </div> --}}
{{--             
            <div id="localizacao-map-container" style="width:100%;height:200px; ">
                <div style="width: 100%; height: 100%" id="localizacao-map"></div>
            </div> --}}
            {{-- @if(!env('GOOGLE_MAPS_API_KEY'))
                <b>'GOOGLE_MAPS_API_KEY' is not set in the .env</b>
            @endif --}}
            
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
        <i class="fa fa-save"> </i>
    SALVAR</button>
    {{-- {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-primary btn-lg']) !!} --}}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
   <script src="/adminlte/js/mapInput.js"></script>
   <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>
   <script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
        function validarTelefoneOk (){
                                $('#fone').keyup(function(){
                        var value = $(this).val();
                        var letras,
                            numeros,
                            letrasMaiusculas,
                            especial;
                                    var t = document.getElementById('fone').value;
                        if(/[A-Z,a-z]/gm.test(value)){
                        letras = "entrou"
                        }else{
                            letras = "";
                        }
                      if ( letras == "entrou") {
                        document.getElementById('fone').value = "";
                      }else{
                        var telefone= document.getElementById('fone').value;
                        if(telefone.length==1){
                         document.getElementById('fone').value ='(' + telefone;
                          }
                        else if (telefone.length==3){
                         document.getElementById('fone').value = telefone +')';
                        }
                        else if (telefone.length==8){
                         document.getElementById('fone').value = telefone +'-';
                        }
                      }                  
                        });
                        $('#telefone2').keyup(function(){
                        var value = $(this).val();
                        var letras,
                            numeros,
                            letrasMaiusculas,
                            especial;
                                    var t = document.getElementById('telefone2').value;
                        if(/[A-Z,a-z]/gm.test(value)){
                        letras = "entrou"
                        }else{
                            letras = "";
                        }
                      if ( letras == "entrou") {
                        document.getElementById('telefone2').value = "";
                      }else{
                        var telefone= document.getElementById('telefone2').value;
                        if(telefone.length==1){
                         document.getElementById('telefone2').value ='(' + telefone;
                          }
                        else if (telefone.length==3){
                         document.getElementById('telefone2').value = telefone +')';
                        }
                        else if (telefone.length==8){
                         document.getElementById('telefone2').value = telefone +'-';
                        }
                      }                  
                        });
                        
                            }

                    //           function validarTelefoneOk (){
                    //             $('#telefone1').keyup(function(){
                    //     var value = $(this).val();
                    //     var letras,
                    //         numeros,
                    //         letrasMaiusculas,
                    //         especial;
                    //                 var t = document.getElementById('telefone1').value;
                    //     if(/[a-z]/gm.test(value)){
                    //     letras = "entrou"
                    //     }else{
                    //         letras = "";
                    //     }
                    //   if ( letras == "entrou") {
                    //     document.getElementById('telefone1').value = "";
                    //   }else{
                    //     var telefone= document.getElementById('telefone1').value;
                    //     if(telefone.length==1){
                    //      document.getElementById('telefone1').value ='(' + telefone;
                    //       }
                    //     else if (telefone.length==3){
                    //      document.getElementById('telefone1').value = telefone +')';
                    //     }
                    //     else if (telefone.length==8){
                    //      document.getElementById('telefone1').value = telefone +'-';
                    //     }
                    //   }                  
                    //     });
                    //     $('#telefone2').keyup(function(){
                    //     var value = $(this).val();
                    //     var letras,
                    //         numeros,
                    //         letrasMaiusculas,
                    //         especial;
                    //                 var t = document.getElementById('telefone2').value;
                    //     if(/[a-z]/gm.test(value)){
                    //     letras = "entrou"
                    //     }else{
                    //         letras = "";
                    //     }
                    //   if ( letras == "entrou") {
                    //     document.getElementById('telefone2').value = "";
                    //   }else{
                    //     var telefone= document.getElementById('telefone2').value;
                    //     if(telefone.length==1){
                    //      document.getElementById('telefone2').value ='(' + telefone;
                    //       }
                    //     else if (telefone.length==3){
                    //      document.getElementById('telefone2').value = telefone +')';
                    //     }
                    //     else if (telefone.length==8){
                    //      document.getElementById('telefone2').value = telefone +'-';
                    //     }
                    //   }                  
                    //     });
                    //         }
                             function validarCelularOk (){
                                $('#celular').keyup(function(){
                        var value = $(this).val();
                        var letras,
                            numeros,
                            letrasMaiusculas,
                            especial;
                                    var t = document.getElementById('celular').value;
                        if(/[A-Z,a-z]/gm.test(value)){
                        letras = "entrou"
                        }else{
                            letras = "";
                        }
                      if ( letras == "entrou") {
                        document.getElementById('celular').value = "";
                      }else{
                        var telefone= document.getElementById('celular').value;
                        if(telefone.length==1){
                         document.getElementById('celular').value ='(' + telefone;
                          }
                        else if (telefone.length==3){
                         document.getElementById('celular').value = telefone +')';
                        }
                        else if (telefone.length==9){
                         document.getElementById('celular').value = telefone +'-';
                        }
                      }                  
                        });
                        $('#celular2').keyup(function(){
                        var value = $(this).val();
                        var letras,
                            numeros,
                            letrasMaiusculas,
                            especial;
                                    var t = document.getElementById('celular2').value;
                        if(/[A-Z,a-z]/gm.test(value)){
                        letras = "entrou"
                        }else{
                            letras = "";
                        }
                      if ( letras == "entrou") {
                        document.getElementById('celular2').value = "";
                      }else{
                        var telefone= document.getElementById('celular2').value;
                        if(telefone.length==1){
                         document.getElementById('celular2').value ='(' + telefone;
                          }
                        else if (telefone.length==3){
                         document.getElementById('celular2').value = telefone +')';
                        }
                        else if (telefone.length==9){
                         document.getElementById('celular2').value = telefone +'-';
                        }
                      }                  
                        });
                        
                            }
                            function mascaraCep(){
                          
                        var cep = document.getElementById('cep').value;
                         if(cep.length==5){
                          document.getElementById('cep').value = cep +'-';
                     }
                     $('#cep').keyup(function(){
                        var value = $(this).val();
                        var letras;
                        if(/[A-Z,a-z]/gm.test(value)){
                        letras = "entrou"
                        }else{
                            letras = "";
                      
                        }
                        
                      if ( letras == "entrou") {
                        document.getElementById('cep').value = "";
                      }else{
                      
                       
                      }                  
                        });
                     }
                     function numeros(){
                          $('#numero').keyup(function(){
                          var value = $(this).val();
                          var letras;
                          if(/[A-Z,a-z]/gm.test(value)){
                          letras = "entrou"
                          }else{
                              letras = "";
                          }
                        if ( letras == "entrou") {
                          document.getElementById('numero').value = "";
                        }else{
                        
                         
                        }                  
                          });
                       }
                       function verificauf(){
                          $('#txtUf').keyup(function(){
                          var value = $(this).val();
                          var letras;
                          if(/[0-9]/gm.test(value)){
                          letras = "entrou"
                          }else{
                              letras = "";
                          }
                        if ( letras == "entrou") {
                          document.getElementById('txtUf').value = "";
                        }else{
                        
                         
                        }                  
                          });
                       }
                     function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('endereco').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('cidade').value=("");
            document.getElementById('uf').value=("");
            // document.getElementById('ibge').value=("");
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('endereco').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('uf').value=(conteudo.uf);
            // document.getElementById('ibge').value=(conteudo.ibge);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }
                     function pesquisacep(valor) {

//Nova variável "cep" somente com dígitos.
var cep = valor.replace(/\D/g, '');

//Verifica se campo cep possui valor informado.
if (cep != "") {

    //Expressão regular para validar o CEP.
    var validacep = /^[0-9]{8}$/;

    //Valida o formato do CEP.
    if(validacep.test(cep)) {

        //Preenche os campos com "..." enquanto consulta webservice.
        document.getElementById('endereco').value="...";
        document.getElementById('bairro').value="...";
        document.getElementById('cidade').value="...";
        document.getElementById('uf').value="...";
        // document.getElementById('ibge').value="...";

        //Cria um elemento javascript.
        var script = document.createElement('script');

        //Sincroniza com o callback.
        script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

        //Insere script no documento e carrega o conteúdo.
        document.body.appendChild(script);

    } //end if.
    else {
        //cep é inválido.
        limpa_formulário_cep();
        alert("Formato de CEP inválido.");
    }
} //end if.
else {
    //cep sem valor, limpa formulário.
    limpa_formulário_cep();
}};
function moeda(z){

v = z.value;

v=v.replace(/\D/g,"") //permite digitar apenas números

v=v.replace(/[0-9]{12}/,"inválido") //limita pra máximo 999.999.999,99

v=v.replace(/(\d{1})(\d{8})$/,"$1.$2") //coloca ponto antes dos últimos 8 digitos

v=v.replace(/(\d{1})(\d{1,2})$/,"$1.$2") //coloca ponto antes dos últimos 5 digitos

//v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2") //coloca virgula antes dos últimos 2 digitos

z.value = v;

}

    </script>
            
@stop