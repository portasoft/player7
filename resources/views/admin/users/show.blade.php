@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.users.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">VIZUALIZAR - USUÁRIO</h4> 
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.users.fields.name')</th>
                            <td field-key='name'>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.email')</th>
                            <td field-key='email'>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.role')</th>
                            <td field-key='role'>{{ $user->role->title or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.nomecompleto')</th>
                            <td field-key='nomecompleto'>{{ $user->nomecompleto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.datanascimento')</th>
                            <td field-key='datanascimento'>{{ $user->datanascimento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.cep')</th>
                            <td field-key='cep'>{{ $user->cep }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.endereco')</th>
                            <td field-key='endereco'>{{ $user->endereco }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.numero')</th>
                            <td field-key='numero'>{{ $user->numero }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.bairro')</th>
                            <td field-key='bairro'>{{ $user->bairro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.cidade')</th>
                            <td field-key='cidade'>{{ $user->cidade }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.uf')</th>
                            <td field-key='uf'>{{ $user->uf }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.complemento')</th>
                            <td field-key='complemento'>{{ $user->complemento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.fone')</th>
                            <td field-key='fone'>{{ $user->fone }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.celular')</th>
                            <td field-key='celular'>{{ $user->celular }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.cpf')</th>
                            <td field-key='cpf'>{{ $user->cpf }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.rg')</th>
                            <td field-key='rg'>{{ $user->rg }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.datacadastro')</th>
                            <td field-key='datacadastro'>{{ $user->datacadastro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.comissao')</th>
                            <td field-key='comissao'>{{ $user->comissao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.salario')</th>
                            <td field-key='salario'>{{ $user->salario }}</td>
                        </tr>
                        {{-- <tr>
                            <th>@lang('quickadmin.users.fields.empresa')</th>
                            <td field-key='empresa'>{{ $user->empresa }}</td>
                        </tr> --}}
                        <tr>
                            <th>@lang('quickadmin.users.fields.situacao')</th>
                            <td field-key='situacao'>{{ $user->situacao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.obs')</th>
                            <td field-key='obs'>{{ $user->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.vendedor')</th>
                            <td field-key='vendedor'>{{ $user->vendedor }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.foto')</th>
                            <td field-key='foto'>@if($user->foto)<a href="{{ asset(env('UPLOAD_PATH').'/' . $user->foto) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $user->foto) }}"/></a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.users.fields.localizacao')</th>
                            <td field-key='localizacao'>{{ $user->localizacao_address }}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    
<li role="presentation" class="active"><a href="#user_actions" aria-controls="user_actions" role="tab" data-toggle="tab">Ações do usuário</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    
<div role="tabpanel" class="tab-pane active" id="user_actions">
<table class="table table-bordered table-striped {{ count($user_actions) > 0 ? 'datatable' : '' }}">
    <thead>
        <tr>
            <th>@lang('quickadmin.user-actions.created_at')</th>
                        <th>@lang('quickadmin.user-actions.fields.user')</th>
                        <th>@lang('quickadmin.user-actions.fields.action')</th>
                        <th>@lang('quickadmin.user-actions.fields.action-model')</th>
                        <th>@lang('quickadmin.user-actions.fields.action-id')</th>
                        
        </tr>
    </thead>

    <tbody>
        @if (count($user_actions) > 0)
            @foreach ($user_actions as $user_action)
                <tr data-entry-id="{{ $user_action->id }}">
                    <td>{{ $user_action->created_at or '' }}</td>
                                <td field-key='user'>{{ $user_action->user->name or '' }}</td>
                                <td field-key='action'>{{ $user_action->action }}</td>
                                <td field-key='action_model'>{{ $user_action->action_model }}</td>
                                <td field-key='action_id'>{{ $user_action->action_id }}</td>
                                
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7">@lang('quickadmin.qa_no_entries_in_table')</td>
            </tr>
        @endif
    </tbody>
</table>
</div>
</div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.users.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
