@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.orcamento.title')</h3>
    @can('orcamento_create')
    <p>
        <a href="{{ route('admin.orcamentos.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('orcamento_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.orcamentos.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.orcamentos.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('orcamento_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('orcamento_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.orcamento.fields.cliente')</th>
                        <th>@lang('quickadmin.orcamento.fields.produto')</th>
                        <th>@lang('quickadmin.orcamento.fields.qtde')</th>
                        <th>@lang('quickadmin.orcamento.fields.validade')</th>
                        <th>@lang('quickadmin.orcamento.fields.data')</th>
                        <th>@lang('quickadmin.orcamento.fields.valortotal')</th>
                        <th>@lang('quickadmin.orcamento.fields.valorunitario')</th>
                        <th>@lang('quickadmin.orcamento.fields.obs')</th>
                        <th>@lang('quickadmin.orcamento.fields.finalizado')</th>
                        <th>@lang('quickadmin.orcamento.fields.anexo')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('orcamento_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.orcamentos.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.orcamentos.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('orcamento_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'cliente', name: 'cliente'},
                {data: 'produto', name: 'produto'},
                {data: 'qtde', name: 'qtde'},
                {data: 'validade', name: 'validade'},
                {data: 'data', name: 'data'},
                {data: 'valortotal', name: 'valortotal'},
                {data: 'valorunitario', name: 'valorunitario'},
                {data: 'obs', name: 'obs'},
                {data: 'finalizado', name: 'finalizado'},
                {data: 'anexo', name: 'anexo'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection