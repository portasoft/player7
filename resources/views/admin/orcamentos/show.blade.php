@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.orcamento.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.cliente')</th>
                            <td field-key='cliente'>{{ $orcamento->cliente }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.produto')</th>
                            <td field-key='produto'>{{ $orcamento->produto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.qtde')</th>
                            <td field-key='qtde'>{{ $orcamento->qtde }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.validade')</th>
                            <td field-key='validade'>{{ $orcamento->validade }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.data')</th>
                            <td field-key='data'>{{ $orcamento->data }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.valortotal')</th>
                            <td field-key='valortotal'>{{ $orcamento->valortotal }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.valorunitario')</th>
                            <td field-key='valorunitario'>{{ $orcamento->valorunitario }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.obs')</th>
                            <td field-key='obs'>{{ $orcamento->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.finalizado')</th>
                            <td field-key='finalizado'>{{ $orcamento->finalizado }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.orcamento.fields.anexo')</th>
                            <td field-key='anexo'>@if($orcamento->anexo)<a href="{{ asset(env('UPLOAD_PATH').'/' . $orcamento->anexo) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.orcamentos.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
