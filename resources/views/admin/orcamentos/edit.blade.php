@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.orcamento.title')</h3>
    
    {!! Form::model($orcamento, ['method' => 'PUT', 'route' => ['admin.orcamentos.update', $orcamento->id], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('cliente', trans('quickadmin.orcamento.fields.cliente').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('cliente', old('cliente'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('cliente'))
                        <p class="help-block">
                            {{ $errors->first('cliente') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('produto', trans('quickadmin.orcamento.fields.produto').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('produto', old('produto'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('produto'))
                        <p class="help-block">
                            {{ $errors->first('produto') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('qtde', trans('quickadmin.orcamento.fields.qtde').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('qtde', old('qtde'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('qtde'))
                        <p class="help-block">
                            {{ $errors->first('qtde') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('validade', trans('quickadmin.orcamento.fields.validade').'', ['class' => 'control-label']) !!}
                    {!! Form::text('validade', old('validade'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('validade'))
                        <p class="help-block">
                            {{ $errors->first('validade') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('data', trans('quickadmin.orcamento.fields.data').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('data', old('data'), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('data'))
                        <p class="help-block">
                            {{ $errors->first('data') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valortotal', trans('quickadmin.orcamento.fields.valortotal').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('valortotal', old('valortotal'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valortotal'))
                        <p class="help-block">
                            {{ $errors->first('valortotal') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valorunitario', trans('quickadmin.orcamento.fields.valorunitario').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('valorunitario', old('valorunitario'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorunitario'))
                        <p class="help-block">
                            {{ $errors->first('valorunitario') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('obs', trans('quickadmin.orcamento.fields.obs').'', ['class' => 'control-label']) !!}
                    {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('obs'))
                        <p class="help-block">
                            {{ $errors->first('obs') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('finalizado', trans('quickadmin.orcamento.fields.finalizado').'', ['class' => 'control-label']) !!}
                    {!! Form::text('finalizado', old('finalizado'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('finalizado'))
                        <p class="help-block">
                            {{ $errors->first('finalizado') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('anexo', trans('quickadmin.orcamento.fields.anexo').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('anexo', old('anexo')) !!}
                    @if ($orcamento->anexo)
                        <a href="{{ asset(env('UPLOAD_PATH').'/' . $orcamento->anexo) }}" target="_blank">Download file</a>
                    @endif
                    {!! Form::file('anexo', ['class' => 'form-control']) !!}
                    {!! Form::hidden('anexo_max_size', 2) !!}
                    <p class="help-block"></p>
                    @if($errors->has('anexo'))
                        <p class="help-block">
                            {{ $errors->first('anexo') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop