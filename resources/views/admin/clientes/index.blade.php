@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.cliente.title')</h3> --}}
    @can('cliente_create')
    <p>
        <a href="{{ route('admin.clientes.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan

    @can('cliente_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.clientes.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.clientes.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-default">
        <div class="panel-heading">
                <h4>RELATÓRIO - CLIENTES</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('cliente_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('cliente_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.cliente.fields.nome')</th>
                        <th>@lang('quickadmin.cliente.fields.cpf')</th>
                        <th>@lang('quickadmin.cliente.fields.rg')</th>
                        <th>@lang('quickadmin.cliente.fields.celular')</th>
                        <th>@lang('quickadmin.cliente.fields.telefone')</th>
                        <th>@lang('quickadmin.cliente.fields.email')</th>
                        <th>@lang('quickadmin.cliente.fields.cep')</th>
                        <th>@lang('quickadmin.cliente.fields.endereco')</th>
                        <th>@lang('quickadmin.cliente.fields.numero')</th>
                        <th>@lang('quickadmin.cliente.fields.bairro')</th>
                        <th>@lang('quickadmin.cliente.fields.cidade')</th>
                        <th>@lang('quickadmin.cliente.fields.uf')</th>
                        <th>@lang('quickadmin.cliente.fields.complemento')</th>
                        {{-- <th>@lang('quickadmin.cliente.fields.estadocivil')</th> --}}
                        <th>@lang('quickadmin.cliente.fields.sexo')</th>
                        <th>@lang('quickadmin.cliente.fields.datacadastro')</th>
                        <th>@lang('quickadmin.cliente.fields.datanascimento')</th>
                        <th>@lang('quickadmin.cliente.fields.renda')</th>
                        <th>@lang('quickadmin.cliente.fields.limite')</th>
                        <th>@lang('quickadmin.cliente.fields.cartao')</th>
                        <th>@lang('quickadmin.cliente.fields.pontos')</th>
                        <th>@lang('quickadmin.cliente.fields.debito')</th>
                        <th>@lang('quickadmin.cliente.fields.obs')</th>
                        {{-- <th>@lang('quickadmin.cliente.fields.tipopessoa')</th> --}}
                        <th>@lang('quickadmin.cliente.fields.naturalidade')</th>
                        <th>@lang('quickadmin.cliente.fields.credito')</th>
                        <th>@lang('quickadmin.cliente.fields.localizacao')</th>
                        <th>@lang('quickadmin.cliente.fields.foto')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        @can('cliente_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.clientes.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.clientes.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('cliente_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'nome', name: 'nome'},
                {data: 'cpf', name: 'cpf'},
                {data: 'rg', name: 'rg'},
                {data: 'celular', name: 'celular'},
                {data: 'telefone', name: 'telefone'},
                {data: 'email', name: 'email'},
                {data: 'cep', name: 'cep'},
                {data: 'endereco', name: 'endereco'},
                {data: 'numero', name: 'numero'},
                {data: 'bairro', name: 'bairro'},
                {data: 'cidade', name: 'cidade'},
                {data: 'uf', name: 'uf'},
                {data: 'complemento', name: 'complemento'},
                // {data: 'estadocivil', name: 'estadocivil'},
                {data: 'sexo', name: 'sexo'},
                {data: 'datacadastro', name: 'datacadastro'},
                {data: 'datanascimento', name: 'datanascimento'},
                {data: 'renda', name: 'renda'},
                {data: 'limite', name: 'limite'},
                {data: 'cartao', name: 'cartao'},
                {data: 'pontos', name: 'pontos'},
                {data: 'debito', name: 'debito'},
                {data: 'obs', name: 'obs'},
                // {data: 'tipopessoa', name: 'tipopessoa'},
                {data: 'naturalidade', name: 'naturalidade'},
                {data: 'credito', name: 'credito'},
                {data: 'localizacao_address', name: 'localizacao_address'},
                {data: 'foto', name: 'foto'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection