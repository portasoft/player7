@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.cliente.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>VIZUALIZAÇÃO - CLIENTES</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.nome')</th>
                            <td field-key='nome'>{{ $cliente->nome }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.cpf')</th>
                            <td field-key='cpf'>{{ $cliente->cpf }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.rg')</th>
                            <td field-key='rg'>{{ $cliente->rg }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.celular')</th>
                            <td field-key='celular'>{{ $cliente->celular }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.telefone')</th>
                            <td field-key='telefone'>{{ $cliente->telefone }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.email')</th>
                            <td field-key='email'>{{ $cliente->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.cep')</th>
                            <td field-key='cep'>{{ $cliente->cep }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.endereco')</th>
                            <td field-key='endereco'>{{ $cliente->endereco }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.numero')</th>
                            <td field-key='numero'>{{ $cliente->numero }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.bairro')</th>
                            <td field-key='bairro'>{{ $cliente->bairro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.cidade')</th>
                            <td field-key='cidade'>{{ $cliente->cidade }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.uf')</th>
                            <td field-key='uf'>{{ $cliente->uf }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.complemento')</th>
                            <td field-key='complemento'>{{ $cliente->complemento }}</td>
                        </tr>
                        {{-- <tr>
                            <th>@lang('quickadmin.cliente.fields.estadocivil')</th>
                            <td field-key='estadocivil'>{{ $cliente->estadocivil }}</td>
                        </tr> --}}
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.sexo')</th>
                            <td field-key='sexo'>{{ $cliente->sexo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.datacadastro')</th>
                            <td field-key='datacadastro'>{{ $cliente->datacadastro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.datanascimento')</th>
                            <td field-key='datanascimento'>{{ $cliente->datanascimento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.renda')</th>
                            <td field-key='renda'>{{ $cliente->renda }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.limite')</th>
                            <td field-key='limite'>{{ $cliente->limite }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.cartao')</th>
                            <td field-key='cartao'>{{ $cliente->cartao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.pontos')</th>
                            <td field-key='pontos'>{{ $cliente->pontos }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.debito')</th>
                            <td field-key='debito'>{{ $cliente->debito }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.obs')</th>
                            <td field-key='obs'>{{ $cliente->obs }}</td>
                        </tr>
                        {{-- <tr>
                            <th>@lang('quickadmin.cliente.fields.tipopessoa')</th>
                            <td field-key='tipopessoa'>{{ $cliente->tipopessoa }}</td>
                        </tr> --}}
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.naturalidade')</th>
                            <td field-key='naturalidade'>{{ $cliente->naturalidade }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.credito')</th>
                            <td field-key='credito'>{{ $cliente->credito }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.localizacao')</th>
                            <td field-key='localizacao'>{{ $cliente->localizacao_address }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.cliente.fields.foto')</th>
                            <td field-key='foto'>@if($cliente->foto)<a href="{{ asset(env('UPLOAD_PATH').'/' . $cliente->foto) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $cliente->foto) }}"/></a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.clientes.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
