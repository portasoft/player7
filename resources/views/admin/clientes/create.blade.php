@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.cliente.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.clientes.store'], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
          <h4>CADASTRO - CLIENTE</h4>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4 form-group">
                    {!! Form::label('nome', trans('quickadmin.cliente.fields.nome').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('nome', old('nome'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('nome'))
                        <p class="help-block">
                            {{ $errors->first('nome') }}
                        </p>
                    @endif
                </div>
                {{-- <div class="col-xs-2 form-group">
                        {!! Form::label('tipopessoa', trans('quickadmin.cliente.fields.tipopessoa').'', ['class' => 'control-label']) !!}
                        {!! Form::text('tipopessoa', array('Fisica','J'),'S', ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('tipopessoa'))
                            <p class="help-block">
                                {{ $errors->first('tipopessoa') }}
                            </p>
                        @endif
                    </div> --}}
                    <div class="col-xs-2 form-group">
                            {!! Form::label('estadocivil', trans('quickadmin.cliente.fields.estadocivil').'', ['class' => 'control-label']) !!}
                            {!! Form::select('estadocivil',['Casado'=>'Casado','Solteiro'=>'Solteiro'],null, ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('estadocivil'))
                                <p class="help-block">
                                    {{ $errors->first('estadocivil') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-2 form-group">
                                {!! Form::label('sexo', trans('quickadmin.cliente.fields.sexo').'', ['class' => 'control-label']) !!}
                                {!! Form::select('sexo',['Feminino'=>'Feminino','Masculino'=> 'Masculuno','NDF'=>'NDF'],null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('sexo'))
                                    <p class="help-block">
                                      {{ $errors->first('sexo') }}
                                    </p>
                                @endif
                            </div>
                            <div class="col-xs-2 form-group">
                                    {!! Form::label('datacadastro', trans('quickadmin.cliente.fields.datacadastro').'', ['class' => 'control-label']) !!}
                                    {!! Form::date('datacadastro', old('datacadastro'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('datacadastro'))
                                       <p class="help-block">
                                           {{ $errors->first('datacadastro') }}
                                   </p>
                                    @endif
                                </div>
                <div class="col-xs-2 form-group">
                        {!! Form::label('cpf', trans('quickadmin.cliente.fields.cpf').'', ['class' => 'control-label']) !!}
                        {!! Form::text('cpf', old('cpf'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('cpf'))
                            <p class="help-block">
                                {{ $errors->first('cpf') }}
                            </p>
                        @endif
                    </div>
                    <div class="col-xs-2 form-group">
                            {!! Form::label('rg', trans('quickadmin.cliente.fields.rg').'', ['class' => 'control-label']) !!}
                            {!! Form::text('rg', old('rg'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('rg'))
                                <p class="help-block">
                                    {{ $errors->first('rg') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-2 form-group">
                                {!! Form::label('celular', trans('quickadmin.cliente.fields.celular').'', ['class' => 'control-label']) !!}
                                {!! Form::text('celular', old('celular'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('celular'))
                                    <p class="help-block">
                                        {{ $errors->first('celular') }}
                                    </p>
                                @endif
                            </div>
                            <div class="col-xs-2 form-group">
                                    {!! Form::label('telefone', trans('quickadmin.cliente.fields.telefone').'', ['class' => 'control-label']) !!}
                                    {!! Form::text('telefone', old('telefone'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('telefone'))
                                        <p class="help-block">
                                            {{ $errors->first('telefone') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="col-xs-2 form-group">
                                        {!! Form::label('datanascimento', trans('quickadmin.cliente.fields.datanascimento').'', ['class' => 'control-label']) !!}
                                        {!! Form::date('datanascimento', old('datanascimento'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('datanascimento'))
                                            <p class="help-block">
                                                {{ $errors->first('datanascimento') }}
                                            </p>
                                        @endif
                                                                   </div>
                                <div class="col-xs-4 form-group">
                                        {!! Form::label('email', trans('quickadmin.cliente.fields.email').'', ['class' => 'control-label']) !!}
                                        {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('email'))
                                            <p class="help-block">
                                                {{ $errors->first('email') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="col-xs-2 form-group">
                                            {!! Form::label('cep', trans('quickadmin.cliente.fields.cep').'', ['class' => 'control-label']) !!}
                                            {!! Form::text('cep', old('cep'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                            <p class="help-block"></p>
                                            @if($errors->has('cep'))
                                                <p class="help-block">
                                                    {{ $errors->first('cep') }}
                                                </p>
                                            @endif
                                        </div>
                                        <div class="col-xs-3 form-group">
                                                {!! Form::label('endereco', trans('quickadmin.cliente.fields.endereco').'', ['class' => 'control-label']) !!}
                                                {!! Form::text('endereco', old('endereco'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                                <p class="help-block"></p>
                                                @if($errors->has('endereco'))
                                                    <p class="help-block">
                                                        {{ $errors->first('endereco') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="col-xs-1 form-group">
                                                    {!! Form::label('numero', trans('quickadmin.cliente.fields.numero').'', ['class' => 'control-label']) !!}
                                                    {!! Form::text('numero', old('numero'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                                                                <p class="help-block"></p>
                                                @if($errors->has('numero'))
                                                    <p class="help-block">
                                                        {{ $errors->first('numero') }}
                                                    </p>
                                                @endif
                                            </div>
                 <div class="col-xs-3 form-group">
                         {!! Form::label('bairro', trans('quickadmin.cliente.fields.bairro').'', ['class' => 'control-label']) !!}
                         {!! Form::text('bairro', old('bairro'), ['class' => 'form-control', 'placeholder' => '']) !!}
                         <p class="help-block"></p>
                         @if($errors->has('bairro'))
                             <p class="help-block">
                                 {{ $errors->first('bairro') }}
                             </p>
                         @endif
                     </div>
                     <div class="col-xs-3 form-group">
                             {!! Form::label('cidade', trans('quickadmin.cliente.fields.cidade').'', ['class' => 'control-label']) !!}
                             {!! Form::text('cidade', old('cidade'), ['class' => 'form-control', 'placeholder' => '']) !!}
                             <p class="help-block"></p>
                             @if($errors->has('cidade'))
                                 <p class="help-block">
                                     {{ $errors->first('cidade') }}
                                 </p>
                             @endif
                         </div>
                         <div class="col-xs-1 form-group">
                                 {!! Form::label('uf', trans('quickadmin.cliente.fields.uf').'', ['class' => 'control-label']) !!}
                                 {!! Form::text('uf', old('uf'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                 <p class="help-block"></p>
                                 @if($errors->has('uf'))
                                     <p class="help-block">
                                         {{ $errors->first('uf') }}
                                     </p>
                                 @endif
                             </div>
                             <div class="col-xs-5 form-group">
                                     {!! Form::label('complemento', trans('quickadmin.cliente.fields.complemento').'', ['class' => 'control-label']) !!}
                                     {!! Form::text('complemento', old('complemento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                     <p class="help-block"></p>
                                     @if($errors->has('complemento'))
                                         <p class="help-block">
                                             {{ $errors->first('complemento') }}
                                         </p>
                                     @endif
                                 </div>
                          
                                 <div class="col-xs-2 form-group">
                                        {!! Form::label('naturalidade', trans('quickadmin.cliente.fields.naturalidade').'', ['class' => 'control-label']) !!}
                                        {!! Form::text('naturalidade', old('naturalidade'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('naturalidade'))
                                            <p class="help-block">
                                                {{ $errors->first('naturalidade') }}
                                            </p>
                                        @endif
                                    </div>
                                 
                                           
                                                 <div class="col-xs-2 form-group">
                          {!! Form::label('renda', trans('quickadmin.cliente.fields.renda').'', ['class' => 'control-label']) !!}
                          {!! Form::text('renda', old('renda'), ['class' => 'form-control', 'placeholder' => '']) !!}
                          <p class="help-block"></p>
                          @if($errors->has('renda'))
                              <p class="help-block">
                                  {{ $errors->first('renda') }}
                              </p>
                          @endif
                      </div>
                      <div class="col-xs-2 form-group">
                              {!! Form::label('limite', trans('quickadmin.cliente.fields.limite').'', ['class' => 'control-label']) !!}
                              {!! Form::text('limite', old('limite'), ['class' => 'form-control', 'placeholder' => '']) !!}
                              <p class="help-block"></p>
                              @if($errors->has('limite'))
                                  <p class="help-block">
                                      {{ $errors->first('limite') }}
                                  </p>
                              @endif
                     </div>
                     <div class="col-xs-2 form-group">
                            {!! Form::label('credito', trans('quickadmin.cliente.fields.credito').'', ['class' => 'control-label']) !!}
                            {!! Form::text('credito', old('credito'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('credito'))
                                <p class="help-block">
                                    {{ $errors->first('credito') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-2 form-group">
                                {!! Form::label('debito', trans('quickadmin.cliente.fields.debito').'', ['class' => 'control-label']) !!}
                                {!! Form::text('debito', old('debito'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('debito'))
                                    <p class="help-block">
                                        {{ $errors->first('debito') }}
                                    </p>
                                @endif
                            </div>
                     <div class="col-xs-2 form-group">
                            {!! Form::label('cartao', trans('quickadmin.cliente.fields.cartao').'', ['class' => 'control-label']) !!}
                            {!! Form::text('cartao', old('cartao'), ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('cartao'))
                                <p class="help-block">
                                    {{ $errors->first('cartao') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-2 form-group">
                                {!! Form::label('pontos', trans('quickadmin.cliente.fields.pontos').'', ['class' => 'control-label']) !!}
                                {!! Form::text('pontos', old('pontos'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('pontos'))
                                    <p class="help-block">
                                        {{ $errors->first('pontos') }}
                                    </p>
                                @endif
                            </div>
                         
                            <div class="col-xs-4 form-group">
                                    {!! Form::label('foto', trans('quickadmin.cliente.fields.foto').'', ['class' => 'control-label']) !!}
                                    {!! Form::file('foto', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                                    {!! Form::hidden('foto_max_size', 2) !!}
                                    {!! Form::hidden('foto_max_width', 4096) !!}
                                    {!! Form::hidden('foto_max_height', 4096) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('foto'))
                                        <p class="help-block">
                                            {{ $errors->first('foto') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="col-xs-12 form-group">
                                        {!! Form::label('obs', trans('quickadmin.cliente.fields.obs').'', ['class' => 'control-label']) !!}
                                        {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('obs'))
                                            <p class="help-block">
                                                {{ $errors->first('obs') }}
                                            </p>
                                        @endif
                                    </div>
                                
                                        
            </div>
         
          
            {{-- <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('localizacao_address', trans('quickadmin.cliente.fields.localizacao').'', ['class' => 'control-label']) !!}
                    {!! Form::text('localizacao_address', old('localizacao_address'), ['class' => 'form-control map-input', 'id' => 'localizacao-input']) !!}
                    {!! Form::hidden('localizacao_latitude', 0 , ['id' => 'localizacao-latitude']) !!}
                    {!! Form::hidden('localizacao_longitude', 0 , ['id' => 'localizacao-longitude']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('localizacao'))
                        <p class="help-block">
                            {{ $errors->first('localizacao') }}
                        </p>
                    @endif
                </div>
            </div> --}}
{{--             
            <div id="localizacao-map-container" style="width:100%;height:200px; ">
                <div style="width: 100%; height: 100%" id="localizacao-map"></div>
            </div>
            @if(!env('GOOGLE_MAPS_API_KEY'))
                <b>'GOOGLE_MAPS_API_KEY' is not set in the .env</b>
            @endif --}}
            
           
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
   <script src="/adminlte/js/mapInput.js"></script>
   <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop