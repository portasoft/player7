@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contacontabil.title')</h3> --}}

    <div class="panel panel-default">
        <div class="panel-heading">
         <h4>VIZUALIZAÇÃO - CONTA CONTÁBIL</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.contacontabil.fields.origem')</th>
                            <td field-key='origem'>{{ $contacontabil->origem }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.contacontabil.fields.descricao')</th>
                            <td field-key='descricao'>{{ $contacontabil->descricao }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.contacontabils.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
