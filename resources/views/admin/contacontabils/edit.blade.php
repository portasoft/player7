@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.contacontabil.title')</h3> --}}
    
    {!! Form::model($contacontabil, ['method' => 'PUT', 'route' => ['admin.contacontabils.update', $contacontabil->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
           <h4>EDITAR - CONTA CONTÁBIL</h4>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 form-group">
                    {!! Form::label('origem', trans('quickadmin.contacontabil.fields.origem').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('origem',['Receita','Despesa'],null, ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('origem'))
                        <p class="help-block">
                            {{ $errors->first('origem') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-4 form-group">
                    {!! Form::label('descricao', trans('quickadmin.contacontabil.fields.descricao').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('descricao', old('descricao'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('descricao'))
                        <p class="help-block">
                            {{ $errors->first('descricao') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

