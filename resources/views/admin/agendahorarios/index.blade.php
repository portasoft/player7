@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.agendahorario.title')</h3> --}}
    @can('agendahorario_create')
  
    <p>
        <a href="{{ route('admin.agendahorarios.create') }}"  type="submit" class="btn btn-success btn-lg">
            <i class="fa fa-save"> </i>
        NOVO</a>
        
    </p>
    @endcan

    @can('agendahorario_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.agendahorarios.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.agendahorarios.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
            <h4 id="TNome">RELATÓRIO - AGENDA DE HÓRARIO</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>ALTERADO COM SUCESSO!</h4>
         </div> 
        @endif
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('agendahorario_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('agendahorario_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.agendahorario.fields.data')</th>
                        <th>@lang('quickadmin.agendahorario.fields.hora')</th>
                        <th>@lang('quickadmin.agendahorario.fields.descricao')</th>
                        <th>@lang('quickadmin.agendahorario.fields.motivo')</th>
                        <th>@lang('quickadmin.agendahorario.fields.obs')</th>
                        <th>@lang('quickadmin.agendahorario.fields.retorno')</th>
                        <th>@lang('quickadmin.agendahorario.fields.situacao')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
    <script>
        @can('agendahorario_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.agendahorarios.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.agendahorarios.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('agendahorario_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'data', name: 'data'},
                {data: 'hora', name: 'hora'},
                {data: 'descricao', name: 'descricao'},
                {data: 'motivo', name: 'motivo'},
                {data: 'obs', name: 'obs'},
                {data: 'retorno', name: 'retorno'},
                {data: 'situacao', name: 'situacao'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection