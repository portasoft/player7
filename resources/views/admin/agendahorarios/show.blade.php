@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.agendahorario.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
            <h4 id="TNome">VIZUALIZAR - AGENDA HORÁRIO</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.agendahorario.fields.data')</th>
                            <td field-key='data'>{{ $agendahorario->data }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendahorario.fields.hora')</th>
                            <td field-key='hora'>{{ $agendahorario->hora }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendahorario.fields.descricao')</th>
                            <td field-key='descricao'>{{ $agendahorario->descricao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendahorario.fields.motivo')</th>
                            <td field-key='motivo'>{{ $agendahorario->motivo }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendahorario.fields.obs')</th>
                            <td field-key='obs'>{{ $agendahorario->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendahorario.fields.retorno')</th>
                            <td field-key='retorno'>{{ $agendahorario->retorno }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.agendahorario.fields.situacao')</th>
                            <td field-key='situacao'>{{ $agendahorario->situacao }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.agendahorarios.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
