@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.agendahorario.title')</h3> --}}
    
    {!! Form::model($agendahorario, ['method' => 'PUT', 'route' => ['admin.agendahorarios.update', $agendahorario->id]]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">ALTERAR - AGENDA HORÁRIO</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>ALTERADO COM SUCESSO!</h4>
         </div> 
        @endif
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3 form-group">
                    {!! Form::label('data', trans('quickadmin.agendahorario.fields.data').'', ['class' => 'control-label']) !!}
                    {!! Form::date('data', old('data'), ['class' => 'form-control date', 'placeholder' => '','required']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('data'))
                        <p class="help-block">
                            {{ $errors->first('data') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-3 form-group">
                    {!! Form::label('hora', trans('quickadmin.agendahorario.fields.hora').'', ['class' => 'control-label']) !!}
                    {!! Form::text('hora', old('hora'), ['class' => 'form-control timepicker', 'placeholder' => '','required']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('hora'))
                        <p class="help-block">
                            {{ $errors->first('hora') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-6 form-group">
                    {!! Form::label('descricao', trans('quickadmin.agendahorario.fields.descricao').'', ['class' => 'control-label']) !!}
                    {!! Form::text('descricao', old('descricao'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('descricao'))
                        <p class="help-block">
                            {{ $errors->first('descricao') }}
                        </p>
                    @endif
                </div>
          
                {{-- <div class="col-xs-5 form-group">
                    {!! Form::label('motivo', trans('quickadmin.agendahorario.fields.motivo').'', ['class' => 'control-label']) !!}
                    {!! Form::text('motivo', old('motivo'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('motivo'))
                        <p class="help-block">
                            {{ $errors->first('motivo') }}
                        </p>
                    @endif
                </div> --}}
          
                <div class="col-xs-3 form-group">
                        {!! Form::label('situacao', trans('quickadmin.agendahorario.fields.situacao').'', ['class' => 'control-label']) !!}
                        {!! Form::select('situacao', ['Aguardando','Agendado','Reagendado','Concluido','Cancelado'],null, ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('situacao'))
                            <p class="help-block">
                                {{ $errors->first('situacao') }}
                            </p>
                        @endif
                    </div>
                <div class="col-xs-3 form-group">
                    {!! Form::label('retorno', trans('quickadmin.agendahorario.fields.retorno').'', ['class' => 'control-label']) !!}
                    {!! Form::text('retorno', old('retorno'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('retorno'))
                        <p class="help-block">
                            {{ $errors->first('retorno') }}
                        </p>
                    @endif
                </div>
          
           
                
                <div class="col-xs-6 form-group">
                        {!! Form::label('obs', trans('quickadmin.agendahorario.fields.obs').'', ['class' => 'control-label']) !!}
                        {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('obs'))
                            <p class="help-block">
                                {{ $errors->first('obs') }}
                            </p>
                        @endif
                    </div>
            </div>
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
        <i class="fa fa-refresh"> </i>
    ALTERAR</button>
        
    {{-- {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
    <script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
            $('.timepicker').datetimepicker({
                format: "{{ config('app.time_format_moment') }}",
            });
            
        });
    </script>
            
@stop