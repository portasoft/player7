@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
<div class="panel panel-default">
        <div class="panel-heading">
            {{-- @lang('quickadmin.qa_create') --}}
            <h4>ENTRADA DE NOTA</h4>
        </div>
       
             @if (isset($s))
             @if ($s==1)
             <div class="alert alert-danger alert-dismissible" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                    <h4><i class="icon fa fa-ban"></i>Nota já cadastrada!</h4>
                 </div> 
             @endif

             @if ($s==2)
             <div class="alert alert-success alert-dismissible" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                    <h4><i class="icon fa fa-check"></i>Nota cadastrada com sucesso.</h4>
                 </div> 
             @endif
             @if ($s==3)
             <div class="alert alert-warning alert-dismissible" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                    <h4><i class="icon fa fa-ban"></i>Informe o XML.</h4>
                 </div> 
             @endif         
             @endif
      

     


        <div class="panel-body">
            <div class="row">
                    {!! Form::open(['method' => 'POST', 'route' => ['admin.entradanotas.importartela']]) !!}
                    {!! Form::open(['method' => 'POST', 'route' => ['admin.entradanotas.store'], 'files' => true,]) !!}
              
                    <div class="col-xs-12 form-group">
                            <h4>NF-e</h4>
                        <input style="font-size:12px;"  id="importa" rd  name="importa" type="file" >
                        <input type="submit" class="btn btn-primary" id="importabanco" name="importabanco" disabled value="Importar XML">
                        <input type="button" class="btn btn-danger" id="limpar" value="Limpar">
                        {{-- {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!} --}}
                    </div>
                   <div class="col-xs-2">
                        
               <label for="">NF</label>
               @if (isset($notaFiscal))
               <input type="text" id="nf" readonly name="nf"  value="{{ $notaFiscal }}" class="form-control">
               @else
               <input type="text" id="nf" readonly name="nf"  value="" class="form-control">
               @endif
                  </div>
                  <div class="col-xs-2">
                    <label for="">Série</label>
                    @if (isset($serie))
                    <input type="text" id="serie" readonly name="serie"  value="{{ $serie }}" class="form-control">
                    @else
                    <input type="text" id="serie" readonly name="serie"  value="" class="form-control">
                    @endif
                       </div>
                       <div class="col-xs-2 form-group">
                        <label for="">Data Emissão</label>
                        @if (isset($serie))
                        <input type="text" id="dataemissao" readonly name="dataemissao"  value="{{ $emissao }}" class="form-control">
                        @else
                        <input type="text" id="dataemissao" readonly name="dataemissao"  value="" class="form-control">
                        @endif
                    </div>
                    <div class="col-xs-2 form-group">
                        <label for="">Data Entrada</label>
                        {!! Form::date('dataentrada', old('dataentrada'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    </div>
                    <div class="col-xs-4 form-group">
                        <label for="">Chave de Acesso</label>
                        @if (isset($chave))
                        <input type="text" id="chavenfe" readonly name="chavenfe"  value="{{ $chave }}" class="form-control">
                        @else
                        <input type="text" id="chavenfe" readonly name="chavenfe"  class="form-control">
                        @endif
                    </div>
                    <form action="importartela" method="POST">
                    <div class="col-xs-4 form-group">
                        {!! Form::label('fornecedor', trans('quickadmin.entradanota.fields.fornecedor').'', ['class' => 'control-label']) !!}
                @if (isset($emitente))

                <input type="text" id="fornecedor" readonly name="fornecedor"  value="{{ $emitente }}" class="form-control">
                @else
                <input type="text" id="fornecedor" readonly name="fornecedor"  value="" class="form-control">
                {{-- <input type="submit" class="btn btn-primary" value="Cadastrar fornecedor"> --}}
                @endif
                    </div>
                          <div class="col-xs-2 form-group">
                            <label for="">Valor total</label>
                            @if (isset($total))
                            <input type="text" id="valornota" readonly name="valornota"  value="{{ $total }}" class="form-control">
                            @else
                            <input type="text" id="valornota" readonly name="valornota"  value="" class="form-control">
                            @endif
                </div>
                <div class="col-xs-6 form-group">
                    <label for="">Depósito</label>
                    @if (isset($deposito))
                    {!! Form::select('deposito',$deposito,null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    @else
                    {!! Form::text('deposito',old('deposito'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    @endif
               
                </div>
                 </form>
          <br><br>      
                    {!! Form::close() !!}
          
                    {{-- {!! Form::close() !!} --}}
               
            </div>

        </div>
    </div>
    {{--  --}}

    {{--  --}}
    <div class="panel panel-default">
            <div class="panel-heading">
                {{-- @lang('quickadmin.qa_create') --}}
                <h4>PRODUTODAS DA NOTA</h4>
            </div>
            <div class="box-body table-responsive no-padding" >
                    <table class="table table-hover" id="tabelaproduto" >
                      <tbody><tr>
                            <th>Cod.Fornecedor</th>
                            <th>Nome</th>
                            {{-- <th>Ação</th> --}}
                            <th>Nome no fornecedor</th>
                            <th>Qtde</th>
                            <th>Unidade</th>
                      
                            <th>Custo</th>
                            <th>Total</th>
                            <th>NCM</th>
                            <th>CFOP</th>
                            <th>CST</th>
                            <th>%ICMS</th>
                            <th>Valor ICMS</th>
                            <th>CST</th>
                        
                      </tr>
                      <tr>
                            <?php
                                            
                            function format($number, $dec =2){
                           
                           return number_format ((float) $number ,$dec, ".","");
                       }
                       function formatq($number){
                           
                           return number_format ((float) $number);
                       }
                       $link = "/admin/produtos/create"; // Link goes here!
                       if (isset($xml)) {
                           foreach($xml->NFe as $key => $item) {	    	
                               if(isset($item->infNFe)) {	
                               //LENDO AS INFORMAÇÕES DE PRODUTOS NA NF-e (XML)
                                   $semResultado = 0;
                                   for ($i=0; $i <=1000 ; $i++) { 
                                       if(!empty($item->infNFe->det[$i]->prod->cProd)){
                                           $semResultado = 0;
                              
                                           echo "<tr>";
                                           echo "<td>".$item->infNFe->det[$i]->prod->cProd."</td>";      
                                           echo "<td></td>";
                                        //    echo "<td><span class='btn btn-default'><a  id='ver' href='$link'>Visualizar</a></span>
                                            
                                          //  </td>";
                                            // echo "<a href='$link'>Link</a>";

                                           echo "<td>".$item->infNFe->det[$i]->prod->xProd."</td>";
                                           echo "<td>".formatq($item->infNFe->det[$i]->prod->qCom)."</td>";
                                           echo "<td>".$item->infNFe->det[$i]->prod->uCom."</td>";
                                     
                                           echo "<td>".format($item->infNFe->det[$i]->prod->vUnTrib)."</td>";
                                           echo "<td>".format($item->infNFe->det[$i]->prod->vProd)."</td>";
                                           echo "<td>".$item->infNFe->det[$i]->prod->NCM."</td>";
                                           echo "<td>".$item->infNFe->det[$i]->prod->CFOP."</td>";
                                           echo "<td>".$item->infNFe->det[$i]->imposto->ICMS->ICMS90->CST."</td>";
                                           echo "<td>".$item->infNFe->det[$i]->imposto->ICMS->ICMS90->pICMS."</td>";
                                           echo "<td>".$item->infNFe->det[$i]->imposto->ICMS->ICMS90->vICMS."</td>";
                                           
                                           
                                           // <td><span class="label label-danger">Pendente</span></td>
                                                
                                       } else {
                                           //CONTANDO QUANDO NÃO HOUVER RESULTADOS
                                           $semResultado ++;
                                       }
                   
                                       //SE N TIVER RESULTADOS EM SEQUENCIA, PARAR O FOR
                                       if($semResultado >= 10){
                                           break;
                                       }
                                 
                                    
                                   } // FIM DO FOR
                              
                               }
                           }
                       }
                                             

                       ?>
                  
                      </tr>
                    
                    </tbody></table>
                  </div>
            </div>
    {{-- <h3 class="page-title">@lang('quickadmin.entradanota.title')</h3> --}}
 
    {{-- @can('entradanotum_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.entradanotas.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.entradanotas.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan --}}


    <div class="panel panel-default">
        <div class="panel-heading">
            {{-- @lang('quickadmin.qa_list') --}}
            <h5>Nota fiscal</h5>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('entradanotum_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('entradanotum_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.entradanota.fields.fornecedor')</th>
                        <th>@lang('quickadmin.entradanota.fields.dataemissao')</th>
                        <th>@lang('quickadmin.entradanota.fields.dataentrada')</th>
                        <th>@lang('quickadmin.entradanota.fields.valosprodutos')</th>
                        <th>@lang('quickadmin.entradanota.fields.baseicms')</th>
                        <th>@lang('quickadmin.entradanota.fields.valoricms')</th>
                        <th>@lang('quickadmin.entradanota.fields.valorsubst')</th>
                        <th>@lang('quickadmin.entradanota.fields.desconto')</th>
                        <th>@lang('quickadmin.entradanota.fields.despesas')</th>
                        <th>@lang('quickadmin.entradanota.fields.valornota')</th>
                        <th>@lang('quickadmin.entradanota.fields.deposito')</th>
                        <th>@lang('quickadmin.entradanota.fields.horaemissao')</th>
                        <th>@lang('quickadmin.entradanota.fields.valoripi')</th>
                        <th>@lang('quickadmin.entradanota.fields.valorfrete')</th>
                        <th>@lang('quickadmin.entradanota.fields.freteincluso')</th>
                        <th>@lang('quickadmin.entradanota.fields.tiponf')</th>
                        <th>@lang('quickadmin.entradanota.fields.cfop')</th>
                        <th>@lang('quickadmin.entradanota.fields.tipoentrada')</th>
                        <th>@lang('quickadmin.entradanota.fields.formapagamento')</th>
                        <th>@lang('quickadmin.entradanota.fields.valorservico')</th>
                        <th>@lang('quickadmin.entradanota.fields.serie')</th>
                        <th>@lang('quickadmin.entradanota.fields.centrocusto')</th>
                        <th>@lang('quickadmin.entradanota.fields.obs')</th>
                        <th>@lang('quickadmin.entradanota.fields.cnpjtransportadora')</th>
                        <th>@lang('quickadmin.entradanota.fields.chavenfe')</th>
                        <th>@lang('quickadmin.entradanota.fields.placa')</th>
                        <th>@lang('quickadmin.entradanota.fields.unidade')</th>
                        <th>@lang('quickadmin.entradanota.fields.datacadastro')</th>
                        <th>@lang('quickadmin.entradanota.fields.contacontabi')</th>
                        <th>@lang('quickadmin.entradanota.fields.xml')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>


@stop

@section('javascript') 
<script>
        $(document).ready(function () {
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
      
     });
 </script>
 <script>
        $(document).ready(function () {
            jQuery('#importa').click('keyup',function(){
                document.getElementById("importabanco").disabled = false; //desabilitar
                // $f = 11;
                // document.getElementById('nf').value= $f;
});

      
     });
 </script>
    <script>
        @can('entradanotum_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.entradanotas.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.entradanotas.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('entradanotum_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan{data: 'fornecedor', name: 'fornecedor'},
                {data: 'dataemissao', name: 'dataemissao'},
                {data: 'dataentrada', name: 'dataentrada'},
                {data: 'valosprodutos', name: 'valosprodutos'},
                {data: 'baseicms', name: 'baseicms'},
                {data: 'valoricms', name: 'valoricms'},
                {data: 'valorsubst', name: 'valorsubst'},
                {data: 'desconto', name: 'desconto'},
                {data: 'despesas', name: 'despesas'},
                {data: 'valornota', name: 'valornota'},
                {data: 'deposito', name: 'deposito'},
                {data: 'horaemissao', name: 'horaemissao'},
                {data: 'valoripi', name: 'valoripi'},
                {data: 'valorfrete', name: 'valorfrete'},
                {data: 'freteincluso', name: 'freteincluso'},
                {data: 'tiponf', name: 'tiponf'},
                {data: 'cfop', name: 'cfop'},
                {data: 'tipoentrada', name: 'tipoentrada'},
                {data: 'formapagamento', name: 'formapagamento'},
                {data: 'valorservico', name: 'valorservico'},
                {data: 'serie', name: 'serie'},
                {data: 'centrocusto', name: 'centrocusto'},
                {data: 'obs', name: 'obs'},
                {data: 'cnpjtransportadora', name: 'cnpjtransportadora'},
                {data: 'chavenfe', name: 'chavenfe'},
                {data: 'placa', name: 'placa'},
                {data: 'unidade', name: 'unidade'},
                {data: 'datacadastro', name: 'datacadastro'},
                {data: 'contacontabi', name: 'contacontabi'},
                {data: 'xml', name: 'xml'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>

      <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
      <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
      <script>
          $(function(){
              moment.updateLocale('{{ App::getLocale() }}', {
                  week: { dow: 1 } // Monday is the first day of the week
              });
  
              $('.date').datetimepicker({
                  format: "{{ config('app.date_format_moment') }}",
                  locale: "{{ App::getLocale() }}",
              });
  
          });
      </script>
      <script type="text/javascript">
    jQuery(document).ready(function(){
      jQuery('#limpar').click('keyup',function(){
        if(jQuery(this).attr('name') === 'result'){
        return false;
        }
        document.getElementById('fornecedor').value = "";
        document.getElementById('serie').value = "";
        document.getElementById('dataemissao').value = "";
        document.getElementById('valornota').value = "";
        document.getElementById('nf').value = "";
      //   document.getElementById('importa').value = "";
        document.getElementById('chavenfe').value = "";
        $("#tabela").remove(); 
        $("#tabelaproduto").remove(); 
        
      });
  
    });
    </script>
  
@endsection