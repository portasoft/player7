@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.entradanota.title')</h3>
    
    {!! Form::model($entradanota, ['method' => 'PUT', 'route' => ['admin.entradanotas.update', $entradanota->id], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('fornecedor', trans('quickadmin.entradanota.fields.fornecedor').'', ['class' => 'control-label']) !!}
                    {!! Form::text('fornecedor', old('fornecedor'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('fornecedor'))
                        <p class="help-block">
                            {{ $errors->first('fornecedor') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('dataemissao', trans('quickadmin.entradanota.fields.dataemissao').'', ['class' => 'control-label']) !!}
                    {!! Form::text('dataemissao', old('dataemissao'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('dataemissao'))
                        <p class="help-block">
                            {{ $errors->first('dataemissao') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('dataentrada', trans('quickadmin.entradanota.fields.dataentrada').'', ['class' => 'control-label']) !!}
                    {!! Form::text('dataentrada', old('dataentrada'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('dataentrada'))
                        <p class="help-block">
                            {{ $errors->first('dataentrada') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valosprodutos', trans('quickadmin.entradanota.fields.valosprodutos').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valosprodutos', old('valosprodutos'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valosprodutos'))
                        <p class="help-block">
                            {{ $errors->first('valosprodutos') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('baseicms', trans('quickadmin.entradanota.fields.baseicms').'', ['class' => 'control-label']) !!}
                    {!! Form::text('baseicms', old('baseicms'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('baseicms'))
                        <p class="help-block">
                            {{ $errors->first('baseicms') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valoricms', trans('quickadmin.entradanota.fields.valoricms').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valoricms', old('valoricms'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valoricms'))
                        <p class="help-block">
                            {{ $errors->first('valoricms') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valorsubst', trans('quickadmin.entradanota.fields.valorsubst').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valorsubst', old('valorsubst'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorsubst'))
                        <p class="help-block">
                            {{ $errors->first('valorsubst') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('desconto', trans('quickadmin.entradanota.fields.desconto').'', ['class' => 'control-label']) !!}
                    {!! Form::text('desconto', old('desconto'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('desconto'))
                        <p class="help-block">
                            {{ $errors->first('desconto') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('despesas', trans('quickadmin.entradanota.fields.despesas').'', ['class' => 'control-label']) !!}
                    {!! Form::text('despesas', old('despesas'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('despesas'))
                        <p class="help-block">
                            {{ $errors->first('despesas') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valornota', trans('quickadmin.entradanota.fields.valornota').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valornota', old('valornota'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valornota'))
                        <p class="help-block">
                            {{ $errors->first('valornota') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('deposito', trans('quickadmin.entradanota.fields.deposito').'', ['class' => 'control-label']) !!}
                    {!! Form::text('deposito', old('deposito'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('deposito'))
                        <p class="help-block">
                            {{ $errors->first('deposito') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('horaemissao', trans('quickadmin.entradanota.fields.horaemissao').'', ['class' => 'control-label']) !!}
                    {!! Form::text('horaemissao', old('horaemissao'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('horaemissao'))
                        <p class="help-block">
                            {{ $errors->first('horaemissao') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valoripi', trans('quickadmin.entradanota.fields.valoripi').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valoripi', old('valoripi'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valoripi'))
                        <p class="help-block">
                            {{ $errors->first('valoripi') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valorfrete', trans('quickadmin.entradanota.fields.valorfrete').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valorfrete', old('valorfrete'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorfrete'))
                        <p class="help-block">
                            {{ $errors->first('valorfrete') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('freteincluso', trans('quickadmin.entradanota.fields.freteincluso').'', ['class' => 'control-label']) !!}
                    {!! Form::text('freteincluso', old('freteincluso'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('freteincluso'))
                        <p class="help-block">
                            {{ $errors->first('freteincluso') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('tiponf', trans('quickadmin.entradanota.fields.tiponf').'', ['class' => 'control-label']) !!}
                    {!! Form::text('tiponf', old('tiponf'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tiponf'))
                        <p class="help-block">
                            {{ $errors->first('tiponf') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('cfop', trans('quickadmin.entradanota.fields.cfop').'', ['class' => 'control-label']) !!}
                    {!! Form::text('cfop', old('cfop'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('cfop'))
                        <p class="help-block">
                            {{ $errors->first('cfop') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('tipoentrada', trans('quickadmin.entradanota.fields.tipoentrada').'', ['class' => 'control-label']) !!}
                    {!! Form::text('tipoentrada', old('tipoentrada'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tipoentrada'))
                        <p class="help-block">
                            {{ $errors->first('tipoentrada') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('formapagamento', trans('quickadmin.entradanota.fields.formapagamento').'', ['class' => 'control-label']) !!}
                    {!! Form::text('formapagamento', old('formapagamento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('formapagamento'))
                        <p class="help-block">
                            {{ $errors->first('formapagamento') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('valorservico', trans('quickadmin.entradanota.fields.valorservico').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valorservico', old('valorservico'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorservico'))
                        <p class="help-block">
                            {{ $errors->first('valorservico') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('serie', trans('quickadmin.entradanota.fields.serie').'', ['class' => 'control-label']) !!}
                    {!! Form::text('serie', old('serie'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('serie'))
                        <p class="help-block">
                            {{ $errors->first('serie') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('centrocusto', trans('quickadmin.entradanota.fields.centrocusto').'', ['class' => 'control-label']) !!}
                    {!! Form::text('centrocusto', old('centrocusto'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('centrocusto'))
                        <p class="help-block">
                            {{ $errors->first('centrocusto') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('obs', trans('quickadmin.entradanota.fields.obs').'', ['class' => 'control-label']) !!}
                    {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('obs'))
                        <p class="help-block">
                            {{ $errors->first('obs') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('cnpjtransportadora', trans('quickadmin.entradanota.fields.cnpjtransportadora').'', ['class' => 'control-label']) !!}
                    {!! Form::text('cnpjtransportadora', old('cnpjtransportadora'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('cnpjtransportadora'))
                        <p class="help-block">
                            {{ $errors->first('cnpjtransportadora') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('chavenfe', trans('quickadmin.entradanota.fields.chavenfe').'', ['class' => 'control-label']) !!}
                    {!! Form::text('chavenfe', old('chavenfe'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('chavenfe'))
                        <p class="help-block">
                            {{ $errors->first('chavenfe') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('placa', trans('quickadmin.entradanota.fields.placa').'', ['class' => 'control-label']) !!}
                    {!! Form::text('placa', old('placa'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('placa'))
                        <p class="help-block">
                            {{ $errors->first('placa') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('unidade', trans('quickadmin.entradanota.fields.unidade').'', ['class' => 'control-label']) !!}
                    {!! Form::text('unidade', old('unidade'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('unidade'))
                        <p class="help-block">
                            {{ $errors->first('unidade') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('datacadastro', trans('quickadmin.entradanota.fields.datacadastro').'', ['class' => 'control-label']) !!}
                    {!! Form::text('datacadastro', old('datacadastro'), ['class' => 'form-control date', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('datacadastro'))
                        <p class="help-block">
                            {{ $errors->first('datacadastro') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('contacontabi', trans('quickadmin.entradanota.fields.contacontabi').'', ['class' => 'control-label']) !!}
                    {!! Form::text('contacontabi', old('contacontabi'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('contacontabi'))
                        <p class="help-block">
                            {{ $errors->first('contacontabi') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('xml', trans('quickadmin.entradanota.fields.xml').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('xml', old('xml')) !!}
                    @if ($entradanota->xml)
                        <a href="{{ asset(env('UPLOAD_PATH').'/' . $entradanota->xml) }}" target="_blank">Download file</a>
                    @endif
                    {!! Form::file('xml', ['class' => 'form-control']) !!}
                    {!! Form::hidden('xml_max_size', 2) !!}
                    <p class="help-block"></p>
                    @if($errors->has('xml'))
                        <p class="help-block">
                            {{ $errors->first('xml') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop