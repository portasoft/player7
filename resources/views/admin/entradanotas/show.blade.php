@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.entradanota.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.fornecedor')</th>
                            <td field-key='fornecedor'>{{ $entradanota->fornecedor }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.dataemissao')</th>
                            <td field-key='dataemissao'>{{ $entradanota->dataemissao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.dataentrada')</th>
                            <td field-key='dataentrada'>{{ $entradanota->dataentrada }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.valosprodutos')</th>
                            <td field-key='valosprodutos'>{{ $entradanota->valosprodutos }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.baseicms')</th>
                            <td field-key='baseicms'>{{ $entradanota->baseicms }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.valoricms')</th>
                            <td field-key='valoricms'>{{ $entradanota->valoricms }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.valorsubst')</th>
                            <td field-key='valorsubst'>{{ $entradanota->valorsubst }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.desconto')</th>
                            <td field-key='desconto'>{{ $entradanota->desconto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.despesas')</th>
                            <td field-key='despesas'>{{ $entradanota->despesas }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.valornota')</th>
                            <td field-key='valornota'>{{ $entradanota->valornota }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.deposito')</th>
                            <td field-key='deposito'>{{ $entradanota->deposito }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.horaemissao')</th>
                            <td field-key='horaemissao'>{{ $entradanota->horaemissao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.valoripi')</th>
                            <td field-key='valoripi'>{{ $entradanota->valoripi }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.valorfrete')</th>
                            <td field-key='valorfrete'>{{ $entradanota->valorfrete }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.freteincluso')</th>
                            <td field-key='freteincluso'>{{ $entradanota->freteincluso }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.tiponf')</th>
                            <td field-key='tiponf'>{{ $entradanota->tiponf }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.cfop')</th>
                            <td field-key='cfop'>{{ $entradanota->cfop }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.tipoentrada')</th>
                            <td field-key='tipoentrada'>{{ $entradanota->tipoentrada }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.formapagamento')</th>
                            <td field-key='formapagamento'>{{ $entradanota->formapagamento }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.valorservico')</th>
                            <td field-key='valorservico'>{{ $entradanota->valorservico }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.serie')</th>
                            <td field-key='serie'>{{ $entradanota->serie }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.centrocusto')</th>
                            <td field-key='centrocusto'>{{ $entradanota->centrocusto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.obs')</th>
                            <td field-key='obs'>{{ $entradanota->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.cnpjtransportadora')</th>
                            <td field-key='cnpjtransportadora'>{{ $entradanota->cnpjtransportadora }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.chavenfe')</th>
                            <td field-key='chavenfe'>{{ $entradanota->chavenfe }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.placa')</th>
                            <td field-key='placa'>{{ $entradanota->placa }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.unidade')</th>
                            <td field-key='unidade'>{{ $entradanota->unidade }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.datacadastro')</th>
                            <td field-key='datacadastro'>{{ $entradanota->datacadastro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.contacontabi')</th>
                            <td field-key='contacontabi'>{{ $entradanota->contacontabi }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.entradanota.fields.xml')</th>
                            <td field-key='xml'>@if($entradanota->xml)<a href="{{ asset(env('UPLOAD_PATH').'/' . $entradanota->xml) }}" target="_blank">Download file</a>@endif</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.entradanotas.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
