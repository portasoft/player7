
@extends('layouts.app')

@section('content')

    {{-- <h3 class="page-title">@lang('quickadmin.entradanota.title')</h3> --}}
 
    {!! Form::open(['method' => 'POST', 'route' => ['admin.entradanotas.store'], 'files' => true,]) !!}
    

    <div class="panel panel-default">
        <div class="panel-heading">
            {{-- @lang('quickadmin.qa_create') --}}
            <h4>ENTRADA DE NOTA</h4>
        </div>

        <div class="panel-body">

            <div class="row">

                    {!! Form::open(['method' => 'POST', 'route' => ['Admin.entradanotas.importartela']]) !!}
                    <div class="col-xs-12 form-group">
                            <h4>NF-e</h4>
                        <input style="font-size:12px;"  id="importa"  name="importa" type="file" >
                        <input type="submit" class="btn btn-primary" value="Importar XML">
                        <input type="button" class="btn btn-danger" id="limpar" value="Limpar">
                    </div>
              
                   <div class="col-xs-2">
                        
               <label for="">NF</label>
               @if (isset($notaFiscal))
               <input type="text" id="nf" readonly name="nf"  value="{{ $notaFiscal }}" class="form-control">
               @else
               <input type="text" id="nf" readonly name="nf"  value="" class="form-control">

               @endif

                  </div>
                  <div class="col-xs-2">

                    <label for="">Série</label>
                    @if (isset($serie))

                    <input type="text" id="serie" readonly name="serie"  value="{{ $serie }}" class="form-control">
                    @else
                    <input type="text" id="serie" readonly name="serie"  value="" class="form-control">


                    @endif

                       </div>
                       <div class="col-xs-2 form-group">
                        <label for="">Data Emissão</label>
                        @if (isset($serie))

                        <input type="text" id="dataemissao" readonly name="dataemissao"  value="{{ $emissao }}" class="form-control">
                        @else
                        <input type="text" id="dataemissao" readonly name="dataemissao"  value="" class="form-control">


                        @endif

                    </div>
                    <div class="col-xs-2 form-group">
                        <label for="">Data Entrada</label>
                        {!! Form::date('dataentrada', old('dataentrada'), ['class' => 'form-control date', 'placeholder' => '']) !!}

                    </div>
                    <div class="col-xs-4 form-group">
                        <label for="">Chave de Acesso</label>
                        @if (isset($chave))
                        <input type="text" id="chavenfe" readonly name="chavenfe"  value="{{ $chave }}" class="form-control">

                        @else
                        <input type="text" id="chavenfe" readonly name="chavenfe"  class="form-control">
                        @endif

                    </div>
                    <form action="importartela" method="POST">

                    <div class="col-xs-4 form-group">

                        {!! Form::label('fornecedor', trans('quickadmin.entradanota.fields.fornecedor').'', ['class' => 'control-label']) !!}
                @if (isset($emitente))

                <input type="text" id="fornecedor" readonly name="fornecedor"  value="{{ $emitente }}" class="form-control">
                @else
                <input type="text" id="fornecedor" readonly name="fornecedor"  value="" class="form-control">
                <input type="submit" class="btn btn-primary" value="Cadastrar fornecedor">

                @endif





                    </div>
                          <div class="col-xs-2 form-group">
                            <label for="">Valor total</label>
                            @if (isset($total))

                            <input type="text" id="valornota" readonly name="valornota"  value="{{ $total }}" class="form-control">
                            @else
                            <input type="text" id="valornota" readonly name="valornota"  value="" class="form-control">


                            @endif


                </div>
                <div class="col-xs-6 form-group">
                    <label for="">Depósito</label>
                  
                    {!! Form::select('deposito',$deposito,null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    {{-- <p class="help-block"></p>
                    @if($errors->has('deposito'))
                        <p class="help-block">
                            {{ $errors->first('deposito') }}
                        </p>
                    @endif --}}
                </div>
                <div class="col-xs-12"  style="overflow: auto; width:2000px ">
                <table id="tabela" class="table table-hover ">
                    <tbody><tr>
                      <td class="col-sm-1 btn-lg ">Cod.Fornecedor</td>
                      <td class="col-sm-4 btn-lg ">Produto</td>
                      <td class="col-sm-1 btn-lg" >Qtde</td>
                      <td class="col-sm-1 btn-lg" >Unidade</td>
                      <td class="col-sm-1 btn-lg" >Ação</td>
                      <td class="col-sm-1 btn-lg" >Custo</td>
                      <td class="col-sm-1 btn-lg" >Total</td>
                      <td class="col-sm-1 btn-lg" >NCM</td>
                      <td class="col-sm-1 btn-lg" >CFOP</td>
                      <td class="col-sm-1 btn-lg">CST</td>
                      <td class="col-sm-1 btn-lg">%ICMS</td>
                      <td class="col-sm-2 btn-lg">Valor ICMS</td>
                      <td class="col-sm-1 btn-lg">CST</td>
                      <td class="col-sm-1 btn-lg">CST</td>
                      <td class="col-sm-1 btn-lg">CST</td>
                      <td class="col-sm-1 btn-lg">CST</td>
                      <td class="col-sm-1 btn-lg">CST</td>
                      <td class="col-sm-1 btn-lg">CST</td>
                      <td class="col-sm-1 btn-lg">CST</td>
                    </tr>
                    <tr>
                        <?php
                                            
                             function format($number, $dec =2){
                            
                            return number_format ((float) $number ,$dec, ".","");
                        }
                        function formatq($number){
                            
                            return number_format ((float) $number);
                        }
                        if (isset($xml)) {
                            foreach($xml->NFe as $key => $item) {	    	
                                if(isset($item->infNFe)) {	
                                //LENDO AS INFORMAÇÕES DE PRODUTOS NA NF-e (XML)
                                    $semResultado = 0;
                                    for ($i=0; $i <=1000 ; $i++) { 
                                        if(!empty($item->infNFe->det[$i]->prod->cProd)){
                                            $semResultado = 0;
                               
                                            echo "<tr>";
                                            echo "<td>".$item->infNFe->det[$i]->prod->cProd."</td>";      
                                            echo "<td>".$item->infNFe->det[$i]->prod->xProd."</td>";
                                            echo "<td>".formatq($item->infNFe->det[$i]->prod->qCom)."</td>";
                                            echo "<td>".$item->infNFe->det[$i]->prod->uCom."</td>";
                                            echo "<td><span class='label label-danger'>Pendente</span></td>";
                                            echo "<td>".format($item->infNFe->det[$i]->prod->vUnTrib)."</td>";
                                            echo "<td>".format($item->infNFe->det[$i]->prod->vProd)."</td>";
                                            echo "<td>".$item->infNFe->det[$i]->prod->NCM."</td>";
                                            echo "<td>".$item->infNFe->det[$i]->prod->CFOP."</td>";
                                            echo "<td>".$item->infNFe->det[$i]->imposto->ICMS->ICMS90->CST."</td>";
                                            echo "<td>".$item->infNFe->det[$i]->imposto->ICMS->ICMS90->pICMS."</td>";
                                            echo "<td>".$item->infNFe->det[$i]->imposto->ICMS->ICMS90->vICMS."</td>";
                                            
                                            
                                            // <td><span class="label label-danger">Pendente</span></td>
                                                 
                                        } else {
                                            //CONTANDO QUANDO NÃO HOUVER RESULTADOS
                                            $semResultado ++;
                                        }
                    
                                        //SE N TIVER RESULTADOS EM SEQUENCIA, PARAR O FOR
                                        if($semResultado >= 10){
                                            break;
                                        }
                                  
                                     
                                    } // FIM DO FOR
                               
                                }
                            }
                        }
                                              

                        ?>
                   
                    </tr>


                  </tbody></table>
                </div>


                </form>
          <br><br><br>       <br><br><br>



                    {!! Form::close() !!}
                    {{--




                <div class="col-xs-2 form-group">
                    {!! Form::label('valosprodutos', trans('quickadmin.entradanota.fields.valosprodutos').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valosprodutos', old('valosprodutos'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valosprodutos'))
                        <p class="help-block">
                            {{ $errors->first('valosprodutos') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('baseicms', trans('quickadmin.entradanota.fields.baseicms').'', ['class' => 'control-label']) !!}
                    {!! Form::text('baseicms', old('baseicms'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('baseicms'))
                        <p class="help-block">
                            {{ $errors->first('baseicms') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('valoricms', trans('quickadmin.entradanota.fields.valoricms').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valoricms', old('valoricms'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valoricms'))
                        <p class="help-block">
                            {{ $errors->first('valoricms') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('valorsubst', trans('quickadmin.entradanota.fields.valorsubst').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valorsubst', old('valorsubst'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorsubst'))
                        <p class="help-block">
                            {{ $errors->first('valorsubst') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('desconto', trans('quickadmin.entradanota.fields.desconto').'', ['class' => 'control-label']) !!}
                    {!! Form::text('desconto', old('desconto'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('desconto'))
                        <p class="help-block">
                            {{ $errors->first('desconto') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('despesas', trans('quickadmin.entradanota.fields.despesas').'', ['class' => 'control-label']) !!}
                    {!! Form::text('despesas', old('despesas'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('despesas'))
                        <p class="help-block">
                            {{ $errors->first('despesas') }}
                        </p>
                    @endif
                </div> --}}





                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('horaemissao', trans('quickadmin.entradanota.fields.horaemissao').'', ['class' => 'control-label']) !!}
                    {!! Form::text('horaemissao', old('horaemissao'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('horaemissao'))
                        <p class="help-block">
                            {{ $errors->first('horaemissao') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('valoripi', trans('quickadmin.entradanota.fields.valoripi').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valoripi', old('valoripi'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valoripi'))
                        <p class="help-block">
                            {{ $errors->first('valoripi') }}
                        </p>
                    @endif
                </div>

                <div class="col-xs-12 form-group">
                    {!! Form::label('valorfrete', trans('quickadmin.entradanota.fields.valorfrete').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valorfrete', old('valorfrete'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorfrete'))
                        <p class="help-block">
                            {{ $errors->first('valorfrete') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('freteincluso', trans('quickadmin.entradanota.fields.freteincluso').'', ['class' => 'control-label']) !!}
                    {!! Form::text('freteincluso', old('freteincluso'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('freteincluso'))
                        <p class="help-block">
                            {{ $errors->first('freteincluso') }}
                        </p>
                    @endif
                </div>

                <div class="col-xs-12 form-group">
                    {!! Form::label('tiponf', trans('quickadmin.entradanota.fields.tiponf').'', ['class' => 'control-label']) !!}
                    {!! Form::text('tiponf', old('tiponf'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tiponf'))
                        <p class="help-block">
                            {{ $errors->first('tiponf') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('cfop', trans('quickadmin.entradanota.fields.cfop').'', ['class' => 'control-label']) !!}
                    {!! Form::text('cfop', old('cfop'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('cfop'))
                        <p class="help-block">
                            {{ $errors->first('cfop') }}
                        </p>
                    @endif
                </div>

                <div class="col-xs-12 form-group">
                    {!! Form::label('tipoentrada', trans('quickadmin.entradanota.fields.tipoentrada').'', ['class' => 'control-label']) !!}
                    {!! Form::text('tipoentrada', old('tipoentrada'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tipoentrada'))
                        <p class="help-block">
                            {{ $errors->first('tipoentrada') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-2 form-group">
                    {!! Form::label('formapagamento', trans('quickadmin.entradanota.fields.formapagamento').'', ['class' => 'control-label']) !!}
                    {!! Form::select('formapagamento', $formapagamento,null, ['class' => 'form-control', 'placeholder' => '','required']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('formapagamento'))
                        <p class="help-block">
                            {{ $errors->first('formapagamento') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('valorservico', trans('quickadmin.entradanota.fields.valorservico').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valorservico', old('valorservico'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valorservico'))
                        <p class="help-block">
                            {{ $errors->first('valorservico') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-2 form-group">
                    {!! Form::label('serie', trans('quickadmin.entradanota.fields.serie').'', ['class' => 'control-label']) !!}
                    {!! Form::text('serie', old('serie'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('serie'))
                        <p class="help-block">
                            {{ $errors->first('serie') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('centrocusto', trans('quickadmin.entradanota.fields.centrocusto').'', ['class' => 'control-label']) !!}
                    {!! Form::text('centrocusto', old('centrocusto'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('centrocusto'))
                        <p class="help-block">
                            {{ $errors->first('centrocusto') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-6 form-group">
                    {!! Form::label('obs', trans('quickadmin.entradanota.fields.obs').'', ['class' => 'control-label']) !!}
                    {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('obs'))
                        <p class="help-block">
                            {{ $errors->first('obs') }}
                        </p>
                    @endif
                </div> --}}
{{--
                <div class="col-xs-12 form-group">
                    {!! Form::label('cnpjtransportadora', trans('quickadmin.entradanota.fields.cnpjtransportadora').'', ['class' => 'control-label']) !!}
                    {!! Form::text('cnpjtransportadora', old('cnpjtransportadora'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('cnpjtransportadora'))
                        <p class="help-block">
                            {{ $errors->first('cnpjtransportadora') }}
                        </p>
                    @endif
                </div> --}}



                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('placa', trans('quickadmin.entradanota.fields.placa').'', ['class' => 'control-label']) !!}
                    {!! Form::text('placa', old('placa'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('placa'))
                        <p class="help-block">
                            {{ $errors->first('placa') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-1 form-group">
                    {!! Form::label('unidade', trans('quickadmin.entradanota.fields.unidade').'', ['class' => 'control-label']) !!}
                    {!! Form::text('unidade', old('unidade'), ['class' => 'form-control', 'placeholder' => '','required']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('unidade'))
                        <p class="help-block">
                            {{ $errors->first('unidade') }}
                        </p>
                    @endif
                </div>

                <div class="col-xs-2 form-group">
                    {!! Form::label('datacadastro', trans('quickadmin.entradanota.fields.datacadastro').'', ['class' => 'control-label']) !!}
                    {!! Form::date('datacadastro', old('datacadastro'), ['class' => 'form-control date', 'placeholder' => '','required']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('datacadastro'))
                        <p class="help-block">
                            {{ $errors->first('datacadastro') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('contacontabi', trans('quickadmin.entradanota.fields.contacontabi').'', ['class' => 'control-label']) !!}
                    {!! Form::text('contacontabi', old('contacontabi'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('contacontabi'))
                        <p class="help-block">
                            {{ $errors->first('contacontabi') }}
                        </p>
                    @endif
                </div> --}}

                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('xml', trans('quickadmin.entradanota.fields.xml').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('xml', old('xml')) !!}
                    {!! Form::file('xml', ['class' => 'form-control']) !!}
                    {!! Form::hidden('xml_max_size', 2) !!}
                    <p class="help-block"></p>
                    @if($errors->has('xml'))
                        <p class="help-block">
                            {{ $errors->first('xml') }}
                        </p>
                    @endif
                </div> --}}
            </div>

        </div>
    </div>

    {{-- {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!} --}}
    {{-- {!! Form::close() !!} --}}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });

            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });

        });
    </script>
    <script type="text/javascript">
  jQuery(document).ready(function(){
    jQuery('#limpar').click('keyup',function(){
      if(jQuery(this).attr('name') === 'result'){
      return false;
      }
      document.getElementById('fornecedor').value = "";
      document.getElementById('serie').value = "";
      document.getElementById('dataemissao').value = "";
      document.getElementById('valornota').value = "";
      document.getElementById('nf').value = "";
    //   document.getElementById('importa').value = "";
      document.getElementById('chavenfe').value = "";
      $("#tabela").remove(); 
    });

  });
  </script>

@stop