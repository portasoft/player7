@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.movimento.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.movimentos.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-2 form-group">
                    {!! Form::label('data', trans('quickadmin.movimento.fields.data').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('data', old('data'), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('data'))
                        <p class="help-block">
                            {{ $errors->first('data') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('tipo', trans('quickadmin.movimento.fields.tipo').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('tipo',['Entrada','Saída'],'null', ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('tipo'))
                        <p class="help-block">
                            {{ $errors->first('tipo') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-3 form-group">
                    {!! Form::label('empresa', trans('quickadmin.movimento.fields.empresa').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('empresa',$empresa,null, ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('empresa'))
                        <p class="help-block">
                            {{ $errors->first('empresa') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-3 form-group">
                    {!! Form::label('deposito', trans('quickadmin.movimento.fields.deposito').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('deposito', old('deposito'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('deposito'))
                        <p class="help-block">
                            {{ $errors->first('deposito') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('usuario', trans('quickadmin.movimento.fields.usuario').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('usuario', old('usuario'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('usuario'))
                        <p class="help-block">
                            {{ $errors->first('usuario') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-3 form-group">
                    {!! Form::label('produto', trans('quickadmin.movimento.fields.produto').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('produto', old('produto'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('produto'))
                        <p class="help-block">
                            {{ $errors->first('produto') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-1 form-group">
                    {!! Form::label('qtde', trans('quickadmin.movimento.fields.qtde').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('qtde', old('qtde'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('qtde'))
                        <p class="help-block">
                            {{ $errors->first('qtde') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-3 form-group">
                    {!! Form::label('motivo', trans('quickadmin.movimento.fields.motivo').'', ['class' => 'control-label']) !!}
                    {!! Form::text('motivo', old('motivo'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('motivo'))
                        <p class="help-block">
                            {{ $errors->first('motivo') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-2 form-group">
                    {!! Form::label('documento', trans('quickadmin.movimento.fields.documento').'', ['class' => 'control-label']) !!}
                    {!! Form::text('documento', old('documento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('documento'))
                        <p class="help-block">
                            {{ $errors->first('documento') }}
                        </p>
                    @endif
                </div>
          
                <div class="col-xs-3 form-group">
                    {!! Form::label('obs', trans('quickadmin.movimento.fields.obs').'', ['class' => 'control-label']) !!}
                    {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('obs'))
                        <p class="help-block">
                            {{ $errors->first('obs') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
    </script>
            
@stop