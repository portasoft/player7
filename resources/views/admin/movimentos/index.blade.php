@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
{{-- <h3 class="page-title">@lang('quickadmin.movimento.title')</h3> --}}
{!! Form::open(['method' => 'POST', 'route' => ['admin.movimentos.store']]) !!}
<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
<div class="panel panel-primary">
    <div id="HNome" class="panel-heading">
        {{-- @lang('quickadmin.qa_create') --}}
        <h4 id="TNome">MOIVMENTO DE ESTOQUE</h4>
    </div>
    {{-- @if (isset($s))
    @if ($s==1)
    <div class="alert alert-danger alert-dismissible" id="success-alert">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
           <h4><i class="icon fa fa-ban"></i>Movimento realizado com suesso!</h4>
        </div> 
    @endif
    @endif --}}
    @if (session()->has('success'))
    <div class="alert alert-success alert-dismissible" id="success-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
        <h4><i class="icon fa fa-check"></i>Movimento realizado com suesso!</h4>
     </div> 
@endif
   
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-2 form-group">
                {!! Form::label('data', trans('quickadmin.movimento.fields.data').'*', ['class' => 'control-label']) !!}
                {!! Form::date('data', old('data'), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('data'))
                    <p class="help-block">
                        {{ $errors->first('data') }}
                    </p>
                @endif
            </div>
       
           
            <div class="col-xs-2 form-group">
                {!! Form::label('tipo', trans('quickadmin.movimento.fields.tipo').'*', ['class' => 'control-label']) !!}
                {!! Form::select('tipo',array('Entrada'=>'Entrada','Saida'=>'Saida'),'null', ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('tipo'))
                    <p class="help-block">
                        {{ $errors->first('tipo') }}
                    </p>
                @endif
            </div>
            
            <div class="col-xs-4 form-group">
                {!! Form::label('produto', trans('quickadmin.movimento.fields.produto').'', ['class' => 'control-label']) !!}
           @if (isset($produ))
           {!! Form::text('produto', old('produto'), ['class' =>  'form-control','id'=>'idProdutoAdicionaM','autocomplete'=>'off', 'placeholder' => 'Informe o produto....', 'required' => '']) !!}
           <div id="idProdutoAdicionalistM"></div>
           @else
               
           @endif
                {!! Form::text('produto', old('produto'), ['class' =>  'form-control','id'=>'idProdutoAdicionaM','autocomplete'=>'off', 'placeholder' => 'Informe o produto....', 'required' => '']) !!}
                <div id="idProdutoAdicionalistM"></div>
                
           
            </div>
            {{-- <div class="col-xs-3 form-group">
                {!! Form::label('empresa', trans('quickadmin.movimento.fields.empresa').'*', ['class' => 'control-label']) !!}
                {!! Form::select('empresa',$empresa,null, ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('empresa'))
                    <p class="help-block">
                        {{ $errors->first('empresa') }}
                    </p>
                @endif
            </div>
      
            <div class="col-xs-3 form-group">
                {!! Form::label('deposito', trans('quickadmin.movimento.fields.deposito').'*', ['class' => 'control-label']) !!}
                {!! Form::select('deposito', $deposito,null, ['class' => 'form-control', 'placeholder' => '', 'required' => '','id'=>'deposito']) !!}
                <p class="help-block"></p>
                @if($errors->has('deposito'))
                    <p class="help-block">
                        {{ $errors->first('deposito') }}
                    </p>
                @endif
            </div> --}}
{{--       
            <div class="form-group col-sm-2">
                    {!! Form::label('usuario', trans('quickadmin.movimento.fields.usuario').'*', ['class' => 'control-label']) !!}
            {!! Form::text('usuario',null,['class'=>'form-control','id'=>'idUsuarioAdiciona','name'=>'idUsuarioAdiciona','autofocus'=>'autofocus','placeholder'=>'INFORME O USUÁRIO....']) !!}

            <div id="idUsuarioAdicionalist"></div>
        </div> --}}
        <div class="col-xs-1 form-group">
            {!! Form::label('qtde', trans('quickadmin.movimento.fields.qtde').'*', ['class' => 'control-label']) !!}
            {!! Form::text('qtde', old('qtde'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','onKeyUp'=>"moeda(this)"]) !!}
            <p class="help-block"></p>
            @if($errors->has('qtde'))
                <p class="help-block">
                    {{ $errors->first('qtde') }}
                </p>
            @endif
        </div>
        <div class="col-xs-3 form-group">
            <label for="">COR</label>
             {!! Form::select('cor', $cor,null, ['class' => 'form-control', 'placeholder' => '', 'required' => '','id'=>'cor']) !!}
            <br>
         </div>
      
            

      
      
            
            <div class="col-xs-3 form-group">
                <label for="">FORNECEDOR</label>
                {!! Form::select('fornecedor', $fornecedor,null, ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
               
            </div>
            <div class="col-xs-2 form-group">
                {!! Form::label('usuario', trans('quickadmin.movimento.fields.usuario').'*', ['class' => 'control-label']) !!}
                {!! Form::text('usuario', old('usuario'), ['class' =>  'form-control','id'=>'idUsuarioAdiciona','autocomplete'=>'off',  'placeholder' => 'Informe o usuário....', 'required' => '']) !!}
                <div id="idUsuarioAdicionalist"></div>
           
            </div>
          
            <div class="col-xs-2 form-group">
                {!! Form::label('documento', trans('quickadmin.movimento.fields.documento').'', ['class' => 'control-label']) !!}
                {!! Form::text('documento', old('documento'), ['class' => 'form-control', 'placeholder' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('documento'))
                    <p class="help-block">
                        {{ $errors->first('documento') }}
                    </p>
                @endif
            </div>
            {{-- <div class="col-xs-3 form-group">
                {!! Form::label('motivo', trans('quickadmin.movimento.fields.motivo').'', ['class' => 'control-label']) !!}
                {!! Form::text('motivo', old('motivo'), ['class' => 'form-control', 'placeholder' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('motivo'))
                    <p class="help-block">
                        {{ $errors->first('motivo') }}
                    </p>
                @endif
            </div> --}}
      
        
      
            <div class="col-xs-5 form-group">
                {!! Form::label('obs', trans('quickadmin.movimento.fields.obs').'', ['class' => 'control-label']) !!}
                {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('obs'))
                    <p class="help-block">
                        {{ $errors->first('obs') }}
                    </p>
                @endif
            </div>
        </div>
        
    </div>
</div>
 
<button type="submit" class="btn btn-primary btn-lg">
        <i class="fa fa-save"> </i>
    SALVAR</button>
{{-- {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!} --}}
{!! Form::close() !!}
    {{-- <h3 class="page-title">@lang('quickadmin.movimento.title')</h3> --}}
    {{-- @can('movimento_create')
    <p>
        <a href="{{ route('admin.movimentos.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan --}}

    @can('movimento_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.movimentos.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.movimentos.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="hNome" class="panel-heading">
       <h4 id="TNome">RELAÇÃO - MOVIMENTOS</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($movimentos) > 0 ? 'datatable' : '' }} @can('movimento_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('movimento_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.movimento.fields.data')</th>
                        <th>@lang('quickadmin.movimento.fields.tipo')</th>
                        <th>@lang('quickadmin.movimento.fields.produto')</th>
                        <th>@lang('quickadmin.movimento.fields.qtde')</th>
                        <th>COR</th>
                        <th>FORNECEDOR</th>
                        {{-- <th>@lang('quickadmin.movimento.fields.empresa')</th>
                        <th>@lang('quickadmin.movimento.fields.deposito')</th> --}}
                        <th>@lang('quickadmin.movimento.fields.usuario')</th>
            
                  
                        {{-- <th>@lang('quickadmin.movimento.fields.motivo')</th> --}}
                        <th>@lang('quickadmin.movimento.fields.documento')</th>
                        <th>@lang('quickadmin.movimento.fields.obs')</th>
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($movimentos) > 0)
                        @foreach ($movimentos as $movimento)
                            <tr data-entry-id="{{ $movimento->id }}">
                                @can('movimento_delete')
                                    @if ( request('show_deleted') != 1 )<td></td>@endif
                                @endcan

                                <td field-key='data'>{{ $movimento->data }}</td>
                                <td field-key='tipo'>{{ $movimento->tipo }}</td>
                                <td field-key='produto'>{{ $movimento->produto }}</td>
                                    
                                <td field-key='qtde'>{{ $movimento->qtde }}</td>
                                @foreach ($cors as $c)
                                @if ($movimento->cor == $c->id)
                                <td field-key='cor'>{{ $c->descricao}}</td>
                                @endif
                                @endforeach
                                {{-- <td field-key='qtde'>{{ $movimento->cor }}</td> --}}
                                @foreach ($fornecedors as $f)
                                @if ($movimento->fornecedor  == $f->id)
                                <td field-key='fornecedor'>{{ $f->razao}}</td>
                                @endif
                                @endforeach
                                {{-- <td field-key='qtde'>{{ $movimento->fornecedor }}</td> --}}
                                {{-- <td field-key='empresa'>{{ $movimento->empresa }}</td>
                                <td field-key='deposito'>{{ $movimento->deposito }}</td> --}}
                                @foreach ($users as $u)
                                @if ($movimento->usuario  == $u->id)
                                <td field-key='usuario'>{{ $u->name}}</td>
                                @endif
                                @endforeach
                                {{-- <td field-key='usuario'>{{ $movimento->usuario }}</td> --}}
                 
                                {{-- <td field-key='motivo'>{{ $movimento->motivo }}</td> --}}
                                <td field-key='documento'>{{ $movimento->documento }}</td>
                                <td field-key='obs'>{{ $movimento->obs }}</td>
                                @if( request('show_deleted') == 1 )
                                <td>
                                    @can('movimento_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'POST',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.movimentos.restore', $movimento->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_restore'), array('class' => 'btn btn-xs btn-success')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                    @can('movimento_delete')
                                                                        {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.movimentos.perma_del', $movimento->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_permadel'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                @endcan
                                </td>
                                @else
                                <td>
                                    @can('movimento_view')
                                    <a href="{{ route('admin.movimentos.show',[$movimento->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.qa_view')</a>
                                    @endcan
                                    {{-- @can('movimento_edit')
                                    <a href="{{ route('admin.movimentos.edit',[$movimento->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.qa_edit')</a>
                                    @endcan
                                    @can('movimento_delete')
{!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.qa_are_you_sure")."');",
                                        'route' => ['admin.movimentos.destroy', $movimento->id])) !!}
                                    {!! Form::submit(trans('quickadmin.qa_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                    @endcan --}}
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="15">@lang('quickadmin.qa_no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
<script>
    $(document).ready(function () {
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
$("#success-alert").slideUp(500);
});
  
 });
 function moeda(z){

v = z.value;

v=v.replace(/\D/g,"") //permite digitar apenas números

v=v.replace(/[0-9]{12}/,"inválido") //limita pra máximo 999.999.999,99

v=v.replace(/(\d{1})(\d{8})$/,"$1.$2") //coloca ponto antes dos últimos 8 digitos

v=v.replace(/(\d{1})(\d{1,2})$/,"$1.$2") //coloca ponto antes dos últimos 5 digitos

//v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2") //coloca virgula antes dos últimos 2 digitos

z.value = v;

}
</script>
<script type="text/javascript">
    $(document).ready(function(){
             $('#idProdutoAdicionaM').keyup(function(){
                 var query = $(this).val();
                 if(query.text != '' )
                 {
                   var _token = $('input[name="_token"]').val();
                      $.ajax({
                         url:"{{ route('movimento.fetch')}}",
                   
                         method:"POST",
                         data:{query:query,_token:_token},
                         success:function(data)
                         {
                             $('#idProdutoAdicionalistM').fadeIn();
                             $('#idProdutoAdicionalistM').html(data);
                         }
                     })
                 }
               
             });
        
             $(document).on('click','li.c',function(){
                // var queryID = $(this).val();
                                //  document.getElementById('idUsuarioAdiciona').value =  'sssss';
                                 $('#idProdutoAdicionaM').val($(this).text());
                                 $('#idProdutoAdicionalistM').fadeOut()
                            
                              })
                     })
          
               
      
</script>
<script type="text/javascript">
    $(document).ready(function(){
             $('#idUsuarioAdiciona').keyup(function(){
                 var query = $(this).val();
                 if(query.text != '' )
                 {
                   var _token = $('input[name="_token"]').val();
                      $.ajax({
                         url:"{{ route('movimento.Usuarioadiciona')}}",
                   
                         method:"POST",
                         data:{query:query,_token:_token},
                         success:function(data)
                         {
                             $('#idUsuarioAdicionalist').fadeIn();
                             $('#idUsuarioAdicionalist').html(data);
                         }
                     })
                 }
               
             });
        
             $(document).on('click','li.u',function(){
                                 
                                 $('#idUsuarioAdiciona').val($(this).text());
                                 $('#idUsuarioAdicionalist').fadeOut()
                            
                         
                              })
                     })
          
               
      
</script>
    <script>
        @can('movimento_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.movimentos.mass_destroy') }}'; @endif
        @endcan

    </script>
  
@endsection