@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.produto.title')</h3> --}}
    <link rel="stylesheet" href="{{ URL::asset('css/menuss.css') }}">
    {!! Form::model($produto, ['method' => 'PUT', 'route' => ['admin.produtos.update', $produto->id], 'files' => true,]) !!}
    <div class="panel panel-primary " style="background-color:white">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">ALTERAR - PRODUTO</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>ALTERADO COM SUCESSO!</h4>
         </div> 
    @endif
    @if (session()->has('success2'))
    <div class="alert alert-warning alert-dismissible" id="success-alert2">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
        <h4><i class="icon fa fa-check"></i>ESTE KIT JÁ ESTA  CADASTRADO!</h4>
     </div> 
@endif
        <div class="panel-body">
            <div class="panel-body">
                <div class="row">
                        <div class="col-xs-2 form-group">
                                {!! Form::label('finalidade', trans('quickadmin.produto.fields.finalidade').'', ['class' => 'control-label']) !!}
                                {!! Form::select('finalidade', ['PRODUTO'=>'PRODUTO','ITEM'=>'ITEM'],null, ['class' => 'form-control','id'=>'finalidade', 'placeholder' => '','required']) !!}
                                <p class="help-block"></p>
                                @if($errors->has('finalidade'))
                                    <p class="help-block">
                                        {{ $errors->first('finalidade') }}
                                    </p>
                                @endif
                            </div>
                
                    <div class="col-xs-4 form-group">
                        {!! Form::label('descricao', trans('quickadmin.produto.fields.descricao').'', ['class' => 'control-label']) !!}
                        {!! Form::text('descricao', old('descricao'), ['class' => 'form-control','disabled'=>'true', 'placeholder' => '', 'required' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('descricao'))
                            <p class="help-block">
                                {{ $errors->first('descricao') }}
                            </p>
                        @endif
                    </div>
                 
                    <div class="col-xs-2 form-group">
                            {!! Form::label('custo', trans('quickadmin.produto.fields.custo').'', ['class' => 'control-label']) !!}
                            {!! Form::text('custo', old('custo'), ['class' => 'form-control','disabled'=>'true', 'onKeyUp'=>"moeda(this)" ]) !!}
                            <p class="help-block"></p>
                            @if($errors->has('custo'))
                                <p class="help-block">
                                    {{ $errors->first('custo') }}
                                </p>
                            @endif
                        </div>
                        <div class="col-xs-2 form-group">
                                {!! Form::label('unidade', trans('quickadmin.produto.fields.unidade').'', ['class' => 'control-label']) !!}
                                {!! Form::select('unidade',$unidade,null, ['class' => 'form-control','disabled'=>'true','required','placeholder'=>'']) !!}
                                <p class="help-block"></p>
                             
                            </div>
                          
                            <div class="col-xs-2 form-group">
                                <label for="">CADASTRO</label>
                                      {!! Form::date('datacadastro', old('datacadastro'), ['class' => 'form-control date','disabled'=>'true','id'=>'data']) !!}
                                      <p class="help-block"></p>
                                      @if($errors->has('datacadastro'))
                                          <p class="help-block">
                                              {{ $errors->first('datacadastro') }}
                                          </p>
                                      @endif
                                  </div>
                                  <div class="col-xs-3 form-group">
                                        <label for="">COMPOSIÇÃO</label>
                                        {!! Form::select('marca',['NDF'=>'NDF','100% ALGODAO FIO 20'=>'100% ALGODAO FIO 20','100%'=>'100%','100% ALGODAO FIO 24'=>'100% ALGODAO FIO 24','75/25'=>'75/25','50/50'=>'50/50'],NULL, ['class' => 'form-control','id'=>'marca','disabled'=>'true', 'placeholder'=>'','required']) !!}
                                      
                                      </div>
                    <div class="col-xs-2 form-group">
                        {!! Form::label('estoque', trans('quickadmin.produto.fields.estoque').'', ['class' => 'control-label']) !!}
                        {!! Form::text('estoque', old('estoque'), ['class' => 'form-control','disabled'=>'true', 'placeholder' => '']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('estoque'))
                            <p class="help-block">
                                {{ $errors->first('estoque') }}
                            </p>
                        @endif
                    </div>
                    <div class="col-xs-4 form-group">
                        <label for="">FORNECEDOR</label>
                
                      {!! Form::select('codforne',$forne,null, ['class' => 'form-control', 'placeholder' => '','disabled'=>'true','id'=>'codforne','required']) !!}
                          <p class="help-block"></p>
                       
                      </div>
                      <div class="col-xs-3 form-group">
                        {!! Form::label('deposito', trans('quickadmin.produto.fields.deposito').'', ['class' => 'control-label']) !!}
                        {!! Form::select('deposito',$deposito,null, ['class' => 'form-control', 'placeholder' => '','disabled'=>'true','id'=>'deposito','required']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('deposito'))
                            <p class="help-block">
                                {{ $errors->first('deposito') }}
                            </p>
                        @endif
               </div>
               <div class="col-xs-3 form-group">
                {!! Form::label('cor', trans('quickadmin.produto.fields.cor').'', ['class' => 'control-label']) !!}
                {!! Form::select('cor',$cor,null, ['class' => 'form-control','id'=>'cor', 'name'=>'cor','disabled'=>'true','required','placeholder'=>'']) !!}
                <p class="help-block"></p>
                @if($errors->has('cor'))
                    <p class="help-block">
                        {{ $errors->first('cor') }}
                    </p>
                @endif
            </div>
                 
             
              
            
         
                   
                         
                        
                                <div class="col-xs-3 form-group" >
                                    {!! Form::label('foto', trans('quickadmin.produto.fields.foto').'', ['class' => 'control-label','disabled'=>'true','id'=>'foto']) !!}
                                    {!! Form::file('foto', ['class' => 'form-control']) !!}
                                    {!! Form::hidden('foto_max_size', 2) !!}
                                    {!! Form::hidden('foto_max_width', 4096) !!}
                                    {!! Form::hidden('foto_max_height', 4096) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('foto'))
                                        <p class="help-block">
                                            {{ $errors->first('foto') }}
                                        </p>
                                    @endif
                                </div>
                
               
                    <div class="col-xs-6 form-group">
                        {!! Form::label('obs', trans('quickadmin.produto.fields.obs').'', ['class' => 'control-label']) !!}
                        {!! Form::text('obs', old('obs'), ['class' => 'form-control', 'placeholder' => '','disabled'=>'true','id'=>'obs']) !!}
                        <p class="help-block"></p>
                        @if($errors->has('obs'))
                            <p class="help-block">
                                {{ $errors->first('obs') }}
                            </p>
                        @endif
                    </div>
                
                
                 
                
                
                
                
                </div>
                 
            </div> 
                
               
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-lg">
        <i class="fa fa-refresh"> </i>
    ALTERAR</button>
        
  
   {{-- {!! Form::submit(trans('quickadmin.qa_save'), [ 'class' => 'btn btn-primary btn-lg']) !!} --}}
    {!! Form::close() !!}
   
    {{-- {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
            function moeda(z){

v = z.value;

v=v.replace(/\D/g,"") //permite digitar apenas números

v=v.replace(/[0-9]{12}/,"inválido") //limita pra máximo 999.999.999,99

v=v.replace(/(\d{1})(\d{8})$/,"$1.$2") //coloca ponto antes dos últimos 8 digitos

v=v.replace(/(\d{1})(\d{1,2})$/,"$1.$2") //coloca ponto antes dos últimos 5 digitos

//v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2") //coloca virgula antes dos últimos 2 digitos

z.value = v;

}
        $(function(){
            moment.updateLocale('{{ App::getLocale() }}', {
                week: { dow: 1 } // Monday is the first day of the week
            });
            
            $('.date').datetimepicker({
                format: "{{ config('app.date_format_moment') }}",
                locale: "{{ App::getLocale() }}",
            });
            
        });
             $(document).ready(function () {
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
    });
      
     });
     $(document).ready(function () {
            $("#success-alert2").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert2").slideUp(500);
    });
      
     });
    </script>
         <script type="text/javascript">
            $(document).ready(function(){
                  $('#finalidade').click(function(){
                    if (document.getElementById('finalidade').value == '') {
                        
                        document.getElementById("descricao").disabled = true;
                        document.getElementById("custo").disabled = true;
                        document.getElementById("unidade").disabled = true;
                        document.getElementById("data").disabled = true;
                        document.getElementById("marca").disabled = true;
                         document.getElementById("estoque").disabled = true;
                         document.getElementById("codforne").disabled = true;
                         document.getElementById("deposito").disabled = true;   
                        document.getElementById("cor").disabled = true;
                        document.getElementById("foto").disabled = true;
                        document.getElementById("obs").disabled = true;
                  
                      }
                      if (document.getElementById('finalidade').value == 'PRODUTO') {
                        
                        document.getElementById("descricao").disabled = false;
                        document.getElementById("obs").disabled = false;
                        document.getElementById("custo").disabled = false;
                        document.getElementById("unidade").disabled = false;
                        document.getElementById("unidade").value = 'UN';
                        // document.getElementById("unidade").disabled = true;
                        document.getElementById("data").disabled = false;
                        document.getElementById("marca").disabled = true;
                        document.getElementById("cor").disabled = true;
                        document.getElementById("codforne").disabled = true;
                        document.getElementById("estoque").disabled = true;
                        document.getElementById("deposito").disabled = true;
                        document.getElementById("custo").disabled = false;
                
            
                             document.getElementById("marca").required = false;
                        document.getElementById("cor").required = false;
                        document.getElementById("codforne").required = false;
                        document.getElementById("estoque").required = false;
                        document.getElementById("deposito").required = false;
                        document.getElementById("unidade").required = false;
                   
                      }
                      if (document.getElementById('finalidade').value == 'ITEM') {
                        document.getElementById("descricao").disabled = false;
                        document.getElementById("obs").disabled = false;
                        document.getElementById("custo").disabled = false;
                        document.getElementById("unidade").disabled = false;
                        document.getElementById("unidade").value = 'KG';
                        document.getElementById("custo").required = true;
                        document.getElementById("estoque").required = true;
                        document.getElementById("codforne").required = true;
                        document.getElementById("data").disabled = false;
                     
                        document.getElementById("marca").disabled = false;
                        document.getElementById("cor").disabled = false;
                        document.getElementById("codforne").disabled = false;
                        document.getElementById("estoque").disabled = false;
                        document.getElementById("deposito").disabled = false;
                    
                   
                      }
                    //   if (document.getElementById('finalidade').value == 'KIT') {
                    //     document.getElementById("descricao").disabled = false;
                    //     document.getElementById("codforne").required = true;
                    //     document.getElementById("marca").disabled = true;
                    //     document.getElementById("cor").disabled = true;
                    //     document.getElementById("codforne").disabled = true;
                    //     document.getElementById("estoque").disabled = true;
                    //     document.getElementById("deposito").disabled = true;
                    //     document.getElementById("custo").disabled = true;
                    //     document.getElementById("unidade").value = 'UN';
                    //     document.getElementById("unidade").disabled = true;
                   
                    //     document.getElementById("codforne").required = false;
                    //     document.getElementById("estoque").required = false;
                    //     document.getElementById("deposito").required = false;
                    //     document.getElementById("custo").required = false;
                    //     document.getElementById("codforne").required = false;
                    //     document.getElementById("unidade").required = false;
                       
                    //   }
                  })
              });
              </script>   
@stop