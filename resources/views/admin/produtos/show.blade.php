@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.produto.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
                <h4 id="TNome">VIZUALIZAR - PRODUTO</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.produto.fields.codigobarra')</th>
                            <td field-key='codigobarra'>{{ $produto->codigobarra }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.descricao')</th>
                            <td field-key='descricao'>{{ $produto->descricao }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.cor')</th>
                            <td field-key='cor'>{{ $produto->cor }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.tamanho')</th>
                            <td field-key='tamanho'>{{ $produto->tamanho }}</td>
                        </tr>
                        {{-- <tr>
                            <th>@lang('quickadmin.produto.fields.grupo')</th>
                            <td field-key='grupo'>{{ $produto->grupo }}</td>
                        </tr> --}}
                        {{-- <tr>
                            <th>@lang('quickadmin.produto.fields.marca')</th>
                            <td field-key='marca'>{{ $produto->marca }}</td>
                        </tr> --}}
                        <tr>
                            <th>@lang('quickadmin.produto.fields.finalidade')</th>
                            <td field-key='finalidade'>{{ $produto->finalidade }}</td>
                        </tr>
                      
                     
                   
                        {{-- <tr>
                            <th>@lang('quickadmin.produto.fields.regraimposto')</th>
                            <td field-key='regraimposto'>{{ $produto->regraimposto }}</td>
                        </tr> --}}
                        {{-- <tr>
                            <th>@lang('quickadmin.produto.fields.tiporegra')</th>
                            <td field-key='tiporegra'>{{ $produto->tiporegra }}</td>
                        </tr> --}}
                        <tr>
                            <th>@lang('quickadmin.produto.fields.estoque')</th>
                            <td field-key='estoque'>{{ $produto->estoque }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.estoquemin')</th>
                            <td field-key='estoquemin'>{{ $produto->estoquemin }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.referencia')</th>
                            <td field-key='referencia'>{{ $produto->referencia }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.custo')</th>
                            <td field-key='custo'>{{ $produto->custo }}</td>
                        </tr>
                        {{-- <tr>
                            <th>@lang('quickadmin.produto.fields.preco')</th>
                            <td field-key='preco'>{{ $produto->preco }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.margem')</th>
                            <td field-key='margem'>{{ $produto->margem }}</td>
                        </tr> --}}
                        <tr>
                            <th>@lang('quickadmin.produto.fields.status')</th>
                            <td field-key='obs'>{{ $produto->status }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.deposito')</th>
                            <td field-key='obs'>{{ $produto->deposito }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.unidade')</th>
                            <td field-key='obs'>{{ $produto->unidade }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.datacadastro')</th>
                            <td field-key='datacadastro'>{{ $produto->datacadastro }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.codforne')</th>
                            <td field-key='datacadastro'>{{ $produto->codforne }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.obs')</th>
                            <td field-key='obs'>{{ $produto->obs }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.foto')</th>
                            <td field-key='foto'>@if($produto->foto)<a href="{{ asset(env('UPLOAD_PATH').'/' . $produto->foto) }}" target="_blank"><img src="{{ asset(env('UPLOAD_PATH').'/thumb/' . $produto->foto) }}"/></a>@endif</td>
                        </tr>
                        {{-- <tr>
                            <th>@lang('quickadmin.produto.fields.ncm')</th>
                            <td field-key='ncm'>{{ $produto->ncm }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.produto.fields.kit')</th>
                            <td field-key='kit'>{{ $produto->kit }}</td>
                        </tr> --}}
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.produtos.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
