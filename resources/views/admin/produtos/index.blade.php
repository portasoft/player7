@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/menuss.css') }}">
    {{-- <h3 class="page-title">@lang('quickadmin.produto.title')</h3> --}}
    @can('produto_create')
    <p>
        <a href="{{ route('admin.produtos.create') }}"  type="submit" class="btn btn-success btn-lg">
            <i class="fa fa-save"> </i>
        NOVO</a>
        
    </p>
   
    @endcan

    @can('produto_delete')
    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.produtos.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.produtos.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan


    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">RELATÓRIO - PRODUTOS</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>ALTERADO COM SUCESSO!</h4>
         </div> 
        @endif
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('produto_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('produto_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        {{-- <th>@lang('quickadmin.produto.fields.codigobarra')</th> --}}
                        <th>@lang('quickadmin.produto.fields.descricao')</th>
                        <th>COMPOSIÇÃO</th>
                        <th>FINALIDADE</th>
                        <th>@lang('DEPOSITO')</th>
                        <th>@lang('FORNECEDOR')</th>
                        {{-- <th>@lang('quickadmin.produto.fields.grupo')</th> --}}
                        {{-- <th>@lang('quickadmin.produto.fields.marca')</th> --}}
                 
            
                        <th>@lang('quickadmin.produto.fields.cor')</th>
                        <th>@lang('quickadmin.produto.fields.tamanho')</th>
          
         
                        <th>@lang('quickadmin.produto.fields.estoque')</th>
                        {{-- <th>@lang('quickadmin.produto.fields.estoquemin')</th> --}}
                        <th>@lang('quickadmin.produto.fields.custo')</th>
                        {{-- <th>@lang('quickadmin.produto.fields.preco')</th> --}}
                        {{-- <th>@lang('quickadmin.produto.fields.margem')</th> --}}
           
                        {{-- <th>@lang('quickadmin.produto.fields.obs')</th> --}}
                                        <th>@lang('quickadmin.produto.fields.foto')</th>
                        {{-- <th>@lang('quickadmin.produto.fields.ncm')</th> --}}
          
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
    <script>
        @can('produto_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.produtos.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.produtos.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('produto_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                // @endcan{data: 'codigobarra', name: 'codigobarra'},
                {data: 'descricao', name: 'descricao'},
                {data: 'marca', name: 'marca'},
                {data: 'finalidade', name: 'finalidade'},
                {data: 'deposito', name: 'deposito'},
                {data: 'codforne', name: 'codforne'},
                {data: 'cor', name: 'cor'},
                {data: 'tamanho', name: 'tamanho'},
                {data: 'estoque', name: 'estoque'},
                {data: 'custo', name: 'custo'},
                // {data: 'preco', name: 'preco'},
                // {data: 'margem', name: 'margem'},
      
                // {data: 'obs', name: 'obs'},
           
                {data: 'foto', name: 'foto'},
                // {data: 'ncm', name: 'ncm'},
        
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
        });
    </script>
@endsection