@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.roles.title')</h3> --}}
    {!! Form::open(['method' => 'POST', 'route' => ['admin.roles.store']]) !!}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
     <h4 id="TNome">CADASTRO - FUNÇÃO</h4>
        </div>
        @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" id="success-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <h4><i class="icon fa fa-check"></i>CADASTRADO COM SUCESSO!</h4>
         </div> 
        @endif
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 form-group">
                    {!! Form::label('title', trans('quickadmin.roles.fields.title').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>
 
    <button type="submit" class="btn btn-primary btn-lg">
        <i class="fa fa-save"> </i>
    SALVAR</button>
{{--         
    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!} --}}
    {!! Form::close() !!}
@stop
@section('javascript')
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>
@endsection
