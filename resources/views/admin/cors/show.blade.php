@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.cor.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">VIZUALIZAR - COR</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.cor.fields.descricao')</th>
                            <td field-key='descricao'>{{ $cor->descricao }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.cors.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
