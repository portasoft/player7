@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
{{-- <h3 class="page-title">@lang('quickadmin.kititen.title')</h3> --}}
{!! Form::open(['method' => 'POST', 'route' => ['admin.kititens.store'], 'files' => true,]) !!}

<div class="panel panel-primary">
    <div id="HNome" class="panel-heading">
      <h4 id="TNome">CADASTRO - KIT</h4>
    </div>
    @if (session()->has('success'))
    <div class="alert alert-success alert-dismissible" id="success-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
        <h4><i class="icon fa fa-check"></i>CADASTRADO COM SUCESSO!</h4>
     </div> 
@endif
@if (session()->has('alert'))
<div class="alert alert-warning alert-dismissible" id="alert-alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
<h4><i class="icon fa fa-check"></i>OPERAÇÃO NÃO REALIZADA, VERIFIQUE O ESTOQUE!</h4>
 </div> 
@endif
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-4 form-group">
                <label for="">PRODUTO</label>
 
                {!! Form::text('produto', old('produto'), ['class' => 'form-control','autofocus', 'placeholder' => '','autocomplete'=>'off', 'required' => '','id'=>'produtokit']) !!}
                <div id="produtokitlist"></div>
            </div>
            <div class="col-xs-4 form-group">
                <label for="">KIT</label>
                @if (isset($ff))
                
                {{-- {!! Form::text('kit', old('kit'), ['class' => 'form-control', 'placeholder' => '','autocomplete'=>'off', 'required' => '','id'=>'kit','']) !!} --}}
            <input type="text" id="kit" name="kit" value="{{$ff}}" placeholder="" autocomplete="off" class="form-control" required>
                <div id="kitlist"></div>
                @else
                  {!! Form::text('kit', old('kit'), ['class' => 'form-control', 'placeholder' => '','autocomplete'=>'off', 'required' => '','id'=>'kit']) !!}
                <div id="kitlist"></div>
                @endif
           
                         
            </div>
            <div class="col-xs-2 form-group">
              <label for="">QTDE</label>
              <input type="text" class="form-control" id="qtde" name="qtde" required onKeyUp="moeda(this)">
              {{-- {!! Form::text('qtde', old('qtde'), ['class' => 'form-control','id'=>'qtde', 'required' ]) !!} --}}
                          
            </div>
            <div class="col-xs-2 form-group">
                <label for="">COR</label>
            
                 {!! Form::select('cor',['1'=>'1','2'=>'2'],NULL, ['class' => 'form-control','required','id'=>'cor']) !!}
                 
                </div>
            {{-- <div class="col-xs-2 form-group">
                {!! Form::label('valor', trans('quickadmin.kititen.fields.valor').'', ['class' => 'control-label']) !!}
                {!! Form::text('valor', old('valor'), ['class' => 'form-control', 'placeholder' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('valor'))
                    <p class="help-block">
                        {{ $errors->first('valor') }}
                    </p>
                @endif
            </div> --}}
   
            {{-- <div class="col-xs-2 form-group">
         <label for="">CUSTO</label>
                {!! Form::text('base', old('base'), ['class' => 'form-control', 'placeholder' => '','id'=>'base']) !!}
                <p class="help-block"></p>
                @if($errors->has('base'))
                    <p class="help-block">
                        {{ $errors->first('base') }}
                    </p>
                @endif
            </div> --}}
   
       
   
            {{-- <div class="col-xs-12 form-group">
                {!! Form::label('anexo', trans('quickadmin.kititen.fields.anexo').'', ['class' => 'control-label']) !!}
                {!! Form::hidden('anexo', old('anexo')) !!}
                {!! Form::file('anexo', ['class' => 'form-control']) !!}
                {!! Form::hidden('anexo_max_size', 2) !!}
                <p class="help-block"></p>
                @if($errors->has('anexo'))
                    <p class="help-block">
                        {{ $errors->first('anexo') }}
                    </p>
                @endif
            </div> --}}
        </div>
        
    </div>
</div>



<p>
        <button type="submit" class="btn btn-primary btn-lg">
                <i class="fa fa-save"> </i>
            SALVAR</button>
        {{-- {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger','id'=>'salvar']) !!} --}}
    {{-- <a id="novo" value="novo" href="{{ route('admin.kititens.index') }}"   type="submit" class="btn btn-success btn-lg">
        <i class="fa fa-plus-square"> </i>
    NOVO</a> --}}
    <a href="{{ route('admin.kititens.index') }}" id="novo"  type="submit" class="btn btn-success btn-lg">
        <i class="fa fa-plus-square"> </i>
    NOVO</a>
    
    
</p>
{{-- <input type="button" class="btn btn-primary" id="novo" value="novo"> --}}
{!! Form::close() !!}
    {{-- <h3 class="page-title">@lang('quickadmin.kititen.title')</h3> --}}
    {{-- @can('kititen_create')
    <p>
        <a href="{{ route('admin.kititens.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
        
    </p>
    @endcan --}}

    @can('kititen_delete')

    <p>
        <ul class="list-inline">
            <li><a href="{{ route('admin.kititens.index') }}" style="{{ request('show_deleted') == 1 ? '' : 'font-weight: 700' }}">@lang('quickadmin.qa_all')</a></li> |
            <li><a href="{{ route('admin.kititens.index') }}?show_deleted=1" style="{{ request('show_deleted') == 1 ? 'font-weight: 700' : '' }}">@lang('quickadmin.qa_trash')</a></li>
        </ul>
    </p>
    @endcan

    @if (session()->has('success'))
    <div class="alert alert-success alert-dismissible" id="success-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
        <h4><i class="icon fa fa-check"></i>CADASTRADO COM SUCESSO!</h4>
     </div> 
@endif
    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
<h4 id="TNome">RELAÇÃO - KIT</h4>
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped ajaxTable @can('kititen_delete') @if ( request('show_deleted') != 1 ) dt-select @endif @endcan">
                <thead>
                    <tr>
                        @can('kititen_delete')
                            @if ( request('show_deleted') != 1 )<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>@endif
                        @endcan

                        <th>@lang('quickadmin.kititen.fields.produto')</th>
                        <th>KIT</th>
                        <th>@lang('quickadmin.kititen.fields.qtde')</th>
                        <th>COR</th>
                        {{-- <th>@lang('quickadmin.kititen.fields.tamanho')</th> --}}
                        {{-- <th>@lang('quickadmin.kititen.fields.valor')</th> --}}
                        {{-- <th>@lang('quickadmin.kititen.fields.base')</th> --}}
                    
                        {{-- <th>@lang('quickadmin.kititen.fields.anexo')</th> --}}
                        @if( request('show_deleted') == 1 )
                        <th>&nbsp;</th>
                        @else
                        <th>&nbsp;</th>
                        @endif
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript') 
<script>
        $(document).ready(function () {
            $("#alert-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#alert-alert").slideUp(500);
    });
      
     });
     function moeda(z){

v = z.value;

v=v.replace(/\D/g,"") //permite digitar apenas números

v=v.replace(/[0-9]{12}/,"inválido") //limita pra máximo 999.999.999,99

v=v.replace(/(\d{1})(\d{8})$/,"$1.$2") //coloca ponto antes dos últimos 8 digitos

v=v.replace(/(\d{1})(\d{1,3})$/,"$1.$2") //coloca ponto antes dos últimos 5 digitos

//v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2") //coloca virgula antes dos últimos 2 digitos

z.value = v;

}
    </script>
<script type="text/javascript" src="{{ URL::asset('js/messages.js') }}"></script>

    <script>
        @can('kititen_delete')
            @if ( request('show_deleted') != 1 ) window.route_mass_crud_entries_destroy = '{{ route('admin.kititens.mass_destroy') }}'; @endif
        @endcan
        $(document).ready(function () {
            window.dtDefaultOptions.ajax = '{!! route('admin.kititens.index') !!}?show_deleted={{ request('show_deleted') }}';
            window.dtDefaultOptions.columns = [@can('kititen_delete')
                @if ( request('show_deleted') != 1 )
                    {data: 'massDelete', name: 'id', searchable: false, sortable: false},
                @endif
                @endcan
                {data: 'kit',name: 'kit'},
                {data: 'item', name: 'item'},
                {data: 'qtde', name: 'qtde'},
                {data: 'OrdemCor', name: 'OrdemCor'},
                // {data: 'tamanho', name: 'tamanho'},
                // {data: 'valor', name: 'valor'},
                // {data: 'base', name: 'base'},
      
                // {data: 'anexo', name: 'anexo'},
                
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ];
            processAjaxTables();
 
            $(document).ready(function(){
             $('#produtokit').keyup(function(){
                 var query = $(this).val();
                 if(query.text != '' )
                 {
                   var _token = $('input[name="_token"]').val();
                      $.ajax({
                         url:"{{ route('kititens.fetch')}}",
                   
                         method:"POST",
                         data:{query:query,_token:_token},
                         success:function(data)
                         {
                             $('#produtokitlist').fadeIn();
                             $('#produtokitlist').html(data);
                         }
                     })
                 }
               
             });
        
             $(document).on('click','li.c',function(){
                                 
                                 $('#produtokit').val($(this).text());
                                 $('#produtokitlist').fadeOut()
                            
                         
                              })
                     })
                     $(document).ready(function(){
             $('#kit').keyup(function(){
                 var query = $(this).val();
                 if(query.text != '' )
                 {
                   var _token = $('input[name="_token"]').val();
                      $.ajax({
                         url:"{{ route('kititens.fetchkit')}}",
                   
                         method:"POST",
                         data:{query:query,_token:_token},
                         success:function(data)
                         {
                             $('#kitlist').fadeIn();
                             $('#kitlist').html(data);
                         }
                     })
                 }
               
             });
        
             $(document).on('click','li.ckit',function(){
                                 
                                 $('#kit').val($(this).text());
                                 $('#kitlist').fadeOut()
                            
                         
                              })
                     })
  
  
          
        });
     



     
    </script>
   
//     <script type="text/javascript">
//   jQuery(document).ready(function(){
//     jQuery('#salvar').click('keyup',function(){
//       if(jQuery(this).attr('name') === 'result'){
//       return false;
//       }
//       document.getElementById('kit').value = "";
//       document.getElementById('qtde').value = "";
//       document.getElementById('tamanho').value = "";
//       document.getElementById('base').value = "";
    

//     });
  
//   });
//   </script>

@endsection