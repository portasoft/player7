@extends('layouts.app')

@section('content')
    {{-- <h3 class="page-title">@lang('quickadmin.kititen.title')</h3> --}}

    <div class="panel panel-primary">
        <div id="HNome" class="panel-heading">
           <h4 id="TNome">VIZUALIZAR - COMPOSIÇÃO</h4>
        </div>

        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.kititen.fields.produto')</th>
                            <td field-key='produto'>{{ $kititen->produto }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.kititen.fields.kit')</th>
                            <td field-key='kit'>{{ $kititen->kit }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.kititen.fields.qtde')</th>
                            <td field-key='qtde'>{{ $kititen->qtde }}</td>
                        </tr>
                        {{-- <tr>
                            <th>@lang('quickadmin.kititen.fields.valor')</th>
                            <td field-key='valor'>{{ $kititen->valor }}</td>
                        </tr> --}}
                        <tr>
                            <th>COR</th>
                            <td field-key='base'>{{ $kititen->OrdemCor }}</td>
                        </tr>
                     
                        {{-- <tr>
                            <th>@lang('quickadmin.kititen.fields.anexo')</th>
                            <td field-key='anexo'>@if($kititen->anexo)<a href="{{ asset(env('UPLOAD_PATH').'/' . $kititen->anexo) }}" target="_blank">Download file</a>@endif</td>
                        </tr> --}}
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.kititens.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
