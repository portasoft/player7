@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.kititen.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.kititens.store'], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3 form-group">
                    {!! Form::label('produto', trans('quickadmin.kititen.fields.produto').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('produto', old('produto'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('produto'))
                        <p class="help-block">
                            {{ $errors->first('produto') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-2 form-group">
                    <label for="">Produto Insumo</label>
                    {!! Form::text('kit', old('kit'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('kit'))
                        <p class="help-block">
                            {{ $errors->first('kit') }}
                        </p>
                    @endif
                </div>
                <div class="col-xs-1 form-group">
                    {!! Form::label('qtde', trans('quickadmin.kititen.fields.qtde').'', ['class' => 'control-label']) !!}
                    {!! Form::number('qtde', old('qtde'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                 
                </div>
       
                {{-- <div class="col-xs-2 form-group">
                    {!! Form::label('valor', trans('quickadmin.kititen.fields.valor').'', ['class' => 'control-label']) !!}
                    {!! Form::text('valor', old('valor'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('valor'))
                        <p class="help-block">
                            {{ $errors->first('valor') }}
                        </p>
                    @endif
                </div> --}}
       
                <div class="col-xs-2 form-group">
             <label for="">Custo</label>
                    {!! Form::text('base', old('base'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('base'))
                        <p class="help-block">
                            {{ $errors->first('base') }}
                        </p>
                    @endif
                </div>
       
           
       
                {{-- <div class="col-xs-12 form-group">
                    {!! Form::label('anexo', trans('quickadmin.kititen.fields.anexo').'', ['class' => 'control-label']) !!}
                    {!! Form::hidden('anexo', old('anexo')) !!}
                    {!! Form::file('anexo', ['class' => 'form-control']) !!}
                    {!! Form::hidden('anexo_max_size', 2) !!}
                    <p class="help-block"></p>
                    @if($errors->has('anexo'))
                        <p class="help-block">
                            {{ $errors->first('anexo') }}
                        </p>
                    @endif
                </div> --}}
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

