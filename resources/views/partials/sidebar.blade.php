@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

             

            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">DASHBOARD</span>
                </a>
            </li>
            @can('agendatelefonica_access')
            <li>
                <a href="{{ route('admin.agendatelefonicas.index') }}">
                    <i class="fa fa-phone"></i>
                    <span>@lang('quickadmin.agendatelefonica.title')</span>
                </a>
            </li>@endcan
            @can('agendahorario_access')
            <li>
                <a href="{{ route('admin.agendahorarios.index') }}">
                    <i class="fa fa-calendar-times-o"></i>
                    <span>@lang('quickadmin.agendahorario.title')</span>
                </a>
            </li>@endcan
{{-- CAIXA --}}

{{-- @can('cliente_access')
<li class="treeview">
    <a href="#">
        <i class="fa fa-address-card"></i>
        <span>@lang('quickadmin.caixa.title')</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('vendaespera_access')
        <li>
            <a href="{{ route('admin.vendaesperas.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.vendaespera.title')</span>
            </a>
        </li>@endcan
        @can('vendaesperaiten_access')
        <li>
            <a href="{{ route('admin.vendaesperaitens.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.vendaesperaitens.title')</span>
            </a>
        </li>@endcan
        @can('vendatemp_access')
        <li>
            <a href="{{ route('admin.vendatemps.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.vendatemp.title')</span>
            </a>
        </li>@endcan
        
        @can('venda_access')
        <li>
            <a href="{{ route('admin.vendas.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.vendas.title')</span>
            </a>
        </li>@endcan
        
        @can('vendaitem_access')
        <li>
            <a href="{{ route('admin.vendaitems.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.vendaitem.title')</span>
            </a>
        </li>@endcan
        
        @can('devolucao_access')
        <li>
            <a href="{{ route('admin.devolucaos.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.devolucao.title')</span>
            </a>
        </li>@endcan
        @can('orcamento_access')
        <li>
            <a href="{{ route('admin.orcamentos.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.orcamento.title')</span>
            </a>
        </li>@endcan
        
        @can('recebimento_access')
        <li>
            <a href="{{ route('admin.recebimentos.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.recebimento.title')</span>
            </a>
        </li>@endcan
        
        @can('troca_access')
        <li>
            <a href="{{ route('admin.trocas.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.troca.title')</span>
            </a>
        </li>@endcan
        
        @can('movimentacao_access')
        <li>
            <a href="{{ route('admin.movimentacaos.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.movimentacao.title')</span>
            </a>
        </li>@endcan
        
        @can('entrega_access')
        <li>
            <a href="{{ route('admin.entregas.index') }}">
                <i class="fa fa-gears"></i>
                <span>@lang('quickadmin.entrega.title')</span>
            </a>
        </li>@endcan

        
     
        
    </ul>
</li>@endcan --}}


{{-- FIMCAIXA --}}
            @can('user_management_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>@lang('quickadmin.user-management.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('user_access')
                    <li>
                        <a href="{{ route('admin.users.index') }}">
                            <i class="fa fa-user"></i>
                            <span>@lang('quickadmin.users.title')</span>
                        </a>
                    </li>@endcan
                    <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                        <a href="{{ route('auth.change_password') }}">
                            <i class="fa fa-key"></i>
                            <span class="title">@lang('quickadmin.qa_change_password')</span>
                        </a>
                    {{-- @can('dependenterh_access')
                    <li>
                        <a href="{{ route('admin.dependenterhs.index') }}">
                            <i class="fa fa-user-circle"></i>
                            <span>@lang('quickadmin.dependenterh.title')</span>
                        </a>
                    </li>@endcan --}}
                    
                    @can('role_access')
                    <li>
                        <a href="{{ route('admin.roles.index') }}">
                            <i class="fa fa-briefcase"></i>
                            <span>@lang('quickadmin.roles.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('user_action_access')
                    <li>
                        <a href="{{ route('admin.user_actions.index') }}">
                            <i class="fa fa-th-list"></i>
                            <span>@lang('quickadmin.user-actions.title')</span>
                        </a>
                    </li>@endcan
                    
                    {{-- @can('alteracaosalario_access')
                    <li>
                        <a href="{{ route('admin.alteracaosalarios.index') }}">
                            <i class="fa fa-money"></i>
                            <span>@lang('quickadmin.alteracaosalario.title')</span>
                        </a>
                    </li>@endcan --}}
                    
                    {{-- @can('motoristum_access')
                    <li>
                        <a href="{{ route('admin.motoristas.index') }}">
                            <i class="fa fa-motorcycle"></i>
                            <span>@lang('quickadmin.motorista.title')</span>
                        </a>
                    </li>@endcan --}}
                    
                    {{-- @can('adiantamento_access')
                    <li>
                        <a href="{{ route('admin.adiantamentos.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.adiantamento.title')</span>
                        </a>
                    </li>@endcan --}}
                    
                    {{-- @can('feria_access')
                    <li>
                        <a href="{{ route('admin.ferias.index') }}">
                            <i class="fa fa-train"></i>
                            <span>@lang('quickadmin.ferias.title')</span>
                        </a>
                    </li>@endcan --}}
                    
            {{-- @can('afastamento_access')
            <li>
                <a href="{{ route('admin.afastamentos.index') }}">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.afastamento.title')</span>
                </a>
            </li>@endcan --}}
            
            {{-- @can('horariorh_access')
            <li>
                <a href="{{ route('admin.horariorhs.index') }}">
                    <i class="fa fa-calendar-times-o"></i>
                    <span>@lang('quickadmin.horariorh.title')</span>
                </a>
            </li>@endcan     --}}
            
            {{-- @can('profissional_access')
            <li>
                <a href="{{ route('admin.profissionals.index') }}">
                    <i class="fa fa-user-md"></i>
                    <span>@lang('quickadmin.profissional.title')</span>
                </a>
            </li>@endcan --}}

            {{-- @can('agendatelegone_access')
            <li>
                <a href="{{ route('admin.agendatelegones.index') }}">
                    <i class="fa fa-phone"></i>
                    <span>@lang('quickadmin.agendatelegone.title')</span>
                </a>
            </li>@endcan --}}
                    
                </ul>
            </li>@endcan
            

            {{-- clientes --}}
            {{-- @can('cliente_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-address-card"></i>
                    <span>@lang('quickadmin.clientes.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('cliente_access')
                    <li>
                        <a href="{{ route('admin.clientes.index') }}">
                            <i class="fa fa-user-plus"></i>
                            <span>@lang('quickadmin.cliente.title')</span>
                        </a>
                    </li>@endcan
                    
              
                  
                    
                </ul>
            </li>@endcan --}}
            {{-- fimclientes --}}

            {{-- produto --}}
            @can('produto_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-basket"></i>
                    <span>@lang('quickadmin.produtos.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('produto_access')
                    <li>
                        <a href="{{ route('admin.produtos.index') }}">
                            <i class="fa fa-shopping-basket"></i>
                            <span>Produto/Item</span>
                        </a>
                    </li>@endcan
                    @can('kititen_access')
                    <li>
                        <a href="{{ route('admin.kititens.index') }}">
                            <i class="fa fa-flask"></i>
                            <span>KIT</span>
                        </a>
                    </li>@endcan
                    {{-- @can('itenskit_access')
                    <li>
                        <a href="{{ route('admin.itenskits.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.itenskit.title')</span>
                        </a>
                    </li>@endcan
                     --}}
                 
                    {{-- @can('entradanotum_access')

                    <li>
                        <a href="{{ route('admin.entradanotas.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.entradanota.title')</span>
                        </a>
                    </li>@endcan --}}
                    @can('movimento_access')
                    <li>
                        <a href="{{ route('admin.movimentos.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.movimento.title')</span>
                        </a>
                    </li>@endcan
                    
        
                    
                    @can('unidade_access')
                    <li>
                        <a href="{{ route('admin.unidades.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.unidade.title')</span>
                        </a>
                    </li>@endcan
                    @can('deposito_access')
                    <li>
                        <a href="{{ route('admin.depositos.index') }}">
                            <i class="fa fa-codepen"></i>
                            <span>@lang('quickadmin.deposito.title')</span>
                        </a>
                    </li>@endcan
                    
                  
                    
                    {{-- @can('grupo_access')
                    <li>
                        <a href="{{ route('admin.grupos.index') }}">
                            <i class="fa fa-object-ungroup"></i>
                            <span>@lang('quickadmin.grupo.title')</span>
                        </a>
                    </li>@endcan --}}
                    {{-- @can('referencium_access')
            <li>
                <a href="{{ route('admin.referencias.index') }}">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.referencia.title')</span>
                </a>
            </li>@endcan --}}
            
            @can('cor_access')
            <li>
                <a href="{{ route('admin.cors.index') }}">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.cor.title')</span>
                </a>
            </li>@endcan
            
            {{-- @can('grade_access')
            <li>
                <a href="{{ route('admin.grades.index') }}">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.grade.title')</span>
                </a>
            </li>@endcan --}}
            
            {{-- @can('marca_access')
            <li>
                <a href="{{ route('admin.marcas.index') }}">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.marca.title')</span>
                </a>
            </li>@endcan --}}
            
            @can('tamanho_access')
            <li>
                <a href="{{ route('admin.tamanhos.index') }}">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.tamanho.title')</span>
                </a>
            </li>@endcan
            {{-- @can('listapreco_access')
            <li>
                <a href="{{ route('admin.listaprecos.index') }}">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.listapreco.title')</span>
                </a>
            </li>@endcan --}}
            
            {{-- @can('pedido_access')
            <li>
                <a href="{{ route('admin.pedidos.index') }}">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.pedido.title')</span>
                </a>
            </li>@endcan --}}
            
          
{{--             
            @can('promocao_access')
            <li>
                <a href="{{ route('admin.promocaos.index') }}">
                    <i class="fa fa-gears"></i>
                    <span>@lang('quickadmin.promocao.title')</span>
                </a>
            </li>@endcan
             --}}
                    
                </ul>
            </li>@endcan
            @can('producao_access')
            <li>
                <a href="{{ route('admin.producaos.index') }}">
                    <i class="fa fa-shopping-bag"></i>
                    <span>@lang('quickadmin.producao.title')</span>
                </a>
            </li>@endcan
            @can('producao_access')
            <li>
                <a href="{{ route('admin.producaos.listproducao') }}">
                    <i class="fa fa-list"></i>
                    <span>LISTA PRODUÇÃO</span>
                </a>
            </li>@endcan
            @can('fornecedor_access')
            <li>
                <a href="{{ route('admin.fornecedors.index') }}">
                    <i class="fa fa-truck"></i>
                    <span>@lang('quickadmin.fornecedor.title')</span>
                </a>
            </li>@endcan
       
            {{-- fimproduto --}}
            
            {{-- pet --}}

            {{-- @can('petshop_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-paw"></i>
                    <span>@lang('quickadmin.petshop.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('animal_access')
                    <li>
                        <a href="{{ route('admin.animals.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.animal.title')</span>
                        </a>
                    </li>@endcan
                    @can('atendimento_access')
                    <li>
                        <a href="{{ route('admin.atendimentos.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.atendimento.title')</span>
                        </a>
                    </li>@endcan
                    <li>
                            <a href="{{url('admin/calendar')}}">
                              <i class="fa fa-calendar"></i>
                              <span class="title">
                                Agenda
                              </span>
                            </a>
                        </li>
                    @can('carteirinha_access')
                    <li>
                        <a href="{{ route('admin.carteirinhas.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.carteirinha.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('animalvacina_access')
                    <li>
                        <a href="{{ route('admin.animalvacinas.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.animalvacina.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('vermifugo_access')
                    <li>
                        <a href="{{ route('admin.vermifugos.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.vermifugo.title')</span>
                        </a>
                    </li>@endcan
                    @can('gruposervico_access')
                    <li>
                        <a href="{{ route('admin.gruposervicos.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.gruposervico.title')</span>
                        </a>
                    </li>@endcan
                    @can('especie_access')
                    <li>
                        <a href="{{ route('admin.especies.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.especie.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('pelagem_access')
                    <li>
                        <a href="{{ route('admin.pelagems.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.pelagem.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('tipopelagem_access')
                    <li>
                        <a href="{{ route('admin.tipopelagems.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.tipopelagem.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('temperamento_access')
                    <li>
                        <a href="{{ route('admin.temperamentos.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.temperamento.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('tipoprofissional_access')
                    <li>
                        <a href="{{ route('admin.tipoprofissionals.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.tipoprofissional.title')</span>
                        </a>
                    </li>@endcan
                    @can('fotoexame_access')
                    <li>
                        <a href="{{ route('admin.fotoexames.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.fotoexames.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('servico_access')
                    <li>
                        <a href="{{ route('admin.servicos.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.servico.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('animalveterinario_access')
                    <li>
                        <a href="{{ route('admin.animalveterinarios.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.animalveterinario.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('raca_access')
                    <li>
                        <a href="{{ route('admin.racas.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.raca.title')</span>
                        </a>
                    </li>@endcan --}}
                    
                    {{-- @can('animalservico_access')
                    <li>
                        <a href="{{ route('admin.animalservicos.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.animalservico.title')</span>
                        </a>
                    </li>@endcan --}}
                  
{{--           
                </ul>
            </li>@endcan --}}
          

            {{-- fimpet --}}

            {{-- fiscal --}}
            {{-- @can('fiscal_access')
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart"></i>
                    <span>@lang('quickadmin.fiscal.title')</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('tiporegra_access')
                    <li>
                        <a href="{{ route('admin.tiporegras.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.tiporegra.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('taxatributarium_access')
                    <li>
                        <a href="{{ route('admin.taxatributarias.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.taxatributaria.title')</span>
                        </a>
                    </li>@endcan
                    
                    @can('regraimposto_access')
                    <li>
                        <a href="{{ route('admin.regraimpostos.index') }}">
                            <i class="fa fa-gears"></i>
                            <span>@lang('quickadmin.regraimposto.title')</span>
                        </a>
                    </li>@endcan
                    
                </ul>
            </li>@endcan
            
            @can('sat_access')
            <li>
                <a href="{{ route('admin.sats.index') }}">
                    <i class="fa fa-shopping-cart"></i>
                    <span>@lang('quickadmin.sat.title')</span>
                </a>
            </li>@endcan --}}
 
                {{-- fimfiscal --}}


                {{-- administracao --}}
                @can('administracao_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gears"></i>
                        <span>@lang('quickadmin.administracao.title')</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                            @can('empresa_access')
            <li>
                <a href="{{ route('admin.empresas.index') }}">
                    <i class="fa fa-building"></i>
                    <span>@lang('quickadmin.empresa.title')</span>
                </a>
            </li>@endcan
                        {{-- @can('mesa_access')
                        <li>
                            <a href="{{ route('admin.mesas.index') }}">
                                <i class="fa fa-gears"></i>
                                <span>@lang('quickadmin.mesa.title')</span>
                            </a>
                        </li>@endcan --}}
                        
                    
{{--                         
                        @can('kit_access')
                        <li>
                            <a href="{{ route('admin.kits.index') }}">
                                <i class="fa fa-gears"></i>
                                <span>@lang('quickadmin.kit.title')</span>
                            </a>
                        </li>@endcan
                      --}}
                        
                        {{-- @can('formapagamento_access')
                        <li>
                            <a href="{{ route('admin.formapagamentos.index') }}">
                                <i class="fa fa-gears"></i>
                                <span>@lang('quickadmin.formapagamento.title')</span>
                            </a>
                        </li>@endcan
                         --}}
                  
                        
                    
                        
                    </ul>
                </li>@endcan

                {{-- fimadministracao --}}

                {{-- financeiro --}}
                {{-- @can('financeiro_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-line-chart"></i>
                        <span>@lang('quickadmin.financeiro.title')</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @can('contaapagar_access')
                        <li>
                            <a href="{{ route('admin.contaapagars.index') }}">
                                <i class="fa fa-gears"></i>
                                <span>@lang('quickadmin.contaapagar.title')</span>
                            </a>
                        </li>@endcan
                        
                        @can('contareceber_access')
                        <li>
                            <a href="{{ route('admin.contarecebers.index') }}">
                                <i class="fa fa-gears"></i>
                                <span>@lang('quickadmin.contareceber.title')</span>
                            </a>
                        </li>@endcan
                        
                        @can('despesa_access')
                        <li>
                            <a href="{{ route('admin.despesas.index') }}">
                                <i class="fa fa-gears"></i>
                                <span>@lang('quickadmin.despesa.title')</span>
                            </a>
                        </li>@endcan
                        @can('contacontabil_access')
                        <li>
                            <a href="{{ route('admin.contacontabils.index') }}">
                                <i class="fa fa-gears"></i>
                                <span>@lang('quickadmin.contacontabil.title')</span>
                            </a>
                        </li>@endcan
                        
                        @can('contacorrente_access')
                        <li>
                            <a href="{{ route('admin.contacorrentes.index') }}">
                                <i class="fa fa-gears"></i>
                                <span>@lang('quickadmin.contacorrente.title')</span>
                            </a>
                        </li>@endcan --}}
                        {{-- @can('cartao_access')
                        <li>
                            <a href="{{ route('admin.cartaos.index') }}">
                                <i class="fa fa-credit-card"></i>
                                <span>@lang('quickadmin.cartao.title')</span>
                            </a>
                        </li>@endcan --}}
                        {{-- @can('promissorium_access')
                        <li>
                            <a href="{{ route('admin.promissorias.index') }}">
                                <i class="fa fa-gears"></i>
                                <span>@lang('quickadmin.promissoria.title')</span>
                            </a>
                        </li>@endcan
                    </ul> --}}
                {{-- </li>@endcan --}}

                {{-- fimfinaceiro --}}

               
            </li>
            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('quickadmin.qa_logout')</span>
                </a>
            </li>
          
        </ul>
    
    </section>
</aside>

