<?php
Route::get('/', function () { return redirect('/admin/home'); });
// Route::group(['prefix' => 'api'], function () {
//     Route::group(['prefix' => 'user'], function () {
//         Route::get('',function(){
//             return 'ok';
//         });
//     });
// });
// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');
$this->post('autenticar', 'Auth\LoginController@autenticar')->name('auth.autenticar');
// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');
Route::post('/kititens/fetchkit','Admin\KititensController@fetchkit')->name('kititens.fetchkit');

Route::post('/kititens/fetch','Admin\KititensController@fetch')->name('kititens.fetch');
Route::get('producaos/listproducao', 'Admin\ProducaosController@listproducao')->name('admin.producaos.listproducao');
Route::post('/producaos/fetchProducao','Admin\ProducaosController@fetchProducao')->name('producaos.fetchProducao');


Route::post('/entradanotas/lerxml','Admin\EntradanotasController@lerxml')->name('Admin.entradanotas.lerxml');
Route::post('/movimento/fetch','Admin\MovimentosController@fetch')->name('movimento.fetch');
Route::post('/movimento/Usuarioadiciona','Admin\MovimentosController@Usuarioadiciona')->name('movimento.Usuarioadiciona');

// Route::resource('movimentos', 'Admin\MovimentosController');
//NF-e
Route::post('/sats/store','NFeController@store')->name('Admin.sats.store');

//FIM NF-e
Route::post('/producaos/importartelaP','Admin\ProducaosController@importartelaP')->name('admin.producaos.importartelaP');
//Route::post('/entradanotas/preenchertela','Admin\EntradanotasController@preenchertela')->name('admin.entradanotas.preenchertela');
Route::post('/entradanotas/importartela','Admin\EntradanotasController@importartela')->name('admin.entradanotas.importartela');
// Route::post('/sats/pesquisaCliente','Admin\SatsController@pesquisaCliente')->name('Admin.sats.pesquisaCliente');
// Route::post('/sats/buscaNvenda','Admin\SatsController@buscaNvenda')->name('Admin.sats.buscaNvenda');


// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');


//fim atendimento
//conta a pagar
Route::post('/contaapagars/searchfornecedor','Admin\ContaapagarsController@searchfornecedor')->name('contaapagars.searchfornecedor');
Route::post('/contaapagars/searchcontacontabil','Admin\ContaapagarsController@searchcontacontabil')->name('contaapagars.searchcontacontabil');
Route::post('/contaapagars/searchusuario','Admin\ContaapagarsController@searchusuario')->name('contaapagars.searchusuario');
//fim contas a pagar
//contas a receber
Route::post('/contarecebers/searchcliente','Admin\ContarecebersController@searchcliente')->name('contarecebers.searchcliente');
Route::post('/contarecebers/searchcontacontabil','Admin\ContarecebersController@searchcontacontabil')->name('contarecebers.searchcontacontabil');
Route::post('/contarecebers/searchusuario','Admin\ContarecebersController@searchusuario')->name('contarecebers.searchusuario');
// fim contas a receber
//despesa
Route::post('/despesas/searchfornecedor','Admin\DespesasController@searchfornecedor')->name('despesas.searchfornecedor');
Route::post('/despesas/searchcontacontabil','Admin\DespesasController@searchcontacontabil')->name('despesas.searchcontacontabil');
Route::post('/despesas/searchusuario','Admin\DespesasController@searchusuario')->name('despesas.searchusuario');
// fim despesa

Route::post('/promissorias/searchcliente','Admin\PromissoriasController@searchcliente')->name('promissorias.searchcliente');
Route::post('/promissorias/searchusuario','Admin\PromissoriasController@searchusuario')->name('promissorias.searchusuario');
// fim promissorias
// Route::post('/animals/searchcliente', ['uses' => 'Admin\AnimalsController@searchcliente', 'as' => 'animals.searchcliente']);
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('user_actions', 'Admin\UserActionsController');
   
    Route::resource('empresas', 'Admin\EmpresasController');
    Route::post('empresas_mass_destroy', ['uses' => 'Admin\EmpresasController@massDestroy', 'as' => 'empresas.mass_destroy']);
    Route::post('empresas_restore/{id}', ['uses' => 'Admin\EmpresasController@restore', 'as' => 'empresas.restore']);
    Route::delete('empresas_perma_del/{id}', ['uses' => 'Admin\EmpresasController@perma_del', 'as' => 'empresas.perma_del']);
 
    Route::resource('clientes', 'Admin\ClientesController');
    Route::post('clientes_mass_destroy', ['uses' => 'Admin\ClientesController@massDestroy', 'as' => 'clientes.mass_destroy']);
    Route::post('clientes_restore/{id}', ['uses' => 'Admin\ClientesController@restore', 'as' => 'clientes.restore']);
    Route::delete('clientes_perma_del/{id}', ['uses' => 'Admin\ClientesController@perma_del', 'as' => 'clientes.perma_del']);
  
    Route::resource('agendahorarios', 'Admin\AgendahorariosController');
    Route::post('agendahorarios_mass_destroy', ['uses' => 'Admin\AgendahorariosController@massDestroy', 'as' => 'agendahorarios.mass_destroy']);
    Route::post('agendahorarios_restore/{id}', ['uses' => 'Admin\AgendahorariosController@restore', 'as' => 'agendahorarios.restore']);
    Route::delete('agendahorarios_perma_del/{id}', ['uses' => 'Admin\AgendahorariosController@perma_del', 'as' => 'agendahorarios.perma_del']);
    Route::resource('produtos', 'Admin\ProdutosController');
    Route::post('produtos_mass_destroy', ['uses' => 'Admin\ProdutosController@massDestroy', 'as' => 'produtos.mass_destroy']);
    Route::post('produtos_restore/{id}', ['uses' => 'Admin\ProdutosController@restore', 'as' => 'produtos.restore']);
    Route::delete('produtos_perma_del/{id}', ['uses' => 'Admin\ProdutosController@perma_del', 'as' => 'produtos.perma_del']);
    Route::resource('depositos', 'Admin\DepositosController');
    Route::post('depositos_mass_destroy', ['uses' => 'Admin\DepositosController@massDestroy', 'as' => 'depositos.mass_destroy']);
    Route::post('depositos_restore/{id}', ['uses' => 'Admin\DepositosController@restore', 'as' => 'depositos.restore']);
    Route::delete('depositos_perma_del/{id}', ['uses' => 'Admin\DepositosController@perma_del', 'as' => 'depositos.perma_del']);
    Route::resource('fornecedors', 'Admin\FornecedorsController');
    Route::post('fornecedors_mass_destroy', ['uses' => 'Admin\FornecedorsController@massDestroy', 'as' => 'fornecedors.mass_destroy']);
    Route::post('fornecedors_restore/{id}', ['uses' => 'Admin\FornecedorsController@restore', 'as' => 'fornecedors.restore']);
    Route::delete('fornecedors_perma_del/{id}', ['uses' => 'Admin\FornecedorsController@perma_del', 'as' => 'fornecedors.perma_del']);
  
    Route::resource('referencias', 'Admin\ReferenciasController');
    Route::post('referencias_mass_destroy', ['uses' => 'Admin\ReferenciasController@massDestroy', 'as' => 'referencias.mass_destroy']);
    Route::post('referencias_restore/{id}', ['uses' => 'Admin\ReferenciasController@restore', 'as' => 'referencias.restore']);
    Route::delete('referencias_perma_del/{id}', ['uses' => 'Admin\ReferenciasController@perma_del', 'as' => 'referencias.perma_del']);
    Route::resource('cors', 'Admin\CorsController');
    Route::post('cors_mass_destroy', ['uses' => 'Admin\CorsController@massDestroy', 'as' => 'cors.mass_destroy']);
    Route::post('cors_restore/{id}', ['uses' => 'Admin\CorsController@restore', 'as' => 'cors.restore']);
    Route::delete('cors_perma_del/{id}', ['uses' => 'Admin\CorsController@perma_del', 'as' => 'cors.perma_del']);
   
    Route::resource('tamanhos', 'Admin\TamanhosController');
    Route::post('tamanhos_mass_destroy', ['uses' => 'Admin\TamanhosController@massDestroy', 'as' => 'tamanhos.mass_destroy']);
    Route::post('tamanhos_restore/{id}', ['uses' => 'Admin\TamanhosController@restore', 'as' => 'tamanhos.restore']);
    Route::delete('tamanhos_perma_del/{id}', ['uses' => 'Admin\TamanhosController@perma_del', 'as' => 'tamanhos.perma_del']);

    Route::resource('entradanotas', 'Admin\EntradanotasController');
    Route::post('entradanotas_mass_destroy', ['uses' => 'Admin\EntradanotasController@massDestroy', 'as' => 'entradanotas.mass_destroy']);
    Route::post('entradanotas_restore/{id}', ['uses' => 'Admin\EntradanotasController@restore', 'as' => 'entradanotas.restore']);
    Route::delete('entradanotas_perma_del/{id}', ['uses' => 'Admin\EntradanotasController@perma_del', 'as' => 'entradanotas.perma_del']);
    
    Route::resource('devolucaos', 'Admin\DevolucaosController');
    Route::post('devolucaos_mass_destroy', ['uses' => 'Admin\DevolucaosController@massDestroy', 'as' => 'devolucaos.mass_destroy']);
    Route::post('devolucaos_restore/{id}', ['uses' => 'Admin\DevolucaosController@restore', 'as' => 'devolucaos.restore']);
    Route::delete('devolucaos_perma_del/{id}', ['uses' => 'Admin\DevolucaosController@perma_del', 'as' => 'devolucaos.perma_del']);
  
    Route::resource('kititens', 'Admin\KititensController');
    Route::post('kititens_mass_destroy', ['uses' => 'Admin\KititensController@massDestroy', 'as' => 'kititens.mass_destroy']);
    Route::post('kititens_restore/{id}', ['uses' => 'Admin\KititensController@restore', 'as' => 'kititens.restore']);
    Route::delete('kititens_perma_del/{id}', ['uses' => 'Admin\KititensController@perma_del', 'as' => 'kititens.perma_del']);
   
    Route::resource('contacontabils', 'Admin\ContacontabilsController');
    Route::post('contacontabils_mass_destroy', ['uses' => 'Admin\ContacontabilsController@massDestroy', 'as' => 'contacontabils.mass_destroy']);
    Route::post('contacontabils_restore/{id}', ['uses' => 'Admin\ContacontabilsController@restore', 'as' => 'contacontabils.restore']);
    Route::delete('contacontabils_perma_del/{id}', ['uses' => 'Admin\ContacontabilsController@perma_del', 'as' => 'contacontabils.perma_del']);
    Route::resource('contacorrentes', 'Admin\ContacorrentesController');
    Route::post('contacorrentes_mass_destroy', ['uses' => 'Admin\ContacorrentesController@massDestroy', 'as' => 'contacorrentes.mass_destroy']);
    Route::post('contacorrentes_restore/{id}', ['uses' => 'Admin\ContacorrentesController@restore', 'as' => 'contacorrentes.restore']);
    Route::delete('contacorrentes_perma_del/{id}', ['uses' => 'Admin\ContacorrentesController@perma_del', 'as' => 'contacorrentes.perma_del']);
  
    Route::resource('contaapagars', 'Admin\ContaapagarsController');
    Route::post('contaapagars_mass_destroy', ['uses' => 'Admin\ContaapagarsController@massDestroy', 'as' => 'contaapagars.mass_destroy']);
    Route::post('contaapagars_restore/{id}', ['uses' => 'Admin\ContaapagarsController@restore', 'as' => 'contaapagars.restore']);
    Route::delete('contaapagars_perma_del/{id}', ['uses' => 'Admin\ContaapagarsController@perma_del', 'as' => 'contaapagars.perma_del']);
    Route::resource('contarecebers', 'Admin\ContarecebersController');
    Route::post('contarecebers_mass_destroy', ['uses' => 'Admin\ContarecebersController@massDestroy', 'as' => 'contarecebers.mass_destroy']);
    Route::post('contarecebers_restore/{id}', ['uses' => 'Admin\ContarecebersController@restore', 'as' => 'contarecebers.restore']);
    Route::delete('contarecebers_perma_del/{id}', ['uses' => 'Admin\ContarecebersController@perma_del', 'as' => 'contarecebers.perma_del']);
    Route::resource('despesas', 'Admin\DespesasController');
    Route::post('despesas_mass_destroy', ['uses' => 'Admin\DespesasController@massDestroy', 'as' => 'despesas.mass_destroy']);
    Route::post('despesas_restore/{id}', ['uses' => 'Admin\DespesasController@restore', 'as' => 'despesas.restore']);
    Route::delete('despesas_perma_del/{id}', ['uses' => 'Admin\DespesasController@perma_del', 'as' => 'despesas.perma_del']);
  
    Route::get('/calendar', 'Admin\SystemCalendarController@index'); 
  
    Route::resource('unidades', 'Admin\UnidadesController');
    Route::post('unidades_mass_destroy', ['uses' => 'Admin\UnidadesController@massDestroy', 'as' => 'unidades.mass_destroy']);
    Route::post('unidades_restore/{id}', ['uses' => 'Admin\UnidadesController@restore', 'as' => 'unidades.restore']);
    Route::delete('unidades_perma_del/{id}', ['uses' => 'Admin\UnidadesController@perma_del', 'as' => 'unidades.perma_del']);
    Route::resource('movimentos', 'Admin\MovimentosController');
    Route::post('movimentos_mass_destroy', ['uses' => 'Admin\MovimentosController@massDestroy', 'as' => 'movimentos.mass_destroy']);
    Route::post('movimentos_restore/{id}', ['uses' => 'Admin\MovimentosController@restore', 'as' => 'movimentos.restore']);
    Route::delete('movimentos_perma_del/{id}', ['uses' => 'Admin\MovimentosController@perma_del', 'as' => 'movimentos.perma_del']);
    Route::resource('agendatelefonicas', 'Admin\AgendatelefonicasController');
    Route::post('agendatelefonicas_mass_destroy', ['uses' => 'Admin\AgendatelefonicasController@massDestroy', 'as' => 'agendatelefonicas.mass_destroy']);
    Route::post('agendatelefonicas_restore/{id}', ['uses' => 'Admin\AgendatelefonicasController@restore', 'as' => 'agendatelefonicas.restore']);
    Route::delete('agendatelefonicas_perma_del/{id}', ['uses' => 'Admin\AgendatelefonicasController@perma_del', 'as' => 'agendatelefonicas.perma_del']);
 
    Route::resource('producaos', 'Admin\ProducaosController');
    Route::post('producaos_mass_destroy', ['uses' => 'Admin\ProducaosController@massDestroy', 'as' => 'producaos.mass_destroy']);
    Route::post('producaos_restore/{id}', ['uses' => 'Admin\ProducaosController@restore', 'as' => 'producaos.restore']);
    Route::delete('producaos_perma_del/{id}', ['uses' => 'Admin\ProducaosController@perma_del', 'as' => 'producaos.perma_del']);














 
});
