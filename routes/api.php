<?php

use Yajra\Datatables\Request;
use App\Http\Controllers\NFeController;

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {

});
Route::resource('/nfe',\NFeController::class);

