<?php

$factory->define(App\Feria::class, function (Faker\Generator $faker) {
    return [
        "colaborador" => $faker->name,
        "datainicial" => $faker->date("Y-m-d", $max = 'now'),
        "datafinal" => $faker->date("Y-m-d", $max = 'now'),
        "anoinicial" => $faker->name,
        "anofinal" => $faker->name,
        "dias" => $faker->name,
        "dias_abono" => $faker->name,
        "data_pagamento" => $faker->date("Y-m-d", $max = 'now'),
        "dataretorno" => $faker->date("Y-m-d", $max = 'now'),
        "salario" => $faker->randomNumber(2),
        "salariobruto" => $faker->randomNumber(2),
        "obs" => $faker->name,
    ];
});
