<?php

$factory->define(App\Dependenterh::class, function (Faker\Generator $faker) {
    return [
        "nome" => $faker->name,
        "cpf" => $faker->name,
        "rg" => $faker->name,
        "celular" => $faker->name,
        "telefone" => $faker->name,
        "email" => $faker->safeEmail,
        "datanascimento" => $faker->date("Y-m-d", $max = 'now'),
        "sexo" => $faker->name,
        "debito" => $faker->randomNumber(2),
        "cep" => $faker->name,
        "complemento" => $faker->name,
        "endereco" => $faker->name,
        "numero" => $faker->name,
        "bairro" => $faker->name,
        "cidade" => $faker->name,
        "uf" => $faker->name,
        "obs" => $faker->name,
        "colaborador" => $faker->name,
        "credito" => $faker->randomNumber(2),
    ];
});
