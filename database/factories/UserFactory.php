<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
        "email" => $faker->safeEmail,
        "password" => str_random(10),
        "role_id" => factory('App\Role')->create(),
        "remember_token" => $faker->name,
        "nomecompleto" => $faker->name,
        "datanascimento" => $faker->date("Y-m-d", $max = 'now'),
        "cep" => $faker->name,
        "endereco" => $faker->name,
        "numero" => $faker->name,
        "bairro" => $faker->name,
        "cidade" => $faker->name,
        "uf" => $faker->name,
        "complemento" => $faker->name,
        "fone" => $faker->name,
        "celular" => $faker->name,
        "cpf" => $faker->name,
        "rg" => $faker->name,
        "datacadastro" => $faker->date("Y-m-d", $max = 'now'),
        "comissao" => $faker->name,
        "salario" => $faker->randomNumber(2),
        "empresa" => $faker->name,
        "situacao" => $faker->name,
        "obs" => $faker->name,
        "vendedor" => $faker->name,
    ];
});
