<?php

$factory->define(App\Alteracaosalario::class, function (Faker\Generator $faker) {
    return [
        "colaborador" => $faker->name,
        "data" => $faker->date("Y-m-d", $max = 'now'),
        "motivo" => $faker->name,
        "valor" => $faker->randomNumber(2),
        "obs" => $faker->name,
    ];
});
