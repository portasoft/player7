<?php

$factory->define(App\Motoristum::class, function (Faker\Generator $faker) {
    return [
        "nome" => $faker->name,
        "rg" => $faker->name,
        "cpf" => $faker->name,
        "cep" => $faker->name,
        "endereco" => $faker->name,
        "numero" => $faker->name,
        "bairro" => $faker->name,
        "cidade" => $faker->name,
        "uf" => $faker->name,
        "complemento" => $faker->name,
        "fone" => $faker->name,
        "celular" => $faker->name,
        "obs" => $faker->name,
        "cnh" => $faker->name,
        "empresa" => $faker->name,
    ];
});
