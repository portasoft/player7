<?php

$factory->define(App\Adiantamento::class, function (Faker\Generator $faker) {
    return [
        "colaborador" => $faker->name,
        "valor" => $faker->randomNumber(2),
        "obs" => $faker->name,
    ];
});
