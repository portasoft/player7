<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'name' => 'Admin', 'email' => 'admin@admin.com', 'password' => '$2y$10$FBJt5JdCDGH1Se5b33MznOsKyUAnXtDSa/vVqZuezokB5NEwk.yt6', 'role_id' => 1, 'remember_token' => '', 'nomecompleto' => null, 'datanascimento' => '', 'cep' => null, 'endereco' => null, 'numero' => null, 'bairro' => null, 'cidade' => null, 'uf' => null, 'complemento' => null, 'fone' => null, 'celular' => null, 'cpf' => null, 'rg' => null, 'datacadastro' => '', 'comissao' => null, 'salario' => null, 'empresa' => null, 'situacao' => null, 'obs' => null, 'vendedor' => null, 'foto' => null, 'localizacao_address' => null, 'localizacao_latitude' => null, 'localizacao_longitude' => null,],

        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
