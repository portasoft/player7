<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534009261VermifugosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('vermifugos')) {
            Schema::create('vermifugos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('peso')->nullable();
                $table->string('vermifugo')->nullable();
                $table->string('dose')->nullable();
                $table->date('data')->nullable();
                $table->string('animal')->nullable();
                $table->string('obs')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vermifugos');
    }
}
