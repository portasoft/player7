<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1534262274AtendimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('atendimentos', function (Blueprint $table) {
            
if (!Schema::hasColumn('atendimentos', 'raca')) {
                $table->string('raca')->nullable();
                }
if (!Schema::hasColumn('atendimentos', 'temperamento')) {
                $table->string('temperamento')->nullable();
                }
if (!Schema::hasColumn('atendimentos', 'pacote')) {
                $table->string('pacote')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('atendimentos', function (Blueprint $table) {
            $table->dropColumn('raca');
            $table->dropColumn('temperamento');
            $table->dropColumn('pacote');
            
        });

    }
}
