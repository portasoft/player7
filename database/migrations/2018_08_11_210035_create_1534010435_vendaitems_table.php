<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534010435VendaitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('vendaitems')) {
            Schema::create('vendaitems', function (Blueprint $table) {
                $table->increments('id');
                $table->string('produto')->nullable();
                $table->integer('qtde')->nullable();
                $table->decimal('unitario', 15, 2)->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->string('caixa')->nullable();
                $table->decimal('valortotal', 15, 2)->nullable();
                $table->string('devolvido')->nullable();
                $table->decimal('valortotalitens', 15, 2)->nullable();
                $table->string('idvenda')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendaitems');
    }
}
