<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1537300211MovimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('movimentos')) {
            Schema::create('movimentos', function (Blueprint $table) {
                $table->increments('id');
                $table->date('data')->nullable();
                $table->string('tipo')->nullable();
                $table->string('empresa')->nullable();
                $table->integer('deposito')->nullable();
                $table->integer('usuario')->nullable();
                $table->integer('produto')->nullable();
                $table->integer('qtde')->nullable();
                $table->string('motivo')->nullable();
                $table->string('documento')->nullable();
                $table->string('obs')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimentos');
    }
}
