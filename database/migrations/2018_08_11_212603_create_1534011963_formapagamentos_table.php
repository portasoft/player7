<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534011963FormapagamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('formapagamentos')) {
            Schema::create('formapagamentos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('origem')->nullable();
                $table->string('tipo')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->string('obs')->nullable();
                $table->string('categoria')->nullable();
                $table->string('cdc')->nullable();
                $table->string('tipodia')->nullable();
                $table->string('ultimodia')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formapagamentos');
    }
}
