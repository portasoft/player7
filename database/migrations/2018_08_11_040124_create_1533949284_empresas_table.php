<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533949284EmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('empresas')) {
            Schema::create('empresas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cnpj')->nullable();
                $table->string('razao')->nullable();
                $table->date('datacadastro')->nullable();
                $table->string('fantasia')->nullable();
                $table->string('ie')->nullable();
                $table->string('im')->nullable();
                $table->string('contato')->nullable();
                $table->string('regime')->nullable();
                $table->string('fone')->nullable();
                $table->string('celular')->nullable();
                $table->string('email')->nullable();
                $table->string('cep')->nullable();
                $table->string('endereco')->nullable();
                $table->string('numero')->nullable();
                $table->string('bairro')->nullable();
                $table->string('cidade')->nullable();
                $table->string('uf')->nullable();
                $table->string('complemento')->nullable();
                $table->string('foto')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
