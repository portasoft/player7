<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534010013VendaesperasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('vendaesperas')) {
            Schema::create('vendaesperas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('produto')->nullable();
                $table->string('comanda')->nullable();
                $table->decimal('unitario', 15, 2)->nullable();
                $table->decimal('total', 15, 2)->nullable();
                $table->integer('qtde')->nullable();
                $table->date('data')->nullable();
                $table->string('hora')->nullable();
                $table->string('colaborador')->nullable();
                $table->string('obs')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendaesperas');
    }
}
