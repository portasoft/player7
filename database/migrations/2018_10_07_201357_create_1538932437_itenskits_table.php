<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1538932437ItenskitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('itenskits')) {
            Schema::create('itenskits', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produtokit')->nullable();
                $table->integer('produtoinsumo')->nullable();
                $table->decimal('custo', 15, 2)->nullable();
                $table->decimal('qtde', 15, 2)->nullable();
                $table->string('obs')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itenskits');
    }
}
