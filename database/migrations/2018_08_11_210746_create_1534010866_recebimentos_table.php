<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534010866RecebimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('recebimentos')) {
            Schema::create('recebimentos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cliente')->nullable();
                $table->string('contacontabil')->nullable();
                $table->string('contacorrente')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->date('data')->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->decimal('juros', 15, 2)->nullable();
                $table->string('conta')->nullable();
                $table->string('obs')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recebimentos');
    }
}
