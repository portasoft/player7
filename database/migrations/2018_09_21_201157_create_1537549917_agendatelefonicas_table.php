<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1537549917AgendatelefonicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('agendatelefonicas')) {
            Schema::create('agendatelefonicas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome')->nullable();
                $table->string('operadora')->nullable();
                $table->string('celular1')->nullable();
                $table->string('celular2')->nullable();
                $table->string('telefone1')->nullable();
                $table->string('telefone2')->nullable();
                $table->string('emailpessoal')->nullable();
                $table->string('emailcomercial')->nullable();
                $table->date('dataaniversario')->nullable();
                $table->date('dataabertura')->nullable();
                $table->string('fundacao')->nullable();
                $table->string('endereco')->nullable();
                $table->string('numero')->nullable();
                $table->string('bairro')->nullable();
                $table->string('cidade')->nullable();
                $table->string('uf')->nullable();
                $table->string('cep')->nullable();
                $table->string('obs')->nullable();
                $table->string('tipo')->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendatelefonicas');
    }
}
