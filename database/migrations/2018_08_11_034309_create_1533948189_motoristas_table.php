<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533948189MotoristasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('motoristas')) {
            Schema::create('motoristas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome')->nullable();
                $table->string('rg')->nullable();
                $table->string('cpf')->nullable();
                $table->string('cep')->nullable();
                $table->string('endereco')->nullable();
                $table->string('numero')->nullable();
                $table->string('bairro')->nullable();
                $table->string('cidade')->nullable();
                $table->string('uf')->nullable();
                $table->string('complemento')->nullable();
                $table->string('fone')->nullable();
                $table->string('celular')->nullable();
                $table->string('obs')->nullable();
                $table->string('cnh')->nullable();
                $table->string('empresa')->nullable();
                $table->string('foto')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motoristas');
    }
}
