<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533951441ProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('produtos')) {
            Schema::create('produtos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('codigobarra')->nullable();
                $table->string('descricao')->nullable();
                $table->string('grupo')->nullable();
                $table->string('marca')->nullable();
                $table->string('finalidade')->nullable();
                $table->string('referencia')->nullable();
                $table->string('cor')->nullable();
                $table->string('tamanho')->nullable();
                $table->string('regraimposto')->nullable();
                $table->string('tiporegra')->nullable();
                $table->integer('estoque')->nullable();
                $table->integer('estoquemin')->nullable();
                $table->decimal('custo', 15, 2)->nullable();
                $table->decimal('preco', 15, 2)->nullable();
                $table->decimal('margem', 15, 2)->nullable();
                $table->date('datacadastro')->nullable();
                $table->string('obs')->nullable();
                $table->string('foto')->nullable();
                $table->string('ncm')->nullable();
                $table->string('kit')->nullable();
     
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
