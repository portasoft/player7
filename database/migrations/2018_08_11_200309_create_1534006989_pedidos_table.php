<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534006989PedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('pedidos')) {
            Schema::create('pedidos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('fornecedor')->nullable();
                $table->date('datapedido')->nullable();
                $table->string('contato')->nullable();
                $table->string('prazo')->nullable();
                $table->string('transportadora')->nullable();
                $table->date('dataaprovacao')->nullable();
                $table->date('dataentrega')->nullable();
                $table->string('empresa')->nullable();
                $table->string('obs')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
