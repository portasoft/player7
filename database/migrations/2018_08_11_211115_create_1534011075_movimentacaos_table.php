<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534011075MovimentacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('movimentacaos')) {
            Schema::create('movimentacaos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('caixa')->nullable();
                $table->date('data')->nullable();
                $table->time('hora')->nullable();
                $table->string('tipo')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->string('motivo')->nullable();
                $table->string('origem')->nullable();
                $table->decimal('saldobanco', 15, 2)->nullable();
                $table->string('obs')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimentacaos');
    }
}
