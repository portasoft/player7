<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534008357AnimalveterinariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('animalveterinarios')) {
            Schema::create('animalveterinarios', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome')->nullable();
                $table->string('crmv')->nullable();
                $table->string('fone')->nullable();
                $table->string('email')->nullable();
                $table->string('ativo')->nullable();
                $table->string('colaborador')->nullable();
                $table->string('tipo')->nullable();
                $table->string('matricula')->nullable();
                $table->string('foto')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animalveterinarios');
    }
}
