<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534009190AnimalvacinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('animalvacinas')) {
            Schema::create('animalvacinas', function (Blueprint $table) {
                $table->increments('id');
                $table->date('data')->nullable();
                $table->string('vacina')->nullable();
                $table->string('reforno')->nullable();
                $table->string('animal')->nullable();
                $table->string('animalveterinario')->nullable();
                $table->string('obs')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animalvacinas');
    }
}
