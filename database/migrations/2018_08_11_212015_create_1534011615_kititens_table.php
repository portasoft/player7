<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534011615KititensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('kititens')) {
            Schema::create('kititens', function (Blueprint $table) {
                $table->increments('id');
                $table->string('produto')->nullable();
                $table->integer('qtde')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->decimal('base', 15, 2)->nullable();
                $table->string('kit')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kititens');
    }
}
