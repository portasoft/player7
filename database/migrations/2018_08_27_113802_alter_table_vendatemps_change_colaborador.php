<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVendatempsChangeColaborador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendatemps', function (Blueprint $table) {
            $table->integer('colaborador')->nullable()->change();
            $table->renameColumn('colaborador', 'id_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendatemps', function (Blueprint $table) {
            //
        });
    }
}
