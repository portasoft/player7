<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533950776AgendahorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('agendahorarios')) {
            Schema::create('agendahorarios', function (Blueprint $table) {
                $table->increments('id');
                $table->date('data')->nullable();
                $table->time('hora')->nullable();
                $table->string('descricao')->nullable();
                $table->string('motivo')->nullable();
                $table->string('obs')->nullable();
                $table->date('retorno')->nullable();
                $table->string('situacao')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendahorarios');
    }
}
