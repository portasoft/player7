<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534009128CarteirinhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('carteirinhas')) {
            Schema::create('carteirinhas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('animal')->nullable();
                $table->string('cliente')->nullable();
                $table->string('empresa')->nullable();
                $table->string('servico')->nullable();
                $table->date('datacadastro')->nullable();
                $table->date('dataservivo')->nullable();
                $table->string('obs')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carteirinhas');
    }
}
