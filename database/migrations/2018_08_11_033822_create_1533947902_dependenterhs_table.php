<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533947902DependenterhsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('dependenterhs')) {
            Schema::create('dependenterhs', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome')->nullable();
                $table->string('cpf')->nullable();
                $table->string('rg')->nullable();
                $table->string('celular')->nullable();
                $table->string('telefone')->nullable();
                $table->string('email')->nullable();
                $table->date('datanascimento')->nullable();
                $table->string('sexo')->nullable();
                $table->decimal('debito', 15, 2)->nullable();
                $table->string('cep')->nullable();
                $table->string('complemento')->nullable();
                $table->string('endereco')->nullable();
                $table->string('numero')->nullable();
                $table->string('bairro')->nullable();
                $table->string('cidade')->nullable();
                $table->string('uf')->nullable();
                $table->string('obs')->nullable();
                $table->string('colaborador')->nullable();
                $table->string('foto')->nullable();
                $table->string('localizacao_address')->nullable();
                $table->double('localizacao_latitude')->nullable();
                $table->double('localizacao_longitude')->nullable();
                $table->decimal('credito', 15, 2)->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependenterhs');
    }
}
