<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533952058FornecedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('fornecedors')) {
            Schema::create('fornecedors', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cnpj')->nullable();
                $table->string('razao')->nullable();
                $table->string('fantasia')->nullable();
                $table->string('contato')->nullable();
                $table->date('datacadastro')->nullable();
                $table->string('ie')->nullable();
                $table->string('im')->nullable();
                $table->string('segmento')->nullable();
                $table->string('telefone')->nullable();
                $table->string('celular')->nullable();
                $table->string('email')->nullable();
                $table->string('cep')->nullable();
                $table->string('endereco')->nullable();
                $table->string('numero')->nullable();
                $table->string('bairro')->nullable();
                $table->string('cidade')->nullable();
                $table->string('uf')->nullable();
                $table->string('complemento')->nullable();
                $table->string('obs')->nullable();
                $table->string('foto')->nullable();
                $table->string('localizacao_address')->nullable();
                $table->double('localizacao_latitude')->nullable();
                $table->double('localizacao_longitude')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedors');
    }
}
