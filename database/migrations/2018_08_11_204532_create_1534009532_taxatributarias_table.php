<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534009532TaxatributariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('taxatributarias')) {
            Schema::create('taxatributarias', function (Blueprint $table) {
                $table->increments('id');
                $table->string('descricao')->nullable();
                $table->string('icms')->nullable();
                $table->string('tipo')->nullable();
                $table->string('cfopestadual')->nullable();
                $table->string('cfopinterestadual')->nullable();
                $table->string('totalizador')->nullable();
                $table->string('cfoptranferencia')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxatributarias');
    }
}
