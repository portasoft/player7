<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534009766RegraimpostosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('regraimpostos')) {
            Schema::create('regraimpostos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('tiporegra')->nullable();
                $table->string('iva')->nullable();
                $table->string('uforigem')->nullable();
                $table->string('ufodestino')->nullable();
                $table->string('baseicms')->nullable();
                $table->string('aliquotaicms')->nullable();
                $table->string('baseicmsst')->nullable();
                $table->string('cst')->nullable();
                $table->string('csosn')->nullable();
                $table->string('taxatributaria')->nullable();
                $table->string('decreto')->nullable();
                $table->string('modalidade')->nullable();
                $table->string('cfop')->nullable();
                $table->string('classeipi')->nullable();
                $table->string('codigoenquadramento')->nullable();
                $table->string('tipocalculo')->nullable();
                $table->string('aliquotaipi')->nullable();
                $table->decimal('valorunidadeipi', 15, 2)->nullable();
                $table->string('stpis')->nullable();
                $table->string('tipocalculocofins')->nullable();
                $table->string('tributacaoissqn')->nullable();
                $table->string('ipizerado')->nullable();
                $table->string('stpisentrada')->nullable();
                $table->string('basepis')->nullable();
                $table->string('basecofins')->nullable();
                $table->string('basepisentrada')->nullable();
                $table->string('basecofinsentrada')->nullable();
                $table->string('aliquotapissentrada')->nullable();
                $table->string('aliquotacofinssentrada')->nullable();
                $table->string('stentrada')->nullable();
                $table->string('cfopentrada')->nullable();
                $table->string('csosnentrada')->nullable();
                $table->string('codsuframa')->nullable();
                $table->string('stipi')->nullable();
                $table->string('tipocalculoipisaida')->nullable();
                $table->string('aliquotaipisaida')->nullable();
                $table->decimal('valorunidadeipisaida', 15, 2)->nullable();
                $table->string('stipiisaida')->nullable();
                $table->string('aliquotaissqn')->nullable();
                $table->string('codigoservicoiss')->nullable();
                $table->string('cfopservico')->nullable();
                $table->string('aliquotainternaicms')->nullable();
                $table->string('coftributacaomunicipal')->nullable();
                $table->string('cnae')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regraimpostos');
    }
}
