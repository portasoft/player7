<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534012837DespesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('despesas')) {
            Schema::create('despesas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('fornecedor')->nullable();
                $table->date('data')->nullable();
                $table->decimal('valor_conta', 15, 2)->nullable();
                $table->decimal('valorpago', 15, 2)->nullable();
                $table->integer('parcela')->nullable();
                $table->string('formapagamento')->nullable();
                $table->string('contacontabil')->nullable();
                $table->string('docmercantil')->nullable();
                $table->string('contacorrente')->nullable();
                $table->string('documento')->nullable();
                $table->string('obs')->nullable();
                $table->string('colaborador')->nullable();
                $table->string('tipo')->nullable();
                $table->decimal('juros', 15, 2)->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despesas');
    }
}
