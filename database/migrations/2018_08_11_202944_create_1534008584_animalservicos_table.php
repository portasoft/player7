<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534008584AnimalservicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('animalservicos')) {
            Schema::create('animalservicos', function (Blueprint $table) {
                $table->increments('id');
                $table->date('data')->nullable();
                $table->string('servico')->nullable();
                $table->time('hora')->nullable();
                $table->time('horapegar')->nullable();
                $table->time('horaentrega')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->string('concluido')->nullable();
                $table->string('obs')->nullable();
                $table->string('colaborador')->nullable();
                $table->date('dataalteracao')->nullable();
                $table->time('horaalteracao')->nullable();
                $table->integer('qtde')->nullable();
                $table->date('dataentrega')->nullable();
                $table->string('venda')->nullable();
                $table->string('finalizado')->nullable();
                $table->string('atendimento')->nullable();
                $table->integer('qtdeservico')->nullable();
                $table->integer('qtdepacote')->nullable();
                $table->string('sericogerado')->nullable();
                $table->date('datavalidade')->nullable();
                $table->string('laboratorio')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animalservicos');
    }
}
