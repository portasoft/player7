<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533948997HorariorhsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('horariorhs')) {
            Schema::create('horariorhs', function (Blueprint $table) {
                $table->increments('id');
                $table->string('descricao')->nullable();
                $table->date('datainicio')->nullable();
                $table->date('datafim')->nullable();
                $table->string('tipo')->nullable();
                $table->time('horaentrada')->nullable();
                $table->time('horasaida')->nullable();
                $table->string('duracao')->nullable();
                $table->string('tipointervalo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horariorhs');
    }
}
