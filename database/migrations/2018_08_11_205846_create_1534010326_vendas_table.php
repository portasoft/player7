<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534010326VendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('vendas')) {
            Schema::create('vendas', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('qtde')->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->date('data')->nullable();
                $table->string('hora')->nullable();
                $table->string('caixa')->nullable();
                $table->string('formapagamento')->nullable();
                $table->string('cliente')->nullable();
                $table->string('colaborador')->nullable();
                $table->string('entrada')->nullable();
                $table->string('tipopagamento')->nullable();
                $table->decimal('valorentrada', 15, 2)->nullable();
                $table->decimal('valortotal', 15, 2)->nullable();
                $table->string('cancelado')->nullable();
                $table->decimal('valorpago', 15, 2)->nullable();
                $table->string('obs')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendas');
    }
}
