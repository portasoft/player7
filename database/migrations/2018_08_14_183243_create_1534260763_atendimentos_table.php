<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534260763AtendimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('atendimentos')) {
            Schema::create('atendimentos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cliente')->nullable();
                $table->string('animal')->nullable();
                $table->string('servico')->nullable();
                $table->date('data')->nullable();
                $table->time('hora')->nullable();
                $table->date('dataentrega')->nullable();
                $table->time('horaentrega')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->integer('qtde')->nullable();
                $table->string('usuario')->nullable();
                $table->string('profissional')->nullable();
                $table->string('situacao')->nullable();
                $table->string('continuo')->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->string('venda')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atendimentos');
    }
}
