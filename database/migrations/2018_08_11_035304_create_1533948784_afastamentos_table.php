<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533948784AfastamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('afastamentos')) {
            Schema::create('afastamentos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('motivo')->nullable();
                $table->string('descricao')->nullable();
                $table->date('datainicial')->nullable();
                $table->date('datafinal')->nullable();
                $table->string('obs')->nullable();
                $table->string('arquivo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('afastamentos');
    }
}
