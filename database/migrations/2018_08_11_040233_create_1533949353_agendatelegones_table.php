<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533949353AgendatelegonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('agendatelegones')) {
            Schema::create('agendatelegones', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome')->nullable();
                $table->string('fone')->nullable();
                $table->string('celular')->nullable();
                $table->string('obs')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendatelegones');
    }
}
