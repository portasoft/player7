<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534253102GruposervicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('gruposervicos')) {
            Schema::create('gruposervicos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('descricao')->nullable();
                $table->tinyInteger('produtos')->nullable()->default('0');
                $table->tinyInteger('doses')->nullable()->default('0');
                $table->tinyInteger('outros')->nullable()->default('0');
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gruposervicos');
    }
}
