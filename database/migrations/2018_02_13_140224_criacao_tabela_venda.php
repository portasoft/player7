<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriacaoTabelaVenda extends Migration{

    public function up(){

        Schema::create('venda', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('valorTotal',10,5);
            $table->string('formaPagamento',100);
            $table->timestamps();
        });

    }

    public function down(){

        Schema::dropIfExists('venda');

    }
}
