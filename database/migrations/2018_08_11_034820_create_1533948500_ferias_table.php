<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533948500FeriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('ferias')) {
            Schema::create('ferias', function (Blueprint $table) {
                $table->increments('id');
                $table->string('colaborador')->nullable();
                $table->date('datainicial')->nullable();
                $table->date('datafinal')->nullable();
                $table->string('anoinicial')->nullable();
                $table->string('anofinal')->nullable();
                $table->string('dias')->nullable();
                $table->string('dias_abono')->nullable();
                $table->date('data_pagamento')->nullable();
                $table->date('dataretorno')->nullable();
                $table->decimal('salario', 15, 2)->nullable();
                $table->decimal('salariobruto', 15, 2)->nullable();
                $table->string('obs')->nullable();
                $table->string('arquivo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ferias');
    }
}
