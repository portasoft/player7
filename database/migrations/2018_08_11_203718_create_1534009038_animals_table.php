<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534009038AnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('animals')) {
            Schema::create('animals', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome')->nullable();
                $table->string('porte')->nullable();
                $table->string('peso')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->string('obs')->nullable();
                $table->string('potencial')->nullable();
                $table->string('sexo')->nullable();
                $table->string('cliente')->nullable();
                $table->string('pelagem')->nullable();
                $table->string('tipopelagem')->nullable();
                $table->string('raca')->nullable();
                $table->string('carteira')->nullable();
                $table->date('dataobito')->nullable();
                $table->date('datacadastro')->nullable();
                $table->date('datanascimento')->nullable();
                $table->string('rga')->nullable();
                $table->string('chip')->nullable();
                $table->string('pedigre')->nullable();
                $table->string('temperamento')->nullable();
                $table->string('situacao')->nullable();
                $table->string('foto')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
