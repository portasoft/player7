<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534008130FotoexamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('fotoexames')) {
            Schema::create('fotoexames', function (Blueprint $table) {
                $table->increments('id');
                $table->string('exame')->nullable();
                $table->string('foto')->nullable();
                $table->string('anexo')->nullable();
                $table->string('obs')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fotoexames');
    }
}
