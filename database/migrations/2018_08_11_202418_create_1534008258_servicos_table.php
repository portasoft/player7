<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534008258ServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('servicos')) {
            Schema::create('servicos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->string('horas')->nullable();
                $table->string('minutos')->nullable();
                $table->decimal('comissao', 15, 2)->nullable();
                $table->string('gruposervico')->nullable();
                $table->string('doses')->nullable();
                $table->string('periodo')->nullable();
                $table->decimal('valobase', 15, 2)->nullable();
                $table->date('datacarga')->nullable();
                $table->string('inativo')->nullable();
                $table->decimal('custo', 15, 2)->nullable();
                $table->string('ncm')->nullable();
                $table->string('participante')->nullable();
                $table->string('servivoconsulta')->nullable();
                $table->string('servivoretorno')->nullable();
                $table->string('obs')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicos');
    }
}
