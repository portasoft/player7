<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534011455MesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('mesas')) {
            Schema::create('mesas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('descricao')->nullable();
                $table->string('fechamento')->nullable();
                $table->string('ultimopedido')->nullable();
                $table->string('ordem')->nullable();
                $table->string('obs')->nullable();
                $table->string('foto')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mesas');
    }
}
