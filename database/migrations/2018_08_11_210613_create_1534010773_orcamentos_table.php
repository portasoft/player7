<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534010773OrcamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('orcamentos')) {
            Schema::create('orcamentos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cliente')->nullable();
                $table->string('produto')->nullable();
                $table->integer('qtde')->nullable();
                $table->date('validade')->nullable();
                $table->date('data')->nullable();
                $table->decimal('valortotal', 15, 2)->nullable();
                $table->decimal('valorunitario', 15, 2)->nullable();
                $table->string('obs')->nullable();
                $table->string('finalizado')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orcamentos');
    }
}
