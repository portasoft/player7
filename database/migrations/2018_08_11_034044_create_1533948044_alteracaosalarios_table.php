<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533948044AlteracaosalariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('alteracaosalarios')) {
            Schema::create('alteracaosalarios', function (Blueprint $table) {
                $table->increments('id');
                $table->string('colaborador')->nullable();
                $table->date('data')->nullable();
                $table->string('motivo')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->string('obs')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alteracaosalarios');
    }
}
