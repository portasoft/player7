<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534010151VendatempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('vendatemps')) {
            Schema::create('vendatemps', function (Blueprint $table) {
                $table->increments('id');
                $table->string('produto')->nullable();
                $table->string('colaborador')->nullable();
                $table->decimal('preco', 15, 2)->nullable();
                $table->decimal('totalitens', 15, 2)->nullable();
                $table->decimal('totalvenda', 15, 2)->nullable();
                $table->date('data')->nullable();
                $table->string('hora')->nullable();
                $table->string('formapagamento')->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->string('venda')->nullable();
                $table->integer('qtde')->nullable();
                $table->string('nrcaixa')->nullable();
                $table->string('obs')->nullable();
                $table->string('parcelado')->nullable();
                $table->string('espera')->nullable();
                $table->string('orcamento')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendatemps');
    }
}
