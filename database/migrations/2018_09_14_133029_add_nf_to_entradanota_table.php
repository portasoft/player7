<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNfToEntradanotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entradanotas', function (Blueprint $table) {
            $table->integer('nf')->default(0);
       
        });
    }

    /**
     * Reverse the migrations./
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entradanotas', function (Blueprint $table) {
            //
        });
    }
}
