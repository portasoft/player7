<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1535397256VendaesperaitensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('vendaesperaitens')) {
            Schema::create('vendaesperaitens', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('idproduto')->nullable();
                $table->integer('qtde')->nullable();
                $table->decimal('unitario', 15, 2)->nullable();
                $table->decimal('totalitens', 15, 2)->nullable();
                $table->integer('comanda')->nullable();
                $table->integer('venda')->nullable();
                $table->string('obs')->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->date('data')->nullable();
                $table->time('hora')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendaesperaitens');
    }
}
