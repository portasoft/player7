<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534010986TrocasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('trocas')) {
            Schema::create('trocas', function (Blueprint $table) {
                $table->increments('id');
                $table->date('data')->nullable();
                $table->string('cliente')->nullable();
                $table->decimal('totaldevovido', 15, 2)->nullable();
                $table->decimal('totallevado', 15, 2)->nullable();
                $table->decimal('diferenca', 15, 2)->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->decimal('troco', 15, 2)->nullable();
                $table->string('finalizada')->nullable();
                $table->string('colaborador')->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->decimal('valorcredito', 15, 2)->nullable();
                $table->string('obs')->nullable();
                $table->string('caixa')->nullable();
                $table->string('notafiscal')->nullable();
                $table->string('vendaorigem')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trocas');
    }
}
