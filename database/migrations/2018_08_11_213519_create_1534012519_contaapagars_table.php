<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534012519ContaapagarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('contaapagars')) {
            Schema::create('contaapagars', function (Blueprint $table) {
                $table->increments('id');
                $table->string('fornecedor')->nullable();
                $table->date('vencimento')->nullable();
                $table->decimal('valorconta', 15, 2)->nullable();
                $table->decimal('valorapagar', 15, 2)->nullable();
                $table->integer('parcela')->nullable();
                $table->string('formapagamento')->nullable();
                $table->string('contacontabil')->nullable();
                $table->string('docmercantil')->nullable();
                $table->string('contacorrente')->nullable();
                $table->string('documento')->nullable();
                $table->string('colaborador')->nullable();
                $table->string('obs')->nullable();
                $table->string('tipo')->nullable();
                $table->string('baixado')->nullable();
                $table->date('databaixa')->nullable();
                $table->decimal('juros', 15, 2)->nullable();
                $table->decimal('desconto', 15, 2)->nullable();
                $table->date('dataemissao')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contaapagars');
    }
}
