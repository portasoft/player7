<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollunTbVendatemp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendatemps', function (Blueprint $table) {
            $table->decimal('dinheiro', 15, 2)->nullable();
            $table->decimal('cartao', 15, 2)->nullable();
            $table->decimal('cheque', 15, 2)->nullable();
            $table->decimal('crediaio', 15, 2)->nullable();
            $table->string('tipocartao')->nullable();
            $table->string('dadocheque')->nullable();
            $table->string('dias')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendatemps', function (Blueprint $table) {
            //
        });
    }
}
