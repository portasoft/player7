<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534011144EntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('entregas')) {
            Schema::create('entregas', function (Blueprint $table) {
                $table->increments('id');
                $table->date('data')->nullable();
                $table->string('placa')->nullable();
                $table->string('motorista')->nullable();
                $table->string('kminicial')->nullable();
                $table->string('kmfinal')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->string('obs')->nullable();
                $table->string('situacao')->nullable();
                $table->string('prazo')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entregas');
    }
}
