<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534012033PromissoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('promissorias')) {
            Schema::create('promissorias', function (Blueprint $table) {
                $table->increments('id');
                $table->date('dataemissao')->nullable();
                $table->date('datavencimento')->nullable();
                $table->decimal('valor', 15, 2)->nullable();
                $table->string('venda')->nullable();
                $table->string('cliente')->nullable();
                $table->string('colaborador')->nullable();
                $table->string('obs')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promissorias');
    }
}
