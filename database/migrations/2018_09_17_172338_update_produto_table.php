<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProdutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produtos', function (Blueprint $table) {
            $table->date('validade')->nullable();
            $table->string('cfop')->nullable();
            $table->string('cst')->nullable();
            $table->string('stipi')->nullable();
            $table->string('stpis')->nullable();
            $table->string('stcofins')->nullable();
            $table->string('codigobarrasforne')->nullable();
            $table->string('descontomax')->nullable();
            $table->string('descontomin')->nullable();
            $table->string('fabricante')->nullable();
            $table->string('cest')->nullable();
            $table->string('empresa')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produtos', function (Blueprint $table) {
            //
        });
    }
}
