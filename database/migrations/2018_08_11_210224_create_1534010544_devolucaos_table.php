<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534010544DevolucaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('devolucaos')) {
            Schema::create('devolucaos', function (Blueprint $table) {
                $table->increments('id');
                $table->date('data')->nullable();
                $table->string('fornecedor')->nullable();
                $table->string('motivo')->nullable();
                $table->string('produto')->nullable();
                $table->string('obs')->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devolucaos');
    }
}
