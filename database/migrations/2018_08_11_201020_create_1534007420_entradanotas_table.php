<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534007420EntradanotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('entradanotas')) {
            Schema::create('entradanotas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('fornecedor')->nullable();
                $table->date('dataemissao')->nullable();
                $table->date('dataentrada')->nullable();
                $table->decimal('valosprodutos', 15, 2)->nullable();
                $table->string('baseicms')->nullable();
                $table->decimal('valoricms', 15, 2)->nullable();
                $table->decimal('valorsubst', 15, 2)->nullable();
                $table->string('desconto')->nullable();
                $table->string('despesas')->nullable();
                $table->decimal('valornota', 15, 2)->nullable();
                $table->string('deposito')->nullable();
                $table->string('horaemissao')->nullable();
                $table->decimal('valoripi', 15, 2)->nullable();
                $table->decimal('valorfrete', 15, 2)->nullable();
                $table->string('freteincluso')->nullable();
                $table->string('tiponf')->nullable();
                $table->string('cfop')->nullable();
                $table->string('tipoentrada')->nullable();
                $table->string('formapagamento')->nullable();
                $table->decimal('valorservico', 15, 2)->nullable();
                $table->string('serie')->nullable();
                $table->string('centrocusto')->nullable();
                $table->string('obs')->nullable();
                $table->string('cnpjtransportadora')->nullable();
                $table->string('chavenfe')->nullable();
                $table->string('placa')->nullable();
                $table->string('unidade')->nullable();
                $table->date('datacadastro')->nullable();
                $table->string('contacontabi')->nullable();
                $table->string('xml')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entradanotas');
    }
}
