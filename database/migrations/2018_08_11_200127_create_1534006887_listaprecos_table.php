<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534006887ListaprecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('listaprecos')) {
            Schema::create('listaprecos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('descricao')->nullable();
                $table->string('tipo')->nullable();
                $table->decimal('precobase', 15, 2)->nullable();
                $table->string('margem')->nullable();
                $table->date('validade')->nullable();
                $table->string('grade')->nullable();
                $table->string('tamanho')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listaprecos');
    }
}
