<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534011898ContacorrentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('contacorrentes')) {
            Schema::create('contacorrentes', function (Blueprint $table) {
                $table->increments('id');
                $table->string('agencia')->nullable();
                $table->string('conta')->nullable();
                $table->string('banco')->nullable();
                $table->string('empresa')->nullable();
                $table->decimal('saldoinicial', 15, 2)->nullable();
                $table->decimal('saldo', 15, 2)->nullable();
                $table->string('anexo')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacorrentes');
    }
}
