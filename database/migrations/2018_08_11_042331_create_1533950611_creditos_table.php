<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533950611CreditosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('creditos')) {
            Schema::create('creditos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cliente')->nullable();
                $table->string('colaborador')->nullable();
                $table->decimal('valoratual', 15, 2)->nullable();
                $table->decimal('valornovo', 15, 2)->nullable();
                $table->string('tipo')->nullable();
                $table->date('datacadastro')->nullable();
                $table->decimal('valortotal', 15, 2)->nullable();
                $table->string('obs')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creditos');
    }
}
