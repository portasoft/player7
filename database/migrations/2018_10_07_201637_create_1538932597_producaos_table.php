<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1538932597ProducaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('producaos')) {
            Schema::create('producaos', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('produtoid')->nullable();
                $table->decimal('estoqueatual', 15, 2)->nullable();
                $table->integer('depositoorigem')->nullable();
                $table->integer('depositodestino')->nullable();
                $table->decimal('custo', 15, 2)->nullable();
                $table->integer('qtde')->nullable();
                $table->string('obs')->nullable();
                $table->string('barra')->nullable();
                $table->string('bolso')->nullable();
                $table->string('costa')->nullable();
                $table->string('etiqueta')->nullable();
                $table->string('capuz')->nullable();
                $table->string('frente')->nullable();
                $table->string('punho')->nullable();
                $table->string('manga')->nullable();
                $table->string('outros')->nullable();
                $table->string('situacao');
                $table->string('referencia')->nullable();
                $table->string('tamanho');
                $table->string('orelha')->nullable();
                $table->string('chifre')->nullable();
                $table->date('data');
                $table->integer('idfornecedor')->nulaable();
                $table->integer('ordem');
                $table->string('lote')->nullable;
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producaos');
    }
}
