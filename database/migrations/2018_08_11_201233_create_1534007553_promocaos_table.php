<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1534007553PromocaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('promocaos')) {
            Schema::create('promocaos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('produto')->nullable();
                $table->date('datacadastro')->nullable();
                $table->date('validadeinicial')->nullable();
                $table->date('validadefinal')->nullable();
                $table->decimal('preco', 15, 2)->nullable();
                $table->decimal('precopromocao', 15, 2)->nullable();
                $table->string('quantidademin')->nullable();
                $table->string('usuario')->nullable();
                $table->string('obs')->nullable();
                $table->string('prorrogada')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocaos');
    }
}
