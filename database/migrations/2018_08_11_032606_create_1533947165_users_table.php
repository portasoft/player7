<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1533947165UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('email');
                $table->string('password');
                $table->string('remember_token')->nullable();
                $table->string('nomecompleto')->nullable();
                $table->date('datanascimento')->nullable();
                $table->string('cep')->nullable();
                $table->string('endereco')->nullable();
                $table->string('numero')->nullable();
                $table->string('bairro')->nullable();
                $table->string('cidade')->nullable();
                $table->string('uf')->nullable();
                $table->string('complemento')->nullable();
                $table->string('fone')->nullable();
                $table->string('celular')->nullable();
                $table->string('cpf')->nullable();
                $table->string('rg')->nullable();
                $table->date('datacadastro')->nullable();
                $table->decimal('comissao',15, 2)->nullable();
                $table->decimal('salario',15, 2)->nullable();
                $table->string('empresa')->nullable();
                $table->string('situacao')->nullable();
                $table->string('obs')->nullable();
                $table->string('vendedor')->nullable();
                $table->string('foto')->nullable();
                $table->string('localizacao_address')->nullable();
                $table->string('localizacao_latitude')->nullable();
                $table->string('localizacao_longitude')->nullable();
                $table->timestamps();
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
